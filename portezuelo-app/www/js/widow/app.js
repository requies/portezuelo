
angular.module('portezuelo', [
	'ui.router',
	'controllers.index'
])

.config(function ($stateProvider, $urlRouterProvider) {

	$stateProvider

	.state('index', {
		url: '/',
		templateUrl: 'templates/index.html',
		controller: 'IndexCtrl'
	})

	$urlRouterProvider.otherwise('/')

})

