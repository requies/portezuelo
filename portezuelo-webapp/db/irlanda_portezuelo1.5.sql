/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     28/07/2014 21:33:41                          */
/*==============================================================*/


drop table Compra;

drop table alerta_cambio_precio;

drop index Index_14;

drop index Index_13;

drop table asistencia;

drop index Index_1;

drop table caja;

drop table compra_detalle;

drop index Index_15;

drop table egreso;

drop index Index_7;

drop table familia_producto;

drop table impuesto;

drop index Index_16;

drop table ingreso;

drop table inventario;

drop table inventario_detalle;

drop index Index_3;

drop table log;

drop table maquina;

drop index Index_10;

drop index Index_9;

drop table merma;

drop table negocio;

drop table pedido;

drop table pedido_detalle;

drop index Index_4;

drop table producto;

drop index Index_18;

drop table promocion;

drop table promocion_detalle;

drop table stock;

drop table tipo_egreso;

drop table tipo_ingreso;

drop table tipo_merma;

drop table tipo_producto;

drop table traspaso;

drop table traspaso_detalle;

drop table unidad_producto;

drop index Index_2;

drop index Index_5;

drop table users;

drop index indx_caja;

drop index indx_venta_fecha;

drop table venta;

drop table venta_anulada;

drop index Index_8;

drop index index_venta_detalle;

drop table venta_detalle;

drop sequence nro_venta;

drop sequence producto_barcode_seq;

create sequence nro_venta
start 1;

create sequence producto_barcode_seq
start 1000000;

/*==============================================================*/
/* Table: Compra                                                */
/*==============================================================*/
create table Compra (
   id                   SERIAL not null,
   fecha                DATE                 not null,
   descripcion          VARCHAR(255)         null,
   user_id              INT2                 not null,
   negocio_id           INT2                 not null,
   egreso_id            INT4                 null,
   constraint PK_COMPRA primary key (id)
);

/*==============================================================*/
/* Table: alerta_cambio_precio                                  */
/*==============================================================*/
create table alerta_cambio_precio (
   id                   SERIAL               not null,
   leido                BOOL                 not null,
   negocio_id           INT2                 null,
   barcode              int8                 null,
   descripcion          VARCHAR(255)         null,
   precio_antiguo       FLOAT8               null,
   precio_nuevo         FLOAT8               null,
   fecha                TIMESTAMP            null,
   constraint PK_ALERTA_CAMBIO_PRECIO primary key (id)
);

comment on column alerta_cambio_precio.barcode is
'llave primaria';

/*==============================================================*/
/* Table: asistencia                                            */
/*==============================================================*/
create table asistencia (
   id                   SERIAL               not null,
   user_id              INT4                 not null,
   negocio_id           INT4                 not null,
   entrada              timestamp            null,
   salida               timestamp            null,
   constraint pk_asistencia primary key (id)
);

/*==============================================================*/
/* Index: Index_13                                              */
/*==============================================================*/
create unique index Index_13 on asistencia (
id
);

/*==============================================================*/
/* Index: Index_14                                              */
/*==============================================================*/
create  index Index_14 on asistencia (
user_id
);

/*==============================================================*/
/* Table: caja                                                  */
/*==============================================================*/
create table caja (
   id                   SERIAL not null,
   maquina_id           INT2                 null,
   user_id              INT2                 not null,
   fecha_inicio         timestamp            null,
   monto_inicio         int4                 null,
   fecha_termino        timestamp            null,
   monto_termino        int4                 null,
   perdida              int4                 null,
   constraint pk_caja primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on caja (
id
);

/*==============================================================*/
/* Table: compra_detalle                                        */
/*==============================================================*/
create table compra_detalle (
   barcode              int8                 not null,
   compra_id            INT4                 not null,
   cantidad             FLOAT8               not null,
   costo                INT4                 not null,
   constraint PK_COMPRA_DETALLE primary key (barcode, compra_id)
);

comment on column compra_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: egreso                                                */
/*==============================================================*/
create table egreso (
   id                   SERIAL not null,
   tipo_egreso_id       int2                 null,
   caja_id              INT2                 null,
   monto                int4                 not null,
   fecha                timestamp            not null,
   user_id              INT2                 null,
   constraint pk_egreso primary key (id)
);

comment on column egreso.user_id is
'Es null si no se tiene una caja asociada, en caso de haber una caja asociada el user se obtiene seg�n el usuario de la caja.';

/*==============================================================*/
/* Index: Index_15                                              */
/*==============================================================*/
create unique index Index_15 on egreso (
id
);

/*==============================================================*/
/* Table: familia_producto                                      */
/*==============================================================*/
create table familia_producto (
   id                   SERIAL not null,
   nombre               varchar(32)          not null,
   constraint pk_familias primary key (id),
   constraint AK_AK_NOMBRE_FAMILIA_FAMILIA_ unique (nombre)
);

/*==============================================================*/
/* Index: Index_7                                               */
/*==============================================================*/
create unique index Index_7 on familia_producto (
id
);

/*==============================================================*/
/* Table: impuesto                                              */
/*==============================================================*/
create table impuesto (
   id                   SERIAL not null,
   descripcion          varchar(64)          not null,
   monto                float8               not null,
   removable            boolean              not null default 't',
   visible              BOOL                 not null,
   constraint pk_impuesto primary key (id)
);

/*==============================================================*/
/* Table: ingreso                                               */
/*==============================================================*/
create table ingreso (
   id                   SERIAL not null,
   tipo_ingreso_id      int2                 not null,
   caja_id              INT2                 null,
   monto                int4                 not null,
   fecha                timestamp            not null,
   user_id              INT2                 null,
   constraint pk_ingreso primary key (id)
);

/*==============================================================*/
/* Index: Index_16                                              */
/*==============================================================*/
create unique index Index_16 on ingreso (
id
);

/*==============================================================*/
/* Table: inventario                                            */
/*==============================================================*/
create table inventario (
   id                   SERIAL               not null,
   negocio_id           INT4                 not null,
   user_id              INT2                 null,
   fecha                TIMESTAMP            null,
   constraint PK_INVENTARIO primary key (id)
);

/*==============================================================*/
/* Table: inventario_detalle                                    */
/*==============================================================*/
create table inventario_detalle (
   id                   SERIAL               not null,
   inventario_id        INT4                 not null,
   barcode              int8                 not null,
   cantidad             FLOAT8               not null,
   cantidad_esperada    FLOAT8               not null,
   constraint PK_INVENTARIO_DETALLE primary key (id)
);

comment on column inventario_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: log                                                   */
/*==============================================================*/
create table log (
   id                   SERIAL not null,
   fecha                timestamp            null,
   maquina              int4                 null,
   seller               int4                 null,
   text                 text                 null,
   constraint pk_log primary key (id)
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create unique index Index_3 on log (
id
);

/*==============================================================*/
/* Table: maquina                                               */
/*==============================================================*/
create table maquina (
   id                   SERIAL not null,
   codigo_maquina       varchar(1000)        null,
   nombre_maqina        varchar(128)         null,
   constraint PK_MAQUINA primary key (id)
);

/*==============================================================*/
/* Table: merma                                                 */
/*==============================================================*/
create table merma (
   id                   SERIAL not null,
   barcode              int8                 null,
   tipo_merma_id        int2                 null,
   tipo                 int4                 null,
   unidades             float8               null,
   fecha                timestamp            null,
   constraint pk_merma primary key (id)
);

/*==============================================================*/
/* Index: Index_9                                               */
/*==============================================================*/
create unique index Index_9 on merma (
id
);

/*==============================================================*/
/* Index: Index_10                                              */
/*==============================================================*/
create  index Index_10 on merma (
barcode
);

/*==============================================================*/
/* Table: negocio                                               */
/*==============================================================*/
create table negocio (
   id                   SERIAL               not null,
   rut                  VARCHAR(15)          not null,
   razon_social         varchar(255)         null,
   nombre               varchar(255)         not null,
   fono                 varchar(15)          null,
   fax                  varchar(15)          null,
   direccion            varchar(255)         null,
   comuna               varchar(64)          null,
   ciudad               varchar(64)          null,
   giro                 varchar(100)         null,
   seleccionado         BOOL                 not null,
   constraint PK_NEGOCIO primary key (id),
   constraint AK_KEY_NOMBRE_NEGOCIO unique (nombre)
);

comment on table negocio is
'Representa los datos del negocio, s�lo tiene una �nica fila';

/*==============================================================*/
/* Table: pedido                                                */
/*==============================================================*/
create table pedido (
   id                   SERIAL               not null,
   user_id              INT2                 null,
   negocio_id           INT2                 null,
   fecha                TIMESTAMP            null,
   descripcion          VARCHAR(255)         null,
   estado               INT4                 not null,
   constraint PK_PEDIDO primary key (id)
);

/*==============================================================*/
/* Table: pedido_detalle                                        */
/*==============================================================*/
create table pedido_detalle (
   barcode              int8                 not null,
   pedido_id            INT4                 not null,
   cantidad             FLOAT8               null,
   costo                INT4                 not null,
   constraint PK_PEDIDO_DETALLE primary key (barcode, pedido_id)
);

comment on column pedido_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: producto                                              */
/*==============================================================*/
create table producto (
   barcode              int8                 not null default nextval('producto_barcode_seq'::regclass),
   codigo_corto         varchar(16)          not null,
   marca                varchar(35)          null,
   descripcion          varchar(50)          null,
   contenido            varchar(10)          null,
   unidad               varchar(10)          null,
   stock                float8               null default 0,
   precio               float8               null,
   precio_neto          float8               null,
   costo_promedio       float8               null,
   fifo                 FLOAT8               null,
   vendidos             float8               null,
   aplica_iva           bool                 null,
   otros_impuestos      INT2                 null,
   familia              int2                 not null default 1,
   id_tipo_producto     INT2                 not null,
   es_perecible         bool                 null,
   dias_stock           float8               null default 1,
   margen_promedio      float8               null default 0,
   es_venta_fraccionada bool                 null,
   stock_pro            float8               null default 0.0,
   tasa_canje           float8               null default 1.0,
   precio_mayor         float8               null,
   cantidad_mayor       float8               null,
   es_mayorista         bool                 null,
   estado               bool                 null,
   precio_editable      bool                 null,
   constraint pk_producto primary key (barcode),
   constraint unique_short_code unique (codigo_corto)
);

comment on column producto.barcode is
'llave primaria';

comment on column producto.codigo_corto is
'crear ckeck point';

comment on column producto.costo_promedio is
'antes fifo';

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create unique index Index_4 on producto (
barcode
);

/*==============================================================*/
/* Table: promocion                                             */
/*==============================================================*/
create table promocion (
   id                   SERIAL               not null,
   barcode              int8                 not null,
   nombre               VARCHAR(255)         null,
   inicio               TIMESTAMP            null,
   termino              TIMESTAMP            null,
   habilitado           BOOL                 null,
   precio               FLOAT8               null,
   constraint PK_PROMOCION primary key (id)
);

comment on column promocion.barcode is
'llave primaria';

/*==============================================================*/
/* Index: Index_18                                              */
/*==============================================================*/
create unique index Index_18 on promocion (
id
);

/*==============================================================*/
/* Table: promocion_detalle                                     */
/*==============================================================*/
create table promocion_detalle (
   barcode              int8                 not null,
   promocion_id         INT4                 not null,
   cantidad             FLOAT8               null,
   constraint PK_PROMOCION_DETALLE primary key (promocion_id, barcode)
);

comment on column promocion_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: stock                                                 */
/*==============================================================*/
create table stock (
   id                   Serial               not null,
   barcode              int8                 null default nextval('producto_barcode_seq'::regclass),
   stock                float8               not null,
   created_at           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   negocio_id           INT4                 null,
   constraint PK_STOCK primary key (id)
);

comment on column stock.barcode is
'llave primaria';

/*==============================================================*/
/* Table: tipo_egreso                                           */
/*==============================================================*/
create table tipo_egreso (
   id                   SERIAL not null,
   descripcion          varchar(64)          null,
   removable            BOOL                 not null,
   constraint pk_tipo_egreso primary key (id)
);

/*==============================================================*/
/* Table: tipo_ingreso                                          */
/*==============================================================*/
create table tipo_ingreso (
   id                   SERIAL not null,
   descripcion          varchar(64)          null,
   removable            BOOL                 not null,
   constraint pk_tipo_ingreso primary key (id)
);

/*==============================================================*/
/* Table: tipo_merma                                            */
/*==============================================================*/
create table tipo_merma (
   id                   SERIAL not null,
   nombre               varchar(25)          not null,
   constraint pk_tipo_merma primary key (id)
);

/*==============================================================*/
/* Table: tipo_producto                                         */
/*==============================================================*/
create table tipo_producto (
   id                   SERIAL not null,
   nombre               VARCHAR(16)          not null,
   constraint PK_TIPO_PRODUCTO primary key (id)
);

/*==============================================================*/
/* Table: traspaso                                              */
/*==============================================================*/
create table traspaso (
   id                   SERIAL               not null,
   origen_id            INT2                 null,
   destino_id           INT2                 null,
   user_id              INT2                 null,
   monto                float8               null,
   fecha                timestamp            null,
   constraint pk_traspaso primary key (id)
);

/*==============================================================*/
/* Table: traspaso_detalle                                      */
/*==============================================================*/
create table traspaso_detalle (
   id                   SERIAL               not null,
   barcode              int8                 not null,
   traspaso_id          int4                 not null,
   cantidad             float8               null,
   precio               float8               not null,
   precio_venta         float8               null,
   costo_modificado     boolean              null,
   constraint pk_traspaso_detalle primary key (id)
);

/*==============================================================*/
/* Table: unidad_producto                                       */
/*==============================================================*/
create table unidad_producto (
   id                   SERIAL not null,
   nombre               VARCHAR(16)          not null,
   constraint pk_unidad_producuto primary key (id),
   constraint AK_AK_NOMBRE_UNIDAD_P_UNIDAD_P unique (nombre)
);

/*==============================================================*/
/* Table: users                                                 */
/*==============================================================*/
create table users (
   id                   SERIAL not null,
   rut                  varchar(15)          not null,
   passwd               varchar(128)         not null,
   nombre               varchar(64)          not null,
   apell_p              varchar(64)          not null,
   apell_m              varchar(64)          null,
   fecha_ingreso        timestamp            null,
   tipo_usuario         int2                 null,
   activo               BOOL                 not null,
   constraint pk_users primary key (id),
   constraint AK_KEY_2_USERS unique (rut)
);

/*==============================================================*/
/* Index: Index_5                                               */
/*==============================================================*/
create unique index Index_5 on users (
id
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create  index Index_2 on users using HASH (
( users.rut )
);

/*==============================================================*/
/* Table: venta                                                 */
/*==============================================================*/
create table venta (
   id                   SERIAL not null,
   nro_venta            INT4                 not null,
   caja_id              INT4                 null,
   monto                int4                 null,
   fecha                timestamp            null,
   maquina_id           INT2                 null,
   tipo_documento       int2                 null,
   tipo_venta           int2                 null,
   descuento            INT4                 null,
   id_documento         int4                 null,
   cancelada            bool                 null,
   constraint pk_venta primary key (id),
   constraint AK_AK_NUM_VENTA_VENTA unique (nro_venta)
);

/*==============================================================*/
/* Index: indx_venta_fecha                                      */
/*==============================================================*/
create  index indx_venta_fecha on venta using BTREE (
fecha
);

/*==============================================================*/
/* Index: indx_caja                                             */
/*==============================================================*/
create  index indx_caja on venta (
caja_id
);

/*==============================================================*/
/* Table: venta_anulada                                         */
/*==============================================================*/
create table venta_anulada (
   venta_id             INT4                 not null,
   fecha                timestamp            null default now(),
   motivo               text                 null,
   caja_id              INT4                 not null,
   constraint pk_venta_anulada primary key (venta_id)
);

/*==============================================================*/
/* Table: venta_detalle                                         */
/*==============================================================*/
create table venta_detalle (
   id                   SERIAL               not null,
   venta_id             int4                 not null,
   barcode              int8                 not null,
   cantidad             float8               null,
   precio               float8               not null,
   precio_neto          float8               not null,
   fifo                 float8               not null,
   ganancia             float8               not null,
   iva                  float8               null,
   otros                float8               null,
   iva_residual         float8               null,
   otros_residual       float8               null,
   proporcion_iva       float8               null,
   proporcion_otros     float8               null,
   tiene_impuestos      bool                 null,
   constraint pk_venta_detalle primary key (id)
);

comment on table venta_detalle is
'antes era products_sell_history';

/*==============================================================*/
/* Index: index_venta_detalle                                   */
/*==============================================================*/
create unique index index_venta_detalle on venta_detalle (
id
);

/*==============================================================*/
/* Index: Index_8                                               */
/*==============================================================*/
create  index Index_8 on venta_detalle (
barcode
);

alter table Compra
   add constraint FK_COMPRA_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table Compra
   add constraint FK_COMPRA_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table Compra
   add constraint FK_COMPRA_REFERENCE_EGRESO foreign key (egreso_id)
      references egreso (id)
      on delete restrict on update restrict;

alter table alerta_cambio_precio
   add constraint FK_ALERTA_C_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table alerta_cambio_precio
   add constraint FK_ALERTA_C_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table asistencia
   add constraint FK_ASISTENC_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table asistencia
   add constraint fk_asistencia_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table caja
   add constraint fk_caja_maquina foreign key (maquina_id)
      references maquina (id)
      on delete restrict on update restrict;

alter table caja
   add constraint fk_caja_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table compra_detalle
   add constraint FK_COMPRA_D_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table compra_detalle
   add constraint FK_COMPRA_D_REFERENCE_COMPRA foreign key (compra_id)
      references Compra (id)
      on delete restrict on update restrict;

alter table egreso
   add constraint FK_EGRESO_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table egreso
   add constraint fk_egreso_caja foreign key (caja_id)
      references caja (id)
      on delete restrict on update restrict;

alter table egreso
   add constraint fk_egreso_tipo_egreso foreign key (tipo_egreso_id)
      references tipo_egreso (id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint FK_INGRESO_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint fk_ingreso_caja foreign key (caja_id)
      references caja (id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint fk_ingreso_tipo_ingreso foreign key (tipo_ingreso_id)
      references tipo_ingreso (id)
      on delete restrict on update restrict;

alter table inventario
   add constraint FK_INVENTAR_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table inventario
   add constraint FK_INVENTAR_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table inventario_detalle
   add constraint FK_INVENTAR_REFERENCE_INVENTAR foreign key (inventario_id)
      references inventario (id)
      on delete restrict on update restrict;

alter table inventario_detalle
   add constraint FK_INVENTAR_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table merma
   add constraint fk_merma_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table merma
   add constraint fk_merma_tipo_merma foreign key (tipo_merma_id)
      references tipo_merma (id)
      on delete restrict on update restrict;

alter table pedido
   add constraint FK_PEDIDO_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table pedido
   add constraint FK_PEDIDO_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table pedido_detalle
   add constraint FK_PEDIDO_D_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table pedido_detalle
   add constraint FK_PEDIDO_D_REFERENCE_PEDIDO foreign key (pedido_id)
      references pedido (id)
      on delete restrict on update restrict;

alter table producto
   add constraint FK_PRODUCTO_REFERENCE_FAMILIA_ foreign key (familia)
      references familia_producto (id)
      on delete restrict on update restrict;

alter table producto
   add constraint FK_PRODUCTO_REFERENCE_IMPUESTO foreign key (otros_impuestos)
      references impuesto (id)
      on delete restrict on update restrict;

alter table producto
   add constraint FK_PRODUCTO_FK_PRODUC_TIPO_PRO foreign key (id_tipo_producto)
      references tipo_producto (id)
      on delete restrict on update restrict;

alter table promocion
   add constraint FK_PROMOCIO_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table promocion_detalle
   add constraint FK_PROMOCIO_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table promocion_detalle
   add constraint FK_PROMOCIO_REFERENCE_PROMOCIO foreign key (promocion_id)
      references promocion (id)
      on delete restrict on update restrict;

alter table stock
   add constraint FK_STOCK_REFERENCE_PRODUCTO foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table stock
   add constraint FK_STOCK_REFERENCE_NEGOCIO foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table traspaso
   add constraint FK_TRASPASO_FK_TRASPA_NEGOCIO foreign key (origen_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table traspaso
   add constraint FK_TRASPASO_REFERENCE_NEGOCIO foreign key (destino_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table traspaso
   add constraint FK_TRASPASO_REFERENCE_USERS foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table traspaso_detalle
   add constraint fk_productos_barcode foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table traspaso_detalle
   add constraint fk_traspaso_id foreign key (traspaso_id)
      references traspaso (id)
      on delete restrict on update restrict;

alter table venta
   add constraint FK_VENTA_REFERENCE_MAQUINA foreign key (maquina_id)
      references maquina (id)
      on delete restrict on update restrict;

alter table venta
   add constraint FK_VENTA_REFERENCE_CAJA foreign key (caja_id)
      references caja (id)
      on delete restrict on update restrict;

alter table venta_anulada
   add constraint FK_VENTA_AN_REFERENCE_CAJA foreign key (caja_id)
      references caja (id)
      on delete restrict on update restrict;

alter table venta_anulada
   add constraint fk_venta_anulada_venta foreign key (venta_id)
      references venta (id)
      on delete restrict on update restrict;

alter table venta_detalle
   add constraint fk_productos_barcode foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table venta_detalle
   add constraint fk_venta_id foreign key (venta_id)
      references venta (id)
      on delete restrict on update restrict;

