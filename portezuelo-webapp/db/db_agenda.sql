
create table contacto (
   id                   SERIAL not null,
   nombre               VARCHAR(255)         null,
   constraint PK_CONTACTO primary key (id)
);

create table numero (
	id 					SERIAL not null,
	numero 				VARCHAR(255) null,
	contacto_id			int not null,
	constraint PK_NUMERO primary key (id)
);

alter table numero 
	add constraint fk_contacto_numero foreign key (contacto_id)
	references contacto (id)
	on delete restrict on update restrict;






-- create a schema named "audit"
CREATE schema audit;
REVOKE CREATE ON schema audit FROM public;

DROP TABLE IF EXISTS audit.logged_actions;
 
CREATE TABLE audit.logged_actions (
    id 					serial not null,
    schema_name 		text NOT NULL,
    table_name 			text NOT NULL,
    user_name 			text,
    action_tstamp 		timestamp WITH time zone NOT NULL DEFAULT current_timestamp,
    action 				TEXT NOT NULL CHECK (action IN ('I','D','U')),
    original_data 		text,
    new_data 			text,
    query 				text,
    p_key_name          text NOT NULL,
    p_key               text NOT NULL,
    job_state 			text NOT NULL CHECK (job_state IN ('P', 'T')),
    constraint PK_LOGGED_ACTIONS primary key(id)
) WITH (fillfactor=100);
 
 
-- You may wish to use different permissions; this lets anybody
-- see the full audit data. In Pg 9.0 and above you can use column
-- permissions for fine-grained control.
-- REVOKE ALL ON audit.logged_actions FROM public;
-- GRANT SELECT ON audit.logged_actions TO public;
GRANT ALL ON audit.logged_actions TO public;
 
CREATE INDEX logged_actions_schema_table_idx 
ON audit.logged_actions(((schema_name||'.'||table_name)::TEXT));
 
CREATE INDEX logged_actions_action_idx 
ON audit.logged_actions(action);

CREATE INDEX logged_actions_id_idx
ON audit.logged_actions(id);

CREATE INDEX logged_actions_status_idx
ON audit.logged_actions(job_state);
 
--
-- Now, define the actual trigger function:
--
CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
DECLARE
    v_old_data TEXT;
    v_new_data TEXT;
    v_pkey TEXT;
BEGIN
    /*  If this actually for real auditing (where you need to log EVERY action),
        then you would need to use something like dblink or plperl that could log outside the transaction,
        regardless of whether the transaction committed or rolled back.
    */

    v_pkey := (SELECT array_agg(column_name::TEXT) AS pkeys
                    FROM information_schema.key_column_usage 
                    JOIN information_schema.table_constraints 
                    ON (information_schema.table_constraints.constraint_name = information_schema.key_column_usage.constraint_name) 
                    WHERE constraint_type='PRIMARY KEY'
                    AND key_column_usage.table_name=TG_TABLE_NAME::TEXT);

    /* This dance with casting the NEW and OLD values to a ROW is not necessary in pg 9.0+ */
    IF (TG_OP = 'UPDATE') THEN
        v_old_data := ROW(OLD.*);
        v_new_data := ROW(NEW.*);
        INSERT INTO audit.logged_actions (id, schema_name,table_name,user_name,action,original_data,new_data,query, p_key_name, p_key, job_state) 
        VALUES (DEFAULT, TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data,v_new_data, current_query(), v_pkey::TEXT, 'testing', 'P');
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        v_old_data := ROW(OLD.*);
        INSERT INTO audit.logged_actions (id, schema_name,table_name,user_name,action,original_data,query, p_key_name, p_key, job_state)
        VALUES (DEFAULT, TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data, current_query(), v_pkey::TEXT, 'testing', 'P');
        RETURN OLD;
    ELSIF (TG_OP = 'INSERT') THEN
        v_new_data := ROW(NEW.*);
        INSERT INTO audit.logged_actions (id, schema_name,table_name,user_name,action,new_data,query, p_key_name, p_key, job_state)
        VALUES (DEFAULT, TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_new_data, current_query(), v_pkey::TEXT, 'testing', 'P');
        RETURN NEW;
    ELSE
        RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - Other action occurred: %, at %',TG_OP,now();
        RETURN NULL;
    END IF;
 
EXCEPTION
    WHEN data_exception THEN
        RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
        RETURN NULL;
    WHEN unique_violation THEN
        RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
        RETURN NULL;
    WHEN OTHERS THEN
        RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
        RETURN NULL;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = pg_catalog, audit;

--
-- To add this trigger to a table, use:
-- CREATE TRIGGER tablename_audit
-- AFTER INSERT OR UPDATE OR DELETE ON tablename
-- FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
--

--
-- Obtener las primary key
-- SELECT * FROM information_schema.key_column_usage 
-- JOIN information_schema.table_constraints 
-- ON (information_schema.table_constraints.constraint_name = information_schema.key_column_usage.constraint_name) 
-- WHERE constraint_type='PRIMARY KEY' ORDER BY key_column_usage.constraint_name;
