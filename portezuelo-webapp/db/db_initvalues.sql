INSERT INTO impuesto VALUES (DEFAULT, 'Sin Impuesto adicional', 0, 'f', 't');
INSERT INTO impuesto VALUES (DEFAULT, 'Impuesto al Valor Agregado', 19, 'f', 'f');
INSERT INTO impuesto VALUES (DEFAULT, 'Bebidas', 13, 't', 't');
INSERT INTO impuesto VALUES (DEFAULT, 'Piscos y Whisky', 27, 't', 't');
INSERT INTO impuesto VALUES (DEFAULT, 'Vinos y Cervezas', 15, 't', 't');

INSERT INTO tipo_ingreso VALUES (DEFAULT, 'Sencillo', 'f');
INSERT INTO tipo_ingreso VALUES (DEFAULT, 'Exceso de caja', 'f');
INSERT INTO tipo_ingreso VALUES (DEFAULT, 'Deposito envases', 'f');

INSERT INTO tipo_egreso VALUES (DEFAULT, 'Compras de mercadería', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Retiro', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Perdida', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Gastos Corrientes', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Devolucion envases', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Nulidad de Venta', 'f');
INSERT INTO tipo_egreso VALUES (DEFAULT, 'Retiro por cierre', 'f');

INSERT INTO tipo_producto VALUES (DEFAULT, 'Normal');
INSERT INTO tipo_producto VALUES (DEFAULT, 'Sin cantidad y precio predefinido');
INSERT INTO tipo_producto VALUES (DEFAULT, 'Compuesto, pack u oferta');

INSERT INTO tipo_merma VALUES (DEFAULT, 'Hurto');
INSERT INTO tipo_merma VALUES (DEFAULT, 'Vencimiento');
INSERT INTO tipo_merma VALUES (DEFAULT, 'Destruccion o Falla');
INSERT INTO tipo_merma VALUES (DEFAULT, 'Diferencia conteo');
INSERT INTO tipo_merma VALUES (DEFAULT, 'Diferencia cuadratura');

INSERT INTO maquina VALUES (DEFAULT, 'M1', 'CAJA 1', 't'); --El código de máquina debería ser GPG u otro ID único

INSERT INTO unidad_producto VALUES (DEFAULT, 'LT');
INSERT INTO unidad_producto VALUES (DEFAULT, 'CC');
INSERT INTO unidad_producto VALUES (DEFAULT, 'GR');
INSERT INTO unidad_producto VALUES (DEFAULT, 'KG');
INSERT INTO unidad_producto VALUES (DEFAULT, 'UN');
INSERT INTO unidad_producto VALUES (DEFAULT, 'ML');

INSERT INTO familia_producto VALUES (DEFAULT, 'GENERICO');

INSERT INTO producto( 
	barcode, codigo_corto, marca, descripcion, 
	contenido, unidad, stock, precio, precio_neto, 				
	costo_promedio, vendidos, aplica_iva, otros_impuestos, familia, 
    id_tipo_producto, es_perecible, es_venta_fraccionada, 
    stock_pro, precio_editable)	
    
    VALUES (DEFAULT, 1, '', 'Ventas menores', 1, 'UN', -1, 
            0, 0, 0, 0, 't', 1, 
            1, 1, 'f', 'f', -1, 't');
