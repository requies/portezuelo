User.create!([
  {rut: "171885701", passwd: "plop", nombre: "Mario", apell_p: "López", apell_m: "Landes", fecha_ingreso: "2014-07-29 16:19:33", tipo_usuario: 0, activo: true}
])
# FamiliaProducto.create!([
#   {nombre: "Cervezas"},
#   {nombre: "Vinos"},
#   {nombre: "Bebidas"},
#   {nombre: "Completos"}
# ])
Negocio.create!([
  {rut: "171885701", razon_social: "qwer", nombre: "wqer", fono: "qwer", fax: "qwer", direccion: "qwer", comuna: "qwer", ciudad: "qwer", giro: "qwer", seleccionado: true},
  {rut: "19", razon_social: "654", nombre: "321", fono: "3654", fax: "321", direccion: "654", comuna: "987", ciudad: "987", giro: "654", seleccionado: false},
  {rut: "40k", razon_social: "qwer", nombre: "qwer", fono: "qwer", fax: "qwer", direccion: "qwer", comuna: "qwer", ciudad: "qwer", giro: "qwer", seleccionado: false}
])
Asistencia.create!([
  {user_id: 1, negocio_id: 2, entrada: "2014-07-23 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-24 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-25 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-26 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-27 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-28 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-29 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-30 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-07-31 08:00:00", salida: "2014-07-23 23:00:00"},
  {user_id: 1, negocio_id: 2, entrada: "2014-08-01 08:00:00", salida: "2014-07-23 23:00:00"}
])
# Impuesto.create!([
#   {descripcion: "Sin Impuesto", monto: 0.0, removable: false, visible: true},
#   {descripcion: "Impuesto al Valor Agregado", monto: 19.0, removable: false, visible: false},
#   {descripcion: "Bebidas", monto: 13.0, removable: true, visible: true},
#   {descripcion: "Piscos y Whisky", monto: 27.0, removable: true, visible: true},
#   {descripcion: "Vinos y Cervezas", monto: 15.0, removable: true, visible: true}
# ])
# TipoProducto.create!([
#   {nombre: "Corriente"},
#   {nombre: "Servicio"},
#   {nombre: "Compuesta"}
# ])
Producto.create!([
  {barcode: 1000000, codigo_corto: "123412341234", marca: "Apple", descripcion: "Computador", contenido: "ASDF", unidad: "UN", stock: 100.0, precio: 460000.0, precio_neto: 500000.0, costo_promedio: nil, fifo: nil, vendidos: nil, aplica_iva: true, otros_impuestos: 2, familia: 2, id_tipo_producto: 1, es_perecible: false, dias_stock: 1.0, margen_promedio: 0.0, es_venta_fraccionada: false, stock_pro: 0.0, tasa_canje: 1.0, precio_mayor: nil, cantidad_mayor: nil, es_mayorista: nil, estado: nil, precio_editable: true},
  {barcode: 123412341234, codigo_corto: "234523452345", marca: "Samsung", descripcion: "Galaxy S2", contenido: "ASDF", unidad: "UN", stock: 500.0, precio: 99000.0, precio_neto: 100000.0, costo_promedio: nil, fifo: nil, vendidos: nil, aplica_iva: true, otros_impuestos: 1, familia: 4, id_tipo_producto: 1, es_perecible: false, dias_stock: 1.0, margen_promedio: 0.0, es_venta_fraccionada: false, stock_pro: 0.0, tasa_canje: 1.0, precio_mayor: nil, cantidad_mayor: nil, es_mayorista: nil, estado: nil, precio_editable: true}
])
Pedido.create!([
  {user_id: 1, negocio_id: 2, fecha: "2014-07-23 08:00:00", descripcion: "Estoy corto de cigarros", estado: 1 },
  {user_id: 1, negocio_id: 2, fecha: "2014-07-24 08:00:00", descripcion: "Faltan destilados", estado: 1 }
])
PedidoDetalle.create!([
  {pedido_id: 1, barcode: 1000000, cantidad: 50, costo: 0},
  {pedido_id: 1, barcode: 123412341234, cantidad: 45, costo: 0},
  {pedido_id: 2, barcode: 1000000, cantidad: 30, costo: 0}
])
Inventario.create!([
  {negocio_id: 2, user_id: 1, fecha: "2014-08-01 05:33:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-08-01 05:38:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-09-09 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-09-10 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-10-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-10-06 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-07-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-06-13 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-05-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-05-04 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-04-04 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-03-16 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-03-11 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-02-02 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-02-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-01-01 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-01-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-11-18 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-11-20 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-12-20 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-12-05 00:00:00"},
  {negocio_id: 3, user_id: 1, fecha: "2014-08-09 00:00:00"}
])
InventarioDetalle.create!([
  {inventario_id: 1, barcode: 1000000, cantidad: 30.0, cantidad_esperada: 30.0},
  {inventario_id: 1, barcode: 123412341234, cantidad: 40.0, cantidad_esperada: 40.0},
  {inventario_id: 2, barcode: 1000000, cantidad: 20.0, cantidad_esperada: 20.0},
  {inventario_id: 2, barcode: 123412341234, cantidad: 10.0, cantidad_esperada: 10.0},
  {inventario_id: 24, barcode: 1000000, cantidad: 38.0, cantidad_esperada: 38.0},
  {inventario_id: 3, barcode: 123412341234, cantidad: 60.0, cantidad_esperada: 60.0},
  {inventario_id: 3, barcode: 1000000, cantidad: 50.0, cantidad_esperada: 50.0},
  {inventario_id: 9, barcode: 1000000, cantidad: 2.0, cantidad_esperada: 2.0},
  {inventario_id: 10, barcode: 123412341234, cantidad: 3.0, cantidad_esperada: 3.0},
  {inventario_id: 10, barcode: 1000000, cantidad: 4.0, cantidad_esperada: 4.0},
  {inventario_id: 11, barcode: 123412341234, cantidad: 5.0, cantidad_esperada: 5.0},
  {inventario_id: 11, barcode: 1000000, cantidad: 6.0, cantidad_esperada: 6.0},
  {inventario_id: 12, barcode: 123412341234, cantidad: 7.0, cantidad_esperada: 7.0},
  {inventario_id: 12, barcode: 1000000, cantidad: 8.0, cantidad_esperada: 8.0},
  {inventario_id: 13, barcode: 123412341234, cantidad: 9.0, cantidad_esperada: 9.0},
  {inventario_id: 13, barcode: 1000000, cantidad: 10.0, cantidad_esperada: 10.0},
  {inventario_id: 14, barcode: 123412341234, cantidad: 11.0, cantidad_esperada: 11.0},
  {inventario_id: 14, barcode: 1000000, cantidad: 12.0, cantidad_esperada: 12.0},
  {inventario_id: 15, barcode: 123412341234, cantidad: 13.0, cantidad_esperada: 13.0},
  {inventario_id: 4, barcode: 1000000, cantidad: 80.0, cantidad_esperada: 80.0},
  {inventario_id: 4, barcode: 123412341234, cantidad: 45.0, cantidad_esperada: 45.0},
  {inventario_id: 5, barcode: 123412341234, cantidad: 5.0, cantidad_esperada: 5.0},
  {inventario_id: 5, barcode: 1000000, cantidad: 56.0, cantidad_esperada: 56.0},
  {inventario_id: 6, barcode: 123412341234, cantidad: 8.0, cantidad_esperada: 8.0},
  {inventario_id: 6, barcode: 1000000, cantidad: 7.0, cantidad_esperada: 7.0},
  {inventario_id: 7, barcode: 123412341234, cantidad: 23.0, cantidad_esperada: 23.0},
  {inventario_id: 7, barcode: 1000000, cantidad: 25.0, cantidad_esperada: 25.0},
  {inventario_id: 8, barcode: 123412341234, cantidad: 47.0, cantidad_esperada: 47.0},
  {inventario_id: 8, barcode: 1000000, cantidad: 89.0, cantidad_esperada: 1.0},
  {inventario_id: 9, barcode: 123412341234, cantidad: 1.0, cantidad_esperada: 1.0},
  {inventario_id: 15, barcode: 1000000, cantidad: 20.0, cantidad_esperada: 20.0},
  {inventario_id: 16, barcode: 123412341234, cantidad: 21.0, cantidad_esperada: 21.0},
  {inventario_id: 16, barcode: 1000000, cantidad: 22.0, cantidad_esperada: 22.0},
  {inventario_id: 17, barcode: 123412341234, cantidad: 23.0, cantidad_esperada: 23.0},
  {inventario_id: 17, barcode: 1000000, cantidad: 24.0, cantidad_esperada: 24.0},
  {inventario_id: 18, barcode: 123412341234, cantidad: 25.0, cantidad_esperada: 25.0},
  {inventario_id: 18, barcode: 1000000, cantidad: 26.0, cantidad_esperada: 26.0},
  {inventario_id: 19, barcode: 123412341234, cantidad: 27.0, cantidad_esperada: 27.0},
  {inventario_id: 19, barcode: 1000000, cantidad: 28.0, cantidad_esperada: 28.0},
  {inventario_id: 20, barcode: 123412341234, cantidad: 29.0, cantidad_esperada: 29.0},
  {inventario_id: 20, barcode: 1000000, cantidad: 30.0, cantidad_esperada: 30.0},
  {inventario_id: 21, barcode: 123412341234, cantidad: 31.0, cantidad_esperada: 31.0},
  {inventario_id: 21, barcode: 1000000, cantidad: 32.0, cantidad_esperada: 32.0},
  {inventario_id: 22, barcode: 123412341234, cantidad: 33.0, cantidad_esperada: 33.0},
  {inventario_id: 22, barcode: 1000000, cantidad: 34.0, cantidad_esperada: 34.0},
  {inventario_id: 23, barcode: 123412341234, cantidad: 35.0, cantidad_esperada: 35.0},
  {inventario_id: 23, barcode: 1000000, cantidad: 36.0, cantidad_esperada: 36.0},
  {inventario_id: 24, barcode: 123412341234, cantidad: 37.0, cantidad_esperada: 37.0}
])
TipoMerma.create!([
  {nombre: "Hurto"},
  {nombre: "Vencimiento"},
  {nombre: "Destruccion o Falla"},
  {nombre: "Diferencia conteo"},
  {nombre: "Diferencia cuadratura"}
])
Caja.create!([
  {negocio_id: 3, user_id: 1, fecha_inicio: "2014-08-08 00:00:00", monto_inicio: 250000, fecha_termino: "2014-08-09 00:00:00", monto_termino: 258000, perdida: 56}
])
Venta.create!([
  {id: 1, negocio_id: 3, nro_venta: 1, caja_id: 1, caj_negocio_id: 3, monto: 3, fecha: "2014-02-02 00:00:00", tipo_documento: 4, tipo_venta: 4, descuento: 4, id_documento: 2, cancelada: false, user_id: 1},
  {id: 2, negocio_id: 3, nro_venta: 2, caja_id: 1, caj_negocio_id: 3, monto: 4, fecha: "2014-02-03 00:00:00", tipo_documento: 4, tipo_venta: 4, descuento: 4, id_documento: 2, cancelada: false, user_id: 1},
  {id: 3, negocio_id: 3, nro_venta: 3, caja_id: 1, caj_negocio_id: 3, monto: 5, fecha: "2014-03-03 00:00:00", tipo_documento: 4, tipo_venta: 4, descuento: 4, id_documento: 4, cancelada: false, user_id: 1},
  {id: 4, negocio_id: 3, nro_venta: 4, caja_id: 1, caj_negocio_id: 3, monto: 6, fecha: "2014-03-04 00:00:00", tipo_documento: 6, tipo_venta: 6, descuento: 6, id_documento: 6, cancelada: false, user_id: 1},
  {id: 5, negocio_id: 3, nro_venta: 5, caja_id: 1, caj_negocio_id: 3, monto: 7, fecha: "2014-04-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 7, id_documento: 7, cancelada: false, user_id: 1},
  {id: 6, negocio_id: 3, nro_venta: 6, caja_id: 1, caj_negocio_id: 3, monto: 8, fecha: "2014-04-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 8, id_documento: 7, cancelada: false, user_id: 1},
  {id: 7, negocio_id: 3, nro_venta: 7, caja_id: 1, caj_negocio_id: 3, monto: 9, fecha: "2014-05-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 9, id_documento: 7, cancelada: false, user_id: 1},
  {id: 8, negocio_id: 3, nro_venta: 8, caja_id: 1, caj_negocio_id: 3, monto: 10, fecha: "2014-05-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 6, id_documento: 7, cancelada: false, user_id: 1},
  {id: 9, negocio_id: 3, nro_venta: 9, caja_id: 1, caj_negocio_id: 3, monto: 11, fecha: "2014-06-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 5, id_documento: 7, cancelada: false, user_id: 1},
  {id: 10, negocio_id: 3, nro_venta: 10, caja_id: 1, caj_negocio_id: 3, monto: 12, fecha: "2014-06-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 4, id_documento: 7, cancelada: false, user_id: 1},
  {id: 11, negocio_id: 3, nro_venta: 11, caja_id: 1, caj_negocio_id: 3, monto: 13, fecha: "2014-07-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 5, id_documento: 7, cancelada: false, user_id: 1},
  {id: 12, negocio_id: 3, nro_venta: 12, caja_id: 1, caj_negocio_id: 3, monto: 14, fecha: "2014-07-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 2, id_documento: 7, cancelada: false, user_id: 1},
  {id: 13, negocio_id: 3, nro_venta: 13, caja_id: 1, caj_negocio_id: 3, monto: 15, fecha: "2014-08-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 1, id_documento: 7, cancelada: false, user_id: 1},
  {id: 14, negocio_id: 3, nro_venta: 14, caja_id: 1, caj_negocio_id: 3, monto: 16, fecha: "2014-08-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 4, id_documento: 7, cancelada: false, user_id: 1},
  {id: 15, negocio_id: 3, nro_venta: 15, caja_id: 1, caj_negocio_id: 3, monto: 17, fecha: "2014-09-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 5, id_documento: 7, cancelada: false, user_id: 1},
  {id: 16, negocio_id: 3, nro_venta: 16, caja_id: 1, caj_negocio_id: 3, monto: 18, fecha: "2014-09-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 3, id_documento: 7, cancelada: false, user_id: 1},
  {id: 17, negocio_id: 3, nro_venta: 17, caja_id: 1, caj_negocio_id: 3, monto: 19, fecha: "2014-10-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 6, id_documento: 7, cancelada: false, user_id: 1},
  {id: 18, negocio_id: 3, nro_venta: 18, caja_id: 1, caj_negocio_id: 3, monto: 20, fecha: "2014-10-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 58, id_documento: 7, cancelada: false, user_id: 1},
  {id: 19, negocio_id: 3, nro_venta: 19, caja_id: 1, caj_negocio_id: 3, monto: 21, fecha: "2014-11-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 7, id_documento: 7, cancelada: false, user_id: 1},
  {id: 20, negocio_id: 3, nro_venta: 20, caja_id: 1, caj_negocio_id: 3, monto: 22, fecha: "2014-11-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 4, id_documento: 7, cancelada: false, user_id: 1},
  {id: 21, negocio_id: 3, nro_venta: 21, caja_id: 1, caj_negocio_id: 3, monto: 23, fecha: "2014-12-01 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 1, id_documento: 7, cancelada: false, user_id: 1},
  {id: 22, negocio_id: 3, nro_venta: 22, caja_id: 1, caj_negocio_id: 3, monto: 24, fecha: "2014-12-02 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 2, id_documento: 7, cancelada: false, user_id: 1},
  {id: 23, negocio_id: 3, nro_venta: 23, caja_id: 1, caj_negocio_id: 3, monto: 25, fecha: "2014-12-03 00:00:00", tipo_documento: 7, tipo_venta: 7, descuento: 456, id_documento: 7, cancelada: false, user_id: 1}
])
VentaDetalle.create!([
  {venta_id: 1, negocio_id: 3, barcode: 1000000, cantidad: 59.0, precio: 25000.0, precio_neto: 3000.0, fifo: 56.0, ganancia: 89.0, iva: 89.0, otros: 89.0, iva_residual: 89.0, otros_residual: 89.0, proporcion_iva: 89.0, proporcion_otros: 89.0, tiene_impuestos: false},
  {venta_id: 3, negocio_id: 3, barcode: 1000000, cantidad: 59.0, precio: 26000.0, precio_neto: 3444.0, fifo: 56.0, ganancia: 89.0, iva: 78.0, otros: 45.0, iva_residual: 12.0, otros_residual: 23.0, proporcion_iva: 56.0, proporcion_otros: 89.0, tiene_impuestos: false},
  {venta_id: 7, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 26455.0, precio_neto: 6565.0, fifo: 65.0, ganancia: 65.0, iva: 65.0, otros: 65.0, iva_residual: 65.0, otros_residual: 65.0, proporcion_iva: 65.0, proporcion_otros: 65.0, tiene_impuestos: false},
  {venta_id: 16, negocio_id: 3, barcode: 123412341234, cantidad: 56.0, precio: 9.0, precio_neto: 9.0, fifo: 9.0, ganancia: 6.0, iva: 96.0, otros: 96.0, iva_residual: 9.0, otros_residual: 69.0, proporcion_iva: 69.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 5, negocio_id: 3, barcode: 1000000, cantidad: 59.0, precio: 26000.0, precio_neto: 3444.0, fifo: 56.0, ganancia: 89.0, iva: 78.0, otros: 45.0, iva_residual: 12.0, otros_residual: 23.0, proporcion_iva: 56.0, proporcion_otros: 89.0, tiene_impuestos: false},
  {venta_id: 17, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 23.0, precio_neto: 2.0, fifo: 32.0, ganancia: 3.0, iva: 23.0, otros: 23.0, iva_residual: 23.0, otros_residual: 23.0, proporcion_iva: 23.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 9, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 5656.0, precio_neto: 5656.0, fifo: 56.0, ganancia: 56.0, iva: 56.0, otros: 56.0, iva_residual: 56.0, otros_residual: 56.0, proporcion_iva: 56.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 4, negocio_id: 3, barcode: 123412341234, cantidad: 59.0, precio: 26000.0, precio_neto: 3444.0, fifo: 56.0, ganancia: 89.0, iva: 78.0, otros: 45.0, iva_residual: 12.0, otros_residual: 23.0, proporcion_iva: 56.0, proporcion_otros: 89.0, tiene_impuestos: false},
  {venta_id: 6, negocio_id: 3, barcode: 123412341234, cantidad: 59.0, precio: 26000.0, precio_neto: 3444.0, fifo: 56.0, ganancia: 89.0, iva: 78.0, otros: 45.0, iva_residual: 12.0, otros_residual: 23.0, proporcion_iva: 56.0, proporcion_otros: 89.0, tiene_impuestos: false},
  {venta_id: 8, negocio_id: 3, barcode: 123412341234, cantidad: 23.0, precio: 2323.0, precio_neto: 2323.0, fifo: 2323.0, ganancia: 23.0, iva: 23.0, otros: 23.0, iva_residual: 23.0, otros_residual: 23.0, proporcion_iva: 23.0, proporcion_otros: 23.0, tiene_impuestos: false},
  {venta_id: 18, negocio_id: 3, barcode: 123412341234, cantidad: 56.0, precio: 5656.0, precio_neto: 5656.0, fifo: 56.0, ganancia: 56.0, iva: 56.0, otros: 56.0, iva_residual: 56.0, otros_residual: 56.0, proporcion_iva: 56.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 10, negocio_id: 3, barcode: 123412341234, cantidad: 12.0, precio: 2123.0, precio_neto: 23123.0, fifo: 654.0, ganancia: 32.0, iva: 165.0, otros: 456.0, iva_residual: 896.0, otros_residual: 56.0, proporcion_iva: 61561.0, proporcion_otros: 687.0, tiene_impuestos: false},
  {venta_id: 11, negocio_id: 3, barcode: 1000000, cantidad: 564.0, precio: 21.0, precio_neto: 54.0, fifo: 362.0, ganancia: 165.0, iva: 56.0, otros: 1.0, iva_residual: 654.0, otros_residual: 64.0, proporcion_iva: 61.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 12, negocio_id: 3, barcode: 123412341234, cantidad: 4.0, precio: 5656.0, precio_neto: 56.0, fifo: 556.0, ganancia: 4.0, iva: 84.0, otros: 56489.0, iva_residual: 23.0, otros_residual: 61.0, proporcion_iva: 56.0, proporcion_otros: 561.0, tiene_impuestos: false},
  {venta_id: 13, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 8.0, precio_neto: 2.0, fifo: 8.0, ganancia: 56.0, iva: 1478.0, otros: 6.0, iva_residual: 5.0, otros_residual: 4.0, proporcion_iva: 7.0, proporcion_otros: 8.0, tiene_impuestos: false},
  {venta_id: 14, negocio_id: 3, barcode: 123412341234, cantidad: 45.0, precio: 6.0, precio_neto: 2.0, fifo: 1.0, ganancia: 5.0, iva: 9.0, otros: 8.0, iva_residual: 4.0, otros_residual: 2.0, proporcion_iva: 6.0, proporcion_otros: 54.0, tiene_impuestos: false},
  {venta_id: 15, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 2.0, precio_neto: 69.0, fifo: 2.0, ganancia: 56.0, iva: 1.0, otros: 0.0, iva_residual: 1.0, otros_residual: 2.0, proporcion_iva: 2.0, proporcion_otros: 1.0, tiene_impuestos: false},
  {venta_id: 19, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 5656.0, precio_neto: 8.0, fifo: 45.0, ganancia: 16.0, iva: 1.0, otros: 651.0, iva_residual: 561.0, otros_residual: 56.0, proporcion_iva: 1561.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 20, negocio_id: 3, barcode: 123412341234, cantidad: 56.0, precio: 4.0, precio_neto: 4.0, fifo: 1.0, ganancia: 54.0, iva: 6.0, otros: 6.0, iva_residual: 5.0, otros_residual: 45.0, proporcion_iva: 46.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 21, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 8.0, precio_neto: 5656.0, fifo: 89.0, ganancia: 89.0, iva: 89.0, otros: 8.0, iva_residual: 98.0, otros_residual: 98.0, proporcion_iva: 98.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 22, negocio_id: 3, barcode: 123412341234, cantidad: 56.0, precio: 4.0, precio_neto: 0.0, fifo: 21.0, ganancia: 4.0, iva: 14.0, otros: 1.0, iva_residual: 56.0, otros_residual: 56.0, proporcion_iva: 56.0, proporcion_otros: 56.0, tiene_impuestos: false},
  {venta_id: 23, negocio_id: 3, barcode: 1000000, cantidad: 56.0, precio: 998.0, precio_neto: 9.0, fifo: 89.0, ganancia: 89.0, iva: 8.0, otros: 78.0, iva_residual: 48.0, otros_residual: 56.0, proporcion_iva: 251.0, proporcion_otros: 51.0, tiene_impuestos: false},
  {venta_id: 2, negocio_id: 3, barcode: 123412341234, cantidad: 59.0, precio: 26000.0, precio_neto: 3444.0, fifo: 56.0, ganancia: 89.0, iva: 78.0, otros: 45.0, iva_residual: 12.0, otros_residual: 23.0, proporcion_iva: 56.0, proporcion_otros: 89.0, tiene_impuestos: false}
])
Stock.create!([
  {negocio_id: 3, barcode: 1000000, cantidad: 60, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 0, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 1, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 2, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 10, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 20, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 25, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 30, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 35, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 40, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 45, fecha: "2014-02-08 00:00:00"},
  {negocio_id: 3, barcode: 1000000, cantidad: 50, fecha: "2014-02-08 00:00:00"},
  
  {negocio_id: 2, barcode: 1000000, cantidad: 40, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 58, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 60, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 80, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 0, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 0, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 1, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 12, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 20, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 37, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 38, fecha: "2014-02-08 00:00:00"},
  {negocio_id: 2, barcode: 1000000, cantidad: 50, fecha: "2014-02-08 00:00:00"},
  
  {negocio_id: 1, barcode: 1000000, cantidad: 32, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 35, fecha: "2014-07-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 38, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 40, fecha: "2014-06-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 45, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 47, fecha: "2014-05-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 47, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 48, fecha: "2014-04-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 48, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 48, fecha: "2014-03-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 50, fecha: "2014-02-08 00:00:00"},
  {negocio_id: 1, barcode: 1000000, cantidad: 50, fecha: "2014-02-08 00:00:00"}
])
