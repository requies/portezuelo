/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     14/08/2014 0:10:13                           */
/*==============================================================*/


drop table compra CASCADE;

drop table alerta_cambio_precio CASCADE;

drop index index_14;

drop index index_13;

drop table asistencia CASCADE;

drop index index_1;

drop table caja CASCADE;

drop table compra_detalle CASCADE;

drop index index_15;

drop table egreso CASCADE;

drop index index_7;

drop table familia_producto CASCADE;

drop table impuesto CASCADE;

drop index index_16;

drop table ingreso CASCADE;

drop table inventario CASCADE;

drop table inventario_detalle CASCADE;

drop index index_3;

drop table log CASCADE;

drop table maquina CASCADE;

drop index index_10;

drop index index_9;

drop table merma CASCADE;

drop table negocio CASCADE;

drop table pedido CASCADE;

drop table pedido_detalle CASCADE;

drop index index_4;

drop table producto CASCADE;

drop index index_18;

drop table promocion CASCADE;

drop table promocion_detalle CASCADE;

drop table stocks CASCADE;

drop table tipo_egreso CASCADE;

drop table tipo_ingreso CASCADE;

drop table tipo_merma CASCADE;

drop table tipo_producto CASCADE;

drop table traspaso CASCADE;

drop table traspaso_detalle CASCADE;

drop table unidad_producto CASCADE;

drop index index_2;

drop index index_5;

drop table users CASCADE;

drop index indx_caja;

drop index indx_venta_fecha;

drop table venta CASCADE;

drop table venta_anulada CASCADE;

drop index index_8;

drop index index_venta_detalle;

drop table venta_detalle CASCADE;

drop sequence nro_venta;

drop sequence producto_barcode_seq;

create sequence nro_venta
start 1;

create sequence producto_barcode_seq
start 1000000;

/*==============================================================*/
/* Table: compra                                                */
/*==============================================================*/
create table compra (
   id                   serial not null,
   fecha                date                 not null,
   descripcion          varchar(255)         null,
   user_id              int2                 not null,
   negocio_id           int2                 not null,
   egreso_id            int4                 null,
   constraint pk_compra primary key (id)
);

/*==============================================================*/
/* Table: alerta_cambio_precio                                  */
/*==============================================================*/
create table alerta_cambio_precio (
   id                   serial               not null,
   leido                bool                 not null,
   negocio_id           int2                 null,
   barcode              int8                 null,
   descripcion          varchar(255)         null,
   precio_antiguo       float8               null,
   precio_nuevo         float8               null,
   fecha                timestamp            null,
   constraint pk_alerta_cambio_precio primary key (id)
);

comment on column alerta_cambio_precio.barcode is
'llave primaria';

/*==============================================================*/
/* Table: asistencia                                            */
/*==============================================================*/
create table asistencia (
   id                   serial               not null,
   user_id              int4                 not null,
   negocio_id           int4                 not null,
   entrada              timestamp            null,
   salida               timestamp            null,
   constraint pk_asistencia primary key (id)
);

/*==============================================================*/
/* Index: index_13                                              */
/*==============================================================*/
create unique index index_13 on asistencia (
id
);

/*==============================================================*/
/* Index: index_14                                              */
/*==============================================================*/
create  index index_14 on asistencia (
user_id
);

/*==============================================================*/
/* Table: caja                                                  */
/*==============================================================*/
create table caja (
   id                   serial not null,
   negocio_id           int4                 not null,
   maquina_id           int2                 null,
   user_id              int2                 not null,
   fecha_inicio         timestamp            null,
   monto_inicio         int4                 null,
   fecha_termino        timestamp            null,
   monto_termino        int4                 null,
   perdida              int4                 null,
   constraint pk_caja primary key (id, negocio_id)
);

/*==============================================================*/
/* Index: index_1                                               */
/*==============================================================*/
create unique index index_1 on caja (
id,
negocio_id
);

/*==============================================================*/
/* Table: compra_detalle                                        */
/*==============================================================*/
create table compra_detalle (
   barcode              int8                 not null,
   compra_id            int4                 not null,
   cantidad             float8               not null,
   costo                int4                 not null,
   constraint pk_compra_detalle primary key (barcode, compra_id)
);

comment on column compra_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: egreso                                                */
/*==============================================================*/
create table egreso (
   id                   serial not null,
   tipo_egreso_id       int2                 null,
   caja_id              int2                 null,
   negocio_id           int4                 null,
   monto                int4                 not null,
   fecha                timestamp            not null,
   user_id              int2                 null,
   constraint pk_egreso primary key (id)
);

comment on column egreso.user_id is
'Es null si no se tiene una caja asociada, en caso de haber una caja asociada el user se obtiene segun el usuario de la caja.';

/*==============================================================*/
/* Index: index_15                                              */
/*==============================================================*/
create unique index index_15 on egreso (
id
);

/*==============================================================*/
/* Table: familia_producto                                      */
/*==============================================================*/
create table familia_producto (
   id                   serial not null,
   nombre               varchar(32)          not null,
   constraint pk_familias primary key (id),
   constraint ak_ak_nombre_familia_familia_ unique (nombre)
);

/*==============================================================*/
/* Index: index_7                                               */
/*==============================================================*/
create unique index index_7 on familia_producto (
id
);

/*==============================================================*/
/* Table: impuesto                                              */
/*==============================================================*/
create table impuesto (
   id                   serial not null,
   descripcion          varchar(64)          not null,
   monto                float8               not null,
   removable            boolean              not null default 't',
   visible              bool                 not null,
   constraint pk_impuesto primary key (id)
);

/*==============================================================*/
/* Table: ingreso                                               */
/*==============================================================*/
create table ingreso (
   id                   serial not null,
   tipo_ingreso_id      int2                 not null,
   caja_id              int2                 null,
   negocio_id           int4                 null,
   monto                int4                 not null,
   fecha                timestamp            not null,
   user_id              int2                 null,
   constraint pk_ingreso primary key (id)
);

/*==============================================================*/
/* Index: index_16                                              */
/*==============================================================*/
create unique index index_16 on ingreso (
id
);

/*==============================================================*/
/* Table: inventario                                            */
/*==============================================================*/
create table inventario (
   id                   serial               not null,
   negocio_id           int4                 not null,
   user_id              int2                 null,
   fecha                timestamp            null,
   constraint pk_inventario primary key (id)
);

/*==============================================================*/
/* Table: inventario_detalle                                    */
/*==============================================================*/
create table inventario_detalle (
   id                   serial               not null,
   inventario_id        int4                 not null,
   barcode              int8                 not null,
   cantidad             float8               not null,
   cantidad_esperada    float8               not null,
   constraint pk_inventario_detalle primary key (id)
);

comment on column inventario_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: log                                                   */
/*==============================================================*/
create table log (
   id                   serial not null,
   fecha                timestamp            null,
   maquina              int4                 null,
   seller               int4                 null,
   text                 text                 null,
   constraint pk_log primary key (id)
);

/*==============================================================*/
/* Index: index_3                                               */
/*==============================================================*/
create unique index index_3 on log (
id
);

/*==============================================================*/
/* Table: maquina                                               */
/*==============================================================*/
create table maquina (
   id                   serial not null,
   negocio_id           int4                 null,
   codigo_maquina       varchar(1000)        null,
   nombre_maqina        varchar(128)         null,
   habilitada           boolean              null,
   constraint pk_maquina primary key (id)
);

/*==============================================================*/
/* Table: merma                                                 */
/*==============================================================*/
create table merma (
   id                   serial not null,
   negocio_id           int4                 not null,
   barcode              int8                 null,
   tipo_merma_id        int2                 null,
   tipo                 int4                 null,
   unidades             float8               null,
   fecha                timestamp            null,
   constraint pk_merma primary key (id, negocio_id)
);

/*==============================================================*/
/* Index: index_9                                               */
/*==============================================================*/
create unique index index_9 on merma (
id,
negocio_id
);

/*==============================================================*/
/* Index: index_10                                              */
/*==============================================================*/
create  index index_10 on merma (
barcode
);

/*==============================================================*/
/* Table: negocio                                               */
/*==============================================================*/
create table negocio (
   id                   serial               not null,
   rut                  varchar(15)          not null,
   razon_social         varchar(255)         null,
   nombre               varchar(255)         not null,
   fono                 varchar(15)          null,
   fax                  varchar(15)          null,
   direccion            varchar(255)         null,
   comuna               varchar(64)          null,
   ciudad               varchar(64)          null,
   giro                 varchar(100)         null,
   seleccionado         bool                 not null,
   constraint pk_negocio primary key (id),
   constraint ak_key_nombre_negocio unique (nombre)
);

comment on table negocio is
'Representa los datos del negocio, solo tiene una unica fila';

/*==============================================================*/
/* Table: pedido                                                */
/*==============================================================*/
create table pedido (
   id                   serial               not null,
   user_id              int2                 null,
   negocio_id           int2                 null,
   fecha                timestamp            null,
   descripcion          varchar(255)         null,
   estado               int4                 not null,
   constraint pk_pedido primary key (id)
);

/*==============================================================*/
/* Table: pedido_detalle                                        */
/*==============================================================*/
create table pedido_detalle (
   barcode              int8                 not null,
   pedido_id            int4                 not null,
   cantidad             float8               null,
   costo                int4                 not null,
   constraint pk_pedido_detalle primary key (barcode, pedido_id)
);

comment on column pedido_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: producto                                              */
/*==============================================================*/
create table producto (
   barcode              int8                 not null default nextval('producto_barcode_seq'::regclass),
   codigo_corto         varchar(16)          not null,
   marca                varchar(35)          null,
   descripcion          varchar(50)          null,
   contenido            varchar(10)          null,
   unidad               varchar(10)          null,
   stock                float8               null default 0,
   precio               float8               null,
   precio_neto          float8               null,
   costo_promedio       float8               null,
   fifo                 float8               null,
   vendidos             float8               null,
   aplica_iva           bool                 null,
   otros_impuestos      int2                 null,
   familia              int2                 not null default 1,
   id_tipo_producto     int2                 not null,
   es_perecible         bool                 null,
   dias_stock           float8               null default 1,
   margen_promedio      float8               null default 0,
   es_venta_fraccionada bool                 null,
   stock_pro            float8               null default 0.0,
   tasa_canje           float8               null default 1.0,
   precio_mayor         float8               null,
   cantidad_mayor       float8               null,
   es_mayorista         bool                 null,
   estado               bool                 null,
   precio_editable      bool                 null,
   constraint pk_producto primary key (barcode),
   constraint unique_short_code unique (codigo_corto)
);

comment on column producto.barcode is
'llave primaria';

comment on column producto.codigo_corto is
'crear ckeck point';

comment on column producto.costo_promedio is
'antes fifo';

/*==============================================================*/
/* Index: index_4                                               */
/*==============================================================*/
create unique index index_4 on producto (
barcode
);

/*==============================================================*/
/* Table: promocion                                             */
/*==============================================================*/
create table promocion (
   id                   serial               not null,
   barcode              int8                 not null,
   nombre               varchar(255)         null,
   inicio               timestamp            null,
   termino              timestamp            null,
   habilitado           bool                 null,
   precio               float8               null,
   constraint pk_promocion primary key (id)
);

comment on column promocion.barcode is
'llave primaria';

/*==============================================================*/
/* Index: index_18                                              */
/*==============================================================*/
create unique index index_18 on promocion (
id
);

/*==============================================================*/
/* Table: promocion_detalle                                     */
/*==============================================================*/
create table promocion_detalle (
   barcode              int8                 not null,
   promocion_id         int4                 not null,
   cantidad             float8               null,
   constraint pk_promocion_detalle primary key (promocion_id, barcode)
);

comment on column promocion_detalle.barcode is
'llave primaria';

/*==============================================================*/
/* Table: stocks                                                */
/*==============================================================*/
create table stocks (
   id                   serial               not null,
   negocio_id           int4                 null,
   barcode              int8                 null default nextval('producto_barcode_seq'::regclass),
   cantidad             float8               null,
   fecha                timestamp            null,
   constraint pk_stocks primary key (id)
);

comment on column stocks.barcode is
'llave primaria';

/*==============================================================*/
/* Table: tipo_egreso                                           */
/*==============================================================*/
create table tipo_egreso (
   id                   serial not null,
   descripcion          varchar(64)          null,
   removable            bool                 not null,
   constraint pk_tipo_egreso primary key (id)
);

/*==============================================================*/
/* Table: tipo_ingreso                                          */
/*==============================================================*/
create table tipo_ingreso (
   id                   serial not null,
   descripcion          varchar(64)          null,
   removable            bool                 not null,
   constraint pk_tipo_ingreso primary key (id)
);

/*==============================================================*/
/* Table: tipo_merma                                            */
/*==============================================================*/
create table tipo_merma (
   id                   serial not null,
   nombre               varchar(25)          not null,
   constraint pk_tipo_merma primary key (id)
);

/*==============================================================*/
/* Table: tipo_producto                                         */
/*==============================================================*/
create table tipo_producto (
   id                   serial not null,
   nombre               varchar(255)         not null,
   constraint pk_tipo_producto primary key (id)
);

/*==============================================================*/
/* Table: traspaso                                              */
/*==============================================================*/
create table traspaso (
   id                   serial               not null,
   origen_id            int2                 null,
   destino_id           int2                 null,
   user_id              int2                 null,
   monto                float8               null,
   fecha                timestamp            null,
   constraint pk_traspaso primary key (id)
);

/*==============================================================*/
/* Table: traspaso_detalle                                      */
/*==============================================================*/
create table traspaso_detalle (
   id                   serial               not null,
   barcode              int8                 not null,
   traspaso_id          int4                 not null,
   cantidad             float8               null,
   precio               float8               not null,
   precio_venta         float8               null,
   costo_modificado     boolean              null,
   constraint pk_traspaso_detalle primary key (id)
);

/*==============================================================*/
/* Table: unidad_producto                                       */
/*==============================================================*/
create table unidad_producto (
   id                   serial not null,
   nombre               varchar(16)          not null,
   constraint pk_unidad_producuto primary key (id),
   constraint ak_ak_nombre_unidad_p_unidad_p unique (nombre)
);

/*==============================================================*/
/* Table: users                                                 */
/*==============================================================*/
create table users (
   id                   serial not null,
   rut                  varchar(15)          not null,
   passwd               varchar(128)         not null,
   nombre               varchar(64)          not null,
   apell_p              varchar(64)          not null,
   apell_m              varchar(64)          null,
   fecha_ingreso        timestamp            null,
   tipo_usuario         int2                 null,
   activo               bool                 not null,
   constraint pk_users primary key (id),
   constraint ak_key_2_users unique (rut)
);

/*==============================================================*/
/* Index: index_5                                               */
/*==============================================================*/
create unique index index_5 on users (
id
);

/*==============================================================*/
/* Index: index_2                                               */
/*==============================================================*/
create  index index_2 on users using hash (
( users.rut )
);

/*==============================================================*/
/* Table: venta                                                 */
/*==============================================================*/
create table venta (
   id                   serial               not null,
   negocio_id           int4                 not null,
   local_id             int4                 null,
   nro_venta            int4                 not null,
   caja_id              int4                 null,
   caj_negocio_id       int4                 null,
   monto                int4                 null,
   fecha                timestamp            null,
   maquina_id           int2                 null,
   tipo_documento       int2                 null,
   tipo_venta           int2                 null,
   descuento            int4                 null,
   id_documento         int4                 null,
   cancelada            bool                 null,
   user_id              int4                 null,
   constraint pk_venta primary key (id, negocio_id)
);

/*==============================================================*/
/* Index: indx_venta_fecha                                      */
/*==============================================================*/
create  index indx_venta_fecha on venta using btree (
fecha
);

/*==============================================================*/
/* Index: indx_caja                                             */
/*==============================================================*/
create  index indx_caja on venta (
caja_id,
caj_negocio_id
);

/*==============================================================*/
/* Table: venta_anulada                                         */
/*==============================================================*/
create table venta_anulada (
   venta_id             int4                 not null,
   negocio_id           int4                 not null,
   fecha                timestamp            null default now(),
   motivo               text                 null,
   caja_id              int4                 null,
   caj_negocio_id       int4                 null,
   constraint pk_venta_anulada primary key (venta_id, negocio_id)
);

/*==============================================================*/
/* Table: venta_detalle                                         */
/*==============================================================*/
create table venta_detalle (
   id                   serial               not null,
   venta_id             int4                 not null,
   negocio_id           int4                 not null,
   barcode              int8                 not null,
   cantidad             float8               null,
   precio               float8               not null,
   precio_neto          float8               not null,
   fifo                 float8               not null,
   ganancia             float8               not null,
   iva                  float8               null,
   otros                float8               null,
   iva_residual         float8               null,
   otros_residual       float8               null,
   proporcion_iva       float8               null,
   proporcion_otros     float8               null,
   tiene_impuestos      bool                 null,
   constraint pk_venta_detalle primary key (id)
);

comment on table venta_detalle is
'antes era products_sell_history';

/*==============================================================*/
/* Index: index_venta_detalle                                   */
/*==============================================================*/
create unique index index_venta_detalle on venta_detalle (
id
);

/*==============================================================*/
/* Index: index_8                                               */
/*==============================================================*/
create  index index_8 on venta_detalle (
barcode
);

alter table compra
   add constraint fk_compra_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table compra
   add constraint fk_compra_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table compra
   add constraint fk_compra_reference_egreso foreign key (egreso_id)
      references egreso (id)
      on delete restrict on update restrict;

alter table alerta_cambio_precio
   add constraint fk_alerta_c_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table alerta_cambio_precio
   add constraint fk_alerta_c_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table asistencia
   add constraint fk_asistenc_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table asistencia
   add constraint fk_asistencia_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table caja
   add constraint fk_caja_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table caja
   add constraint fk_caja_maquina foreign key (maquina_id)
      references maquina (id)
      on delete restrict on update restrict;

alter table caja
   add constraint fk_caja_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table compra_detalle
   add constraint fk_compra_d_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table compra_detalle
   add constraint fk_compra_d_reference_compra foreign key (compra_id)
      references compra (id)
      on delete restrict on update restrict;

alter table egreso
   add constraint fk_egreso_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table egreso
   add constraint fk_egreso_caja foreign key (caja_id, negocio_id)
      references caja (id, negocio_id)
      on delete restrict on update restrict;

alter table egreso
   add constraint fk_egreso_tipo_egreso foreign key (tipo_egreso_id)
      references tipo_egreso (id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint fk_ingreso_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint fk_ingreso_caja foreign key (caja_id, negocio_id)
      references caja (id, negocio_id)
      on delete restrict on update restrict;

alter table ingreso
   add constraint fk_ingreso_tipo_ingreso foreign key (tipo_ingreso_id)
      references tipo_ingreso (id)
      on delete restrict on update restrict;

alter table inventario
   add constraint fk_inventar_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table inventario
   add constraint fk_inventar_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table inventario_detalle
   add constraint fk_inventar_reference_inventar foreign key (inventario_id)
      references inventario (id)
      on delete restrict on update restrict;

alter table inventario_detalle
   add constraint fk_inventar_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table maquina
   add constraint fk_maquina_reference_negocio foreign key (id)
      references negocio (id)
      on delete restrict on update restrict;

alter table merma
   add constraint fk_merma_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table merma
   add constraint fk_merma_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table merma
   add constraint fk_merma_tipo_merma foreign key (tipo_merma_id)
      references tipo_merma (id)
      on delete restrict on update restrict;

alter table pedido
   add constraint fk_pedido_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table pedido
   add constraint fk_pedido_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table pedido_detalle
   add constraint fk_pedido_d_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table pedido_detalle
   add constraint fk_pedido_d_reference_pedido foreign key (pedido_id)
      references pedido (id)
      on delete restrict on update restrict;

alter table producto
   add constraint fk_producto_reference_familia_ foreign key (familia)
      references familia_producto (id)
      on delete restrict on update restrict;

alter table producto
   add constraint fk_producto_reference_impuesto foreign key (otros_impuestos)
      references impuesto (id)
      on delete restrict on update restrict;

alter table producto
   add constraint fk_producto_fk_produc_tipo_pro foreign key (id_tipo_producto)
      references tipo_producto (id)
      on delete restrict on update restrict;

alter table promocion
   add constraint fk_promocio_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table promocion_detalle
   add constraint fk_promocio_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table promocion_detalle
   add constraint fk_promocio_reference_promocio foreign key (promocion_id)
      references promocion (id)
      on delete restrict on update restrict;

alter table stocks
   add constraint fk_stocks_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table stocks
   add constraint fk_stocks_reference_producto foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table traspaso
   add constraint fk_traspaso_fk_traspa_negocio foreign key (origen_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table traspaso
   add constraint fk_traspaso_reference_negocio foreign key (destino_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table traspaso
   add constraint fk_traspaso_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table traspaso_detalle
   add constraint fk_productos_barcode foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table traspaso_detalle
   add constraint fk_traspaso_id foreign key (traspaso_id)
      references traspaso (id)
      on delete restrict on update restrict;

alter table venta
   add constraint fk_venta_reference_maquina foreign key (maquina_id)
      references maquina (id)
      on delete restrict on update restrict;

alter table venta
   add constraint fk_venta_reference_caja foreign key (caja_id, caj_negocio_id)
      references caja (id, negocio_id)
      on delete restrict on update restrict;

alter table venta
   add constraint fk_venta_reference_negocio foreign key (negocio_id)
      references negocio (id)
      on delete restrict on update restrict;

alter table venta
   add constraint fk_venta_reference_users foreign key (user_id)
      references users (id)
      on delete restrict on update restrict;

alter table venta_anulada
   add constraint fk_venta_an_reference_caja foreign key (caja_id, caj_negocio_id)
      references caja (id, negocio_id)
      on delete restrict on update restrict;

alter table venta_anulada
   add constraint fk_venta_anulada_venta foreign key (venta_id, negocio_id)
      references venta (id, negocio_id)
      on delete restrict on update restrict;

alter table venta_detalle
   add constraint fk_productos_barcode foreign key (barcode)
      references producto (barcode)
      on delete restrict on update restrict;

alter table venta_detalle
   add constraint fk_venta_id foreign key (venta_id, negocio_id)
      references venta (id, negocio_id)
      on delete restrict on update restrict;

