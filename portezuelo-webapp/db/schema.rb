# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alerta_cambio_precio", force: true do |t|
    t.boolean  "leido",                    null: false
    t.integer  "negocio_id",     limit: 2
    t.integer  "barcode",        limit: 8
    t.string   "descripcion"
    t.float    "precio_antiguo"
    t.float    "precio_nuevo"
    t.datetime "fecha"
  end

  create_table "asistencia", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "negocio_id", null: false
    t.datetime "entrada"
    t.datetime "salida"
  end

  add_index "asistencia", ["id"], name: "index_13", unique: true, using: :btree
  add_index "asistencia", ["user_id"], name: "index_14", using: :btree

  create_table "caja", force: true do |t|
    t.integer  "negocio_id",              null: false
    t.integer  "maquina_id",    limit: 2
    t.integer  "user_id",       limit: 2, null: false
    t.datetime "fecha_inicio"
    t.integer  "monto_inicio"
    t.datetime "fecha_termino"
    t.integer  "monto_termino"
    t.integer  "perdida"
  end

  add_index "caja", ["id", "negocio_id"], name: "index_1", unique: true, using: :btree

  create_table "compra", force: true do |t|
    t.date    "fecha",                 null: false
    t.string  "descripcion"
    t.integer "user_id",     limit: 2, null: false
    t.integer "negocio_id",  limit: 2, null: false
    t.integer "egreso_id"
  end

  create_table "compra_detalle", id: false, force: true do |t|
    t.integer "barcode",   limit: 8, null: false
    t.integer "compra_id",           null: false
    t.float   "cantidad",            null: false
    t.integer "costo",               null: false
  end

  create_table "egreso", force: true do |t|
    t.integer  "tipo_egreso_id", limit: 2
    t.integer  "caja_id",        limit: 2
    t.integer  "negocio_id"
    t.integer  "monto",                    null: false
    t.datetime "fecha",                    null: false
    t.integer  "user_id",        limit: 2
  end

  add_index "egreso", ["id"], name: "index_15", unique: true, using: :btree

  create_table "familia_producto", force: true do |t|
    t.string "nombre", limit: 32, null: false
  end

  add_index "familia_producto", ["id"], name: "index_7", unique: true, using: :btree
  add_index "familia_producto", ["nombre"], name: "ak_ak_nombre_familia_familia_", unique: true, using: :btree

  create_table "impuesto", force: true do |t|
    t.string  "descripcion", limit: 64,                null: false
    t.float   "monto",                                 null: false
    t.boolean "removable",              default: true, null: false
    t.boolean "visible",                               null: false
  end

  create_table "ingreso", force: true do |t|
    t.integer  "tipo_ingreso_id", limit: 2, null: false
    t.integer  "caja_id",         limit: 2
    t.integer  "negocio_id"
    t.integer  "monto",                     null: false
    t.datetime "fecha",                     null: false
    t.integer  "user_id",         limit: 2
  end

  add_index "ingreso", ["id"], name: "index_16", unique: true, using: :btree

  create_table "inventario", force: true do |t|
    t.integer  "negocio_id",           null: false
    t.integer  "user_id",    limit: 2
    t.datetime "fecha"
  end

  create_table "inventario_detalle", force: true do |t|
    t.integer "inventario_id",               null: false
    t.integer "barcode",           limit: 8, null: false
    t.float   "cantidad",                    null: false
    t.float   "cantidad_esperada",           null: false
  end

  create_table "log", force: true do |t|
    t.datetime "fecha"
    t.integer  "maquina"
    t.integer  "seller"
    t.text     "text"
  end

  add_index "log", ["id"], name: "index_3", unique: true, using: :btree

  create_table "maquina", force: true do |t|
    t.string "codigo_maquina", limit: 1000
    t.string "nombre_maqina",  limit: 128
  end

  create_table "merma", force: true do |t|
    t.integer  "negocio_id",              null: false
    t.integer  "barcode",       limit: 8
    t.integer  "tipo_merma_id", limit: 2
    t.integer  "tipo"
    t.float    "unidades"
    t.datetime "fecha"
  end

  add_index "merma", ["barcode"], name: "index_10", using: :btree
  add_index "merma", ["id", "negocio_id"], name: "index_9", unique: true, using: :btree

  create_table "negocio", force: true do |t|
    t.string  "rut",          limit: 15,  null: false
    t.string  "razon_social"
    t.string  "nombre",                   null: false
    t.string  "fono",         limit: 15
    t.string  "fax",          limit: 15
    t.string  "direccion"
    t.string  "comuna",       limit: 64
    t.string  "ciudad",       limit: 64
    t.string  "giro",         limit: 100
    t.boolean "seleccionado",             null: false
  end

  add_index "negocio", ["nombre"], name: "ak_key_nombre_negocio", unique: true, using: :btree

  create_table "pedido", force: true do |t|
    t.integer  "user_id",     limit: 2
    t.integer  "negocio_id",  limit: 2
    t.datetime "fecha"
    t.string   "descripcion"
    t.integer  "estado",                null: false
  end

  create_table "pedido_detalle", id: false, force: true do |t|
    t.integer "barcode",   limit: 8, null: false
    t.integer "pedido_id",           null: false
    t.float   "cantidad"
    t.integer "costo",               null: false
  end

  create_table "producto", primary_key: "barcode", force: true do |t|
    t.string  "codigo_corto",         limit: 16,               null: false
    t.string  "marca",                limit: 35
    t.string  "descripcion",          limit: 50
    t.string  "contenido",            limit: 10
    t.string  "unidad",               limit: 10
    t.float   "stock",                           default: 0.0
    t.float   "precio"
    t.float   "precio_neto"
    t.float   "costo_promedio"
    t.float   "fifo"
    t.float   "vendidos"
    t.boolean "aplica_iva"
    t.integer "otros_impuestos",      limit: 2
    t.integer "familia",              limit: 2,  default: 1,   null: false
    t.integer "id_tipo_producto",     limit: 2,                null: false
    t.boolean "es_perecible"
    t.float   "dias_stock",                      default: 1.0
    t.float   "margen_promedio",                 default: 0.0
    t.boolean "es_venta_fraccionada"
    t.float   "stock_pro",                       default: 0.0
    t.float   "tasa_canje",                      default: 1.0
    t.float   "precio_mayor"
    t.float   "cantidad_mayor"
    t.boolean "es_mayorista"
    t.boolean "estado"
    t.boolean "precio_editable"
  end

  add_index "producto", ["barcode"], name: "index_4", unique: true, using: :btree
  add_index "producto", ["codigo_corto"], name: "unique_short_code", unique: true, using: :btree

  create_table "promocion", force: true do |t|
    t.integer  "barcode",    limit: 8, null: false
    t.string   "nombre"
    t.datetime "inicio"
    t.datetime "termino"
    t.boolean  "habilitado"
    t.float    "precio"
  end

  add_index "promocion", ["id"], name: "index_18", unique: true, using: :btree

  create_table "promocion_detalle", id: false, force: true do |t|
    t.integer "barcode",      limit: 8, null: false
    t.integer "promocion_id",           null: false
    t.float   "cantidad"
  end

  create_table "stocks", force: true do |t|
    t.integer  "negocio_id"
    t.integer  "barcode",    limit: 8, default: "nextval('producto_barcode_seq'::regclass)"
    t.float    "cantidad"
    t.datetime "fecha"
  end

  create_table "tipo_egreso", force: true do |t|
    t.string  "descripcion", limit: 64
    t.boolean "removable",              null: false
  end

  create_table "tipo_ingreso", force: true do |t|
    t.string  "descripcion", limit: 64
    t.boolean "removable",              null: false
  end

  create_table "tipo_merma", force: true do |t|
    t.string "nombre", limit: 25, null: false
  end

  create_table "tipo_producto", force: true do |t|
    t.string "nombre", limit: 16, null: false
  end

  create_table "traspaso", force: true do |t|
    t.integer  "origen_id",  limit: 2
    t.integer  "destino_id", limit: 2
    t.integer  "user_id",    limit: 2
    t.float    "monto"
    t.datetime "fecha"
  end

  create_table "traspaso_detalle", force: true do |t|
    t.integer "barcode",          limit: 8, null: false
    t.integer "traspaso_id",                null: false
    t.float   "cantidad"
    t.float   "precio",                     null: false
    t.float   "precio_venta"
    t.boolean "costo_modificado"
  end

  create_table "unidad_producto", force: true do |t|
    t.string "nombre", limit: 16, null: false
  end

  add_index "unidad_producto", ["nombre"], name: "ak_ak_nombre_unidad_p_unidad_p", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "rut",           limit: 15,  null: false
    t.string   "passwd",        limit: 128, null: false
    t.string   "nombre",        limit: 64,  null: false
    t.string   "apell_p",       limit: 64,  null: false
    t.string   "apell_m",       limit: 64
    t.datetime "fecha_ingreso"
    t.integer  "tipo_usuario",  limit: 2
    t.boolean  "activo",                    null: false
  end

  add_index "users", ["id"], name: "index_5", unique: true, using: :btree
  add_index "users", ["rut"], name: "ak_key_2_users", unique: true, using: :btree
  add_index "users", ["rut"], name: "index_2", using: :hash

  create_table "venta", id: false, force: true do |t|
    t.integer  "id",                       null: false
    t.integer  "negocio_id",               null: false
    t.integer  "nro_venta",                null: false
    t.integer  "caja_id"
    t.integer  "caj_negocio_id"
    t.integer  "monto"
    t.datetime "fecha"
    t.integer  "maquina_id",     limit: 2
    t.integer  "tipo_documento", limit: 2
    t.integer  "tipo_venta",     limit: 2
    t.integer  "descuento"
    t.integer  "id_documento"
    t.boolean  "cancelada"
    t.integer  "user_id"
  end

  add_index "venta", ["caja_id", "caj_negocio_id"], name: "indx_caja", using: :btree
  add_index "venta", ["fecha"], name: "indx_venta_fecha", using: :btree
  add_index "venta", ["nro_venta"], name: "ak_ak_num_venta_venta", unique: true, using: :btree

  create_table "venta_anulada", id: false, force: true do |t|
    t.integer  "venta_id",                         null: false
    t.integer  "negocio_id",                       null: false
    t.datetime "fecha",          default: "now()"
    t.text     "motivo"
    t.integer  "caja_id",                          null: false
    t.integer  "caj_negocio_id",                   null: false
  end

  create_table "venta_detalle", force: true do |t|
    t.integer "venta_id",                   null: false
    t.integer "negocio_id",                 null: false
    t.integer "barcode",          limit: 8, null: false
    t.float   "cantidad"
    t.float   "precio",                     null: false
    t.float   "precio_neto",                null: false
    t.float   "fifo",                       null: false
    t.float   "ganancia",                   null: false
    t.float   "iva"
    t.float   "otros"
    t.float   "iva_residual"
    t.float   "otros_residual"
    t.float   "proporcion_iva"
    t.float   "proporcion_otros"
    t.boolean "tiene_impuestos"
  end

  add_index "venta_detalle", ["barcode"], name: "index_8", using: :btree
  add_index "venta_detalle", ["id"], name: "index_venta_detalle", unique: true, using: :btree

end
