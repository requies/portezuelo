require 'test_helper'

class UnidadProductosControllerTest < ActionController::TestCase
  setup do
    @unidad_producto = unidad_productos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unidad_productos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unidad_producto" do
    assert_difference('UnidadProducto.count') do
      post :create, unidad_producto: { nombre: @unidad_producto.nombre }
    end

    assert_redirected_to unidad_producto_path(assigns(:unidad_producto))
  end

  test "should show unidad_producto" do
    get :show, id: @unidad_producto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unidad_producto
    assert_response :success
  end

  test "should update unidad_producto" do
    patch :update, id: @unidad_producto, unidad_producto: { nombre: @unidad_producto.nombre }
    assert_redirected_to unidad_producto_path(assigns(:unidad_producto))
  end

  test "should destroy unidad_producto" do
    assert_difference('UnidadProducto.count', -1) do
      delete :destroy, id: @unidad_producto
    end

    assert_redirected_to unidad_productos_path
  end
end
