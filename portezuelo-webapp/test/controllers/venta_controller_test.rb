require 'test_helper'

class VentaControllerTest < ActionController::TestCase
  setup do
    @ventum = venta(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:venta)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ventum" do
    assert_difference('Venta.count') do
      post :create, ventum: { caj_negocio_id: @ventum.caj_negocio_id, caja_id: @ventum.caja_id, cancelada: @ventum.cancelada, descuento: @ventum.descuento, fecha: @ventum.fecha, id: @ventum.id, id_documento: @ventum.id_documento, integer: @ventum.integer, maquina_id: @ventum.maquina_id, monto: @ventum.monto, negocio_id: @ventum.negocio_id, nro_venta: @ventum.nro_venta, tipo_documento: @ventum.tipo_documento, tipo_venta: @ventum.tipo_venta, user_id: @ventum.user_id }
    end

    assert_redirected_to ventum_path(assigns(:ventum))
  end

  test "should show ventum" do
    get :show, id: @ventum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ventum
    assert_response :success
  end

  test "should update ventum" do
    patch :update, id: @ventum, ventum: { caj_negocio_id: @ventum.caj_negocio_id, caja_id: @ventum.caja_id, cancelada: @ventum.cancelada, descuento: @ventum.descuento, fecha: @ventum.fecha, id: @ventum.id, id_documento: @ventum.id_documento, integer: @ventum.integer, maquina_id: @ventum.maquina_id, monto: @ventum.monto, negocio_id: @ventum.negocio_id, nro_venta: @ventum.nro_venta, tipo_documento: @ventum.tipo_documento, tipo_venta: @ventum.tipo_venta, user_id: @ventum.user_id }
    assert_redirected_to ventum_path(assigns(:ventum))
  end

  test "should destroy ventum" do
    assert_difference('Venta.count', -1) do
      delete :destroy, id: @ventum
    end

    assert_redirected_to venta_index_path
  end
end
