require 'test_helper'

class TipoMermasControllerTest < ActionController::TestCase
  setup do
    @tipo_merma = tipo_mermas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_mermas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_merma" do
    assert_difference('TipoMerma.count') do
      post :create, tipo_merma: { nombre: @tipo_merma.nombre }
    end

    assert_redirected_to tipo_merma_path(assigns(:tipo_merma))
  end

  test "should show tipo_merma" do
    get :show, id: @tipo_merma
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_merma
    assert_response :success
  end

  test "should update tipo_merma" do
    patch :update, id: @tipo_merma, tipo_merma: { nombre: @tipo_merma.nombre }
    assert_redirected_to tipo_merma_path(assigns(:tipo_merma))
  end

  test "should destroy tipo_merma" do
    assert_difference('TipoMerma.count', -1) do
      delete :destroy, id: @tipo_merma
    end

    assert_redirected_to tipo_mermas_path
  end
end
