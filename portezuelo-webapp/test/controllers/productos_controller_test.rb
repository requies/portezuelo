require 'test_helper'

class ProductosControllerTest < ActionController::TestCase
  setup do
    @producto = productos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:productos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create producto" do
    assert_difference('Producto.count') do
      post :create, producto: { aplica_iva: @producto.aplica_iva, cantidad_mayor: @producto.cantidad_mayor, codigo_corto: @producto.codigo_corto, contenido: @producto.contenido, costo_promedio: @producto.costo_promedio, descripcion: @producto.descripcion, dias_stock: @producto.dias_stock, es_mayorista: @producto.es_mayorista, es_perecible: @producto.es_perecible, es_venta_fraccionada: @producto.es_venta_fraccionada, estado: @producto.estado, familia: @producto.familia, fifo: @producto.fifo, id_tipo_producto: @producto.id_tipo_producto, marca: @producto.marca, margen_promedio: @producto.margen_promedio, otros_impuestos: @producto.otros_impuestos, precio: @producto.precio, precio_editable: @producto.precio_editable, precio_mayor: @producto.precio_mayor, precio_neto: @producto.precio_neto, stock: @producto.stock, stock_pro: @producto.stock_pro, tasa_canje: @producto.tasa_canje, unidad: @producto.unidad, vendidos: @producto.vendidos }
    end

    assert_redirected_to producto_path(assigns(:producto))
  end

  test "should show producto" do
    get :show, id: @producto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @producto
    assert_response :success
  end

  test "should update producto" do
    patch :update, id: @producto, producto: { aplica_iva: @producto.aplica_iva, cantidad_mayor: @producto.cantidad_mayor, codigo_corto: @producto.codigo_corto, contenido: @producto.contenido, costo_promedio: @producto.costo_promedio, descripcion: @producto.descripcion, dias_stock: @producto.dias_stock, es_mayorista: @producto.es_mayorista, es_perecible: @producto.es_perecible, es_venta_fraccionada: @producto.es_venta_fraccionada, estado: @producto.estado, familia: @producto.familia, fifo: @producto.fifo, id_tipo_producto: @producto.id_tipo_producto, marca: @producto.marca, margen_promedio: @producto.margen_promedio, otros_impuestos: @producto.otros_impuestos, precio: @producto.precio, precio_editable: @producto.precio_editable, precio_mayor: @producto.precio_mayor, precio_neto: @producto.precio_neto, stock: @producto.stock, stock_pro: @producto.stock_pro, tasa_canje: @producto.tasa_canje, unidad: @producto.unidad, vendidos: @producto.vendidos }
    assert_redirected_to producto_path(assigns(:producto))
  end

  test "should destroy producto" do
    assert_difference('Producto.count', -1) do
      delete :destroy, id: @producto
    end

    assert_redirected_to productos_path
  end
end
