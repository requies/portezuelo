json.array!(@impuestos) do |impuesto|
  json.extract! impuesto, :id, :descripcion, :monto, :removable, :visible
  json.url impuesto_url(impuesto, format: :json)
end
