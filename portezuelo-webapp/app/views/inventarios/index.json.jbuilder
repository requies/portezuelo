json.array!(@inventarios) do |inventario|
  json.extract! inventario, :id, :negocio_id, :user_id, :fecha
  json.url inventario_url(inventario, format: :json)
end
