json.array!(@pedidos) do |pedido|
  json.extract! pedido, :id, :user_id, :negocio_id, :fecha, :descripcion, :estado
  json.url pedido_url(pedido, format: :json)
end
