json.array!(@tipo_ingresos) do |tipo_ingreso|
  json.extract! tipo_ingreso, :id, :descripcion, :removable
  json.url tipo_ingreso_url(tipo_ingreso, format: :json)
end
