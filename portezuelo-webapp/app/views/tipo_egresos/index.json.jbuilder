json.array!(@tipo_egresos) do |tipo_egreso|
  json.extract! tipo_egreso, :id, :descripcion, :removable
  json.url tipo_egreso_url(tipo_egreso, format: :json)
end
