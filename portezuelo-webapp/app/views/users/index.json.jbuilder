json.array!(@users) do |user|
  json.extract! user, :id, :rut, :passwd, :nombre, :apell_p, :apell_m, :fecha_ingreso, :tipo_usuario, :activo
  json.url user_url(user, format: :json)
end
