json.array!(@negocios) do |negocio|
  json.extract! negocio, :id, :rut, :razon_social, :nombre, :fono, :fax, :direccion, :comuna, :ciudad, :giro, :seleccionado
  json.url negocio_url(negocio, format: :json)
end
