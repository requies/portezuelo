json.array!(@tipo_mermas) do |tipo_merma|
  json.extract! tipo_merma, :id, :nombre
  json.url tipo_merma_url(tipo_merma, format: :json)
end
