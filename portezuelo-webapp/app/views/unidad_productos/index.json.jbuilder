json.array!(@unidad_productos) do |unidad_producto|
  json.extract! unidad_producto, :id, :nombre
  json.url unidad_producto_url(unidad_producto, format: :json)
end
