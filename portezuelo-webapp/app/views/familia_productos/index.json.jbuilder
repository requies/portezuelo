json.array!(@familia_productos) do |familia_producto|
  json.extract! familia_producto, :id, :nombre
  json.url familia_producto_url(familia_producto, format: :json)
end
