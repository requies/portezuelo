class UnidadProducto < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'unidad_producto'
	self.per_page = 10
		
	# Validations
	validates :nombre, presence: { message: "Es necesario proporcionar un nombre." }, length: { maximum: 16, message: "El largo maximo del nombre es de 16 caracteres."}, uniqueness: { :message => "Ya existe una unidad llamada así." }
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
		scope :nombre, -> (nombre) { where nombre: nombre }
	
end
