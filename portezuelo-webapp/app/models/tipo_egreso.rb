class TipoEgreso < ActiveRecord::Base
	self.table_name = 'tipo_egreso'
	self.per_page = 10

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	validates :descripcion, presence: { :message => "Es necesario proporcionar una descripcion" }, length: { minimum: 4, maximum: 128, message: "La descripcion debe tener como mínimo 4 caracteres y como máximo 128" }
	# validates :removable, <validations>
	
	# Scopes (used for search form)
	scope :descripcion, -> (descripcion) { where descripcion: descripcion }
	scope :removable, -> (removable) { where removable: removable }
	
end
