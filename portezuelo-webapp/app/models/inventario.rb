class Inventario < ActiveRecord::Base
	self.table_name = 'inventario'
	self.per_page = 10
	belongs_to :negocio
	belongs_to :user
	has_many :inventario_detalles, :class_name => "InventarioDetalle"

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	# validates :negocio_id, <validations>
	# validates :user_id, <validations>
	# validates :fecha, <validations>
	
	# Scopes (used for search form)
	scope :usuario, -> (usuario) { where("nombre ilike ? or rut ilike ?", "%#{usuario}%", "%#{usuario}%").joins(:user) }
	scope :negocio, -> (negocio) { where("nombre ilike ?", "%#{negocio}%").joins(:negocio) }
	
end
