class VentaDetalle < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable
		
	self.table_name = 'venta_detalle'
	self.per_page = 10
	belongs_to :venta
	belongs_to :producto, :foreign_key => 'barcode'

	# Validations
	# validates :venta_id, <validations>
	# validates :negocio_id, <validations>
	# validates :barcode, <validations>
	# validates :cantidad, <validations>
	# validates :precio, <validations>
	# validates :precio_neto, <validations>
	# validates :fifo, <validations>
	# validates :ganancia, <validations>
	# validates :iva, <validations>
	# validates :otros, <validations>
	# validates :iva_residual, <validations>
	# validates :otros_residual, <validations>
	# validates :proporcion_iva, <validations>
	# validates :proporcion_otros, <validations>
	# validates :tiene_impuestos, <validations>
	
	# Scopes (used for search form)
	scope :venta_id, -> (venta_id) { where venta_id: venta_id }
	scope :negocio_id, -> (negocio_id) { where negocio_id: negocio_id }
	scope :barcode, -> (barcode) { where barcode: barcode }
	scope :cantidad, -> (cantidad) { where cantidad: cantidad }
	scope :precio, -> (precio) { where precio: precio }
	scope :precio_neto, -> (precio_neto) { where precio_neto: precio_neto }
	scope :fifo, -> (fifo) { where fifo: fifo }
	scope :ganancia, -> (ganancia) { where ganancia: ganancia }
	scope :iva, -> (iva) { where iva: iva }
	scope :otros, -> (otros) { where otros: otros }
	scope :iva_residual, -> (iva_residual) { where iva_residual: iva_residual }
	scope :otros_residual, -> (otros_residual) { where otros_residual: otros_residual }
	scope :proporcion_iva, -> (proporcion_iva) { where proporcion_iva: proporcion_iva }
	scope :proporcion_otros, -> (proporcion_otros) { where proporcion_otros: proporcion_otros }
	scope :tiene_impuestos, -> (tiene_impuestos) { where tiene_impuestos: tiene_impuestos }
	
end
