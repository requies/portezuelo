class Asistencia < ActiveRecord::Base
	self.table_name = 'asistencia'
	self.per_page = 10
	belongs_to :user
	belongs_to :negocio

	include Filterable
	include Importable
	include Exportable
		
	# Validations
	# validates :user_id, <validations>
	# validates :negocio_id, <validations>
	# validates :entrada, <validations>
	# validates :salida, <validations>
	
	# Scopes (used for search form)
	scope :user_id, -> (user_id) { where user_id: user_id }
	scope :negocio_id, -> (negocio_id) { where negocio_id: negocio_id }
	scope :entrada, -> (entrada) { where entrada: entrada }
	scope :salida, -> (salida) { where salida: salida }
	
end
