class FamiliaProducto < ActiveRecord::Base
	self.table_name = 'familia_producto'
	self.per_page = 10
	has_many :productos, :class_name => 'Producto', :primary_key => 'id', :foreign_key => 'familia'

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	validates :nombre, presence: { :message => "Es necesario proporcionar un nombre de mínimo 4 caracteres" }, length: { minimum: 4, maximum: 128 }
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
	scope :nombre, -> (nombre) { where nombre: nombre }
	
end
