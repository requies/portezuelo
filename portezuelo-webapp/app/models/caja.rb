class Caja < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'caja'
	self.per_page = 10
	has_many :ventas, :class_name => 'Venta'
	belongs_to :user, :class_name => 'User'
	belongs_to :negocio, :class_name => 'Negocio'


		
	# Validations
	# validates :negocio_id, <validations>
	# validates :maquina_id, <validations>
	# validates :user_id, <validations>
	# validates :fecha_inicio, <validations>
	# validates :monto_inicio, <validations>
	# validates :fecha_termino, <validations>
	# validates :monto_termino, <validations>
	# validates :perdida, <validations>
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
	scope :negocio_id, -> (negocio_id) { where negocio_id: negocio_id }
	scope :maquina_id, -> (maquina_id) { where maquina_id: maquina_id }
	scope :user_id, -> (user_id) { where user_id: user_id }
	scope :fecha_inicio, -> (fecha_inicio) { where fecha_inicio: fecha_inicio }
	scope :monto_inicio, -> (monto_inicio) { where monto_inicio: monto_inicio }
	scope :fecha_termino, -> (fecha_termino) { where fecha_termino: fecha_termino }
	scope :monto_termino, -> (monto_termino) { where monto_termino: monto_termino }
	scope :perdida, -> (perdida) { where perdida: perdida }
	
end
