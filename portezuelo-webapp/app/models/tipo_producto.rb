class TipoProducto < ActiveRecord::Base
	self.table_name = 'tipo_producto'
	self.per_page = 10
	has_many :productos, :foreign_key => 'id_tipo_producto'

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	validates :nombre, presence: { :message => "Es necesario proporcionar un nombre de mínimo 4 caracteres" }, length: { minimum: 4, maximum: 128, message: "El nombre debe tener como mínimo 4 caracteres y como máximo 128" }
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
	scope :nombre, -> (nombre) { where nombre: nombre }
	
end
