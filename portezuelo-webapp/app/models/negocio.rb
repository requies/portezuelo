require 'validators/rut_validator'

class Negocio < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'negocio'
	self.per_page = 10
	has_many :asistencias, :class_name => 'Asistencia'
	has_many :pedidos
	has_many :ventas, :class_name => 'Venta'
	has_many :alerta_cambio_precios, :class_name => 'AlertaCambioPrecio'
	has_many :stocks
	
	# Validations
	validates :rut, rut: true
	# validates :razon_social, <validations>
	validates :nombre, presence: { :message => "Es necesario proporcionar un nombre para el negocio" }, uniqueness: { :message => "Ya existe un negocio con este nombre." }
	validates :fono, numericality: { :only_integer => true, :message => "El fono debe ser un conjunto de números." }
	validates :fax, numericality: { :only_integer => true, :message => "El fax debe ser un conjunto de números." }
	validates :direccion, presence: { :message => "Es necesario proporcionar una dirección." }, length: { :maximum => 255, :message => "La dirección no puede superar los 255 caracteres." }
	validates :comuna, presence: { :message => "Es necesario proporcionar una comuna." }, length: { :maximum => 64, :message => "La comuna no puede superar los 64 caracteres." }
	validates :ciudad, presence: { :message => "Es necesario proporcionar una ciudad." }, length: { :maximum => 64, :message => "La ciudad no puede superar los 64 caracteres." }
	# validates :giro, <validations>
	# validates :seleccionado, <validations>
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
	scope :rut, -> (rut) { where rut: rut }
	scope :razon_social, -> (razon_social) { where razon_social: razon_social }
	scope :nombre, -> (nombre) { where nombre: nombre }
	scope :fono, -> (fono) { where fono: fono }
	scope :fax, -> (fax) { where fax: fax }
	scope :direccion, -> (direccion) { where direccion: direccion }
	scope :comuna, -> (comuna) { where comuna: comuna }
	scope :ciudad, -> (ciudad) { where ciudad: ciudad }
	scope :giro, -> (giro) { where giro: giro }
	scope :seleccionado, -> (seleccionado) { where seleccionado: seleccionado }

	def initialize(attributes={})
		attr_with_defaults = { :seleccionado => false }.merge(attributes)
		super(attr_with_defaults)
	end
	
end
