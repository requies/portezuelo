class Impuesto < ActiveRecord::Base
	self.table_name = 'impuesto'
	self.per_page = 10
	has_many :productos, :foreign_key => 'otros_impuestos'

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	validates :descripcion, presence: { :message => "Es necesario proporcionar una descripcion de mínimo 4 caracteres" }, length: { minimum: 4, maximum: 128 }
	validates :monto, presence: { :message => "Es necesario proporcionar un monto porcentual de entre 0.0 y 100.0"}, numericality: { only_float: true , greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
	# validates :removable, <validations>
	# validates :visible, <validations>
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
	scope :descripcion, -> (descripcion) { where descripcion: descripcion }
	scope :monto, -> (monto) { where monto: monto }
	scope :removable, -> (removable) { where removable: removable }
	scope :visible, -> (visible) { where visible: visible }
	
end
