class TipoIngreso < ActiveRecord::Base
	self.table_name = 'tipo_ingreso'
	self.per_page = 10

	include Filterable
	include Importable
	include Exportable

		
	# Validations
		# validates :descripcion, <validations>
		# validates :removable, <validations>
	
	# Scopes (used for search form)
	#   To search by full text use { where("attribute like ?", "%#{attribute}%") }
	#   To search by string use { where attribute: attribute }
		scope :descripcion, -> (descripcion) { where descripcion: descripcion }
		scope :removable, -> (removable) { where removable: removable }

end
