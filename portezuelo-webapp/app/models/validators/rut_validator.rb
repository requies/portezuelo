class RutValidator < ActiveModel::Validator

	def validate(record)
		unless valid_rut(record.rut)
			puts "Escribiendo errores"
			record.errors[:rut] << "Es necesario proporcionar un rut válido"
		end
	end

	private

	def valid_rut(rut)
		return false if rut.size < 2

			# Prepare
			rut = rut.delete '.,-'
			rut = rut.downcase

			# Parse
			dv = rut[rut.size-1]
			rut = rut[0...rut.size-1]

			# Validate
			# puts "\n\n"
			# puts "Dígito verificador resultante #{rut}: #{get_dv(rut)} (#{dv})"
			if get_dv(rut).to_s == dv.to_s
				# puts "Rut válido"
				return true
			end
			return false
		end

		def get_dv(rut)
			dvr='0'
			suma=0
			mul=2
			rut.reverse.split("").each do |c|
				suma=suma+c.to_i*mul
				if mul==7
					mul=2
				else
					mul+=1
				end
			end
			res=suma%11
			if res==1
				return 'k'
			elsif res==0
				return '0'
			else
				return 11-res
			end
		end

	end
