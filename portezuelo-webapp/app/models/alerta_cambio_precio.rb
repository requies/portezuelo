class AlertaCambioPrecio < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'alerta_cambio_precio'
	self.per_page = 10
	belongs_to :negocio
	belongs_to :producto
		
	# Validations
	# validates :leido, <validations>
	# validates :negocio_id, <validations>
	# validates :barcode, <validations>
	# validates :descripcion, <validations>
	# validates :precio_antiguo, <validations>
	# validates :precio_nuevo, <validations>
	# validates :fecha, <validations>
	
	# Scopes (used for search form)
	scope :leido, -> (leido) { where leido: leido }
	scope :negocio_id, -> (negocio_id) { where negocio_id: negocio_id }
	scope :barcode, -> (barcode) { where barcode: barcode }
	scope :descripcion, -> (descripcion) { where descripcion: descripcion }
	scope :precio_antiguo, -> (precio_antiguo) { where precio_antiguo: precio_antiguo }
	scope :precio_nuevo, -> (precio_nuevo) { where precio_nuevo: precio_nuevo }
	scope :fecha, -> (fecha) { where fecha: fecha }
	
end
