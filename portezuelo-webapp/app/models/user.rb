require 'bcrypt'
require 'digest'
require 'validators/rut_validator'

class User < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable
	include BCrypt
	
	self.table_name = 'users'
	self.per_page = 10
	has_many :inventarios
	has_many :asistencias, :class_name => 'Asistencia', :order => 'entrada desc'
	has_many :pedidos
	has_many :ventas, :class_name => 'Venta'
	before_save :encrypt_password
	before_create :default_values
		
	# Validations
	# validates :rut, <validations>
	validates :rut, rut: true, uniqueness: { :message => "Ya existe un usuario con este RUT" }
	validates :passwd, presence: { :message => "Es necesario proporcionar una contraseña de mínimo 4 caracteres" }, length: { minimum: 4, maximum: 128 }, on: :create
	validates :passwd, length: { minimum: 4, maximum: 128 }, on: :update, allow_blank: true
	validates_confirmation_of :passwd, message: "Las contraseñas deben ser iguales"
	validates :nombre, presence: { :message => "Es necesario registrar un nombre" }
	validates :apell_p, presence: { :message => "Es necesario registrar un apellido paterno" }
	validates :apell_m, presence: { :message => "Es necesario registrar un apellido materno" }
	# validates :fecha_ingreso, <validations>
	# validates :level, <validations>
	# validates :activo, <validations>
	
	# Scopes (used for search form)
	scope :rut, -> (rut) { where rut: rut }
	scope :passwd, -> (passwd) { where passwd: passwd }
	scope :nombre, -> (nombre) { where nombre: nombre }
	scope :apell_p, -> (apell_p) { where apell_p: apell_p }
	scope :apell_m, -> (apell_m) { where apell_m: apell_m }
	scope :fecha_ingreso, -> (fecha_ingreso) { where fecha_ingreso: fecha_ingreso }
	scope :level, -> (level) { where level: level }
	scope :activo, -> (activo) { where activo: activo }

	
	# Helpers ########################################################################
	def full_name
		self.nombre + " " + self.apell_p + " " + self.apell_m
	end

	def self.authenticate(rut, password)
		rut = rut.delete ',.-'
		user = find_by_rut(rut)
		puts "\n\n\n***********************"
		puts "params #{rut}, #{password}"
		puts "user #{user.to_s}"
		if user && user.passwd == Digest::MD5.hexdigest(password)
			user
		else
			nil
			puts "Permiso denegado"
			puts "db.pass: #{user.passwd}"
			puts "post.pass: #{Digest::MD5.hexdigest(password)}"
		end
	end

	def encrypt_password
		if passwd.present?
			self.passwd = Digest::MD5.hexdigest(passwd)
		end
	end

	# Getter y Setters ################################################################
	def rut=(val)
		self[:rut] = val.delete ',.-'
	end

	def default_values
		if(self.fecha_ingreso == nil)
			self.fecha_ingreso = Time.new
		end
	end

end
