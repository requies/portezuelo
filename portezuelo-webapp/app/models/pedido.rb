class Pedido < ActiveRecord::Base
	self.table_name = 'pedido'
	self.per_page = 10
	belongs_to :user
	belongs_to :negocio
	has_many :detalle, :class_name => 'PedidoDetalle'

	include Filterable
	include Importable
	include Exportable
		
	# Validations
	# validates :user_id, <validations>
	# validates :negocio_id, <validations>
	# validates :fecha, <validations>
	# validates :descripcion, <validations>
	# validates :estado, <validations>
	
	# Scopes (used for search form)
	scope :user, -> (user) { where("nombre ilike ?", "%#{user}%") }
	scope :negocio, -> (negocio) { where("nommbre ilike ?", "%#{negocio}%") }
	
	# Estados
	# public static final short ESTADO_CREADO = 0;
	# public static final short ESTADO_LEIDO = 1;
	# public static final short ESTADO_ENVIADO = 2;
	# public static final short ESTADO_RECIBIDO = 3;
	# public static final short ESTADO_CANCELADO = 4;
	def get_estado
		case estado
		when 0
			'Creado'
		when 1
			'Leído'
		when 2
			'Enviado'
		when 3
			'Recibido'
		when 4
			'Cancelado'
		end
	end

end
