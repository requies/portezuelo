class Producto < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'producto'
	self.per_page = 10
	belongs_to :tipo_producto, :foreign_key => 'id_tipo_producto'
	belongs_to :familia_producto, :class_name => 'FamiliaProducto', :foreign_key => 'familia'
	belongs_to :impuesto, :foreign_key => 'otros_impuestos'
	has_many :inventario_detalle, :class_name => 'InventarioDetalle', :foreign_key => 'barcode'
	has_many :pedido_detalle
	has_many :venta_detalle, :class_name => 'VentaDetalle', :foreign_key => 'barcode'
	has_many :alerta_cambio_precios, :class_name => 'AlertaCambioPrecio'
	has_many :stocks, :class_name => 'Stock', :foreign_key => 'barcode'
	
	# Validations
	validates :barcode, presence: { :message => "Es necesario proporcionar un código de barras" }, uniqueness: { :message => "Este código de barras ya esta registrado" }, numericality: { :only_integer => true, :message => "El código de barras debe estar formado solo por números." }
	validates :codigo_corto, presence: { :message => "Es necesario proporcionar un código corto" }, length: { maximum: 16, :message => "El código corto debe ser como máximo de 16 caracteres" }, uniqueness: { :message => "El código corto ya esta registrado." }
	validates :marca, presence: { :message => "Es necesario proporcionar una marca" }, length: { maximum: 35, :message => "La marca debe ser de como máximo 35 caracteres" } 
	validates :descripcion, presence: { :message => "Es necesario proporcionar una descripción" }, length: { maximum: 50, :message => "La descripción debe ser de como máximo 50 caracteres" }
	validates :contenido, numericality: { only_float: true , greater_than_or_equal_to: 0, message: "El contenido debe ser una cantidad positiva" }
	validates :unidad, length: { maximum: 10, :message => "La unidad debe ser de como máximo 10 caracteres" }
	validates :stock, numericality: { only_float: true , greater_than_or_equal_to: -1, message: "El stock debe ser una cantidad mayor a -1" }
	validates :precio, numericality: { only_float: true , greater_than_or_equal_to: 0, message: "El precio debe ser una cantidad positiva" }
	# validates :precio_neto, numericality: { only_float: true , greater_than_or_equal_to: 0, message: "El precio neto debe ser una cantidad positiva" }
	# validates :costo_promedio, numericality: { only_float: true , greater_than_or_equal_to: 0, message: "El costo promedio debe ser una cantidad positiva" }
	# validates :fifo, <validations>
	# validates :vendidos, <validations>
	# validates :aplica_iva, <validations>
	validates :otros_impuestos, presence: { :message => "Es necesario seleccionar otro impuesto" }
	validates :familia, presence: { :message => "Es necesario proporcionar una familia" }
	validates :id_tipo_producto, presence: { :message => "Es necesario proporcionar un tipo de producto" }
	# validates :es_perecible, <validations>
	# validates :dias_stock, <validations>
	# validates :margen_promedio, <validations>
	# validates :es_venta_fraccionada, <validations>
	# validates :stock_pro, <validations>
	# validates :tasa_canje, <validations>
	# validates :precio_mayor, <validations>
	# validates :cantidad_mayor, <validations>
	# validates :es_mayorista, <validations>
	# validates :estado, <validations>
	# validates :precio_editable, <validations>
	
	# Scopes (used for search form)
	scope :barcode, -> (barcode) { where("barcode = ?", "#{barcode.to_i}") }
	scope :codigo_corto, -> (codigo_corto) { where("codigo_corto = ?", "%#{codigo_corto}%") }
	scope :marca, -> (marca) { where("marca ilike ?", "%#{marca}%") }
	scope :familia, -> (familia) { where("nombre ilike ?", "%#{familia}%").joins(:familia_producto) }
	scope :id_tipo_producto, -> (id_tipo_producto) { where("nombre ilike ?", "%#{id_tipo_producto}%").joins(:tipo_producto) }

	before_update :precio_changed

	def initialize(attributes={})
		attr_with_defaults = { 
			:precio_neto => 0,
			:costo_promedio => 0,
			:vendidos => 0,
			:es_perecible => false,
			:stock_pro => 0,
			:aplica_iva => true,
			:stock => 0
		}.merge(attributes)
		super(attr_with_defaults)
	end

	def precio_changed
		if self.changed.include?('precio')
			Negocio.all.each do |negocio|
				AlertaCambioPrecio.new(
					:negocio_id => negocio.id,
					:barcode => self.barcode,
					:leido => false,
					:descripcion => "Se ha actualizado el precio del producto '#{self.descripcion}' quedando a un valor de $#{self.changes["precio"][1]} (antes valía $#{self.changes["precio"][0]})",
					:precio_antiguo => self.changes["precio"][0],
					:precio_nuevo => self.changes["precio"][1],
					:fecha => Time.new
				).save
			end
		end
	end

	def get_last_stocks
		months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
		stats = []
		labels = []
		Negocio.all.each do |n|
			query = ActiveRecord::Base.connection.execute("
				SELECT 
				EXTRACT(YEAR FROM stocks.fecha) AS anio, 
				EXTRACT(MONTH FROM stocks.fecha) AS mes, 
				AVG(stocks.cantidad) AS cantidad 
				FROM producto 
				INNER JOIN stocks ON (stocks.barcode = producto.barcode) 
				INNER JOIN negocio ON (stocks.negocio_id = negocio.id) 
				WHERE negocio.id = #{n.id} 
				AND producto.barcode = #{self.barcode} 
				AND stocks.fecha >= CURRENT_DATE - interval '6 months' 
				AND stocks.fecha <= CURRENT_DATE
				GROUP BY mes, anio
				ORDER BY anio, mes
				".tr("\n", " "))
			data = []
			
			query.each do |avg|
				data.push(avg["cantidad"].to_f)
				labels.push(months[avg["mes"].to_i-1] + ", " + avg["anio"]) if labels.count != 6
			end

			stats.push({
				:name => "#{n.nombre}",
				:data => data
			})
		end
		labels = [] if labels.count != 6
		return { :labels => labels, :stats => stats }
	end

	def get_last_sells
		months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
		stats = []
		labels = []
		Negocio.all.each do |n|
			query = ActiveRecord::Base.connection.execute("
				SELECT 
				EXTRACT(YEAR FROM venta.fecha) AS anio, 
				EXTRACT(MONTH FROM venta.fecha) AS mes, 
				SUM(venta_detalle.cantidad) AS cantidad 
				FROM venta 
				INNER JOIN venta_detalle ON (venta_detalle.venta_id = venta.id) 
				INNER JOIN producto ON (venta_detalle.barcode = producto.barcode) 
				INNER JOIN negocio ON (venta.negocio_id = negocio.id) 
				WHERE producto.barcode = #{self.barcode} 
				AND negocio.id = #{n.id} 
				AND venta.fecha >= CURRENT_DATE - INTERVAL '6 months' 
				AND venta.fecha <= CURRENT_DATE 
				GROUP BY mes, anio 
				ORDER BY anio, mes
				".tr("\n", " "))
			data = []

			query.each do |sum|
				data.push(sum["cantidad"].to_f)
				labels.push(months[sum["mes"].to_i-1] + ", " + sum["anio"]) if labels.count != 6
			end

			stats.push({
				:name => "#{n.nombre}",
				:data => data
			})
		end
		labels = [] if labels.count != 6
		return { :labels => labels, :stats => stats }
	end

end
