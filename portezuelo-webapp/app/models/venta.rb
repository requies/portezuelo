class Venta < ActiveRecord::Base
	include Filterable
	include Importable
	include Exportable

	self.table_name = 'venta'
	self.per_page = 10
	belongs_to :negocio
	belongs_to :user
	belongs_to :caja, :foreign_key => 'caja_id'
	has_many :detalle, :class_name => 'VentaDetalle'
		
	# Validations
	# validates :id, <validations>
	# validates :negocio_id, <validations>
	# validates :user_id, <validations>
	# validates :integer, <validations>
	# validates :nro_venta, <validations>
	# validates :caja_id, <validations>
	# validates :caj_negocio_id, <validations>
	# validates :monto, <validations>
	# validates :fecha, <validations>
	# validates :maquina_id, <validations>
	# validates :tipo_documento, <validations>
	# validates :tipo_venta, <validations>
	# validates :descuento, <validations>
	# validates :id_documento, <validations>
	# validates :cancelada, <validations>
	
	# Scopes (used for search form)
	scope :negocio, -> (negocio) { where("negocio.nombre ilike ?", "%#{negocio}%").joins(:negocio) }
	scope :user, -> (user) { where("users.nombre ilike ? or users.rut ilike ?", "%#{user}%", "%#{user}%").joins(:user) }
	
	def self.get_year_sells
		stats = ActiveRecord::Base.connection.execute("
			SELECT 
			EXTRACT(YEAR from venta.fecha) as anio, 
			EXTRACT(MONTH from venta.fecha) as mes, 
			sum(venta.monto) 
			FROM venta 
			WHERE fecha >= fecha - interval '12 months' 
			GROUP BY mes, anio
			ORDER BY anio desc, mes desc
			".tr("\n", " "))
		data = []
		categories = []
		months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
		j = 0
		last_m = 0
		last_y = 0
		11.downto(0) do |i|
			if j < stats.count
				data[i] = stats[j]["sum"].to_i
				categories[i] = months[stats[j]["mes"].to_i-1] + ", " + stats[j]["anio"]
				last_m = stats[j]["mes"].to_i
				last_y = stats[j]["anio"].to_i
			else
				last_y = (last_y-1) if last_m == 1
				last_m = (last_m-1)%12
				last_m = 12 if last_m == 0
				categories[i] = months[last_m-1] + ", " + last_y.to_s
			end
			j += 1
		end
		return { :data => data, :categories => categories }
	end
end
