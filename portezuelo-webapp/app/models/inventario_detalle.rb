class InventarioDetalle < ActiveRecord::Base
	self.table_name = 'inventario_detalle'
	self.per_page = 10
	belongs_to :inventario
	belongs_to :producto, :foreign_key => 'barcode'

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	# validates :inventario_id, <validations>
	# validates :barcode, <validations>
	# validates :cantidad, <validations>
	# validates :cantidad_esperada, <validations>
	
	# Scopes (used for search form)
	scope :marca, -> (marca) { where("marca ilike ?", "%#{marca}%").joins(:producto) }
	scope :familia, -> (familia) { where("familia_producto.nombre ilike ? ", "%#{familia}%").joins(:producto => :familia_producto) }
	scope :tipo, -> (tipo) { where("nombre ilike ?", "%#{tipo}%").joins(:producto => :tipo_producto) }
	
end
