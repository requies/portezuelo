class PedidoDetalle < ActiveRecord::Base
	self.table_name = 'pedido_detalle'
	self.per_page = 10
	belongs_to :pedido
	belongs_to :producto, :foreign_key => 'barcode'

	include Filterable
	include Importable
	include Exportable

		
	# Validations
	# validates :barcode, <validations>
	# validates :pedido_id, <validations>
	# validates :cantidad, <validations>
	# validates :costo, <validations>
	
	# Scopes (used for search form)
	scope :barcode, -> (barcode) { where barcode: barcode }
	scope :pedido_id, -> (pedido_id) { where pedido_id: pedido_id }
	scope :cantidad, -> (cantidad) { where cantidad: cantidad }
	scope :costo, -> (costo) { where costo: costo }
	
end
