class TipoEgresosController < ApplicationController
  before_action :set_tipo_egreso, only: [:show, :edit, :update, :destroy]

  # GET /tipo_egresos
  def index
    @tipo_egresos = TipoEgreso.paginate :page => params[:page]
  end

  # GET /tipo_egresos/1
  def show
  end

  # GET /tipo_egresos/new
  def new
    @tipo_egreso = TipoEgreso.new
  end

  # GET /tipo_egresos/1/edit
  def edit
  end

  # POST /tipo_egresos
  def create
    @tipo_egreso = TipoEgreso.new(tipo_egreso_params)

    if @tipo_egreso.save
      redirect_to @tipo_egreso, notice: 'Tipo egreso was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tipo_egresos/1
  def update
    if @tipo_egreso.update(tipo_egreso_params)
      redirect_to @tipo_egreso, notice: 'Tipo egreso was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tipo_egresos/1
  def destroy
    @tipo_egreso.destroy
    redirect_to tipo_egresos_url, notice: 'Tipo egreso was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_egreso
      @tipo_egreso = TipoEgreso.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_egreso_params
      params.require(:tipo_egreso).permit(:descripcion, :removable)
    end
end
