class PedidosController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, :only => [:update]

  # GET /pedidos
  def index
    @pedidos = Pedido.all.order('fecha desc').paginate :page => params[:page]
  end

  # GET /pedidos/1
  def show
  end

  # GET /pedidos/new
  def new
    @pedido = Pedido.new
  end

  # GET /pedidos/1/edit
  def edit
  end

  # POST /pedidos
  def create
    @pedido = Pedido.new(pedido_params)

    if @pedido.save
      redirect_to @pedido, notice: 'Pedido was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pedidos/1
  def update
    if @pedido.update(pedido_params)
      if !params[:ajax]
        redirect_to @pedido, notice: 'Pedido was successfully updated.'
      else
        render :text => "OK"
      end
    else
      render :edit
    end
  end

  # DELETE /pedidos/1
  def destroy
    @pedido.destroy
    redirect_to pedidos_url, notice: 'Pedido was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedido
      @pedido = Pedido.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pedido_params
      params.require(:pedido).permit(:user_id, :negocio_id, :fecha, :descripcion, :estado)
    end
end
