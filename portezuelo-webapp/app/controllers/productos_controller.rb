class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]
  
  # GET /productos
  def index
    @productos = Producto.filter(
      params.slice(:barcode, :codigo_corto, :marca, :familia, :id_tipo_producto)
    ).paginate :page => params[:page]
  end

  # GET /productos/1
  def show
  end

  # GET /productos/new
  def new
    @producto = Producto.new
  end

  # GET /productos/1/edit
  def edit
  end

  # POST /productos
  def create
    @producto = Producto.new(producto_params)

    if @producto.save
      redirect_to @producto, notice: 'El producto ha sido creado de forma exitosa.'
    else
      render :new
    end
  end

  # PATCH/PUT /productos/1
  def update
    if @producto.update(producto_params)
      redirect_to @producto, notice: 'El producto ha sido actualizado de forma exitosa.'
    else
      render :edit
    end
  end

  # DELETE /productos/1
  def destroy
    @producto.destroy
    redirect_to productos_url, notice: 'El producto ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producto_params
      params.require(:producto).permit(:barcode, :codigo_corto, :marca, :descripcion, :contenido, :unidad, :stock, :precio, :precio_neto, :costo_promedio, :fifo, :vendidos, :aplica_iva, :otros_impuestos, :familia, :id_tipo_producto, :es_perecible, :dias_stock, :margen_promedio, :es_venta_fraccionada, :stock_pro, :tasa_canje, :precio_mayor, :cantidad_mayor, :es_mayorista, :estado, :precio_editable)
    end
end
