class NegociosController < ApplicationController
  before_action :set_negocio, only: [:show, :edit, :update, :destroy]
  skip_before_filter :check_session, :only => [:index, :show, :new, :edit, :create, :update, :destroy]

  # GET /negocios
  def index
    @negocios = Negocio.paginate :page => params[:page]
  end

  # GET /negocios/1
  def show
  end

  # GET /negocios/new
  def new
    @negocio = Negocio.new
  end

  # GET /negocios/1/edit
  def edit
  end

  # POST /negocios
  def create
    @negocio = Negocio.new(negocio_params)

    if @negocio.save
      redirect_to @negocio, notice: 'Negocio was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /negocios/1
  def update
    if @negocio.update(negocio_params)
      redirect_to @negocio, notice: 'Negocio was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /negocios/1
  def destroy
    @negocio.destroy
    redirect_to negocios_url, notice: 'Negocio was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_negocio
      @negocio = Negocio.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def negocio_params
      params.require(:negocio).permit(:rut, :razon_social, :nombre, :fono, :fax, :direccion, :comuna, :ciudad, :giro, :seleccionado)
    end
end
