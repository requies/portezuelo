class VentaController < ApplicationController
  before_action :set_ventum, only: [:show, :edit, :update, :destroy]

  # GET /venta
  def index
    @venta = Venta.filter(
      params.slice(:negocio, :user)
    ).order('fecha desc').paginate :page => params[:page]
  end

  # GET /venta/1
  def show
  end

  # GET /venta/new
  def new
    @ventum = Venta.new
  end

  # GET /venta/1/edit
  def edit
  end

  # POST /venta
  def create
    @ventum = Venta.new(ventum_params)

    if @ventum.save
      redirect_to @ventum, notice: 'Venta was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /venta/1
  def update
    if @ventum.update(ventum_params)
      redirect_to @ventum, notice: 'Venta was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /venta/1
  def destroy
    @ventum.destroy
    redirect_to venta_index_url, notice: 'Venta was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ventum
      @ventum = Venta.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ventum_params
      params.require(:ventum).permit(:id, :negocio_id, :user_id, :integer, :nro_venta, :caja_id, :caj_negocio_id, :monto, :fecha, :maquina_id, :tipo_documento, :tipo_venta, :descuento, :id_documento, :cancelada)
    end
end
