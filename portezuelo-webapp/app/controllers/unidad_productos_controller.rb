class UnidadProductosController < ApplicationController
  before_action :set_unidad_producto, only: [:show, :edit, :update, :destroy]

  # GET /unidad_productos
  def index
    @unidad_productos = UnidadProducto.paginate :page => params[:page]
  end

  # GET /unidad_productos/1
  def show
  end

  # GET /unidad_productos/new
  def new
    @unidad_producto = UnidadProducto.new
  end

  # GET /unidad_productos/1/edit
  def edit
  end

  # POST /unidad_productos
  def create
    @unidad_producto = UnidadProducto.new(unidad_producto_params)

    if @unidad_producto.save
      redirect_to @unidad_producto, notice: 'La unidad se ha creado de forma existosa.'
    else
      render :new
    end
  end

  # PATCH/PUT /unidad_productos/1
  def update
    if @unidad_producto.update(unidad_producto_params)
      redirect_to @unidad_producto, notice: 'La unidad se ha actualizado de forma existosa.'
    else
      render :edit
    end
  end

  # DELETE /unidad_productos/1
  def destroy
    @unidad_producto.destroy
    redirect_to unidad_productos_url, notice: 'La unidad se ha eliminado de forma existosa.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unidad_producto
      @unidad_producto = UnidadProducto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def unidad_producto_params
      params.require(:unidad_producto).permit(:nombre)
    end
end
