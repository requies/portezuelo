class InventariosController < ApplicationController
  before_action :set_inventario, only: [:show, :edit, :update, :destroy]

  # GET /inventarios
  def index
    @inventarios = Inventario.all.order('fecha desc').filter(
      params.slice(:usuario, :negocio)
    ).paginate :page => params[:page]
  end

  # GET /inventarios/1
  def show
    @detalles = InventarioDetalle.where("inventario_id = ?", "#{@inventario.id}").filter(
      params.slice(:marca, :familia, :tipo)
    )
  end

  # GET /inventarios/new
  def new
    @inventario = Inventario.new
  end

  # GET /inventarios/1/edit
  def edit
  end

  # POST /inventarios
  def create
    @inventario = Inventario.new(inventario_params)

    if @inventario.save
      redirect_to @inventario, notice: 'Inventario was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /inventarios/1
  def update
    if @inventario.update(inventario_params)
      redirect_to @inventario, notice: 'Inventario was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /inventarios/1
  def destroy
    @inventario.destroy
    redirect_to inventarios_url, notice: 'Inventario was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inventario
      @inventario = Inventario.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def inventario_params
      params.require(:inventario).permit(:negocio_id, :user_id, :fecha)
    end
end
