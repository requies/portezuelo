class FamiliaProductosController < ApplicationController
  before_action :set_familia_producto, only: [:show, :edit, :update, :destroy]

  # GET /familia_productos
  def index
    @familia_productos = FamiliaProducto.paginate :page => params[:page]
  end

  # GET /familia_productos/1
  def show
  end

  # GET /familia_productos/new
  def new
    @familia_producto = FamiliaProducto.new
  end

  # GET /familia_productos/1/edit
  def edit
  end

  # POST /familia_productos
  def create
    @familia_producto = FamiliaProducto.new(familia_producto_params)

    if @familia_producto.save
      redirect_to @familia_producto, notice: 'Familia producto was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /familia_productos/1
  def update
    if @familia_producto.update(familia_producto_params)
      redirect_to @familia_producto, notice: 'Familia producto was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /familia_productos/1
  def destroy
    @familia_producto.destroy
    redirect_to familia_productos_url, notice: 'Familia producto was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_familia_producto
      @familia_producto = FamiliaProducto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def familia_producto_params
      params.require(:familia_producto).permit(:nombre)
    end
end
