class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.paginate :page => params[:page]
  end

  # GET /users/1
  def show
    @asistencias = @user.asistencias.paginate :page => params[:page]
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: 'User was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if params[:user][:passwd].blank? && params[:user][:passwd_confirmation].blank?
      params[:user].delete(:passwd)
      params[:user].delete(:passwd_confirmation)
    end
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:rut, :dv, :username, :passwd, :passwd_confirmation, :nombre, :apell_p, :apell_m, :fecha_ingreso, :tipo_usuario, :activo)
    end
  end
