class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Get the current user helper
  helper_method :current_user

  # Login filter
  before_filter :check_session


  def check_session
    reset_session if session[:last_seen] != nil and session[:last_seen] < 5.minutes.ago
    session[:last_seen] = Time.now
    redirect_to root_url if !session[:user_id]
  end  

  private
    # Get the current user
    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

  end
