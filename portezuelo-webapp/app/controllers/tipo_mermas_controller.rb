class TipoMermasController < ApplicationController
  before_action :set_tipo_merma, only: [:show, :edit, :update, :destroy]

  # GET /tipo_mermas
  def index
    @tipo_mermas = TipoMerma.paginate :page => params[:page]
  end

  # GET /tipo_mermas/1
  def show
  end

  # GET /tipo_mermas/new
  def new
    @tipo_merma = TipoMerma.new
  end

  # GET /tipo_mermas/1/edit
  def edit
  end

  # POST /tipo_mermas
  def create
    @tipo_merma = TipoMerma.new(tipo_merma_params)

    if @tipo_merma.save
      redirect_to @tipo_merma, notice: 'Tipo merma was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tipo_mermas/1
  def update
    if @tipo_merma.update(tipo_merma_params)
      redirect_to @tipo_merma, notice: 'Tipo merma was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tipo_mermas/1
  def destroy
    @tipo_merma.destroy
    redirect_to tipo_mermas_url, notice: 'Tipo merma was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_merma
      @tipo_merma = TipoMerma.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_merma_params
      params.require(:tipo_merma).permit(:nombre)
    end
end
