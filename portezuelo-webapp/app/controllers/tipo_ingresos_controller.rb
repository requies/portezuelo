class TipoIngresosController < ApplicationController
  before_action :set_tipo_ingreso, only: [:show, :edit, :update, :destroy]

  # GET /tipo_ingresos
  def index
    @tipo_ingresos = TipoIngreso.paginate :page => params[:page]
  end

  # GET /tipo_ingresos/1
  def show
  end

  # GET /tipo_ingresos/new
  def new
    @tipo_ingreso = TipoIngreso.new
  end

  # GET /tipo_ingresos/1/edit
  def edit
  end

  # POST /tipo_ingresos
  def create
    @tipo_ingreso = TipoIngreso.new(tipo_ingreso_params)

    if @tipo_ingreso.save
      redirect_to @tipo_ingreso, notice: 'Tipo ingreso was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tipo_ingresos/1
  def update
    if @tipo_ingreso.update(tipo_ingreso_params)
      redirect_to @tipo_ingreso, notice: 'Tipo ingreso was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tipo_ingresos/1
  def destroy
    @tipo_ingreso.destroy
    redirect_to tipo_ingresos_url, notice: 'Tipo ingreso was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_ingreso
      @tipo_ingreso = TipoIngreso.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_ingreso_params
      params.require(:tipo_ingreso).permit(:descripcion, :removable)
    end
end
