class SessionsController < ApplicationController
	layout "empty", :only => [:new, :create, :destroy]
	skip_before_filter :check_session, :only => [:create, :new]

	def new
		if current_user
			redirect_to "/index/home"
		end
		reset_session
	end

	def create
		user = User.authenticate(user_params[:rut], user_params[:passwd])
		if user
			session[:user_id] = user.id
			redirect_to "/index/home"
		else
			flash[:type] = "danger"
			flash.now.alert = "Usuario o contraseña no válidos"
			render "new"
		end
	end

	def destroy
		session[:user_id] = nil
		flash[:type] = "success"
		flash[:notice] = "Sesión cerrada"
		redirect_to root_url
	end

	private
		# Only allow a trusted parameter "white list" through.
		def user_params
			params.permit(:rut, :passwd)
		end
end
