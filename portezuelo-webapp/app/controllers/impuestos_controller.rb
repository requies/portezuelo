class ImpuestosController < ApplicationController
  before_action :set_impuesto, only: [:show, :edit, :update, :destroy]

  # GET /impuestos
  def index
    @impuestos = Impuesto.paginate :page => params[:page]
  end

  # GET /impuestos/1
  def show
  end

  # GET /impuestos/new
  def new
    @impuesto = Impuesto.new
  end

  # GET /impuestos/1/edit
  def edit
  end

  # POST /impuestos
  def create
    @impuesto = Impuesto.new(impuesto_params)

    if @impuesto.save
      redirect_to @impuesto, notice: 'Impuesto was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /impuestos/1
  def update
    if @impuesto.update(impuesto_params)
      redirect_to @impuesto, notice: 'Impuesto was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /impuestos/1
  def destroy
    @impuesto.destroy
    redirect_to impuestos_url, notice: 'Impuesto was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_impuesto
      @impuesto = Impuesto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def impuesto_params
      params.require(:impuesto).permit(:descripcion, :monto, :removable, :visible)
    end
end
