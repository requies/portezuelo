/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ConexionRemoteServer;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONConfiguration;
import cl.requies.portezuelo.entities.Negocio;
import java.util.List;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

/**
 *
 * @author victor
 */
public class NegocioRemote {
    private WebResource webResource;
    private Client client;
    private String BASE_URI = "http://irlanda.requies.cl:12000";

    
    
    public NegocioRemote(String url) {
        if (url != null)
            BASE_URI = url;
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        config.getClasses().add(MOXyJsonProvider.class);
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("negocios.json");
        System.out.println("Conectando a: " + webResource.getURI());
    }

    public List<Negocio> getJson() throws UniformInterfaceException {
        WebResource resource = webResource;
        GenericType<List<Negocio>> genericType = new GenericType<List<Negocio>>() {};
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(genericType);
    }

    public void close() {
        client.destroy();
    }
}
