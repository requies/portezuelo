/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.others;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author victor
 */
public class Utils {
    public static final String PERSISTENCE_UNIT = "cl.requies_Portezuelo_PU";
    public static final String NOMBRE_ARCHIVO_CONFIG_DB = "configDatabasePortezuelo.conf";
    private static String DB_URL = null;
    private static String DB_USERNAME = null;
    private static String DB_PASSWORD = null;
    private static String DB_DRIVER = null;
    public static String URL_SERVER = null;
    private static boolean usando_db_default = true;
    
    
    public static String convertToMD5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes("UTF-8"));

            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            password = bigInt.toString(16);
        } catch (Exception e) {
            System.out.println("No se pudo convertir a MD5 la password");
        }
        return password;
    }
    
    public static boolean setConfigInicial(String db_url, String db_username, 
        String db_password, String db_driver, String url_server, boolean saveToFile) {
        Utils.DB_URL = db_url;
        Utils.DB_USERNAME = db_username;
        Utils.DB_PASSWORD = db_password;
        Utils.DB_DRIVER = db_driver;
        Utils.URL_SERVER = url_server;
        if (!saveToFile) {
            return true;
        }
        return saveConfigInicial();
    }
    
    public static boolean saveConfigInicial() {
        Properties prop = new Properties();
        prop.setProperty("DB_URL", Utils.DB_URL);
        prop.setProperty("DB_USERNAME", Utils.DB_USERNAME);
        prop.setProperty("DB_PASSWORD", Utils.DB_PASSWORD);
        prop.setProperty("DB_DRIVER", Utils.DB_DRIVER);
        prop.setProperty("URL_SERVER", Utils.URL_SERVER);
        
        FileWriter outputStream;
        try {
            outputStream = new FileWriter(getNameFileConfig(), false);
            prop.store(outputStream, "Configuraciones de JRequiesPos");
            outputStream.close();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean establecerDBConfig() {
        usando_db_default = !leerConfigDB();
        
        if (usando_db_default) {
            usando_db_default = probarConfigDB(); //Ua config por default en la unidad de persistencia
            return usando_db_default;
        }
        
        return probarConfigDB();
    }
    
    public static boolean probarConfigDB() {
        try {
            EntityManagerFactory emf = getEntityManagerFactory();
            EntityManager em;
            em = emf.createEntityManager();
            em.close();
        }
        catch (Exception ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    private static String getNameFileConfig() {
        String path = System.getProperty("CONFIG_PATH");
        String slash = System.getProperty("file.separator");
        if (path == null) {
            path = System.getProperty("user.home");
            if (path == null) {
                path = "";
            }
            else {
                File carpeta = new File(path.concat(slash).concat("Documents"));
                if (!carpeta.exists()) {
                    carpeta.mkdir();
                }
                path = path.concat(slash).concat("Documents").concat(slash);
            }
        }
        return path.concat(NOMBRE_ARCHIVO_CONFIG_DB);
    }
    
    private static boolean leerConfigDB() {
        Properties prop = new Properties();
        InputStream inputStream;
        try {
            String fileName = getNameFileConfig();
            File arch = new File(fileName);
            if (!arch.exists()) {
                arch.createNewFile();
            }
            inputStream = new FileInputStream(fileName);
            prop.load(inputStream);
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            
            return false;
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        DB_URL = prop.getProperty("DB_URL");
        DB_USERNAME = prop.getProperty("DB_USERNAME");
        DB_PASSWORD = prop.getProperty("DB_PASSWORD");
        DB_DRIVER = prop.getProperty("DB_DRIVER");
        return DB_URL != null && DB_USERNAME != null && DB_PASSWORD != null && DB_DRIVER != null;
    }
    
    public static EntityManagerFactory getEntityManagerFactory() {
        EntityManagerFactory emf;
        
        if (usando_db_default) {
            emf = Persistence.createEntityManagerFactory(Utils.PERSISTENCE_UNIT);
        }
        else {
            Map<String, String> persistenceMap = new HashMap<String, String>();
            persistenceMap.put("javax.persistence.jdbc.url", DB_URL);
            persistenceMap.put("javax.persistence.jdbc.user", DB_USERNAME);
            persistenceMap.put("javax.persistence.jdbc.password", DB_PASSWORD);
            persistenceMap.put("javax.persistence.jdbc.driver", DB_DRIVER);

            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT, persistenceMap);
        }
        return emf;
    }
}
