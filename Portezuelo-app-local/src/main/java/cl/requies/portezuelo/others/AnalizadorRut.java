/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.others;

/**
 * No toma en cuenta el último, por ser digito verificador
 * @author victor
 */
public class AnalizadorRut {
    public int StringToIntFormat(String rutStr) throws NumberFormatException {
        String rutStrSinDv;
        int rutInt;
        char dv;
        rutStr = rutStr.trim().replaceAll("[.]", "").replaceAll("-", "");
        int largo = rutStr.length();
        if (largo >= 2) {
            dv = rutStr.charAt(largo-1);
            rutStrSinDv = rutStr.substring(0, rutStr.length()-1);
            rutInt = Integer.parseInt(rutStrSinDv);
            if (!((dv >= '0' && dv <= '9') || dv == 'k' || dv == 'K')) {
                throw new NumberFormatException("Dígito verificador incorrecto");
            }
        }
        else {
            throw new NumberFormatException("Rut corto");
        }
        return rutInt;
    }
    /**
     * Transforma un rut desde el formato en que se almacena en la base de datos
     * del estilo xxxxxxxxy al formato usado en la GUI, es decir: xx.xxx.xxx-y
     * @return 
     */
    public String DBFormarToString(String dbFormat) {
        StringBuilder strb = new StringBuilder();
        if (dbFormat.isEmpty()) {
            return dbFormat;
        }
        int largo = dbFormat.length();
        if (largo == 1) {
            return dbFormat;
        }
        strb.append(dbFormat.charAt(largo-1)).append('-');
        for (int i = 1; i < largo; i++) {
            strb.append(dbFormat.charAt(largo-1-i));
            if (i % 3 == 0) {
                strb.append('.');
            }
        }
        return strb.reverse().toString();
    }
    
    public String formateaRut(String rutOrig) {
        rutOrig = rutOrig.trim().replaceAll("[.]", "").replaceAll("-", "");
        int rutInt;
        String dvStr = "";
        try {
            rutInt = StringToIntFormat(rutOrig);
            if (!rutOrig.isEmpty()) {
                dvStr = rutOrig.substring(rutOrig.length()-1);
            }
        }
        catch (NumberFormatException nfe) {
            return rutOrig;
        }
        return DBFormarToString(Integer.toString(rutInt).concat(dvStr));
    }
    
    public boolean validarRut(String rut) throws NumberFormatException {  
      
        boolean validacion = false;
        rut =  rut.toUpperCase();  
        rut = rut.replace(".", "");  
        rut = rut.replace("-", "");  
        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));  

        char dv = rut.charAt(rut.length() - 1);  

        int m = 0, s = 1;  
        for (; rutAux != 0; rutAux /= 10) {  
            s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;  
        }  
        if (dv == (char) (s != 0 ? s + 47 : 75)) {  
            validacion = true;  
        }
        return validacion;  
    }
}
