/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Inventario;
import java.util.ArrayList;
import java.util.List;
import cl.requies.portezuelo.entities.Pedido;
import cl.requies.portezuelo.entities.Traspaso;
import cl.requies.portezuelo.entities.AlertaCambioPrecio;
import cl.requies.portezuelo.entities.Asistencia;
import cl.requies.portezuelo.entities.Negocio;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class NegocioJpaController implements Serializable {

    public NegocioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Negocio negocio) {
        if (negocio.getInventarioList() == null) {
            negocio.setInventarioList(new ArrayList<Inventario>());
        }
        if (negocio.getPedidoList() == null) {
            negocio.setPedidoList(new ArrayList<Pedido>());
        }
        if (negocio.getTraspasoList() == null) {
            negocio.setTraspasoList(new ArrayList<Traspaso>());
        }
        if (negocio.getTraspasoList1() == null) {
            negocio.setTraspasoList1(new ArrayList<Traspaso>());
        }
        if (negocio.getAlertaCambioPrecioList() == null) {
            negocio.setAlertaCambioPrecioList(new ArrayList<AlertaCambioPrecio>());
        }
        if (negocio.getAsistenciaList() == null) {
            negocio.setAsistenciaList(new ArrayList<Asistencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Inventario> attachedInventarioList = new ArrayList<Inventario>();
            for (Inventario inventarioListInventarioToAttach : negocio.getInventarioList()) {
                inventarioListInventarioToAttach = em.getReference(inventarioListInventarioToAttach.getClass(), inventarioListInventarioToAttach.getId());
                attachedInventarioList.add(inventarioListInventarioToAttach);
            }
            negocio.setInventarioList(attachedInventarioList);
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : negocio.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            negocio.setPedidoList(attachedPedidoList);
            List<Traspaso> attachedTraspasoList = new ArrayList<Traspaso>();
            for (Traspaso traspasoListTraspasoToAttach : negocio.getTraspasoList()) {
                traspasoListTraspasoToAttach = em.getReference(traspasoListTraspasoToAttach.getClass(), traspasoListTraspasoToAttach.getId());
                attachedTraspasoList.add(traspasoListTraspasoToAttach);
            }
            negocio.setTraspasoList(attachedTraspasoList);
            List<Traspaso> attachedTraspasoList1 = new ArrayList<Traspaso>();
            for (Traspaso traspasoList1TraspasoToAttach : negocio.getTraspasoList1()) {
                traspasoList1TraspasoToAttach = em.getReference(traspasoList1TraspasoToAttach.getClass(), traspasoList1TraspasoToAttach.getId());
                attachedTraspasoList1.add(traspasoList1TraspasoToAttach);
            }
            negocio.setTraspasoList1(attachedTraspasoList1);
            List<AlertaCambioPrecio> attachedAlertaCambioPrecioList = new ArrayList<AlertaCambioPrecio>();
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecioToAttach : negocio.getAlertaCambioPrecioList()) {
                alertaCambioPrecioListAlertaCambioPrecioToAttach = em.getReference(alertaCambioPrecioListAlertaCambioPrecioToAttach.getClass(), alertaCambioPrecioListAlertaCambioPrecioToAttach.getId());
                attachedAlertaCambioPrecioList.add(alertaCambioPrecioListAlertaCambioPrecioToAttach);
            }
            negocio.setAlertaCambioPrecioList(attachedAlertaCambioPrecioList);
            List<Asistencia> attachedAsistenciaList = new ArrayList<Asistencia>();
            for (Asistencia asistenciaListAsistenciaToAttach : negocio.getAsistenciaList()) {
                asistenciaListAsistenciaToAttach = em.getReference(asistenciaListAsistenciaToAttach.getClass(), asistenciaListAsistenciaToAttach.getId());
                attachedAsistenciaList.add(asistenciaListAsistenciaToAttach);
            }
            negocio.setAsistenciaList(attachedAsistenciaList);
            em.persist(negocio);
            for (Inventario inventarioListInventario : negocio.getInventarioList()) {
                Negocio oldNegocioIdOfInventarioListInventario = inventarioListInventario.getNegocioId();
                inventarioListInventario.setNegocioId(negocio);
                inventarioListInventario = em.merge(inventarioListInventario);
                if (oldNegocioIdOfInventarioListInventario != null) {
                    oldNegocioIdOfInventarioListInventario.getInventarioList().remove(inventarioListInventario);
                    oldNegocioIdOfInventarioListInventario = em.merge(oldNegocioIdOfInventarioListInventario);
                }
            }
            for (Pedido pedidoListPedido : negocio.getPedidoList()) {
                Negocio oldNegocioIdOfPedidoListPedido = pedidoListPedido.getNegocioId();
                pedidoListPedido.setNegocioId(negocio);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldNegocioIdOfPedidoListPedido != null) {
                    oldNegocioIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldNegocioIdOfPedidoListPedido = em.merge(oldNegocioIdOfPedidoListPedido);
                }
            }
            for (Traspaso traspasoListTraspaso : negocio.getTraspasoList()) {
                Negocio oldDestinoIdOfTraspasoListTraspaso = traspasoListTraspaso.getDestinoId();
                traspasoListTraspaso.setDestinoId(negocio);
                traspasoListTraspaso = em.merge(traspasoListTraspaso);
                if (oldDestinoIdOfTraspasoListTraspaso != null) {
                    oldDestinoIdOfTraspasoListTraspaso.getTraspasoList().remove(traspasoListTraspaso);
                    oldDestinoIdOfTraspasoListTraspaso = em.merge(oldDestinoIdOfTraspasoListTraspaso);
                }
            }
            for (Traspaso traspasoList1Traspaso : negocio.getTraspasoList1()) {
                Negocio oldOrigenIdOfTraspasoList1Traspaso = traspasoList1Traspaso.getOrigenId();
                traspasoList1Traspaso.setOrigenId(negocio);
                traspasoList1Traspaso = em.merge(traspasoList1Traspaso);
                if (oldOrigenIdOfTraspasoList1Traspaso != null) {
                    oldOrigenIdOfTraspasoList1Traspaso.getTraspasoList1().remove(traspasoList1Traspaso);
                    oldOrigenIdOfTraspasoList1Traspaso = em.merge(oldOrigenIdOfTraspasoList1Traspaso);
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecio : negocio.getAlertaCambioPrecioList()) {
                Negocio oldNegocioIdOfAlertaCambioPrecioListAlertaCambioPrecio = alertaCambioPrecioListAlertaCambioPrecio.getNegocioId();
                alertaCambioPrecioListAlertaCambioPrecio.setNegocioId(negocio);
                alertaCambioPrecioListAlertaCambioPrecio = em.merge(alertaCambioPrecioListAlertaCambioPrecio);
                if (oldNegocioIdOfAlertaCambioPrecioListAlertaCambioPrecio != null) {
                    oldNegocioIdOfAlertaCambioPrecioListAlertaCambioPrecio.getAlertaCambioPrecioList().remove(alertaCambioPrecioListAlertaCambioPrecio);
                    oldNegocioIdOfAlertaCambioPrecioListAlertaCambioPrecio = em.merge(oldNegocioIdOfAlertaCambioPrecioListAlertaCambioPrecio);
                }
            }
            for (Asistencia asistenciaListAsistencia : negocio.getAsistenciaList()) {
                Negocio oldNegocioIdOfAsistenciaListAsistencia = asistenciaListAsistencia.getNegocioId();
                asistenciaListAsistencia.setNegocioId(negocio);
                asistenciaListAsistencia = em.merge(asistenciaListAsistencia);
                if (oldNegocioIdOfAsistenciaListAsistencia != null) {
                    oldNegocioIdOfAsistenciaListAsistencia.getAsistenciaList().remove(asistenciaListAsistencia);
                    oldNegocioIdOfAsistenciaListAsistencia = em.merge(oldNegocioIdOfAsistenciaListAsistencia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Negocio negocio) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Negocio persistentNegocio = em.find(Negocio.class, negocio.getId());
            List<Inventario> inventarioListOld = persistentNegocio.getInventarioList();
            List<Inventario> inventarioListNew = negocio.getInventarioList();
            List<Pedido> pedidoListOld = persistentNegocio.getPedidoList();
            List<Pedido> pedidoListNew = negocio.getPedidoList();
            List<Traspaso> traspasoListOld = persistentNegocio.getTraspasoList();
            List<Traspaso> traspasoListNew = negocio.getTraspasoList();
            List<Traspaso> traspasoList1Old = persistentNegocio.getTraspasoList1();
            List<Traspaso> traspasoList1New = negocio.getTraspasoList1();
            List<AlertaCambioPrecio> alertaCambioPrecioListOld = persistentNegocio.getAlertaCambioPrecioList();
            List<AlertaCambioPrecio> alertaCambioPrecioListNew = negocio.getAlertaCambioPrecioList();
            List<Asistencia> asistenciaListOld = persistentNegocio.getAsistenciaList();
            List<Asistencia> asistenciaListNew = negocio.getAsistenciaList();
            List<String> illegalOrphanMessages = null;
            for (Inventario inventarioListOldInventario : inventarioListOld) {
                if (!inventarioListNew.contains(inventarioListOldInventario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Inventario " + inventarioListOldInventario + " since its negocioId field is not nullable.");
                }
            }
            for (Asistencia asistenciaListOldAsistencia : asistenciaListOld) {
                if (!asistenciaListNew.contains(asistenciaListOldAsistencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Asistencia " + asistenciaListOldAsistencia + " since its negocioId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Inventario> attachedInventarioListNew = new ArrayList<Inventario>();
            for (Inventario inventarioListNewInventarioToAttach : inventarioListNew) {
                inventarioListNewInventarioToAttach = em.getReference(inventarioListNewInventarioToAttach.getClass(), inventarioListNewInventarioToAttach.getId());
                attachedInventarioListNew.add(inventarioListNewInventarioToAttach);
            }
            inventarioListNew = attachedInventarioListNew;
            negocio.setInventarioList(inventarioListNew);
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            negocio.setPedidoList(pedidoListNew);
            List<Traspaso> attachedTraspasoListNew = new ArrayList<Traspaso>();
            for (Traspaso traspasoListNewTraspasoToAttach : traspasoListNew) {
                traspasoListNewTraspasoToAttach = em.getReference(traspasoListNewTraspasoToAttach.getClass(), traspasoListNewTraspasoToAttach.getId());
                attachedTraspasoListNew.add(traspasoListNewTraspasoToAttach);
            }
            traspasoListNew = attachedTraspasoListNew;
            negocio.setTraspasoList(traspasoListNew);
            List<Traspaso> attachedTraspasoList1New = new ArrayList<Traspaso>();
            for (Traspaso traspasoList1NewTraspasoToAttach : traspasoList1New) {
                traspasoList1NewTraspasoToAttach = em.getReference(traspasoList1NewTraspasoToAttach.getClass(), traspasoList1NewTraspasoToAttach.getId());
                attachedTraspasoList1New.add(traspasoList1NewTraspasoToAttach);
            }
            traspasoList1New = attachedTraspasoList1New;
            negocio.setTraspasoList1(traspasoList1New);
            List<AlertaCambioPrecio> attachedAlertaCambioPrecioListNew = new ArrayList<AlertaCambioPrecio>();
            for (AlertaCambioPrecio alertaCambioPrecioListNewAlertaCambioPrecioToAttach : alertaCambioPrecioListNew) {
                alertaCambioPrecioListNewAlertaCambioPrecioToAttach = em.getReference(alertaCambioPrecioListNewAlertaCambioPrecioToAttach.getClass(), alertaCambioPrecioListNewAlertaCambioPrecioToAttach.getId());
                attachedAlertaCambioPrecioListNew.add(alertaCambioPrecioListNewAlertaCambioPrecioToAttach);
            }
            alertaCambioPrecioListNew = attachedAlertaCambioPrecioListNew;
            negocio.setAlertaCambioPrecioList(alertaCambioPrecioListNew);
            List<Asistencia> attachedAsistenciaListNew = new ArrayList<Asistencia>();
            for (Asistencia asistenciaListNewAsistenciaToAttach : asistenciaListNew) {
                asistenciaListNewAsistenciaToAttach = em.getReference(asistenciaListNewAsistenciaToAttach.getClass(), asistenciaListNewAsistenciaToAttach.getId());
                attachedAsistenciaListNew.add(asistenciaListNewAsistenciaToAttach);
            }
            asistenciaListNew = attachedAsistenciaListNew;
            negocio.setAsistenciaList(asistenciaListNew);
            negocio = em.merge(negocio);
            for (Inventario inventarioListNewInventario : inventarioListNew) {
                if (!inventarioListOld.contains(inventarioListNewInventario)) {
                    Negocio oldNegocioIdOfInventarioListNewInventario = inventarioListNewInventario.getNegocioId();
                    inventarioListNewInventario.setNegocioId(negocio);
                    inventarioListNewInventario = em.merge(inventarioListNewInventario);
                    if (oldNegocioIdOfInventarioListNewInventario != null && !oldNegocioIdOfInventarioListNewInventario.equals(negocio)) {
                        oldNegocioIdOfInventarioListNewInventario.getInventarioList().remove(inventarioListNewInventario);
                        oldNegocioIdOfInventarioListNewInventario = em.merge(oldNegocioIdOfInventarioListNewInventario);
                    }
                }
            }
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    pedidoListOldPedido.setNegocioId(null);
                    pedidoListOldPedido = em.merge(pedidoListOldPedido);
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Negocio oldNegocioIdOfPedidoListNewPedido = pedidoListNewPedido.getNegocioId();
                    pedidoListNewPedido.setNegocioId(negocio);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldNegocioIdOfPedidoListNewPedido != null && !oldNegocioIdOfPedidoListNewPedido.equals(negocio)) {
                        oldNegocioIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldNegocioIdOfPedidoListNewPedido = em.merge(oldNegocioIdOfPedidoListNewPedido);
                    }
                }
            }
            for (Traspaso traspasoListOldTraspaso : traspasoListOld) {
                if (!traspasoListNew.contains(traspasoListOldTraspaso)) {
                    traspasoListOldTraspaso.setDestinoId(null);
                    traspasoListOldTraspaso = em.merge(traspasoListOldTraspaso);
                }
            }
            for (Traspaso traspasoListNewTraspaso : traspasoListNew) {
                if (!traspasoListOld.contains(traspasoListNewTraspaso)) {
                    Negocio oldDestinoIdOfTraspasoListNewTraspaso = traspasoListNewTraspaso.getDestinoId();
                    traspasoListNewTraspaso.setDestinoId(negocio);
                    traspasoListNewTraspaso = em.merge(traspasoListNewTraspaso);
                    if (oldDestinoIdOfTraspasoListNewTraspaso != null && !oldDestinoIdOfTraspasoListNewTraspaso.equals(negocio)) {
                        oldDestinoIdOfTraspasoListNewTraspaso.getTraspasoList().remove(traspasoListNewTraspaso);
                        oldDestinoIdOfTraspasoListNewTraspaso = em.merge(oldDestinoIdOfTraspasoListNewTraspaso);
                    }
                }
            }
            for (Traspaso traspasoList1OldTraspaso : traspasoList1Old) {
                if (!traspasoList1New.contains(traspasoList1OldTraspaso)) {
                    traspasoList1OldTraspaso.setOrigenId(null);
                    traspasoList1OldTraspaso = em.merge(traspasoList1OldTraspaso);
                }
            }
            for (Traspaso traspasoList1NewTraspaso : traspasoList1New) {
                if (!traspasoList1Old.contains(traspasoList1NewTraspaso)) {
                    Negocio oldOrigenIdOfTraspasoList1NewTraspaso = traspasoList1NewTraspaso.getOrigenId();
                    traspasoList1NewTraspaso.setOrigenId(negocio);
                    traspasoList1NewTraspaso = em.merge(traspasoList1NewTraspaso);
                    if (oldOrigenIdOfTraspasoList1NewTraspaso != null && !oldOrigenIdOfTraspasoList1NewTraspaso.equals(negocio)) {
                        oldOrigenIdOfTraspasoList1NewTraspaso.getTraspasoList1().remove(traspasoList1NewTraspaso);
                        oldOrigenIdOfTraspasoList1NewTraspaso = em.merge(oldOrigenIdOfTraspasoList1NewTraspaso);
                    }
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListOldAlertaCambioPrecio : alertaCambioPrecioListOld) {
                if (!alertaCambioPrecioListNew.contains(alertaCambioPrecioListOldAlertaCambioPrecio)) {
                    alertaCambioPrecioListOldAlertaCambioPrecio.setNegocioId(null);
                    alertaCambioPrecioListOldAlertaCambioPrecio = em.merge(alertaCambioPrecioListOldAlertaCambioPrecio);
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListNewAlertaCambioPrecio : alertaCambioPrecioListNew) {
                if (!alertaCambioPrecioListOld.contains(alertaCambioPrecioListNewAlertaCambioPrecio)) {
                    Negocio oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio = alertaCambioPrecioListNewAlertaCambioPrecio.getNegocioId();
                    alertaCambioPrecioListNewAlertaCambioPrecio.setNegocioId(negocio);
                    alertaCambioPrecioListNewAlertaCambioPrecio = em.merge(alertaCambioPrecioListNewAlertaCambioPrecio);
                    if (oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio != null && !oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio.equals(negocio)) {
                        oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio.getAlertaCambioPrecioList().remove(alertaCambioPrecioListNewAlertaCambioPrecio);
                        oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio = em.merge(oldNegocioIdOfAlertaCambioPrecioListNewAlertaCambioPrecio);
                    }
                }
            }
            for (Asistencia asistenciaListNewAsistencia : asistenciaListNew) {
                if (!asistenciaListOld.contains(asistenciaListNewAsistencia)) {
                    Negocio oldNegocioIdOfAsistenciaListNewAsistencia = asistenciaListNewAsistencia.getNegocioId();
                    asistenciaListNewAsistencia.setNegocioId(negocio);
                    asistenciaListNewAsistencia = em.merge(asistenciaListNewAsistencia);
                    if (oldNegocioIdOfAsistenciaListNewAsistencia != null && !oldNegocioIdOfAsistenciaListNewAsistencia.equals(negocio)) {
                        oldNegocioIdOfAsistenciaListNewAsistencia.getAsistenciaList().remove(asistenciaListNewAsistencia);
                        oldNegocioIdOfAsistenciaListNewAsistencia = em.merge(oldNegocioIdOfAsistenciaListNewAsistencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = negocio.getId();
                if (findNegocio(id) == null) {
                    throw new NonexistentEntityException("The negocio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Negocio negocio;
            try {
                negocio = em.getReference(Negocio.class, id);
                negocio.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The negocio with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Inventario> inventarioListOrphanCheck = negocio.getInventarioList();
            for (Inventario inventarioListOrphanCheckInventario : inventarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Negocio (" + negocio + ") cannot be destroyed since the Inventario " + inventarioListOrphanCheckInventario + " in its inventarioList field has a non-nullable negocioId field.");
            }
            List<Asistencia> asistenciaListOrphanCheck = negocio.getAsistenciaList();
            for (Asistencia asistenciaListOrphanCheckAsistencia : asistenciaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Negocio (" + negocio + ") cannot be destroyed since the Asistencia " + asistenciaListOrphanCheckAsistencia + " in its asistenciaList field has a non-nullable negocioId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Pedido> pedidoList = negocio.getPedidoList();
            for (Pedido pedidoListPedido : pedidoList) {
                pedidoListPedido.setNegocioId(null);
                pedidoListPedido = em.merge(pedidoListPedido);
            }
            List<Traspaso> traspasoList = negocio.getTraspasoList();
            for (Traspaso traspasoListTraspaso : traspasoList) {
                traspasoListTraspaso.setDestinoId(null);
                traspasoListTraspaso = em.merge(traspasoListTraspaso);
            }
            List<Traspaso> traspasoList1 = negocio.getTraspasoList1();
            for (Traspaso traspasoList1Traspaso : traspasoList1) {
                traspasoList1Traspaso.setOrigenId(null);
                traspasoList1Traspaso = em.merge(traspasoList1Traspaso);
            }
            List<AlertaCambioPrecio> alertaCambioPrecioList = negocio.getAlertaCambioPrecioList();
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecio : alertaCambioPrecioList) {
                alertaCambioPrecioListAlertaCambioPrecio.setNegocioId(null);
                alertaCambioPrecioListAlertaCambioPrecio = em.merge(alertaCambioPrecioListAlertaCambioPrecio);
            }
            em.remove(negocio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Negocio> findNegocioEntities() {
        return findNegocioEntities(true, -1, -1);
    }

    public List<Negocio> findNegocioEntities(int maxResults, int firstResult) {
        return findNegocioEntities(false, maxResults, firstResult);
    }

    private List<Negocio> findNegocioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Negocio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Negocio findNegocio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Negocio.class, id);
        } finally {
            em.close();
        }
    }

    public int getNegocioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Negocio> rt = cq.from(Negocio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Negocio findNegocioByNombre(String nombre) {
        EntityManager em;
        em = emf.createEntityManager();
        Query q = em.createNamedQuery("Negocio.findByNombre");
        q.setParameter("nombre", nombre);
        try {
            List<Negocio> lista = (List<Negocio>)q.getResultList();
            if (!lista.isEmpty()) {
                return lista.get(0);
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            return null;
        }
        finally {
            em.close();
        }
    }
    
}
