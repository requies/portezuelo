/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Producto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.VentaAnulada;
import cl.requies.portezuelo.entities.Venta;
import java.util.ArrayList;
import java.util.List;
import cl.requies.portezuelo.entities.VentaDetalle;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;

/**
 *
 * @author victor
 */
public class VentaJpaController implements Serializable {

    public VentaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Venta venta) {
        if (venta.getVentaDetalleList() == null) {
            venta.setVentaDetalleList(new ArrayList<VentaDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja cajaId = venta.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                venta.setCajaId(cajaId);
            }
            VentaAnulada ventaAnulada = venta.getVentaAnulada();
            if (ventaAnulada != null) {
                ventaAnulada = em.getReference(ventaAnulada.getClass(), ventaAnulada.getVentaId());
                venta.setVentaAnulada(ventaAnulada);
            }
            
            venta.setNroVenta(getNroProxVenta());
            /*
            List<VentaDetalle> attachedVentaDetalleList = new ArrayList<VentaDetalle>();
            for (VentaDetalle ventaDetalleListVentaDetalleToAttach : venta.getVentaDetalleList()) {
                ventaDetalleListVentaDetalleToAttach = em.getReference(ventaDetalleListVentaDetalleToAttach.getClass(), ventaDetalleListVentaDetalleToAttach.getId());
                attachedVentaDetalleList.add(ventaDetalleListVentaDetalleToAttach);
            }
            venta.setVentaDetalleList(attachedVentaDetalleList);
            */
            List<VentaDetalle> attachedVentaDetalleList = venta.getVentaDetalleList();
            venta.setVentaDetalleList(new ArrayList<VentaDetalle>());
            em.persist(venta);
            em.flush();
            
            Double stockActual, cantVendida, nvoStock;
            Producto prodTemp;
            for (VentaDetalle ventaDetalleToAttach : attachedVentaDetalleList) {
                //em.persist(ventaDetalleToAttach);
                //venta.getVentaDetalleList().add(ventaDetalleToAttach);
                //venta = em.merge(venta);
                
                prodTemp = em.getReference(Producto.class, ventaDetalleToAttach.getBarcode().getBarcode());
                em.lock(prodTemp, LockModeType.PESSIMISTIC_WRITE); //GENERA SECCIÓN CRÍTICA EN LA TRANSACCIÓN
            
                //Se actualiza el stock del producto según la cantidad que se vendió
                stockActual = prodTemp.getStock();
                if ((stockActual != null) && (stockActual >= 0)) {
                    cantVendida = ventaDetalleToAttach.getCantidad();
                    nvoStock = stockActual - cantVendida;
                    if (nvoStock < 0)
                        nvoStock = 0.0;
                    prodTemp.setVendidos(prodTemp.getVendidos()+cantVendida);
                    prodTemp.setStock(nvoStock);
                }
                em.merge(prodTemp);
                em.flush();
            }
            
            if (cajaId != null) {
                em.lock(cajaId, LockModeType.PESSIMISTIC_WRITE); //GENERA SECCIÓN CRÍTICA EN LA TRANSACCIÓN
                cajaId.getVentaList().add(venta);
                
                cajaId.setMontoTermino(cajaId.getMontoTermino()+venta.getMonto());
                cajaId = em.merge(cajaId);
                //em.flush();
            }
            if (ventaAnulada != null) {
                Venta oldVentaOfVentaAnulada = ventaAnulada.getVenta();
                if (oldVentaOfVentaAnulada != null) {
                    oldVentaOfVentaAnulada.setVentaAnulada(null);
                    oldVentaOfVentaAnulada = em.merge(oldVentaOfVentaAnulada);
                }
                ventaAnulada.setVenta(venta);
                ventaAnulada = em.merge(ventaAnulada);
            }
            
            /*
            for (VentaDetalle ventaDetalleListVentaDetalle : venta.getVentaDetalleList()) {
                Venta oldVentaOfVentaDetalleListVentaDetalle = ventaDetalleListVentaDetalle.getVenta();
                ventaDetalleListVentaDetalle.setVenta(venta);
                ventaDetalleListVentaDetalle = em.merge(ventaDetalleListVentaDetalle);
                if (oldVentaOfVentaDetalleListVentaDetalle != null) {
                    oldVentaOfVentaDetalleListVentaDetalle.getVentaDetalleList().remove(ventaDetalleListVentaDetalle);
                    oldVentaOfVentaDetalleListVentaDetalle = em.merge(oldVentaOfVentaDetalleListVentaDetalle);
                }
            }
            */
            
            for (VentaDetalle ventaDetalleToAttach : attachedVentaDetalleList) {
                ventaDetalleToAttach.setVenta(venta);
                em.persist(ventaDetalleToAttach);
            }
            
            //venta.setVentaDetalleList(attachedVentaDetalleList);
            //em.merge(venta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Venta venta) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta persistentVenta = em.find(Venta.class, venta.getId());
            Caja cajaIdOld = persistentVenta.getCajaId();
            Caja cajaIdNew = venta.getCajaId();
            VentaAnulada ventaAnuladaOld = persistentVenta.getVentaAnulada();
            VentaAnulada ventaAnuladaNew = venta.getVentaAnulada();
            List<VentaDetalle> ventaDetalleListOld = persistentVenta.getVentaDetalleList();
            List<VentaDetalle> ventaDetalleListNew = venta.getVentaDetalleList();
            List<String> illegalOrphanMessages = null;
            if (ventaAnuladaOld != null && !ventaAnuladaOld.equals(ventaAnuladaNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain VentaAnulada " + ventaAnuladaOld + " since its venta field is not nullable.");
            }
            for (VentaDetalle ventaDetalleListOldVentaDetalle : ventaDetalleListOld) {
                if (!ventaDetalleListNew.contains(ventaDetalleListOldVentaDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentaDetalle " + ventaDetalleListOldVentaDetalle + " since its venta field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                venta.setCajaId(cajaIdNew);
            }
            if (ventaAnuladaNew != null) {
                ventaAnuladaNew = em.getReference(ventaAnuladaNew.getClass(), ventaAnuladaNew.getVentaId());
                venta.setVentaAnulada(ventaAnuladaNew);
            }
            List<VentaDetalle> attachedVentaDetalleListNew = new ArrayList<VentaDetalle>();
            for (VentaDetalle ventaDetalleListNewVentaDetalleToAttach : ventaDetalleListNew) {
                ventaDetalleListNewVentaDetalleToAttach = em.getReference(ventaDetalleListNewVentaDetalleToAttach.getClass(), ventaDetalleListNewVentaDetalleToAttach.getId());
                attachedVentaDetalleListNew.add(ventaDetalleListNewVentaDetalleToAttach);
            }
            ventaDetalleListNew = attachedVentaDetalleListNew;
            venta.setVentaDetalleList(ventaDetalleListNew);
            venta = em.merge(venta);
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getVentaList().remove(venta);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getVentaList().add(venta);
                cajaIdNew = em.merge(cajaIdNew);
            }
            if (ventaAnuladaNew != null && !ventaAnuladaNew.equals(ventaAnuladaOld)) {
                Venta oldVentaOfVentaAnulada = ventaAnuladaNew.getVenta();
                if (oldVentaOfVentaAnulada != null) {
                    oldVentaOfVentaAnulada.setVentaAnulada(null);
                    oldVentaOfVentaAnulada = em.merge(oldVentaOfVentaAnulada);
                }
                ventaAnuladaNew.setVenta(venta);
                ventaAnuladaNew = em.merge(ventaAnuladaNew);
            }
            
            
            for (VentaDetalle ventaDetalleListNewVentaDetalle : ventaDetalleListNew) {
                if (!ventaDetalleListOld.contains(ventaDetalleListNewVentaDetalle)) {
                    Venta oldVentaOfVentaDetalleListNewVentaDetalle = ventaDetalleListNewVentaDetalle.getVenta();
                    ventaDetalleListNewVentaDetalle.setVenta(venta);
                    ventaDetalleListNewVentaDetalle = em.merge(ventaDetalleListNewVentaDetalle);
                    if (oldVentaOfVentaDetalleListNewVentaDetalle != null && !oldVentaOfVentaDetalleListNewVentaDetalle.equals(venta)) {
                        oldVentaOfVentaDetalleListNewVentaDetalle.getVentaDetalleList().remove(ventaDetalleListNewVentaDetalle);
                        oldVentaOfVentaDetalleListNewVentaDetalle = em.merge(oldVentaOfVentaDetalleListNewVentaDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = venta.getId();
                if (findVenta(id) == null) {
                    throw new NonexistentEntityException("The venta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta venta;
            try {
                venta = em.getReference(Venta.class, id);
                venta.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The venta with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            VentaAnulada ventaAnuladaOrphanCheck = venta.getVentaAnulada();
            if (ventaAnuladaOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Venta (" + venta + ") cannot be destroyed since the VentaAnulada " + ventaAnuladaOrphanCheck + " in its ventaAnulada field has a non-nullable venta field.");
            }
            List<VentaDetalle> ventaDetalleListOrphanCheck = venta.getVentaDetalleList();
            for (VentaDetalle ventaDetalleListOrphanCheckVentaDetalle : ventaDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Venta (" + venta + ") cannot be destroyed since the VentaDetalle " + ventaDetalleListOrphanCheckVentaDetalle + " in its ventaDetalleList field has a non-nullable venta field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Caja cajaId = venta.getCajaId();
            if (cajaId != null) {
                cajaId.getVentaList().remove(venta);
                cajaId = em.merge(cajaId);
            }
            
            em.remove(venta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Venta> findVentaEntities() {
        return findVentaEntities(true, -1, -1);
    }

    public List<Venta> findVentaEntities(int maxResults, int firstResult) {
        return findVentaEntities(false, maxResults, firstResult);
    }

    private List<Venta> findVentaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Venta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Venta findVenta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Venta.class, id);
        } finally {
            em.close();
        }
    }
    
    public Venta findVentaByNum(Integer num) {
        EntityManager em = getEntityManager();
        Query q = getEntityManager().createNamedQuery("Venta.findByNumVenta");
        q.setParameter("num", num);
        try {
            return (Venta)q.getSingleResult();
        } finally {
            em.close();
        }
    }

    public int getVentaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Venta> rt = cq.from(Venta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getNroProxVenta() {
        Integer res = null;
        Query q = getEntityManager().createNamedQuery("Venta.lastNroVenta");
        try {
            res = (Integer)q.getSingleResult();
            if (res == null) {
                return 1;
            }
            return res+1;
        }
        catch (NoResultException nre) {
            return 1;
        }
    }
    
}
