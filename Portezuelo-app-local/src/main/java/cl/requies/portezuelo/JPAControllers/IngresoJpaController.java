/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.TipoIngreso;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Ingreso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class IngresoJpaController implements Serializable {

    public IngresoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingreso ingreso) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoIngreso tipoIngresoId = ingreso.getTipoIngresoId();
            if (tipoIngresoId != null) {
                tipoIngresoId = em.getReference(tipoIngresoId.getClass(), tipoIngresoId.getId());
                ingreso.setTipoIngresoId(tipoIngresoId);
            }
            Caja cajaId = ingreso.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                ingreso.setCajaId(cajaId);
            }
            Users userId = ingreso.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                ingreso.setUserId(userId);
            }
            em.persist(ingreso);
            if (tipoIngresoId != null) {
                tipoIngresoId.getIngresoList().add(ingreso);
                tipoIngresoId = em.merge(tipoIngresoId);
            }
            if (cajaId != null) {
                cajaId.getIngresoList().add(ingreso);
                
                cajaId.setMontoTermino(cajaId.getMontoTermino()+ingreso.getMonto());
                cajaId = em.merge(cajaId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingreso ingreso) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingreso persistentIngreso = em.find(Ingreso.class, ingreso.getId());
            TipoIngreso tipoIngresoIdOld = persistentIngreso.getTipoIngresoId();
            TipoIngreso tipoIngresoIdNew = ingreso.getTipoIngresoId();
            Caja cajaIdOld = persistentIngreso.getCajaId();
            Caja cajaIdNew = ingreso.getCajaId();
            Users usersIdNew = ingreso.getUserId();
            if (tipoIngresoIdNew != null) {
                tipoIngresoIdNew = em.getReference(tipoIngresoIdNew.getClass(), tipoIngresoIdNew.getId());
                ingreso.setTipoIngresoId(tipoIngresoIdNew);
            }
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                ingreso.setCajaId(cajaIdNew);
            }
            if (usersIdNew != null) {
                usersIdNew = em.getReference(usersIdNew.getClass(), usersIdNew.getId());
                ingreso.setUserId(usersIdNew);
            }
            ingreso = em.merge(ingreso);
            if (tipoIngresoIdOld != null && !tipoIngresoIdOld.equals(tipoIngresoIdNew)) {
                tipoIngresoIdOld.getIngresoList().remove(ingreso);
                tipoIngresoIdOld = em.merge(tipoIngresoIdOld);
            }
            if (tipoIngresoIdNew != null && !tipoIngresoIdNew.equals(tipoIngresoIdOld)) {
                tipoIngresoIdNew.getIngresoList().add(ingreso);
                tipoIngresoIdNew = em.merge(tipoIngresoIdNew);
            }
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getIngresoList().remove(ingreso);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getIngresoList().add(ingreso);
                cajaIdNew = em.merge(cajaIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingreso.getId();
                if (findIngreso(id) == null) {
                    throw new NonexistentEntityException("The ingreso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingreso ingreso;
            try {
                ingreso = em.getReference(Ingreso.class, id);
                ingreso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingreso with id " + id + " no longer exists.", enfe);
            }
            TipoIngreso tipoIngresoId = ingreso.getTipoIngresoId();
            if (tipoIngresoId != null) {
                tipoIngresoId.getIngresoList().remove(ingreso);
                tipoIngresoId = em.merge(tipoIngresoId);
            }
            Caja cajaId = ingreso.getCajaId();
            if (cajaId != null) {
                cajaId.getIngresoList().remove(ingreso);
                cajaId = em.merge(cajaId);
            }
            em.remove(ingreso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingreso> findIngresoEntities() {
        return findIngresoEntities(true, -1, -1);
    }

    public List<Ingreso> findIngresoEntities(int maxResults, int firstResult) {
        return findIngresoEntities(false, maxResults, firstResult);
    }

    private List<Ingreso> findIngresoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingreso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingreso findIngreso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingreso.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngresoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingreso> rt = cq.from(Ingreso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
