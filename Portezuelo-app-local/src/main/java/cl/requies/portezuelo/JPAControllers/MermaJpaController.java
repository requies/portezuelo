/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Merma;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.TipoMerma;
import cl.requies.portezuelo.entities.Producto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class MermaJpaController implements Serializable {

    public MermaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Merma merma) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoMerma tipoMermaId = merma.getTipoMermaId();
            if (tipoMermaId != null) {
                tipoMermaId = em.getReference(tipoMermaId.getClass(), tipoMermaId.getId());
                merma.setTipoMermaId(tipoMermaId);
            }
            Producto barcode = merma.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                merma.setBarcode(barcode);
            }
            em.persist(merma);
            if (tipoMermaId != null) {
                tipoMermaId.getMermaList().add(merma);
                tipoMermaId = em.merge(tipoMermaId);
            }
            if (barcode != null) {
                barcode.getMermaList().add(merma);
                barcode = em.merge(barcode);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Merma merma) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merma persistentMerma = em.find(Merma.class, merma.getId());
            TipoMerma tipoMermaIdOld = persistentMerma.getTipoMermaId();
            TipoMerma tipoMermaIdNew = merma.getTipoMermaId();
            Producto barcodeOld = persistentMerma.getBarcode();
            Producto barcodeNew = merma.getBarcode();
            if (tipoMermaIdNew != null) {
                tipoMermaIdNew = em.getReference(tipoMermaIdNew.getClass(), tipoMermaIdNew.getId());
                merma.setTipoMermaId(tipoMermaIdNew);
            }
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                merma.setBarcode(barcodeNew);
            }
            merma = em.merge(merma);
            if (tipoMermaIdOld != null && !tipoMermaIdOld.equals(tipoMermaIdNew)) {
                tipoMermaIdOld.getMermaList().remove(merma);
                tipoMermaIdOld = em.merge(tipoMermaIdOld);
            }
            if (tipoMermaIdNew != null && !tipoMermaIdNew.equals(tipoMermaIdOld)) {
                tipoMermaIdNew.getMermaList().add(merma);
                tipoMermaIdNew = em.merge(tipoMermaIdNew);
            }
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getMermaList().remove(merma);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getMermaList().add(merma);
                barcodeNew = em.merge(barcodeNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = merma.getId();
                if (findMerma(id) == null) {
                    throw new NonexistentEntityException("The merma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merma merma;
            try {
                merma = em.getReference(Merma.class, id);
                merma.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The merma with id " + id + " no longer exists.", enfe);
            }
            TipoMerma tipoMermaId = merma.getTipoMermaId();
            if (tipoMermaId != null) {
                tipoMermaId.getMermaList().remove(merma);
                tipoMermaId = em.merge(tipoMermaId);
            }
            Producto barcode = merma.getBarcode();
            if (barcode != null) {
                barcode.getMermaList().remove(merma);
                barcode = em.merge(barcode);
            }
            em.remove(merma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Merma> findMermaEntities() {
        return findMermaEntities(true, -1, -1);
    }

    public List<Merma> findMermaEntities(int maxResults, int firstResult) {
        return findMermaEntities(false, maxResults, firstResult);
    }

    private List<Merma> findMermaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Merma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Merma findMerma(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Merma.class, id);
        } finally {
            em.close();
        }
    }

    public int getMermaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Merma> rt = cq.from(Merma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
