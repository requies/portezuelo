/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Pedido;
import cl.requies.portezuelo.entities.PedidoDetalle;
import cl.requies.portezuelo.entities.PedidoDetallePK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class PedidoDetalleJpaController implements Serializable {

    public PedidoDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PedidoDetalle productoDetalle) throws PreexistingEntityException, Exception {
        if (productoDetalle.getPedidoDetallePK() == null) {
            productoDetalle.setPedidoDetallePK(new PedidoDetallePK());
        }
        productoDetalle.getPedidoDetallePK().setPedidoId(productoDetalle.getPedido().getId());
        productoDetalle.getPedidoDetallePK().setBarcode(productoDetalle.getProducto().getBarcode());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto = productoDetalle.getProducto();
            if (producto != null) {
                producto = em.getReference(producto.getClass(), producto.getBarcode());
                productoDetalle.setProducto(producto);
            }
            Pedido pedido = productoDetalle.getPedido();
            if (pedido != null) {
                pedido = em.getReference(pedido.getClass(), pedido.getId());
                productoDetalle.setPedido(pedido);
            }
            em.persist(productoDetalle);
            if (producto != null) {
                producto.getProductoDetalleList().add(productoDetalle);
                producto = em.merge(producto);
            }
            if (pedido != null) {
                pedido.getPedidoDetalleList().add(productoDetalle);
                pedido = em.merge(pedido);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProductoDetalle(productoDetalle.getPedidoDetallePK()) != null) {
                throw new PreexistingEntityException("ProductoDetalle " + productoDetalle + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PedidoDetalle productoDetalle) throws NonexistentEntityException, Exception {
        productoDetalle.getPedidoDetallePK().setPedidoId(productoDetalle.getPedido().getId());
        productoDetalle.getPedidoDetallePK().setBarcode(productoDetalle.getProducto().getBarcode());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PedidoDetalle persistentProductoDetalle = em.find(PedidoDetalle.class, productoDetalle.getPedidoDetallePK());
            Producto productoOld = persistentProductoDetalle.getProducto();
            Producto productoNew = productoDetalle.getProducto();
            Pedido pedidoOld = persistentProductoDetalle.getPedido();
            Pedido pedidoNew = productoDetalle.getPedido();
            if (productoNew != null) {
                productoNew = em.getReference(productoNew.getClass(), productoNew.getBarcode());
                productoDetalle.setProducto(productoNew);
            }
            if (pedidoNew != null) {
                pedidoNew = em.getReference(pedidoNew.getClass(), pedidoNew.getId());
                productoDetalle.setPedido(pedidoNew);
            }
            productoDetalle = em.merge(productoDetalle);
            if (productoOld != null && !productoOld.equals(productoNew)) {
                productoOld.getProductoDetalleList().remove(productoDetalle);
                productoOld = em.merge(productoOld);
            }
            if (productoNew != null && !productoNew.equals(productoOld)) {
                productoNew.getProductoDetalleList().add(productoDetalle);
                productoNew = em.merge(productoNew);
            }
            if (pedidoOld != null && !pedidoOld.equals(pedidoNew)) {
                pedidoOld.getPedidoDetalleList().remove(productoDetalle);
                pedidoOld = em.merge(pedidoOld);
            }
            if (pedidoNew != null && !pedidoNew.equals(pedidoOld)) {
                pedidoNew.getPedidoDetalleList().add(productoDetalle);
                pedidoNew = em.merge(pedidoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PedidoDetallePK id = productoDetalle.getPedidoDetallePK();
                if (findProductoDetalle(id) == null) {
                    throw new NonexistentEntityException("The productoDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PedidoDetallePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PedidoDetalle productoDetalle;
            try {
                productoDetalle = em.getReference(PedidoDetalle.class, id);
                productoDetalle.getPedidoDetallePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The productoDetalle with id " + id + " no longer exists.", enfe);
            }
            Producto producto = productoDetalle.getProducto();
            if (producto != null) {
                producto.getProductoDetalleList().remove(productoDetalle);
                producto = em.merge(producto);
            }
            Pedido pedido = productoDetalle.getPedido();
            if (pedido != null) {
                pedido.getPedidoDetalleList().remove(productoDetalle);
                pedido = em.merge(pedido);
            }
            em.remove(productoDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PedidoDetalle> findProductoDetalleEntities() {
        return findProductoDetalleEntities(true, -1, -1);
    }

    public List<PedidoDetalle> findProductoDetalleEntities(int maxResults, int firstResult) {
        return findProductoDetalleEntities(false, maxResults, firstResult);
    }

    private List<PedidoDetalle> findProductoDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PedidoDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PedidoDetalle findProductoDetalle(PedidoDetallePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PedidoDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PedidoDetalle> rt = cq.from(PedidoDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
