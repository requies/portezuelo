/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Promocion;
import cl.requies.portezuelo.entities.PromocionDetalle;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class PromocionJpaController implements Serializable {

    public PromocionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Promocion promocion) {
        if (promocion.getPromocionDetalleList() == null) {
            promocion.setPromocionDetalleList(new ArrayList<PromocionDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto barcode = promocion.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                promocion.setBarcode(barcode);
            }
            List<PromocionDetalle> attachedPromocionDetalleList = new ArrayList<PromocionDetalle>();
            for (PromocionDetalle promocionDetalleListPromocionDetalleToAttach : promocion.getPromocionDetalleList()) {
                promocionDetalleListPromocionDetalleToAttach = em.getReference(promocionDetalleListPromocionDetalleToAttach.getClass(), promocionDetalleListPromocionDetalleToAttach.getPromocionDetallePK());
                attachedPromocionDetalleList.add(promocionDetalleListPromocionDetalleToAttach);
            }
            promocion.setPromocionDetalleList(attachedPromocionDetalleList);
            em.persist(promocion);
            if (barcode != null) {
                barcode.getPromocionList().add(promocion);
                barcode = em.merge(barcode);
            }
            for (PromocionDetalle promocionDetalleListPromocionDetalle : promocion.getPromocionDetalleList()) {
                Promocion oldPromocionOfPromocionDetalleListPromocionDetalle = promocionDetalleListPromocionDetalle.getPromocion();
                promocionDetalleListPromocionDetalle.setPromocion(promocion);
                promocionDetalleListPromocionDetalle = em.merge(promocionDetalleListPromocionDetalle);
                if (oldPromocionOfPromocionDetalleListPromocionDetalle != null) {
                    oldPromocionOfPromocionDetalleListPromocionDetalle.getPromocionDetalleList().remove(promocionDetalleListPromocionDetalle);
                    oldPromocionOfPromocionDetalleListPromocionDetalle = em.merge(oldPromocionOfPromocionDetalleListPromocionDetalle);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Promocion promocion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Promocion persistentPromocion = em.find(Promocion.class, promocion.getId());
            Producto barcodeOld = persistentPromocion.getBarcode();
            Producto barcodeNew = promocion.getBarcode();
            List<PromocionDetalle> promocionDetalleListOld = persistentPromocion.getPromocionDetalleList();
            List<PromocionDetalle> promocionDetalleListNew = promocion.getPromocionDetalleList();
            List<String> illegalOrphanMessages = null;
            for (PromocionDetalle promocionDetalleListOldPromocionDetalle : promocionDetalleListOld) {
                if (!promocionDetalleListNew.contains(promocionDetalleListOldPromocionDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PromocionDetalle " + promocionDetalleListOldPromocionDetalle + " since its promocion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                promocion.setBarcode(barcodeNew);
            }
            List<PromocionDetalle> attachedPromocionDetalleListNew = new ArrayList<PromocionDetalle>();
            for (PromocionDetalle promocionDetalleListNewPromocionDetalleToAttach : promocionDetalleListNew) {
                promocionDetalleListNewPromocionDetalleToAttach = em.getReference(promocionDetalleListNewPromocionDetalleToAttach.getClass(), promocionDetalleListNewPromocionDetalleToAttach.getPromocionDetallePK());
                attachedPromocionDetalleListNew.add(promocionDetalleListNewPromocionDetalleToAttach);
            }
            promocionDetalleListNew = attachedPromocionDetalleListNew;
            promocion.setPromocionDetalleList(promocionDetalleListNew);
            promocion = em.merge(promocion);
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getPromocionList().remove(promocion);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getPromocionList().add(promocion);
                barcodeNew = em.merge(barcodeNew);
            }
            for (PromocionDetalle promocionDetalleListNewPromocionDetalle : promocionDetalleListNew) {
                if (!promocionDetalleListOld.contains(promocionDetalleListNewPromocionDetalle)) {
                    Promocion oldPromocionOfPromocionDetalleListNewPromocionDetalle = promocionDetalleListNewPromocionDetalle.getPromocion();
                    promocionDetalleListNewPromocionDetalle.setPromocion(promocion);
                    promocionDetalleListNewPromocionDetalle = em.merge(promocionDetalleListNewPromocionDetalle);
                    if (oldPromocionOfPromocionDetalleListNewPromocionDetalle != null && !oldPromocionOfPromocionDetalleListNewPromocionDetalle.equals(promocion)) {
                        oldPromocionOfPromocionDetalleListNewPromocionDetalle.getPromocionDetalleList().remove(promocionDetalleListNewPromocionDetalle);
                        oldPromocionOfPromocionDetalleListNewPromocionDetalle = em.merge(oldPromocionOfPromocionDetalleListNewPromocionDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = promocion.getId();
                if (findPromocion(id) == null) {
                    throw new NonexistentEntityException("The promocion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Promocion promocion;
            try {
                promocion = em.getReference(Promocion.class, id);
                promocion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The promocion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PromocionDetalle> promocionDetalleListOrphanCheck = promocion.getPromocionDetalleList();
            for (PromocionDetalle promocionDetalleListOrphanCheckPromocionDetalle : promocionDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Promocion (" + promocion + ") cannot be destroyed since the PromocionDetalle " + promocionDetalleListOrphanCheckPromocionDetalle + " in its promocionDetalleList field has a non-nullable promocion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Producto barcode = promocion.getBarcode();
            if (barcode != null) {
                barcode.getPromocionList().remove(promocion);
                barcode = em.merge(barcode);
            }
            em.remove(promocion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Promocion> findPromocionEntities() {
        return findPromocionEntities(true, -1, -1);
    }

    public List<Promocion> findPromocionEntities(int maxResults, int firstResult) {
        return findPromocionEntities(false, maxResults, firstResult);
    }

    private List<Promocion> findPromocionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Promocion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Promocion findPromocion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Promocion.class, id);
        } finally {
            em.close();
        }
    }

    public int getPromocionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Promocion> rt = cq.from(Promocion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
