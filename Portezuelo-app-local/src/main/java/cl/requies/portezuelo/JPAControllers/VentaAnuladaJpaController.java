/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Egreso;
import cl.requies.portezuelo.entities.Producto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Venta;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.VentaAnulada;
import cl.requies.portezuelo.entities.VentaDetalle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;

/**
 *
 * @author victor
 */
public class VentaAnuladaJpaController implements Serializable {

    public VentaAnuladaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VentaAnulada ventaAnulada) throws IllegalOrphanException, PreexistingEntityException, Exception {
        List<String> illegalOrphanMessages = null;
        Venta ventaOrphanCheck = ventaAnulada.getVenta();
        if (ventaOrphanCheck != null) {
            VentaAnulada oldVentaAnuladaOfVenta = ventaOrphanCheck.getVentaAnulada();
            if (oldVentaAnuladaOfVenta != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Venta " + ventaOrphanCheck + " already has an item of type VentaAnulada whose venta column cannot be null. Please make another selection for the venta field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta venta = ventaAnulada.getVenta();
            if (venta != null) {
                venta = em.getReference(venta.getClass(), venta.getId());
                ventaAnulada.setVenta(venta);
            }
            Caja cajaId = ventaAnulada.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                ventaAnulada.setCajaId(cajaId);
            }
            em.persist(ventaAnulada);
            if (venta != null) {
                venta.setVentaAnulada(ventaAnulada);
                venta = em.merge(venta);
            }
            if (cajaId != null) {
                em.lock(cajaId, LockModeType.PESSIMISTIC_WRITE); //GENERA SECCIÓN CRÍTICA EN LA TRANSACCIÓN
                cajaId.getVentaAnuladaList().add(ventaAnulada);
                
                cajaId.setMontoTermino(cajaId.getMontoTermino()-ventaAnulada.getVenta().getMonto());
                cajaId = em.merge(cajaId);
                em.flush();
            }
            
            //Devuelvo los stocks de los productos comprados
            List<VentaDetalle> listaVentaDet = venta.getVentaDetalleList();
            Producto prodTemp;
            int montoTotal = 0;
            Double stockActual;
            for (VentaDetalle vd : listaVentaDet) {
                prodTemp = em.getReference(Producto.class, vd.getBarcode().getBarcode());
                em.lock(prodTemp, LockModeType.PESSIMISTIC_WRITE); //GENERA SECCIÓN CRÍTICA EN LA TRANSACCIÓN
                prodTemp = vd.getBarcode();
                stockActual = prodTemp.getStock();
                if ((stockActual != null) && (stockActual >= 0)) {
                    prodTemp.setStock(stockActual+vd.getCantidad());
                }
                em.merge(prodTemp);
                em.flush();
                montoTotal += (int)(vd.getCantidad()*vd.getPrecio());
            }
            
            //Agrego un ingreso a la caja
            TipoEgresoJpaController controllerTipoEgreso = new TipoEgresoJpaController(emf);
            Egreso egr = new Egreso();
            egr.setFecha(new Date());
            egr.setMonto(montoTotal);
            egr.setCajaId(cajaId);
            egr.setTipoEgresoId(controllerTipoEgreso.findTipoEgreso(TipoEgresoJpaController.EGRESO_POR_VENTA_ANULADA));
            em.persist(egr);
            
            
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVentaAnulada(ventaAnulada.getVentaId()) != null) {
                throw new PreexistingEntityException("VentaAnulada " + ventaAnulada + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VentaAnulada ventaAnulada) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaAnulada persistentVentaAnulada = em.find(VentaAnulada.class, ventaAnulada.getVentaId());
            Venta ventaOld = persistentVentaAnulada.getVenta();
            Venta ventaNew = ventaAnulada.getVenta();
            Caja cajaIdOld = persistentVentaAnulada.getCajaId();
            Caja cajaIdNew = ventaAnulada.getCajaId();
            List<String> illegalOrphanMessages = null;
            if (ventaNew != null && !ventaNew.equals(ventaOld)) {
                VentaAnulada oldVentaAnuladaOfVenta = ventaNew.getVentaAnulada();
                if (oldVentaAnuladaOfVenta != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Venta " + ventaNew + " already has an item of type VentaAnulada whose venta column cannot be null. Please make another selection for the venta field.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (ventaNew != null) {
                ventaNew = em.getReference(ventaNew.getClass(), ventaNew.getId());
                ventaAnulada.setVenta(ventaNew);
            }
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                ventaAnulada.setCajaId(cajaIdNew);
            }
            ventaAnulada = em.merge(ventaAnulada);
            if (ventaOld != null && !ventaOld.equals(ventaNew)) {
                ventaOld.setVentaAnulada(null);
                ventaOld = em.merge(ventaOld);
            }
            if (ventaNew != null && !ventaNew.equals(ventaOld)) {
                ventaNew.setVentaAnulada(ventaAnulada);
                ventaNew = em.merge(ventaNew);
            }
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getVentaAnuladaList().remove(ventaAnulada);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getVentaAnuladaList().add(ventaAnulada);
                cajaIdNew = em.merge(cajaIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ventaAnulada.getVentaId();
                if (findVentaAnulada(id) == null) {
                    throw new NonexistentEntityException("The ventaAnulada with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaAnulada ventaAnulada;
            try {
                ventaAnulada = em.getReference(VentaAnulada.class, id);
                ventaAnulada.getVentaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventaAnulada with id " + id + " no longer exists.", enfe);
            }
            Venta venta = ventaAnulada.getVenta();
            if (venta != null) {
                venta.setVentaAnulada(null);
                venta = em.merge(venta);
            }
            Caja cajaId = ventaAnulada.getCajaId();
            if (cajaId != null) {
                cajaId.getVentaAnuladaList().remove(ventaAnulada);
                cajaId = em.merge(cajaId);
            }
            em.remove(ventaAnulada);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VentaAnulada> findVentaAnuladaEntities() {
        return findVentaAnuladaEntities(true, -1, -1);
    }

    public List<VentaAnulada> findVentaAnuladaEntities(int maxResults, int firstResult) {
        return findVentaAnuladaEntities(false, maxResults, firstResult);
    }

    private List<VentaAnulada> findVentaAnuladaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VentaAnulada.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VentaAnulada findVentaAnulada(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VentaAnulada.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaAnuladaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VentaAnulada> rt = cq.from(VentaAnulada.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
