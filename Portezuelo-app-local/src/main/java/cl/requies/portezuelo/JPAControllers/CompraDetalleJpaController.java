/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Compra;
import cl.requies.portezuelo.entities.CompraDetalle;
import cl.requies.portezuelo.entities.CompraDetallePK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class CompraDetalleJpaController implements Serializable {

    public CompraDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CompraDetalle compraDetalle) throws PreexistingEntityException, Exception {
        if (compraDetalle.getCompraDetallePK() == null) {
            compraDetalle.setCompraDetallePK(new CompraDetallePK());
        }
        compraDetalle.getCompraDetallePK().setBarcode(compraDetalle.getProducto().getBarcode());
        compraDetalle.getCompraDetallePK().setCompraId(compraDetalle.getCompra().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto = compraDetalle.getProducto();
            if (producto != null) {
                producto = em.getReference(producto.getClass(), producto.getBarcode());
                compraDetalle.setProducto(producto);
            }
            Compra compra = compraDetalle.getCompra();
            if (compra != null) {
                compra = em.getReference(compra.getClass(), compra.getId());
                compraDetalle.setCompra(compra);
            }
            em.persist(compraDetalle);
            if (producto != null) {
                producto.getCompraDetalleList().add(compraDetalle);
                producto = em.merge(producto);
            }
            if (compra != null) {
                compra.getCompraDetalleList().add(compraDetalle);
                compra = em.merge(compra);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCompraDetalle(compraDetalle.getCompraDetallePK()) != null) {
                throw new PreexistingEntityException("CompraDetalle " + compraDetalle + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CompraDetalle compraDetalle) throws NonexistentEntityException, Exception {
        compraDetalle.getCompraDetallePK().setBarcode(compraDetalle.getProducto().getBarcode());
        compraDetalle.getCompraDetallePK().setCompraId(compraDetalle.getCompra().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CompraDetalle persistentCompraDetalle = em.find(CompraDetalle.class, compraDetalle.getCompraDetallePK());
            Producto productoOld = persistentCompraDetalle.getProducto();
            Producto productoNew = compraDetalle.getProducto();
            Compra compraOld = persistentCompraDetalle.getCompra();
            Compra compraNew = compraDetalle.getCompra();
            if (productoNew != null) {
                productoNew = em.getReference(productoNew.getClass(), productoNew.getBarcode());
                compraDetalle.setProducto(productoNew);
            }
            if (compraNew != null) {
                compraNew = em.getReference(compraNew.getClass(), compraNew.getId());
                compraDetalle.setCompra(compraNew);
            }
            compraDetalle = em.merge(compraDetalle);
            if (productoOld != null && !productoOld.equals(productoNew)) {
                productoOld.getCompraDetalleList().remove(compraDetalle);
                productoOld = em.merge(productoOld);
            }
            if (productoNew != null && !productoNew.equals(productoOld)) {
                productoNew.getCompraDetalleList().add(compraDetalle);
                productoNew = em.merge(productoNew);
            }
            if (compraOld != null && !compraOld.equals(compraNew)) {
                compraOld.getCompraDetalleList().remove(compraDetalle);
                compraOld = em.merge(compraOld);
            }
            if (compraNew != null && !compraNew.equals(compraOld)) {
                compraNew.getCompraDetalleList().add(compraDetalle);
                compraNew = em.merge(compraNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                CompraDetallePK id = compraDetalle.getCompraDetallePK();
                if (findCompraDetalle(id) == null) {
                    throw new NonexistentEntityException("The compraDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(CompraDetallePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CompraDetalle compraDetalle;
            try {
                compraDetalle = em.getReference(CompraDetalle.class, id);
                compraDetalle.getCompraDetallePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The compraDetalle with id " + id + " no longer exists.", enfe);
            }
            Producto producto = compraDetalle.getProducto();
            if (producto != null) {
                producto.getCompraDetalleList().remove(compraDetalle);
                producto = em.merge(producto);
            }
            Compra compra = compraDetalle.getCompra();
            if (compra != null) {
                compra.getCompraDetalleList().remove(compraDetalle);
                compra = em.merge(compra);
            }
            em.remove(compraDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CompraDetalle> findCompraDetalleEntities() {
        return findCompraDetalleEntities(true, -1, -1);
    }

    public List<CompraDetalle> findCompraDetalleEntities(int maxResults, int firstResult) {
        return findCompraDetalleEntities(false, maxResults, firstResult);
    }

    private List<CompraDetalle> findCompraDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CompraDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CompraDetalle findCompraDetalle(CompraDetallePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CompraDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getCompraDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CompraDetalle> rt = cq.from(CompraDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
