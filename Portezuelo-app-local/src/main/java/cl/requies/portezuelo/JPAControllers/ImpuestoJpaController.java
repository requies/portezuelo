/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Impuesto;
import cl.requies.portezuelo.entities.Impuesto_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author victor
 */
public class ImpuestoJpaController implements Serializable {

    public ImpuestoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Impuesto impuesto) {
        if (impuesto.getProductoList() == null) {
            impuesto.setProductoList(new ArrayList<Producto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Producto> attachedProductoList = new ArrayList<Producto>();
            for (Producto productoListProductoToAttach : impuesto.getProductoList()) {
                productoListProductoToAttach = em.getReference(productoListProductoToAttach.getClass(), productoListProductoToAttach.getBarcode());
                attachedProductoList.add(productoListProductoToAttach);
            }
            impuesto.setProductoList(attachedProductoList);
            em.persist(impuesto);
            for (Producto productoListProducto : impuesto.getProductoList()) {
                Impuesto oldOtrosImpuestosOfProductoListProducto = productoListProducto.getOtrosImpuestos();
                productoListProducto.setOtrosImpuestos(impuesto);
                productoListProducto = em.merge(productoListProducto);
                if (oldOtrosImpuestosOfProductoListProducto != null) {
                    oldOtrosImpuestosOfProductoListProducto.getProductoList().remove(productoListProducto);
                    oldOtrosImpuestosOfProductoListProducto = em.merge(oldOtrosImpuestosOfProductoListProducto);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Impuesto impuesto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Impuesto persistentImpuesto = em.find(Impuesto.class, impuesto.getId());
            List<Producto> productoListOld = persistentImpuesto.getProductoList();
            List<Producto> productoListNew = impuesto.getProductoList();
            List<Producto> attachedProductoListNew = new ArrayList<Producto>();
            for (Producto productoListNewProductoToAttach : productoListNew) {
                productoListNewProductoToAttach = em.getReference(productoListNewProductoToAttach.getClass(), productoListNewProductoToAttach.getBarcode());
                attachedProductoListNew.add(productoListNewProductoToAttach);
            }
            productoListNew = attachedProductoListNew;
            impuesto.setProductoList(productoListNew);
            impuesto = em.merge(impuesto);
            for (Producto productoListOldProducto : productoListOld) {
                if (!productoListNew.contains(productoListOldProducto)) {
                    productoListOldProducto.setOtrosImpuestos(null);
                    productoListOldProducto = em.merge(productoListOldProducto);
                }
            }
            for (Producto productoListNewProducto : productoListNew) {
                if (!productoListOld.contains(productoListNewProducto)) {
                    Impuesto oldOtrosImpuestosOfProductoListNewProducto = productoListNewProducto.getOtrosImpuestos();
                    productoListNewProducto.setOtrosImpuestos(impuesto);
                    productoListNewProducto = em.merge(productoListNewProducto);
                    if (oldOtrosImpuestosOfProductoListNewProducto != null && !oldOtrosImpuestosOfProductoListNewProducto.equals(impuesto)) {
                        oldOtrosImpuestosOfProductoListNewProducto.getProductoList().remove(productoListNewProducto);
                        oldOtrosImpuestosOfProductoListNewProducto = em.merge(oldOtrosImpuestosOfProductoListNewProducto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = impuesto.getId();
                if (findImpuesto(id) == null) {
                    throw new NonexistentEntityException("The impuesto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Impuesto impuesto;
            try {
                impuesto = em.getReference(Impuesto.class, id);
                impuesto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The impuesto with id " + id + " no longer exists.", enfe);
            }
            List<Producto> productoList = impuesto.getProductoList();
            for (Producto productoListProducto : productoList) {
                productoListProducto.setOtrosImpuestos(null);
                productoListProducto = em.merge(productoListProducto);
            }
            em.remove(impuesto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Impuesto> findImpuestoEntities() {
        return findImpuestoEntities(true, -1, -1);
    }

    public List<Impuesto> findImpuestoEntities(int maxResults, int firstResult) {
        return findImpuestoEntities(false, maxResults, firstResult);
    }

    private List<Impuesto> findImpuestoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<Impuesto> impuestoTbl = cq.from(Impuesto.class);
            cq.select(impuestoTbl);
            cq.orderBy(cb.asc(impuestoTbl.get(Impuesto_.id)));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Impuesto> findImpuestoEntitiesOnlyVisibles() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Impuesto.findAllVisibles");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Impuesto findImpuesto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Impuesto.class, id);
        } finally {
            em.close();
        }
    }

    public int getImpuestoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Impuesto> rt = cq.from(Impuesto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
