/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Traspaso;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.TraspasoDetalle;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class TraspasoDetalleJpaController implements Serializable {

    public TraspasoDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TraspasoDetalle traspasoDetalle) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Traspaso traspasoId = traspasoDetalle.getTraspasoId();
            if (traspasoId != null) {
                traspasoId = em.getReference(traspasoId.getClass(), traspasoId.getId());
                traspasoDetalle.setTraspasoId(traspasoId);
            }
            Producto barcode = traspasoDetalle.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                traspasoDetalle.setBarcode(barcode);
            }
            em.persist(traspasoDetalle);
            if (traspasoId != null) {
                traspasoId.getTraspasoDetalleList().add(traspasoDetalle);
                traspasoId = em.merge(traspasoId);
            }
            if (barcode != null) {
                barcode.getTraspasoDetalleList().add(traspasoDetalle);
                barcode = em.merge(barcode);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TraspasoDetalle traspasoDetalle) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TraspasoDetalle persistentTraspasoDetalle = em.find(TraspasoDetalle.class, traspasoDetalle.getId());
            Traspaso traspasoIdOld = persistentTraspasoDetalle.getTraspasoId();
            Traspaso traspasoIdNew = traspasoDetalle.getTraspasoId();
            Producto barcodeOld = persistentTraspasoDetalle.getBarcode();
            Producto barcodeNew = traspasoDetalle.getBarcode();
            if (traspasoIdNew != null) {
                traspasoIdNew = em.getReference(traspasoIdNew.getClass(), traspasoIdNew.getId());
                traspasoDetalle.setTraspasoId(traspasoIdNew);
            }
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                traspasoDetalle.setBarcode(barcodeNew);
            }
            traspasoDetalle = em.merge(traspasoDetalle);
            if (traspasoIdOld != null && !traspasoIdOld.equals(traspasoIdNew)) {
                traspasoIdOld.getTraspasoDetalleList().remove(traspasoDetalle);
                traspasoIdOld = em.merge(traspasoIdOld);
            }
            if (traspasoIdNew != null && !traspasoIdNew.equals(traspasoIdOld)) {
                traspasoIdNew.getTraspasoDetalleList().add(traspasoDetalle);
                traspasoIdNew = em.merge(traspasoIdNew);
            }
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getTraspasoDetalleList().remove(traspasoDetalle);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getTraspasoDetalleList().add(traspasoDetalle);
                barcodeNew = em.merge(barcodeNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = traspasoDetalle.getId();
                if (findTraspasoDetalle(id) == null) {
                    throw new NonexistentEntityException("The traspasoDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TraspasoDetalle traspasoDetalle;
            try {
                traspasoDetalle = em.getReference(TraspasoDetalle.class, id);
                traspasoDetalle.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The traspasoDetalle with id " + id + " no longer exists.", enfe);
            }
            Traspaso traspasoId = traspasoDetalle.getTraspasoId();
            if (traspasoId != null) {
                traspasoId.getTraspasoDetalleList().remove(traspasoDetalle);
                traspasoId = em.merge(traspasoId);
            }
            Producto barcode = traspasoDetalle.getBarcode();
            if (barcode != null) {
                barcode.getTraspasoDetalleList().remove(traspasoDetalle);
                barcode = em.merge(barcode);
            }
            em.remove(traspasoDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TraspasoDetalle> findTraspasoDetalleEntities() {
        return findTraspasoDetalleEntities(true, -1, -1);
    }

    public List<TraspasoDetalle> findTraspasoDetalleEntities(int maxResults, int firstResult) {
        return findTraspasoDetalleEntities(false, maxResults, firstResult);
    }

    private List<TraspasoDetalle> findTraspasoDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TraspasoDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TraspasoDetalle findTraspasoDetalle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TraspasoDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getTraspasoDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TraspasoDetalle> rt = cq.from(TraspasoDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
