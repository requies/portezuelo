/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Merma;
import cl.requies.portezuelo.entities.TipoMerma;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class TipoMermaJpaController implements Serializable {

    public TipoMermaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoMerma tipoMerma) {
        if (tipoMerma.getMermaList() == null) {
            tipoMerma.setMermaList(new ArrayList<Merma>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Merma> attachedMermaList = new ArrayList<Merma>();
            for (Merma mermaListMermaToAttach : tipoMerma.getMermaList()) {
                mermaListMermaToAttach = em.getReference(mermaListMermaToAttach.getClass(), mermaListMermaToAttach.getId());
                attachedMermaList.add(mermaListMermaToAttach);
            }
            tipoMerma.setMermaList(attachedMermaList);
            em.persist(tipoMerma);
            for (Merma mermaListMerma : tipoMerma.getMermaList()) {
                TipoMerma oldTipoMermaIdOfMermaListMerma = mermaListMerma.getTipoMermaId();
                mermaListMerma.setTipoMermaId(tipoMerma);
                mermaListMerma = em.merge(mermaListMerma);
                if (oldTipoMermaIdOfMermaListMerma != null) {
                    oldTipoMermaIdOfMermaListMerma.getMermaList().remove(mermaListMerma);
                    oldTipoMermaIdOfMermaListMerma = em.merge(oldTipoMermaIdOfMermaListMerma);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoMerma tipoMerma) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoMerma persistentTipoMerma = em.find(TipoMerma.class, tipoMerma.getId());
            List<Merma> mermaListOld = persistentTipoMerma.getMermaList();
            List<Merma> mermaListNew = tipoMerma.getMermaList();
            List<Merma> attachedMermaListNew = new ArrayList<Merma>();
            for (Merma mermaListNewMermaToAttach : mermaListNew) {
                mermaListNewMermaToAttach = em.getReference(mermaListNewMermaToAttach.getClass(), mermaListNewMermaToAttach.getId());
                attachedMermaListNew.add(mermaListNewMermaToAttach);
            }
            mermaListNew = attachedMermaListNew;
            tipoMerma.setMermaList(mermaListNew);
            tipoMerma = em.merge(tipoMerma);
            for (Merma mermaListOldMerma : mermaListOld) {
                if (!mermaListNew.contains(mermaListOldMerma)) {
                    mermaListOldMerma.setTipoMermaId(null);
                    mermaListOldMerma = em.merge(mermaListOldMerma);
                }
            }
            for (Merma mermaListNewMerma : mermaListNew) {
                if (!mermaListOld.contains(mermaListNewMerma)) {
                    TipoMerma oldTipoMermaIdOfMermaListNewMerma = mermaListNewMerma.getTipoMermaId();
                    mermaListNewMerma.setTipoMermaId(tipoMerma);
                    mermaListNewMerma = em.merge(mermaListNewMerma);
                    if (oldTipoMermaIdOfMermaListNewMerma != null && !oldTipoMermaIdOfMermaListNewMerma.equals(tipoMerma)) {
                        oldTipoMermaIdOfMermaListNewMerma.getMermaList().remove(mermaListNewMerma);
                        oldTipoMermaIdOfMermaListNewMerma = em.merge(oldTipoMermaIdOfMermaListNewMerma);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoMerma.getId();
                if (findTipoMerma(id) == null) {
                    throw new NonexistentEntityException("The tipoMerma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoMerma tipoMerma;
            try {
                tipoMerma = em.getReference(TipoMerma.class, id);
                tipoMerma.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoMerma with id " + id + " no longer exists.", enfe);
            }
            List<Merma> mermaList = tipoMerma.getMermaList();
            for (Merma mermaListMerma : mermaList) {
                mermaListMerma.setTipoMermaId(null);
                mermaListMerma = em.merge(mermaListMerma);
            }
            em.remove(tipoMerma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoMerma> findTipoMermaEntities() {
        return findTipoMermaEntities(true, -1, -1);
    }

    public List<TipoMerma> findTipoMermaEntities(int maxResults, int firstResult) {
        return findTipoMermaEntities(false, maxResults, firstResult);
    }

    private List<TipoMerma> findTipoMermaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoMerma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoMerma findTipoMerma(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoMerma.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoMermaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoMerma> rt = cq.from(TipoMerma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
