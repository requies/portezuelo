/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Promocion;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.PromocionDetalle;
import cl.requies.portezuelo.entities.PromocionDetallePK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class PromocionDetalleJpaController implements Serializable {

    public PromocionDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PromocionDetalle promocionDetalle) throws PreexistingEntityException, Exception {
        if (promocionDetalle.getPromocionDetallePK() == null) {
            promocionDetalle.setPromocionDetallePK(new PromocionDetallePK());
        }
        promocionDetalle.getPromocionDetallePK().setPromocionId(promocionDetalle.getPromocion().getId());
        promocionDetalle.getPromocionDetallePK().setBarcode(promocionDetalle.getProducto().getBarcode());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Promocion promocion = promocionDetalle.getPromocion();
            if (promocion != null) {
                promocion = em.getReference(promocion.getClass(), promocion.getId());
                promocionDetalle.setPromocion(promocion);
            }
            Producto producto = promocionDetalle.getProducto();
            if (producto != null) {
                producto = em.getReference(producto.getClass(), producto.getBarcode());
                promocionDetalle.setProducto(producto);
            }
            em.persist(promocionDetalle);
            if (promocion != null) {
                promocion.getPromocionDetalleList().add(promocionDetalle);
                promocion = em.merge(promocion);
            }
            if (producto != null) {
                producto.getPromocionDetalleList().add(promocionDetalle);
                producto = em.merge(producto);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPromocionDetalle(promocionDetalle.getPromocionDetallePK()) != null) {
                throw new PreexistingEntityException("PromocionDetalle " + promocionDetalle + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PromocionDetalle promocionDetalle) throws NonexistentEntityException, Exception {
        promocionDetalle.getPromocionDetallePK().setPromocionId(promocionDetalle.getPromocion().getId());
        promocionDetalle.getPromocionDetallePK().setBarcode(promocionDetalle.getProducto().getBarcode());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PromocionDetalle persistentPromocionDetalle = em.find(PromocionDetalle.class, promocionDetalle.getPromocionDetallePK());
            Promocion promocionOld = persistentPromocionDetalle.getPromocion();
            Promocion promocionNew = promocionDetalle.getPromocion();
            Producto productoOld = persistentPromocionDetalle.getProducto();
            Producto productoNew = promocionDetalle.getProducto();
            if (promocionNew != null) {
                promocionNew = em.getReference(promocionNew.getClass(), promocionNew.getId());
                promocionDetalle.setPromocion(promocionNew);
            }
            if (productoNew != null) {
                productoNew = em.getReference(productoNew.getClass(), productoNew.getBarcode());
                promocionDetalle.setProducto(productoNew);
            }
            promocionDetalle = em.merge(promocionDetalle);
            if (promocionOld != null && !promocionOld.equals(promocionNew)) {
                promocionOld.getPromocionDetalleList().remove(promocionDetalle);
                promocionOld = em.merge(promocionOld);
            }
            if (promocionNew != null && !promocionNew.equals(promocionOld)) {
                promocionNew.getPromocionDetalleList().add(promocionDetalle);
                promocionNew = em.merge(promocionNew);
            }
            if (productoOld != null && !productoOld.equals(productoNew)) {
                productoOld.getPromocionDetalleList().remove(promocionDetalle);
                productoOld = em.merge(productoOld);
            }
            if (productoNew != null && !productoNew.equals(productoOld)) {
                productoNew.getPromocionDetalleList().add(promocionDetalle);
                productoNew = em.merge(productoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PromocionDetallePK id = promocionDetalle.getPromocionDetallePK();
                if (findPromocionDetalle(id) == null) {
                    throw new NonexistentEntityException("The promocionDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PromocionDetallePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PromocionDetalle promocionDetalle;
            try {
                promocionDetalle = em.getReference(PromocionDetalle.class, id);
                promocionDetalle.getPromocionDetallePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The promocionDetalle with id " + id + " no longer exists.", enfe);
            }
            Promocion promocion = promocionDetalle.getPromocion();
            if (promocion != null) {
                promocion.getPromocionDetalleList().remove(promocionDetalle);
                promocion = em.merge(promocion);
            }
            Producto producto = promocionDetalle.getProducto();
            if (producto != null) {
                producto.getPromocionDetalleList().remove(promocionDetalle);
                producto = em.merge(producto);
            }
            em.remove(promocionDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PromocionDetalle> findPromocionDetalleEntities() {
        return findPromocionDetalleEntities(true, -1, -1);
    }

    public List<PromocionDetalle> findPromocionDetalleEntities(int maxResults, int firstResult) {
        return findPromocionDetalleEntities(false, maxResults, firstResult);
    }

    private List<PromocionDetalle> findPromocionDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PromocionDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PromocionDetalle findPromocionDetalle(PromocionDetallePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PromocionDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getPromocionDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PromocionDetalle> rt = cq.from(PromocionDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
