/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.FamiliaProducto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class FamiliaProductoJpaController implements Serializable {

    public FamiliaProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(FamiliaProducto familiaProducto) {
        if (familiaProducto.getProductoList() == null) {
            familiaProducto.setProductoList(new ArrayList<Producto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Producto> attachedProductoList = new ArrayList<Producto>();
            for (Producto productoListProductoToAttach : familiaProducto.getProductoList()) {
                productoListProductoToAttach = em.getReference(productoListProductoToAttach.getClass(), productoListProductoToAttach.getBarcode());
                attachedProductoList.add(productoListProductoToAttach);
            }
            familiaProducto.setProductoList(attachedProductoList);
            em.persist(familiaProducto);
            for (Producto productoListProducto : familiaProducto.getProductoList()) {
                FamiliaProducto oldFamiliaOfProductoListProducto = productoListProducto.getFamilia();
                productoListProducto.setFamilia(familiaProducto);
                productoListProducto = em.merge(productoListProducto);
                if (oldFamiliaOfProductoListProducto != null) {
                    oldFamiliaOfProductoListProducto.getProductoList().remove(productoListProducto);
                    oldFamiliaOfProductoListProducto = em.merge(oldFamiliaOfProductoListProducto);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(FamiliaProducto familiaProducto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FamiliaProducto persistentFamiliaProducto = em.find(FamiliaProducto.class, familiaProducto.getId());
            List<Producto> productoListOld = persistentFamiliaProducto.getProductoList();
            List<Producto> productoListNew = familiaProducto.getProductoList();
            List<String> illegalOrphanMessages = null;
            for (Producto productoListOldProducto : productoListOld) {
                if (!productoListNew.contains(productoListOldProducto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Producto " + productoListOldProducto + " since its familia field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Producto> attachedProductoListNew = new ArrayList<Producto>();
            for (Producto productoListNewProductoToAttach : productoListNew) {
                productoListNewProductoToAttach = em.getReference(productoListNewProductoToAttach.getClass(), productoListNewProductoToAttach.getBarcode());
                attachedProductoListNew.add(productoListNewProductoToAttach);
            }
            productoListNew = attachedProductoListNew;
            familiaProducto.setProductoList(productoListNew);
            familiaProducto = em.merge(familiaProducto);
            for (Producto productoListNewProducto : productoListNew) {
                if (!productoListOld.contains(productoListNewProducto)) {
                    FamiliaProducto oldFamiliaOfProductoListNewProducto = productoListNewProducto.getFamilia();
                    productoListNewProducto.setFamilia(familiaProducto);
                    productoListNewProducto = em.merge(productoListNewProducto);
                    if (oldFamiliaOfProductoListNewProducto != null && !oldFamiliaOfProductoListNewProducto.equals(familiaProducto)) {
                        oldFamiliaOfProductoListNewProducto.getProductoList().remove(productoListNewProducto);
                        oldFamiliaOfProductoListNewProducto = em.merge(oldFamiliaOfProductoListNewProducto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = familiaProducto.getId();
                if (findFamiliaProducto(id) == null) {
                    throw new NonexistentEntityException("The familiaProducto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FamiliaProducto familiaProducto;
            try {
                familiaProducto = em.getReference(FamiliaProducto.class, id);
                familiaProducto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The familiaProducto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Producto> productoListOrphanCheck = familiaProducto.getProductoList();
            for (Producto productoListOrphanCheckProducto : productoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This FamiliaProducto (" + familiaProducto + ") cannot be destroyed since the Producto " + productoListOrphanCheckProducto + " in its productoList field has a non-nullable familia field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(familiaProducto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<FamiliaProducto> findFamiliaProductoEntities() {
        return findFamiliaProductoEntities(true, -1, -1);
    }

    public List<FamiliaProducto> findFamiliaProductoEntities(int maxResults, int firstResult) {
        return findFamiliaProductoEntities(false, maxResults, firstResult);
    }

    private List<FamiliaProducto> findFamiliaProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FamiliaProducto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FamiliaProducto findFamiliaProducto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FamiliaProducto.class, id);
        } finally {
            em.close();
        }
    }

    public int getFamiliaProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FamiliaProducto> rt = cq.from(FamiliaProducto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
