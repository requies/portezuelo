/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.TipoEgreso;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Egreso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class EgresoJpaController implements Serializable {

    public EgresoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Egreso egreso) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoEgreso tipoEgresoId = egreso.getTipoEgresoId();
            if (tipoEgresoId != null) {
                tipoEgresoId = em.getReference(tipoEgresoId.getClass(), tipoEgresoId.getId());
                egreso.setTipoEgresoId(tipoEgresoId);
            }
            Caja cajaId = egreso.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                egreso.setCajaId(cajaId);
            }
            Users userId = egreso.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                egreso.setUserId(userId);
            }
            em.persist(egreso);
            if (tipoEgresoId != null) {
                tipoEgresoId.getEgresoList().add(egreso);
                tipoEgresoId = em.merge(tipoEgresoId);
            }
            if (cajaId != null) {
                cajaId.getEgresoList().add(egreso);
                
                cajaId.setMontoTermino(cajaId.getMontoTermino()-egreso.getMonto());
                cajaId = em.merge(cajaId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Egreso egreso) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Egreso persistentEgreso = em.find(Egreso.class, egreso.getId());
            TipoEgreso tipoEgresoIdOld = persistentEgreso.getTipoEgresoId();
            TipoEgreso tipoEgresoIdNew = egreso.getTipoEgresoId();
            Caja cajaIdOld = persistentEgreso.getCajaId();
            Caja cajaIdNew = egreso.getCajaId();
            Users usersIdNew = egreso.getUserId();
            if (tipoEgresoIdNew != null) {
                tipoEgresoIdNew = em.getReference(tipoEgresoIdNew.getClass(), tipoEgresoIdNew.getId());
                egreso.setTipoEgresoId(tipoEgresoIdNew);
            }
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                egreso.setCajaId(cajaIdNew);
            }
            if (usersIdNew != null) {
                usersIdNew = em.getReference(usersIdNew.getClass(), usersIdNew.getId());
                egreso.setUserId(usersIdNew);
            }
            egreso = em.merge(egreso);
            if (tipoEgresoIdOld != null && !tipoEgresoIdOld.equals(tipoEgresoIdNew)) {
                tipoEgresoIdOld.getEgresoList().remove(egreso);
                tipoEgresoIdOld = em.merge(tipoEgresoIdOld);
            }
            if (tipoEgresoIdNew != null && !tipoEgresoIdNew.equals(tipoEgresoIdOld)) {
                tipoEgresoIdNew.getEgresoList().add(egreso);
                tipoEgresoIdNew = em.merge(tipoEgresoIdNew);
            }
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getEgresoList().remove(egreso);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getEgresoList().add(egreso);
                cajaIdNew = em.merge(cajaIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = egreso.getId();
                if (findEgreso(id) == null) {
                    throw new NonexistentEntityException("The egreso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Egreso egreso;
            try {
                egreso = em.getReference(Egreso.class, id);
                egreso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The egreso with id " + id + " no longer exists.", enfe);
            }
            TipoEgreso tipoEgresoId = egreso.getTipoEgresoId();
            if (tipoEgresoId != null) {
                tipoEgresoId.getEgresoList().remove(egreso);
                tipoEgresoId = em.merge(tipoEgresoId);
            }
            Caja cajaId = egreso.getCajaId();
            if (cajaId != null) {
                cajaId.getEgresoList().remove(egreso);
                cajaId = em.merge(cajaId);
            }
            em.remove(egreso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Egreso> findEgresoEntities() {
        return findEgresoEntities(true, -1, -1);
    }

    public List<Egreso> findEgresoEntities(int maxResults, int firstResult) {
        return findEgresoEntities(false, maxResults, firstResult);
    }

    private List<Egreso> findEgresoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Egreso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Egreso findEgreso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Egreso.class, id);
        } finally {
            em.close();
        }
    }

    public int getEgresoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Egreso> rt = cq.from(Egreso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
