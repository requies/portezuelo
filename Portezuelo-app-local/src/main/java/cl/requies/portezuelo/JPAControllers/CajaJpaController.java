/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Caja;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Venta;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Maquina;
import cl.requies.portezuelo.entities.Ingreso;
import java.util.ArrayList;
import java.util.List;
import cl.requies.portezuelo.entities.Egreso;
import cl.requies.portezuelo.entities.VentaAnulada;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class CajaJpaController implements Serializable {

    public CajaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Caja caja) {
        if (caja.getIngresoList() == null) {
            caja.setIngresoList(new ArrayList<Ingreso>());
        }
        if (caja.getEgresoList() == null) {
            caja.setEgresoList(new ArrayList<Egreso>());
        }
        if (caja.getVentaAnuladaList() == null) {
            caja.setVentaAnuladaList(new ArrayList<VentaAnulada>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = caja.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                caja.setUserId(userId);
            }
            Maquina maquinaId = caja.getMaquinaId();
            if (maquinaId != null) {
                maquinaId = em.getReference(maquinaId.getClass(), maquinaId.getId());
                caja.setMaquinaId(maquinaId);
            }
            List<Ingreso> attachedIngresoList = new ArrayList<Ingreso>();
            for (Ingreso ingresoListIngresoToAttach : caja.getIngresoList()) {
                ingresoListIngresoToAttach = em.getReference(ingresoListIngresoToAttach.getClass(), ingresoListIngresoToAttach.getId());
                attachedIngresoList.add(ingresoListIngresoToAttach);
            }
            caja.setIngresoList(attachedIngresoList);
            List<Egreso> attachedEgresoList = new ArrayList<Egreso>();
            for (Egreso egresoListEgresoToAttach : caja.getEgresoList()) {
                egresoListEgresoToAttach = em.getReference(egresoListEgresoToAttach.getClass(), egresoListEgresoToAttach.getId());
                attachedEgresoList.add(egresoListEgresoToAttach);
            }
            caja.setEgresoList(attachedEgresoList);
            List<VentaAnulada> attachedVentaAnuladaList = new ArrayList<VentaAnulada>();
            for (VentaAnulada ventaAnuladaListVentaAnuladaToAttach : caja.getVentaAnuladaList()) {
                ventaAnuladaListVentaAnuladaToAttach = em.getReference(ventaAnuladaListVentaAnuladaToAttach.getClass(), ventaAnuladaListVentaAnuladaToAttach.getVentaId());
                attachedVentaAnuladaList.add(ventaAnuladaListVentaAnuladaToAttach);
            }
            caja.setVentaAnuladaList(attachedVentaAnuladaList);
            em.persist(caja);
            if (userId != null) {
                userId.getCajaList().add(caja);
                userId = em.merge(userId);
            }
            if (maquinaId != null) {
                maquinaId.getCajaList().add(caja);
                maquinaId = em.merge(maquinaId);
            }
            for (Ingreso ingresoListIngreso : caja.getIngresoList()) {
                Caja oldCajaIdOfIngresoListIngreso = ingresoListIngreso.getCajaId();
                ingresoListIngreso.setCajaId(caja);
                ingresoListIngreso = em.merge(ingresoListIngreso);
                if (oldCajaIdOfIngresoListIngreso != null) {
                    oldCajaIdOfIngresoListIngreso.getIngresoList().remove(ingresoListIngreso);
                    oldCajaIdOfIngresoListIngreso = em.merge(oldCajaIdOfIngresoListIngreso);
                }
            }
            for (Egreso egresoListEgreso : caja.getEgresoList()) {
                Caja oldCajaIdOfEgresoListEgreso = egresoListEgreso.getCajaId();
                egresoListEgreso.setCajaId(caja);
                egresoListEgreso = em.merge(egresoListEgreso);
                if (oldCajaIdOfEgresoListEgreso != null) {
                    oldCajaIdOfEgresoListEgreso.getEgresoList().remove(egresoListEgreso);
                    oldCajaIdOfEgresoListEgreso = em.merge(oldCajaIdOfEgresoListEgreso);
                }
            }
            for (VentaAnulada ventaAnuladaListVentaAnulada : caja.getVentaAnuladaList()) {
                Caja oldCajaIdOfVentaAnuladaListVentaAnulada = ventaAnuladaListVentaAnulada.getCajaId();
                ventaAnuladaListVentaAnulada.setCajaId(caja);
                ventaAnuladaListVentaAnulada = em.merge(ventaAnuladaListVentaAnulada);
                if (oldCajaIdOfVentaAnuladaListVentaAnulada != null) {
                    oldCajaIdOfVentaAnuladaListVentaAnulada.getVentaAnuladaList().remove(ventaAnuladaListVentaAnulada);
                    oldCajaIdOfVentaAnuladaListVentaAnulada = em.merge(oldCajaIdOfVentaAnuladaListVentaAnulada);
                }
            }
            
            em.flush(); //Para obtener el id de la caja actualizado
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Caja caja) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja persistentCaja = em.find(Caja.class, caja.getId());
            Users userIdOld = persistentCaja.getUserId();
            Users userIdNew = caja.getUserId();
            Maquina maquinaIdOld = persistentCaja.getMaquinaId();
            Maquina maquinaIdNew = caja.getMaquinaId();
            List<Ingreso> ingresoListOld = persistentCaja.getIngresoList();
            List<Ingreso> ingresoListNew = caja.getIngresoList();
            List<Egreso> egresoListOld = persistentCaja.getEgresoList();
            List<Egreso> egresoListNew = caja.getEgresoList();
            List<VentaAnulada> ventaAnuladaListOld = persistentCaja.getVentaAnuladaList();
            List<VentaAnulada> ventaAnuladaListNew = caja.getVentaAnuladaList();
            List<String> illegalOrphanMessages = null;
            for (Ingreso ingresoListOldIngreso : ingresoListOld) {
                if (!ingresoListNew.contains(ingresoListOldIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ingreso " + ingresoListOldIngreso + " since its cajaId field is not nullable.");
                }
            }
            for (Egreso egresoListOldEgreso : egresoListOld) {
                if (!egresoListNew.contains(egresoListOldEgreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Egreso " + egresoListOldEgreso + " since its cajaId field is not nullable.");
                }
            }
            for (VentaAnulada ventaAnuladaListOldVentaAnulada : ventaAnuladaListOld) {
                if (!ventaAnuladaListNew.contains(ventaAnuladaListOldVentaAnulada)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentaAnulada " + ventaAnuladaListOldVentaAnulada + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                caja.setUserId(userIdNew);
            }
            if (maquinaIdNew != null) {
                maquinaIdNew = em.getReference(maquinaIdNew.getClass(), maquinaIdNew.getId());
                caja.setMaquinaId(maquinaIdNew);
            }
            List<Ingreso> attachedIngresoListNew = new ArrayList<Ingreso>();
            for (Ingreso ingresoListNewIngresoToAttach : ingresoListNew) {
                ingresoListNewIngresoToAttach = em.getReference(ingresoListNewIngresoToAttach.getClass(), ingresoListNewIngresoToAttach.getId());
                attachedIngresoListNew.add(ingresoListNewIngresoToAttach);
            }
            ingresoListNew = attachedIngresoListNew;
            caja.setIngresoList(ingresoListNew);
            List<Egreso> attachedEgresoListNew = new ArrayList<Egreso>();
            for (Egreso egresoListNewEgresoToAttach : egresoListNew) {
                egresoListNewEgresoToAttach = em.getReference(egresoListNewEgresoToAttach.getClass(), egresoListNewEgresoToAttach.getId());
                attachedEgresoListNew.add(egresoListNewEgresoToAttach);
            }
            egresoListNew = attachedEgresoListNew;
            caja.setEgresoList(egresoListNew);
            List<VentaAnulada> attachedVentaAnuladaListNew = new ArrayList<VentaAnulada>();
            for (VentaAnulada ventaAnuladaListNewVentaAnuladaToAttach : ventaAnuladaListNew) {
                ventaAnuladaListNewVentaAnuladaToAttach = em.getReference(ventaAnuladaListNewVentaAnuladaToAttach.getClass(), ventaAnuladaListNewVentaAnuladaToAttach.getVentaId());
                attachedVentaAnuladaListNew.add(ventaAnuladaListNewVentaAnuladaToAttach);
            }
            ventaAnuladaListNew = attachedVentaAnuladaListNew;
            caja.setVentaAnuladaList(ventaAnuladaListNew);
            caja = em.merge(caja);
            
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getCajaList().remove(caja);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getCajaList().add(caja);
                userIdNew = em.merge(userIdNew);
            }
            if (maquinaIdOld != null && !maquinaIdOld.equals(maquinaIdNew)) {
                maquinaIdOld.getCajaList().remove(caja);
                maquinaIdOld = em.merge(maquinaIdOld);
            }
            if (maquinaIdNew != null && !maquinaIdNew.equals(maquinaIdOld)) {
                maquinaIdNew.getCajaList().add(caja);
                maquinaIdNew = em.merge(maquinaIdNew);
            }
            for (Ingreso ingresoListNewIngreso : ingresoListNew) {
                if (!ingresoListOld.contains(ingresoListNewIngreso)) {
                    Caja oldCajaIdOfIngresoListNewIngreso = ingresoListNewIngreso.getCajaId();
                    ingresoListNewIngreso.setCajaId(caja);
                    ingresoListNewIngreso = em.merge(ingresoListNewIngreso);
                    if (oldCajaIdOfIngresoListNewIngreso != null && !oldCajaIdOfIngresoListNewIngreso.equals(caja)) {
                        oldCajaIdOfIngresoListNewIngreso.getIngresoList().remove(ingresoListNewIngreso);
                        oldCajaIdOfIngresoListNewIngreso = em.merge(oldCajaIdOfIngresoListNewIngreso);
                    }
                }
            }
            for (Egreso egresoListNewEgreso : egresoListNew) {
                if (!egresoListOld.contains(egresoListNewEgreso)) {
                    Caja oldCajaIdOfEgresoListNewEgreso = egresoListNewEgreso.getCajaId();
                    egresoListNewEgreso.setCajaId(caja);
                    egresoListNewEgreso = em.merge(egresoListNewEgreso);
                    if (oldCajaIdOfEgresoListNewEgreso != null && !oldCajaIdOfEgresoListNewEgreso.equals(caja)) {
                        oldCajaIdOfEgresoListNewEgreso.getEgresoList().remove(egresoListNewEgreso);
                        oldCajaIdOfEgresoListNewEgreso = em.merge(oldCajaIdOfEgresoListNewEgreso);
                    }
                }
            }
            for (VentaAnulada ventaAnuladaListNewVentaAnulada : ventaAnuladaListNew) {
                if (!ventaAnuladaListOld.contains(ventaAnuladaListNewVentaAnulada)) {
                    Caja oldUserIdOfVentaAnuladaListNewVentaAnulada = ventaAnuladaListNewVentaAnulada.getCajaId();
                    ventaAnuladaListNewVentaAnulada.setCajaId(caja);
                    ventaAnuladaListNewVentaAnulada = em.merge(ventaAnuladaListNewVentaAnulada);
                    if (oldUserIdOfVentaAnuladaListNewVentaAnulada != null && !oldUserIdOfVentaAnuladaListNewVentaAnulada.equals(caja)) {
                        oldUserIdOfVentaAnuladaListNewVentaAnulada.getVentaAnuladaList().remove(ventaAnuladaListNewVentaAnulada);
                        oldUserIdOfVentaAnuladaListNewVentaAnulada = em.merge(oldUserIdOfVentaAnuladaListNewVentaAnulada);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = caja.getId();
                if (findCaja(id) == null) {
                    throw new NonexistentEntityException("The caja with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja caja;
            try {
                caja = em.getReference(Caja.class, id);
                caja.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The caja with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Ingreso> ingresoListOrphanCheck = caja.getIngresoList();
            for (Ingreso ingresoListOrphanCheckIngreso : ingresoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Caja (" + caja + ") cannot be destroyed since the Ingreso " + ingresoListOrphanCheckIngreso + " in its ingresoList field has a non-nullable cajaId field.");
            }
            List<Egreso> egresoListOrphanCheck = caja.getEgresoList();
            for (Egreso egresoListOrphanCheckEgreso : egresoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Caja (" + caja + ") cannot be destroyed since the Egreso " + egresoListOrphanCheckEgreso + " in its egresoList field has a non-nullable cajaId field.");
            }
            List<VentaAnulada> ventaAnuladaListOrphanCheck = caja.getVentaAnuladaList();
            for (VentaAnulada ventaAnuladaListOrphanCheckVentaAnulada : ventaAnuladaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Caja (" + caja + ") cannot be destroyed since the VentaAnulada " + ventaAnuladaListOrphanCheckVentaAnulada + " in its ventaAnuladaList field has a non-nullable cajaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = caja.getUserId();
            if (userId != null) {
                userId.getCajaList().remove(caja);
                userId = em.merge(userId);
            }
            Maquina maquinaId = caja.getMaquinaId();
            if (maquinaId != null) {
                maquinaId.getCajaList().remove(caja);
                maquinaId = em.merge(maquinaId);
            }
            em.remove(caja);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Caja> findCajaEntities() {
        return findCajaEntities(true, -1, -1);
    }

    public List<Caja> findCajaEntities(int maxResults, int firstResult) {
        return findCajaEntities(false, maxResults, firstResult);
    }

    private List<Caja> findCajaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Caja.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Caja findCaja(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Caja.class, id);
        } finally {
            em.close();
        }
    }

    public int getCajaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Caja> rt = cq.from(Caja.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
