/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import cl.requies.portezuelo.entities.Inventario;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Pedido;
import cl.requies.portezuelo.entities.Traspaso;
import cl.requies.portezuelo.entities.Asistencia;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Users_;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author victor
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) {
        if (users.getInventarioList() == null) {
            users.setInventarioList(new ArrayList<Inventario>());
        }
        if (users.getCajaList() == null) {
            users.setCajaList(new ArrayList<Caja>());
        }
        if (users.getPedidoList() == null) {
            users.setPedidoList(new ArrayList<Pedido>());
        }
        if (users.getTraspasoList() == null) {
            users.setTraspasoList(new ArrayList<Traspaso>());
        }
        if (users.getAsistenciaList() == null) {
            users.setAsistenciaList(new ArrayList<Asistencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Inventario> attachedInventarioList = new ArrayList<Inventario>();
            for (Inventario inventarioListInventarioToAttach : users.getInventarioList()) {
                inventarioListInventarioToAttach = em.getReference(inventarioListInventarioToAttach.getClass(), inventarioListInventarioToAttach.getId());
                attachedInventarioList.add(inventarioListInventarioToAttach);
            }
            users.setInventarioList(attachedInventarioList);
            List<Caja> attachedCajaList = new ArrayList<Caja>();
            for (Caja cajaListCajaToAttach : users.getCajaList()) {
                cajaListCajaToAttach = em.getReference(cajaListCajaToAttach.getClass(), cajaListCajaToAttach.getId());
                attachedCajaList.add(cajaListCajaToAttach);
            }
            users.setCajaList(attachedCajaList);
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : users.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            users.setPedidoList(attachedPedidoList);
            List<Traspaso> attachedTraspasoList = new ArrayList<Traspaso>();
            for (Traspaso traspasoListTraspasoToAttach : users.getTraspasoList()) {
                traspasoListTraspasoToAttach = em.getReference(traspasoListTraspasoToAttach.getClass(), traspasoListTraspasoToAttach.getId());
                attachedTraspasoList.add(traspasoListTraspasoToAttach);
            }
            users.setTraspasoList(attachedTraspasoList);
            List<Asistencia> attachedAsistenciaList = new ArrayList<Asistencia>();
            for (Asistencia asistenciaListAsistenciaToAttach : users.getAsistenciaList()) {
                asistenciaListAsistenciaToAttach = em.getReference(asistenciaListAsistenciaToAttach.getClass(), asistenciaListAsistenciaToAttach.getId());
                attachedAsistenciaList.add(asistenciaListAsistenciaToAttach);
            }
            users.setAsistenciaList(attachedAsistenciaList);
            em.persist(users);
            for (Inventario inventarioListInventario : users.getInventarioList()) {
                Users oldUserIdOfInventarioListInventario = inventarioListInventario.getUserId();
                inventarioListInventario.setUserId(users);
                inventarioListInventario = em.merge(inventarioListInventario);
                if (oldUserIdOfInventarioListInventario != null) {
                    oldUserIdOfInventarioListInventario.getInventarioList().remove(inventarioListInventario);
                    oldUserIdOfInventarioListInventario = em.merge(oldUserIdOfInventarioListInventario);
                }
            }
            for (Caja cajaListCaja : users.getCajaList()) {
                Users oldUserIdOfCajaListCaja = cajaListCaja.getUserId();
                cajaListCaja.setUserId(users);
                cajaListCaja = em.merge(cajaListCaja);
                if (oldUserIdOfCajaListCaja != null) {
                    oldUserIdOfCajaListCaja.getCajaList().remove(cajaListCaja);
                    oldUserIdOfCajaListCaja = em.merge(oldUserIdOfCajaListCaja);
                }
            }
            for (Pedido pedidoListPedido : users.getPedidoList()) {
                Users oldUserIdOfPedidoListPedido = pedidoListPedido.getUserId();
                pedidoListPedido.setUserId(users);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldUserIdOfPedidoListPedido != null) {
                    oldUserIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldUserIdOfPedidoListPedido = em.merge(oldUserIdOfPedidoListPedido);
                }
            }
            for (Traspaso traspasoListTraspaso : users.getTraspasoList()) {
                Users oldUserIdOfTraspasoListTraspaso = traspasoListTraspaso.getUserId();
                traspasoListTraspaso.setUserId(users);
                traspasoListTraspaso = em.merge(traspasoListTraspaso);
                if (oldUserIdOfTraspasoListTraspaso != null) {
                    oldUserIdOfTraspasoListTraspaso.getTraspasoList().remove(traspasoListTraspaso);
                    oldUserIdOfTraspasoListTraspaso = em.merge(oldUserIdOfTraspasoListTraspaso);
                }
            }
            for (Asistencia asistenciaListAsistencia : users.getAsistenciaList()) {
                Users oldUserIdOfAsistenciaListAsistencia = asistenciaListAsistencia.getUserId();
                asistenciaListAsistencia.setUserId(users);
                asistenciaListAsistencia = em.merge(asistenciaListAsistencia);
                if (oldUserIdOfAsistenciaListAsistencia != null) {
                    oldUserIdOfAsistenciaListAsistencia.getAsistenciaList().remove(asistenciaListAsistencia);
                    oldUserIdOfAsistenciaListAsistencia = em.merge(oldUserIdOfAsistenciaListAsistencia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getId());
            List<Inventario> inventarioListOld = persistentUsers.getInventarioList();
            List<Inventario> inventarioListNew = users.getInventarioList();
            List<Caja> cajaListOld = persistentUsers.getCajaList();
            List<Caja> cajaListNew = users.getCajaList();
            List<Pedido> pedidoListOld = persistentUsers.getPedidoList();
            List<Pedido> pedidoListNew = users.getPedidoList();
            List<Traspaso> traspasoListOld = persistentUsers.getTraspasoList();
            List<Traspaso> traspasoListNew = users.getTraspasoList();
            List<Asistencia> asistenciaListOld = persistentUsers.getAsistenciaList();
            List<Asistencia> asistenciaListNew = users.getAsistenciaList();
            List<String> illegalOrphanMessages = null;
            for (Caja cajaListOldCaja : cajaListOld) {
                if (!cajaListNew.contains(cajaListOldCaja)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Caja " + cajaListOldCaja + " since its userId field is not nullable.");
                }
            }
            for (Asistencia asistenciaListOldAsistencia : asistenciaListOld) {
                if (!asistenciaListNew.contains(asistenciaListOldAsistencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Asistencia " + asistenciaListOldAsistencia + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            
            List<Inventario> attachedInventarioListNew = new ArrayList<Inventario>();
            for (Inventario inventarioListNewInventarioToAttach : inventarioListNew) {
                inventarioListNewInventarioToAttach = em.getReference(inventarioListNewInventarioToAttach.getClass(), inventarioListNewInventarioToAttach.getId());
                attachedInventarioListNew.add(inventarioListNewInventarioToAttach);
            }
            inventarioListNew = attachedInventarioListNew;
            users.setInventarioList(inventarioListNew);
            List<Caja> attachedCajaListNew = new ArrayList<Caja>();
            for (Caja cajaListNewCajaToAttach : cajaListNew) {
                cajaListNewCajaToAttach = em.getReference(cajaListNewCajaToAttach.getClass(), cajaListNewCajaToAttach.getId());
                attachedCajaListNew.add(cajaListNewCajaToAttach);
            }
            cajaListNew = attachedCajaListNew;
            users.setCajaList(cajaListNew);
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            users.setPedidoList(pedidoListNew);
            List<Traspaso> attachedTraspasoListNew = new ArrayList<Traspaso>();
            for (Traspaso traspasoListNewTraspasoToAttach : traspasoListNew) {
                traspasoListNewTraspasoToAttach = em.getReference(traspasoListNewTraspasoToAttach.getClass(), traspasoListNewTraspasoToAttach.getId());
                attachedTraspasoListNew.add(traspasoListNewTraspasoToAttach);
            }
            traspasoListNew = attachedTraspasoListNew;
            users.setTraspasoList(traspasoListNew);
            
            List<Asistencia> attachedAsistenciaListNew = new ArrayList<Asistencia>();
            for (Asistencia asistenciaListNewAsistenciaToAttach : asistenciaListNew) {
                asistenciaListNewAsistenciaToAttach = em.getReference(asistenciaListNewAsistenciaToAttach.getClass(), asistenciaListNewAsistenciaToAttach.getId());
                attachedAsistenciaListNew.add(asistenciaListNewAsistenciaToAttach);
            }
            asistenciaListNew = attachedAsistenciaListNew;
            users.setAsistenciaList(asistenciaListNew);
            users = em.merge(users);
            for (Inventario inventarioListOldInventario : inventarioListOld) {
                if (!inventarioListNew.contains(inventarioListOldInventario)) {
                    inventarioListOldInventario.setUserId(null);
                    inventarioListOldInventario = em.merge(inventarioListOldInventario);
                }
            }
            for (Inventario inventarioListNewInventario : inventarioListNew) {
                if (!inventarioListOld.contains(inventarioListNewInventario)) {
                    Users oldUserIdOfInventarioListNewInventario = inventarioListNewInventario.getUserId();
                    inventarioListNewInventario.setUserId(users);
                    inventarioListNewInventario = em.merge(inventarioListNewInventario);
                    if (oldUserIdOfInventarioListNewInventario != null && !oldUserIdOfInventarioListNewInventario.equals(users)) {
                        oldUserIdOfInventarioListNewInventario.getInventarioList().remove(inventarioListNewInventario);
                        oldUserIdOfInventarioListNewInventario = em.merge(oldUserIdOfInventarioListNewInventario);
                    }
                }
            }
            for (Caja cajaListNewCaja : cajaListNew) {
                if (!cajaListOld.contains(cajaListNewCaja)) {
                    Users oldUserIdOfCajaListNewCaja = cajaListNewCaja.getUserId();
                    cajaListNewCaja.setUserId(users);
                    cajaListNewCaja = em.merge(cajaListNewCaja);
                    if (oldUserIdOfCajaListNewCaja != null && !oldUserIdOfCajaListNewCaja.equals(users)) {
                        oldUserIdOfCajaListNewCaja.getCajaList().remove(cajaListNewCaja);
                        oldUserIdOfCajaListNewCaja = em.merge(oldUserIdOfCajaListNewCaja);
                    }
                }
            }
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    pedidoListOldPedido.setUserId(null);
                    pedidoListOldPedido = em.merge(pedidoListOldPedido);
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Users oldUserIdOfPedidoListNewPedido = pedidoListNewPedido.getUserId();
                    pedidoListNewPedido.setUserId(users);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldUserIdOfPedidoListNewPedido != null && !oldUserIdOfPedidoListNewPedido.equals(users)) {
                        oldUserIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldUserIdOfPedidoListNewPedido = em.merge(oldUserIdOfPedidoListNewPedido);
                    }
                }
            }
            for (Traspaso traspasoListOldTraspaso : traspasoListOld) {
                if (!traspasoListNew.contains(traspasoListOldTraspaso)) {
                    traspasoListOldTraspaso.setUserId(null);
                    traspasoListOldTraspaso = em.merge(traspasoListOldTraspaso);
                }
            }
            for (Traspaso traspasoListNewTraspaso : traspasoListNew) {
                if (!traspasoListOld.contains(traspasoListNewTraspaso)) {
                    Users oldUserIdOfTraspasoListNewTraspaso = traspasoListNewTraspaso.getUserId();
                    traspasoListNewTraspaso.setUserId(users);
                    traspasoListNewTraspaso = em.merge(traspasoListNewTraspaso);
                    if (oldUserIdOfTraspasoListNewTraspaso != null && !oldUserIdOfTraspasoListNewTraspaso.equals(users)) {
                        oldUserIdOfTraspasoListNewTraspaso.getTraspasoList().remove(traspasoListNewTraspaso);
                        oldUserIdOfTraspasoListNewTraspaso = em.merge(oldUserIdOfTraspasoListNewTraspaso);
                    }
                }
            }
            for (Asistencia asistenciaListNewAsistencia : asistenciaListNew) {
                if (!asistenciaListOld.contains(asistenciaListNewAsistencia)) {
                    Users oldUserIdOfAsistenciaListNewAsistencia = asistenciaListNewAsistencia.getUserId();
                    asistenciaListNewAsistencia.setUserId(users);
                    asistenciaListNewAsistencia = em.merge(asistenciaListNewAsistencia);
                    if (oldUserIdOfAsistenciaListNewAsistencia != null && !oldUserIdOfAsistenciaListNewAsistencia.equals(users)) {
                        oldUserIdOfAsistenciaListNewAsistencia.getAsistenciaList().remove(asistenciaListNewAsistencia);
                        oldUserIdOfAsistenciaListNewAsistencia = em.merge(oldUserIdOfAsistenciaListNewAsistencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Caja> cajaListOrphanCheck = users.getCajaList();
            for (Caja cajaListOrphanCheckCaja : cajaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Caja " + cajaListOrphanCheckCaja + " in its cajaList field has a non-nullable userId field.");
            }
            List<Asistencia> asistenciaListOrphanCheck = users.getAsistenciaList();
            for (Asistencia asistenciaListOrphanCheckAsistencia : asistenciaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Asistencia " + asistenciaListOrphanCheckAsistencia + " in its asistenciaList field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Inventario> inventarioList = users.getInventarioList();
            for (Inventario inventarioListInventario : inventarioList) {
                inventarioListInventario.setUserId(null);
                inventarioListInventario = em.merge(inventarioListInventario);
            }
            List<Pedido> pedidoList = users.getPedidoList();
            for (Pedido pedidoListPedido : pedidoList) {
                pedidoListPedido.setUserId(null);
                pedidoListPedido = em.merge(pedidoListPedido);
            }
            List<Traspaso> traspasoList = users.getTraspasoList();
            for (Traspaso traspasoListTraspaso : traspasoList) {
                traspasoListTraspaso.setUserId(null);
                traspasoListTraspaso = em.merge(traspasoListTraspaso);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            cq.select(cq.from(Users.class));
            Root<Users> usersTbl = cq.from(Users.class);
            cq.select(usersTbl);
            cq.orderBy(cb.asc(usersTbl.get(Users_.fechaIngreso)));
            
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }
    
    public Users findUserByRut(String rut) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Users.findByRut");
            q.setParameter("rut", rut);
            return (Users)q.getSingleResult();
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Users findUserAdmin() {
        EntityManager em;
        em = emf.createEntityManager();
        Query q = em.createNamedQuery("Users.findByTipoUsuario");
        q.setParameter("tipoUsuario", TipoUsuario.ADMINISTRADOR);
        try {
            List<Users> lista = (List<Users>)q.getResultList();
            if (!lista.isEmpty()) {
                return lista.get(0);
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            return null;
        }
        finally {
            em.close();
        }
    }
    
}
