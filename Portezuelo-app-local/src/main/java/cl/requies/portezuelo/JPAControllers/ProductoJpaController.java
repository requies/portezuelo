/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.TipoProducto;
import cl.requies.portezuelo.entities.Impuesto;
import cl.requies.portezuelo.entities.FamiliaProducto;
import cl.requies.portezuelo.entities.PedidoDetalle;
import java.util.ArrayList;
import java.util.List;
import cl.requies.portezuelo.entities.InventarioDetalle;
import cl.requies.portezuelo.entities.PromocionDetalle;
import cl.requies.portezuelo.entities.Merma;
import cl.requies.portezuelo.entities.Promocion;
import cl.requies.portezuelo.entities.VentaDetalle;
import cl.requies.portezuelo.entities.AlertaCambioPrecio;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Producto_;
import cl.requies.portezuelo.entities.TraspasoDetalle;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author victor
 */
public class ProductoJpaController implements Serializable {

    public ProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Producto producto) throws PreexistingEntityException, Exception {
        if (producto.getProductoDetalleList() == null) {
            producto.setProductoDetalleList(new ArrayList<PedidoDetalle>());
        }
        if (producto.getInventarioDetalleList() == null) {
            producto.setInventarioDetalleList(new ArrayList<InventarioDetalle>());
        }
        if (producto.getPromocionDetalleList() == null) {
            producto.setPromocionDetalleList(new ArrayList<PromocionDetalle>());
        }
        if (producto.getMermaList() == null) {
            producto.setMermaList(new ArrayList<Merma>());
        }
        if (producto.getPromocionList() == null) {
            producto.setPromocionList(new ArrayList<Promocion>());
        }
        if (producto.getVentaDetalleList() == null) {
            producto.setVentaDetalleList(new ArrayList<VentaDetalle>());
        }
        if (producto.getAlertaCambioPrecioList() == null) {
            producto.setAlertaCambioPrecioList(new ArrayList<AlertaCambioPrecio>());
        }
        if (producto.getTraspasoDetalleList() == null) {
            producto.setTraspasoDetalleList(new ArrayList<TraspasoDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoProducto idTipoProducto = producto.getIdTipoProducto();
            if (idTipoProducto != null) {
                idTipoProducto = em.getReference(idTipoProducto.getClass(), idTipoProducto.getId());
                producto.setIdTipoProducto(idTipoProducto);
            }
            Impuesto otrosImpuestos = producto.getOtrosImpuestos();
            if (otrosImpuestos != null) {
                otrosImpuestos = em.getReference(otrosImpuestos.getClass(), otrosImpuestos.getId());
                producto.setOtrosImpuestos(otrosImpuestos);
            }
            FamiliaProducto familia = producto.getFamilia();
            if (familia != null) {
                familia = em.getReference(familia.getClass(), familia.getId());
                producto.setFamilia(familia);
            }
            List<PedidoDetalle> attachedProductoDetalleList = new ArrayList<PedidoDetalle>();
            for (PedidoDetalle productoDetalleListPedidoDetalleToAttach : producto.getProductoDetalleList()) {
                productoDetalleListPedidoDetalleToAttach = em.getReference(productoDetalleListPedidoDetalleToAttach.getClass(), productoDetalleListPedidoDetalleToAttach.getPedidoDetallePK());
                attachedProductoDetalleList.add(productoDetalleListPedidoDetalleToAttach);
            }
            producto.setProductoDetalleList(attachedProductoDetalleList);
            List<InventarioDetalle> attachedInventarioDetalleList = new ArrayList<InventarioDetalle>();
            for (InventarioDetalle inventarioDetalleListInventarioDetalleToAttach : producto.getInventarioDetalleList()) {
                inventarioDetalleListInventarioDetalleToAttach = em.getReference(inventarioDetalleListInventarioDetalleToAttach.getClass(), inventarioDetalleListInventarioDetalleToAttach.getId());
                attachedInventarioDetalleList.add(inventarioDetalleListInventarioDetalleToAttach);
            }
            producto.setInventarioDetalleList(attachedInventarioDetalleList);
            List<PromocionDetalle> attachedPromocionDetalleList = new ArrayList<PromocionDetalle>();
            for (PromocionDetalle promocionDetalleListPromocionDetalleToAttach : producto.getPromocionDetalleList()) {
                promocionDetalleListPromocionDetalleToAttach = em.getReference(promocionDetalleListPromocionDetalleToAttach.getClass(), promocionDetalleListPromocionDetalleToAttach.getPromocionDetallePK());
                attachedPromocionDetalleList.add(promocionDetalleListPromocionDetalleToAttach);
            }
            producto.setPromocionDetalleList(attachedPromocionDetalleList);
            List<Merma> attachedMermaList = new ArrayList<Merma>();
            for (Merma mermaListMermaToAttach : producto.getMermaList()) {
                mermaListMermaToAttach = em.getReference(mermaListMermaToAttach.getClass(), mermaListMermaToAttach.getId());
                attachedMermaList.add(mermaListMermaToAttach);
            }
            producto.setMermaList(attachedMermaList);
            List<Promocion> attachedPromocionList = new ArrayList<Promocion>();
            for (Promocion promocionListPromocionToAttach : producto.getPromocionList()) {
                promocionListPromocionToAttach = em.getReference(promocionListPromocionToAttach.getClass(), promocionListPromocionToAttach.getId());
                attachedPromocionList.add(promocionListPromocionToAttach);
            }
            producto.setPromocionList(attachedPromocionList);
            List<VentaDetalle> attachedVentaDetalleList = new ArrayList<VentaDetalle>();
            for (VentaDetalle ventaDetalleListVentaDetalleToAttach : producto.getVentaDetalleList()) {
                ventaDetalleListVentaDetalleToAttach = em.getReference(ventaDetalleListVentaDetalleToAttach.getClass(), ventaDetalleListVentaDetalleToAttach.getId());
                attachedVentaDetalleList.add(ventaDetalleListVentaDetalleToAttach);
            }
            producto.setVentaDetalleList(attachedVentaDetalleList);
            List<AlertaCambioPrecio> attachedAlertaCambioPrecioList = new ArrayList<AlertaCambioPrecio>();
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecioToAttach : producto.getAlertaCambioPrecioList()) {
                alertaCambioPrecioListAlertaCambioPrecioToAttach = em.getReference(alertaCambioPrecioListAlertaCambioPrecioToAttach.getClass(), alertaCambioPrecioListAlertaCambioPrecioToAttach.getId());
                attachedAlertaCambioPrecioList.add(alertaCambioPrecioListAlertaCambioPrecioToAttach);
            }
            producto.setAlertaCambioPrecioList(attachedAlertaCambioPrecioList);
            List<TraspasoDetalle> attachedTraspasoDetalleList = new ArrayList<TraspasoDetalle>();
            for (TraspasoDetalle traspasoDetalleListTraspasoDetalleToAttach : producto.getTraspasoDetalleList()) {
                traspasoDetalleListTraspasoDetalleToAttach = em.getReference(traspasoDetalleListTraspasoDetalleToAttach.getClass(), traspasoDetalleListTraspasoDetalleToAttach.getId());
                attachedTraspasoDetalleList.add(traspasoDetalleListTraspasoDetalleToAttach);
            }
            producto.setTraspasoDetalleList(attachedTraspasoDetalleList);
            em.persist(producto);
            em.flush();
            if (idTipoProducto != null) {
                idTipoProducto.getProductoList().add(producto);
                idTipoProducto = em.merge(idTipoProducto);
            }
            if (otrosImpuestos != null) {
                otrosImpuestos.getProductoList().add(producto);
                otrosImpuestos = em.merge(otrosImpuestos);
            }
            if (familia != null) {
                familia.getProductoList().add(producto);
                familia = em.merge(familia);
            }
            for (PedidoDetalle productoDetalleListPedidoDetalle : producto.getProductoDetalleList()) {
                Producto oldProductoOfProductoDetalleListPedidoDetalle = productoDetalleListPedidoDetalle.getProducto();
                productoDetalleListPedidoDetalle.setProducto(producto);
                productoDetalleListPedidoDetalle = em.merge(productoDetalleListPedidoDetalle);
                if (oldProductoOfProductoDetalleListPedidoDetalle != null) {
                    oldProductoOfProductoDetalleListPedidoDetalle.getProductoDetalleList().remove(productoDetalleListPedidoDetalle);
                    oldProductoOfProductoDetalleListPedidoDetalle = em.merge(oldProductoOfProductoDetalleListPedidoDetalle);
                }
            }
            for (InventarioDetalle inventarioDetalleListInventarioDetalle : producto.getInventarioDetalleList()) {
                Producto oldBarcodeOfInventarioDetalleListInventarioDetalle = inventarioDetalleListInventarioDetalle.getBarcode();
                inventarioDetalleListInventarioDetalle.setBarcode(producto);
                inventarioDetalleListInventarioDetalle = em.merge(inventarioDetalleListInventarioDetalle);
                if (oldBarcodeOfInventarioDetalleListInventarioDetalle != null) {
                    oldBarcodeOfInventarioDetalleListInventarioDetalle.getInventarioDetalleList().remove(inventarioDetalleListInventarioDetalle);
                    oldBarcodeOfInventarioDetalleListInventarioDetalle = em.merge(oldBarcodeOfInventarioDetalleListInventarioDetalle);
                }
            }
            for (PromocionDetalle promocionDetalleListPromocionDetalle : producto.getPromocionDetalleList()) {
                Producto oldProductoOfPromocionDetalleListPromocionDetalle = promocionDetalleListPromocionDetalle.getProducto();
                promocionDetalleListPromocionDetalle.setProducto(producto);
                promocionDetalleListPromocionDetalle = em.merge(promocionDetalleListPromocionDetalle);
                if (oldProductoOfPromocionDetalleListPromocionDetalle != null) {
                    oldProductoOfPromocionDetalleListPromocionDetalle.getPromocionDetalleList().remove(promocionDetalleListPromocionDetalle);
                    oldProductoOfPromocionDetalleListPromocionDetalle = em.merge(oldProductoOfPromocionDetalleListPromocionDetalle);
                }
            }
            for (Merma mermaListMerma : producto.getMermaList()) {
                Producto oldBarcodeOfMermaListMerma = mermaListMerma.getBarcode();
                mermaListMerma.setBarcode(producto);
                mermaListMerma = em.merge(mermaListMerma);
                if (oldBarcodeOfMermaListMerma != null) {
                    oldBarcodeOfMermaListMerma.getMermaList().remove(mermaListMerma);
                    oldBarcodeOfMermaListMerma = em.merge(oldBarcodeOfMermaListMerma);
                }
            }
            for (Promocion promocionListPromocion : producto.getPromocionList()) {
                Producto oldBarcodeOfPromocionListPromocion = promocionListPromocion.getBarcode();
                promocionListPromocion.setBarcode(producto);
                promocionListPromocion = em.merge(promocionListPromocion);
                if (oldBarcodeOfPromocionListPromocion != null) {
                    oldBarcodeOfPromocionListPromocion.getPromocionList().remove(promocionListPromocion);
                    oldBarcodeOfPromocionListPromocion = em.merge(oldBarcodeOfPromocionListPromocion);
                }
            }
            for (VentaDetalle ventaDetalleListVentaDetalle : producto.getVentaDetalleList()) {
                Producto oldBarcodeOfVentaDetalleListVentaDetalle = ventaDetalleListVentaDetalle.getBarcode();
                ventaDetalleListVentaDetalle.setBarcode(producto);
                ventaDetalleListVentaDetalle = em.merge(ventaDetalleListVentaDetalle);
                if (oldBarcodeOfVentaDetalleListVentaDetalle != null) {
                    oldBarcodeOfVentaDetalleListVentaDetalle.getVentaDetalleList().remove(ventaDetalleListVentaDetalle);
                    oldBarcodeOfVentaDetalleListVentaDetalle = em.merge(oldBarcodeOfVentaDetalleListVentaDetalle);
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecio : producto.getAlertaCambioPrecioList()) {
                Producto oldBarcodeOfAlertaCambioPrecioListAlertaCambioPrecio = alertaCambioPrecioListAlertaCambioPrecio.getBarcode();
                alertaCambioPrecioListAlertaCambioPrecio.setBarcode(producto);
                alertaCambioPrecioListAlertaCambioPrecio = em.merge(alertaCambioPrecioListAlertaCambioPrecio);
                if (oldBarcodeOfAlertaCambioPrecioListAlertaCambioPrecio != null) {
                    oldBarcodeOfAlertaCambioPrecioListAlertaCambioPrecio.getAlertaCambioPrecioList().remove(alertaCambioPrecioListAlertaCambioPrecio);
                    oldBarcodeOfAlertaCambioPrecioListAlertaCambioPrecio = em.merge(oldBarcodeOfAlertaCambioPrecioListAlertaCambioPrecio);
                }
            }
            for (TraspasoDetalle traspasoDetalleListTraspasoDetalle : producto.getTraspasoDetalleList()) {
                Producto oldBarcodeOfTraspasoDetalleListTraspasoDetalle = traspasoDetalleListTraspasoDetalle.getBarcode();
                traspasoDetalleListTraspasoDetalle.setBarcode(producto);
                traspasoDetalleListTraspasoDetalle = em.merge(traspasoDetalleListTraspasoDetalle);
                if (oldBarcodeOfTraspasoDetalleListTraspasoDetalle != null) {
                    oldBarcodeOfTraspasoDetalleListTraspasoDetalle.getTraspasoDetalleList().remove(traspasoDetalleListTraspasoDetalle);
                    oldBarcodeOfTraspasoDetalleListTraspasoDetalle = em.merge(oldBarcodeOfTraspasoDetalleListTraspasoDetalle);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProducto(producto.getBarcode()) != null) {
                throw new PreexistingEntityException("Producto " + producto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Producto producto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto persistentProducto = em.find(Producto.class, producto.getBarcode());
            TipoProducto idTipoProductoOld = persistentProducto.getIdTipoProducto();
            TipoProducto idTipoProductoNew = producto.getIdTipoProducto();
            Impuesto otrosImpuestosOld = persistentProducto.getOtrosImpuestos();
            Impuesto otrosImpuestosNew = producto.getOtrosImpuestos();
            FamiliaProducto familiaOld = persistentProducto.getFamilia();
            FamiliaProducto familiaNew = producto.getFamilia();
            List<PedidoDetalle> productoDetalleListOld = persistentProducto.getProductoDetalleList();
            List<PedidoDetalle> productoDetalleListNew = producto.getProductoDetalleList();
            List<InventarioDetalle> inventarioDetalleListOld = persistentProducto.getInventarioDetalleList();
            List<InventarioDetalle> inventarioDetalleListNew = producto.getInventarioDetalleList();
            List<PromocionDetalle> promocionDetalleListOld = persistentProducto.getPromocionDetalleList();
            List<PromocionDetalle> promocionDetalleListNew = producto.getPromocionDetalleList();
            List<Merma> mermaListOld = persistentProducto.getMermaList();
            List<Merma> mermaListNew = producto.getMermaList();
            List<Promocion> promocionListOld = persistentProducto.getPromocionList();
            List<Promocion> promocionListNew = producto.getPromocionList();
            List<VentaDetalle> ventaDetalleListOld = persistentProducto.getVentaDetalleList();
            List<VentaDetalle> ventaDetalleListNew = producto.getVentaDetalleList();
            List<AlertaCambioPrecio> alertaCambioPrecioListOld = persistentProducto.getAlertaCambioPrecioList();
            List<AlertaCambioPrecio> alertaCambioPrecioListNew = producto.getAlertaCambioPrecioList();
            List<TraspasoDetalle> traspasoDetalleListOld = persistentProducto.getTraspasoDetalleList();
            List<TraspasoDetalle> traspasoDetalleListNew = producto.getTraspasoDetalleList();
            List<String> illegalOrphanMessages = null;
            for (PedidoDetalle productoDetalleListOldPedidoDetalle : productoDetalleListOld) {
                if (!productoDetalleListNew.contains(productoDetalleListOldPedidoDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PedidoDetalle " + productoDetalleListOldPedidoDetalle + " since its producto field is not nullable.");
                }
            }
            for (PromocionDetalle promocionDetalleListOldPromocionDetalle : promocionDetalleListOld) {
                if (!promocionDetalleListNew.contains(promocionDetalleListOldPromocionDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PromocionDetalle " + promocionDetalleListOldPromocionDetalle + " since its producto field is not nullable.");
                }
            }
            for (Promocion promocionListOldPromocion : promocionListOld) {
                if (!promocionListNew.contains(promocionListOldPromocion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Promocion " + promocionListOldPromocion + " since its barcode field is not nullable.");
                }
            }
            for (VentaDetalle ventaDetalleListOldVentaDetalle : ventaDetalleListOld) {
                if (!ventaDetalleListNew.contains(ventaDetalleListOldVentaDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentaDetalle " + ventaDetalleListOldVentaDetalle + " since its barcode field is not nullable.");
                }
            }
            for (TraspasoDetalle traspasoDetalleListOldTraspasoDetalle : traspasoDetalleListOld) {
                if (!traspasoDetalleListNew.contains(traspasoDetalleListOldTraspasoDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TraspasoDetalle " + traspasoDetalleListOldTraspasoDetalle + " since its barcode field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idTipoProductoNew != null) {
                idTipoProductoNew = em.getReference(idTipoProductoNew.getClass(), idTipoProductoNew.getId());
                producto.setIdTipoProducto(idTipoProductoNew);
            }
            if (otrosImpuestosNew != null) {
                otrosImpuestosNew = em.getReference(otrosImpuestosNew.getClass(), otrosImpuestosNew.getId());
                producto.setOtrosImpuestos(otrosImpuestosNew);
            }
            if (familiaNew != null) {
                familiaNew = em.getReference(familiaNew.getClass(), familiaNew.getId());
                producto.setFamilia(familiaNew);
            }
            List<PedidoDetalle> attachedProductoDetalleListNew = new ArrayList<PedidoDetalle>();
            for (PedidoDetalle productoDetalleListNewPedidoDetalleToAttach : productoDetalleListNew) {
                productoDetalleListNewPedidoDetalleToAttach = em.getReference(productoDetalleListNewPedidoDetalleToAttach.getClass(), productoDetalleListNewPedidoDetalleToAttach.getPedidoDetallePK());
                attachedProductoDetalleListNew.add(productoDetalleListNewPedidoDetalleToAttach);
            }
            productoDetalleListNew = attachedProductoDetalleListNew;
            producto.setProductoDetalleList(productoDetalleListNew);
            List<InventarioDetalle> attachedInventarioDetalleListNew = new ArrayList<InventarioDetalle>();
            for (InventarioDetalle inventarioDetalleListNewInventarioDetalleToAttach : inventarioDetalleListNew) {
                inventarioDetalleListNewInventarioDetalleToAttach = em.getReference(inventarioDetalleListNewInventarioDetalleToAttach.getClass(), inventarioDetalleListNewInventarioDetalleToAttach.getId());
                attachedInventarioDetalleListNew.add(inventarioDetalleListNewInventarioDetalleToAttach);
            }
            inventarioDetalleListNew = attachedInventarioDetalleListNew;
            producto.setInventarioDetalleList(inventarioDetalleListNew);
            List<PromocionDetalle> attachedPromocionDetalleListNew = new ArrayList<PromocionDetalle>();
            for (PromocionDetalle promocionDetalleListNewPromocionDetalleToAttach : promocionDetalleListNew) {
                promocionDetalleListNewPromocionDetalleToAttach = em.getReference(promocionDetalleListNewPromocionDetalleToAttach.getClass(), promocionDetalleListNewPromocionDetalleToAttach.getPromocionDetallePK());
                attachedPromocionDetalleListNew.add(promocionDetalleListNewPromocionDetalleToAttach);
            }
            promocionDetalleListNew = attachedPromocionDetalleListNew;
            producto.setPromocionDetalleList(promocionDetalleListNew);
            List<Merma> attachedMermaListNew = new ArrayList<Merma>();
            for (Merma mermaListNewMermaToAttach : mermaListNew) {
                mermaListNewMermaToAttach = em.getReference(mermaListNewMermaToAttach.getClass(), mermaListNewMermaToAttach.getId());
                attachedMermaListNew.add(mermaListNewMermaToAttach);
            }
            mermaListNew = attachedMermaListNew;
            producto.setMermaList(mermaListNew);
            List<Promocion> attachedPromocionListNew = new ArrayList<Promocion>();
            for (Promocion promocionListNewPromocionToAttach : promocionListNew) {
                promocionListNewPromocionToAttach = em.getReference(promocionListNewPromocionToAttach.getClass(), promocionListNewPromocionToAttach.getId());
                attachedPromocionListNew.add(promocionListNewPromocionToAttach);
            }
            promocionListNew = attachedPromocionListNew;
            producto.setPromocionList(promocionListNew);
            List<VentaDetalle> attachedVentaDetalleListNew = new ArrayList<VentaDetalle>();
            for (VentaDetalle ventaDetalleListNewVentaDetalleToAttach : ventaDetalleListNew) {
                ventaDetalleListNewVentaDetalleToAttach = em.getReference(ventaDetalleListNewVentaDetalleToAttach.getClass(), ventaDetalleListNewVentaDetalleToAttach.getId());
                attachedVentaDetalleListNew.add(ventaDetalleListNewVentaDetalleToAttach);
            }
            ventaDetalleListNew = attachedVentaDetalleListNew;
            producto.setVentaDetalleList(ventaDetalleListNew);
            List<AlertaCambioPrecio> attachedAlertaCambioPrecioListNew = new ArrayList<AlertaCambioPrecio>();
            for (AlertaCambioPrecio alertaCambioPrecioListNewAlertaCambioPrecioToAttach : alertaCambioPrecioListNew) {
                alertaCambioPrecioListNewAlertaCambioPrecioToAttach = em.getReference(alertaCambioPrecioListNewAlertaCambioPrecioToAttach.getClass(), alertaCambioPrecioListNewAlertaCambioPrecioToAttach.getId());
                attachedAlertaCambioPrecioListNew.add(alertaCambioPrecioListNewAlertaCambioPrecioToAttach);
            }
            alertaCambioPrecioListNew = attachedAlertaCambioPrecioListNew;
            producto.setAlertaCambioPrecioList(alertaCambioPrecioListNew);
            List<TraspasoDetalle> attachedTraspasoDetalleListNew = new ArrayList<TraspasoDetalle>();
            for (TraspasoDetalle traspasoDetalleListNewTraspasoDetalleToAttach : traspasoDetalleListNew) {
                traspasoDetalleListNewTraspasoDetalleToAttach = em.getReference(traspasoDetalleListNewTraspasoDetalleToAttach.getClass(), traspasoDetalleListNewTraspasoDetalleToAttach.getId());
                attachedTraspasoDetalleListNew.add(traspasoDetalleListNewTraspasoDetalleToAttach);
            }
            traspasoDetalleListNew = attachedTraspasoDetalleListNew;
            producto.setTraspasoDetalleList(traspasoDetalleListNew);
            producto = em.merge(producto);
            if (idTipoProductoOld != null && !idTipoProductoOld.equals(idTipoProductoNew)) {
                idTipoProductoOld.getProductoList().remove(producto);
                idTipoProductoOld = em.merge(idTipoProductoOld);
            }
            if (idTipoProductoNew != null && !idTipoProductoNew.equals(idTipoProductoOld)) {
                idTipoProductoNew.getProductoList().add(producto);
                idTipoProductoNew = em.merge(idTipoProductoNew);
            }
            if (otrosImpuestosOld != null && !otrosImpuestosOld.equals(otrosImpuestosNew)) {
                otrosImpuestosOld.getProductoList().remove(producto);
                otrosImpuestosOld = em.merge(otrosImpuestosOld);
            }
            if (otrosImpuestosNew != null && !otrosImpuestosNew.equals(otrosImpuestosOld)) {
                otrosImpuestosNew.getProductoList().add(producto);
                otrosImpuestosNew = em.merge(otrosImpuestosNew);
            }
            if (familiaOld != null && !familiaOld.equals(familiaNew)) {
                familiaOld.getProductoList().remove(producto);
                familiaOld = em.merge(familiaOld);
            }
            if (familiaNew != null && !familiaNew.equals(familiaOld)) {
                familiaNew.getProductoList().add(producto);
                familiaNew = em.merge(familiaNew);
            }
            for (PedidoDetalle productoDetalleListNewPedidoDetalle : productoDetalleListNew) {
                if (!productoDetalleListOld.contains(productoDetalleListNewPedidoDetalle)) {
                    Producto oldProductoOfProductoDetalleListNewPedidoDetalle = productoDetalleListNewPedidoDetalle.getProducto();
                    productoDetalleListNewPedidoDetalle.setProducto(producto);
                    productoDetalleListNewPedidoDetalle = em.merge(productoDetalleListNewPedidoDetalle);
                    if (oldProductoOfProductoDetalleListNewPedidoDetalle != null && !oldProductoOfProductoDetalleListNewPedidoDetalle.equals(producto)) {
                        oldProductoOfProductoDetalleListNewPedidoDetalle.getProductoDetalleList().remove(productoDetalleListNewPedidoDetalle);
                        oldProductoOfProductoDetalleListNewPedidoDetalle = em.merge(oldProductoOfProductoDetalleListNewPedidoDetalle);
                    }
                }
            }
            for (InventarioDetalle inventarioDetalleListOldInventarioDetalle : inventarioDetalleListOld) {
                if (!inventarioDetalleListNew.contains(inventarioDetalleListOldInventarioDetalle)) {
                    inventarioDetalleListOldInventarioDetalle.setBarcode(null);
                    inventarioDetalleListOldInventarioDetalle = em.merge(inventarioDetalleListOldInventarioDetalle);
                }
            }
            for (InventarioDetalle inventarioDetalleListNewInventarioDetalle : inventarioDetalleListNew) {
                if (!inventarioDetalleListOld.contains(inventarioDetalleListNewInventarioDetalle)) {
                    Producto oldBarcodeOfInventarioDetalleListNewInventarioDetalle = inventarioDetalleListNewInventarioDetalle.getBarcode();
                    inventarioDetalleListNewInventarioDetalle.setBarcode(producto);
                    inventarioDetalleListNewInventarioDetalle = em.merge(inventarioDetalleListNewInventarioDetalle);
                    if (oldBarcodeOfInventarioDetalleListNewInventarioDetalle != null && !oldBarcodeOfInventarioDetalleListNewInventarioDetalle.equals(producto)) {
                        oldBarcodeOfInventarioDetalleListNewInventarioDetalle.getInventarioDetalleList().remove(inventarioDetalleListNewInventarioDetalle);
                        oldBarcodeOfInventarioDetalleListNewInventarioDetalle = em.merge(oldBarcodeOfInventarioDetalleListNewInventarioDetalle);
                    }
                }
            }
            for (PromocionDetalle promocionDetalleListNewPromocionDetalle : promocionDetalleListNew) {
                if (!promocionDetalleListOld.contains(promocionDetalleListNewPromocionDetalle)) {
                    Producto oldProductoOfPromocionDetalleListNewPromocionDetalle = promocionDetalleListNewPromocionDetalle.getProducto();
                    promocionDetalleListNewPromocionDetalle.setProducto(producto);
                    promocionDetalleListNewPromocionDetalle = em.merge(promocionDetalleListNewPromocionDetalle);
                    if (oldProductoOfPromocionDetalleListNewPromocionDetalle != null && !oldProductoOfPromocionDetalleListNewPromocionDetalle.equals(producto)) {
                        oldProductoOfPromocionDetalleListNewPromocionDetalle.getPromocionDetalleList().remove(promocionDetalleListNewPromocionDetalle);
                        oldProductoOfPromocionDetalleListNewPromocionDetalle = em.merge(oldProductoOfPromocionDetalleListNewPromocionDetalle);
                    }
                }
            }
            for (Merma mermaListOldMerma : mermaListOld) {
                if (!mermaListNew.contains(mermaListOldMerma)) {
                    mermaListOldMerma.setBarcode(null);
                    mermaListOldMerma = em.merge(mermaListOldMerma);
                }
            }
            for (Merma mermaListNewMerma : mermaListNew) {
                if (!mermaListOld.contains(mermaListNewMerma)) {
                    Producto oldBarcodeOfMermaListNewMerma = mermaListNewMerma.getBarcode();
                    mermaListNewMerma.setBarcode(producto);
                    mermaListNewMerma = em.merge(mermaListNewMerma);
                    if (oldBarcodeOfMermaListNewMerma != null && !oldBarcodeOfMermaListNewMerma.equals(producto)) {
                        oldBarcodeOfMermaListNewMerma.getMermaList().remove(mermaListNewMerma);
                        oldBarcodeOfMermaListNewMerma = em.merge(oldBarcodeOfMermaListNewMerma);
                    }
                }
            }
            for (Promocion promocionListNewPromocion : promocionListNew) {
                if (!promocionListOld.contains(promocionListNewPromocion)) {
                    Producto oldBarcodeOfPromocionListNewPromocion = promocionListNewPromocion.getBarcode();
                    promocionListNewPromocion.setBarcode(producto);
                    promocionListNewPromocion = em.merge(promocionListNewPromocion);
                    if (oldBarcodeOfPromocionListNewPromocion != null && !oldBarcodeOfPromocionListNewPromocion.equals(producto)) {
                        oldBarcodeOfPromocionListNewPromocion.getPromocionList().remove(promocionListNewPromocion);
                        oldBarcodeOfPromocionListNewPromocion = em.merge(oldBarcodeOfPromocionListNewPromocion);
                    }
                }
            }
            for (VentaDetalle ventaDetalleListNewVentaDetalle : ventaDetalleListNew) {
                if (!ventaDetalleListOld.contains(ventaDetalleListNewVentaDetalle)) {
                    Producto oldBarcodeOfVentaDetalleListNewVentaDetalle = ventaDetalleListNewVentaDetalle.getBarcode();
                    ventaDetalleListNewVentaDetalle.setBarcode(producto);
                    ventaDetalleListNewVentaDetalle = em.merge(ventaDetalleListNewVentaDetalle);
                    if (oldBarcodeOfVentaDetalleListNewVentaDetalle != null && !oldBarcodeOfVentaDetalleListNewVentaDetalle.equals(producto)) {
                        oldBarcodeOfVentaDetalleListNewVentaDetalle.getVentaDetalleList().remove(ventaDetalleListNewVentaDetalle);
                        oldBarcodeOfVentaDetalleListNewVentaDetalle = em.merge(oldBarcodeOfVentaDetalleListNewVentaDetalle);
                    }
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListOldAlertaCambioPrecio : alertaCambioPrecioListOld) {
                if (!alertaCambioPrecioListNew.contains(alertaCambioPrecioListOldAlertaCambioPrecio)) {
                    alertaCambioPrecioListOldAlertaCambioPrecio.setBarcode(null);
                    alertaCambioPrecioListOldAlertaCambioPrecio = em.merge(alertaCambioPrecioListOldAlertaCambioPrecio);
                }
            }
            for (AlertaCambioPrecio alertaCambioPrecioListNewAlertaCambioPrecio : alertaCambioPrecioListNew) {
                if (!alertaCambioPrecioListOld.contains(alertaCambioPrecioListNewAlertaCambioPrecio)) {
                    Producto oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio = alertaCambioPrecioListNewAlertaCambioPrecio.getBarcode();
                    alertaCambioPrecioListNewAlertaCambioPrecio.setBarcode(producto);
                    alertaCambioPrecioListNewAlertaCambioPrecio = em.merge(alertaCambioPrecioListNewAlertaCambioPrecio);
                    if (oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio != null && !oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio.equals(producto)) {
                        oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio.getAlertaCambioPrecioList().remove(alertaCambioPrecioListNewAlertaCambioPrecio);
                        oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio = em.merge(oldBarcodeOfAlertaCambioPrecioListNewAlertaCambioPrecio);
                    }
                }
            }
            for (TraspasoDetalle traspasoDetalleListNewTraspasoDetalle : traspasoDetalleListNew) {
                if (!traspasoDetalleListOld.contains(traspasoDetalleListNewTraspasoDetalle)) {
                    Producto oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle = traspasoDetalleListNewTraspasoDetalle.getBarcode();
                    traspasoDetalleListNewTraspasoDetalle.setBarcode(producto);
                    traspasoDetalleListNewTraspasoDetalle = em.merge(traspasoDetalleListNewTraspasoDetalle);
                    if (oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle != null && !oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle.equals(producto)) {
                        oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle.getTraspasoDetalleList().remove(traspasoDetalleListNewTraspasoDetalle);
                        oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle = em.merge(oldBarcodeOfTraspasoDetalleListNewTraspasoDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = producto.getBarcode();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto;
            try {
                producto = em.getReference(Producto.class, id);
                producto.getBarcode();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PedidoDetalle> productoDetalleListOrphanCheck = producto.getProductoDetalleList();
            for (PedidoDetalle productoDetalleListOrphanCheckPedidoDetalle : productoDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the PedidoDetalle " + productoDetalleListOrphanCheckPedidoDetalle + " in its productoDetalleList field has a non-nullable producto field.");
            }
            List<PromocionDetalle> promocionDetalleListOrphanCheck = producto.getPromocionDetalleList();
            for (PromocionDetalle promocionDetalleListOrphanCheckPromocionDetalle : promocionDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the PromocionDetalle " + promocionDetalleListOrphanCheckPromocionDetalle + " in its promocionDetalleList field has a non-nullable producto field.");
            }
            List<Promocion> promocionListOrphanCheck = producto.getPromocionList();
            for (Promocion promocionListOrphanCheckPromocion : promocionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the Promocion " + promocionListOrphanCheckPromocion + " in its promocionList field has a non-nullable barcode field.");
            }
            List<VentaDetalle> ventaDetalleListOrphanCheck = producto.getVentaDetalleList();
            for (VentaDetalle ventaDetalleListOrphanCheckVentaDetalle : ventaDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the VentaDetalle " + ventaDetalleListOrphanCheckVentaDetalle + " in its ventaDetalleList field has a non-nullable barcode field.");
            }
            List<TraspasoDetalle> traspasoDetalleListOrphanCheck = producto.getTraspasoDetalleList();
            for (TraspasoDetalle traspasoDetalleListOrphanCheckTraspasoDetalle : traspasoDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the TraspasoDetalle " + traspasoDetalleListOrphanCheckTraspasoDetalle + " in its traspasoDetalleList field has a non-nullable barcode field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TipoProducto idTipoProducto = producto.getIdTipoProducto();
            if (idTipoProducto != null) {
                idTipoProducto.getProductoList().remove(producto);
                idTipoProducto = em.merge(idTipoProducto);
            }
            Impuesto otrosImpuestos = producto.getOtrosImpuestos();
            if (otrosImpuestos != null) {
                otrosImpuestos.getProductoList().remove(producto);
                otrosImpuestos = em.merge(otrosImpuestos);
            }
            FamiliaProducto familia = producto.getFamilia();
            if (familia != null) {
                familia.getProductoList().remove(producto);
                familia = em.merge(familia);
            }
            List<InventarioDetalle> inventarioDetalleList = producto.getInventarioDetalleList();
            for (InventarioDetalle inventarioDetalleListInventarioDetalle : inventarioDetalleList) {
                inventarioDetalleListInventarioDetalle.setBarcode(null);
                inventarioDetalleListInventarioDetalle = em.merge(inventarioDetalleListInventarioDetalle);
            }
            List<Merma> mermaList = producto.getMermaList();
            for (Merma mermaListMerma : mermaList) {
                mermaListMerma.setBarcode(null);
                mermaListMerma = em.merge(mermaListMerma);
            }
            List<AlertaCambioPrecio> alertaCambioPrecioList = producto.getAlertaCambioPrecioList();
            for (AlertaCambioPrecio alertaCambioPrecioListAlertaCambioPrecio : alertaCambioPrecioList) {
                alertaCambioPrecioListAlertaCambioPrecio.setBarcode(null);
                alertaCambioPrecioListAlertaCambioPrecio = em.merge(alertaCambioPrecioListAlertaCambioPrecio);
            }
            em.remove(producto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(int maxResults, int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            cq.select(cq.from(TipoProducto.class));
            Root<Producto> productoTbl = cq.from(Producto.class);
            cq.select(productoTbl);
            cq.orderBy(cb.asc(productoTbl.get(Producto_.barcode)));
            
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Producto findProducto(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Producto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Producto> rt = cq.from(Producto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Producto> findProductoEntitiesBySearch(String txt, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Producto.findByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public long getProductoCountBySearch(String txt) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Producto.countByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            return (Long)q.getSingleResult();
        }
        catch (Exception ex) {
            return 0;
        }
        finally {
            em.close();
        }
    }
    
    public Producto findProductoByCodigoCorto(String codCorto) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Producto.findByCodigoCorto");
            q.setParameter("codigoCorto", codCorto);
            return (Producto)q.getSingleResult();
        } 
        catch (Exception ex) {
            return null;
        }
        finally {
            em.close();
        }
    }
}
