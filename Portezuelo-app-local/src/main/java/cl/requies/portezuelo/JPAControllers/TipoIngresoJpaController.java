/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Ingreso;
import cl.requies.portezuelo.entities.TipoIngreso;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class TipoIngresoJpaController implements Serializable {

    public static final int INGRESO_POR_SENCILLO = 1;
    public static final int INGRESO_POR_EXCEJO_CAJA = 2;
    public static final int INGRESO_POR_DEPOSITO_ENVASES = 3;
    
    public TipoIngresoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoIngreso tipoIngreso) {
        if (tipoIngreso.getIngresoList() == null) {
            tipoIngreso.setIngresoList(new ArrayList<Ingreso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Ingreso> attachedIngresoList = new ArrayList<Ingreso>();
            for (Ingreso ingresoListIngresoToAttach : tipoIngreso.getIngresoList()) {
                ingresoListIngresoToAttach = em.getReference(ingresoListIngresoToAttach.getClass(), ingresoListIngresoToAttach.getId());
                attachedIngresoList.add(ingresoListIngresoToAttach);
            }
            tipoIngreso.setIngresoList(attachedIngresoList);
            em.persist(tipoIngreso);
            for (Ingreso ingresoListIngreso : tipoIngreso.getIngresoList()) {
                TipoIngreso oldTipoIngresoIdOfIngresoListIngreso = ingresoListIngreso.getTipoIngresoId();
                ingresoListIngreso.setTipoIngresoId(tipoIngreso);
                ingresoListIngreso = em.merge(ingresoListIngreso);
                if (oldTipoIngresoIdOfIngresoListIngreso != null) {
                    oldTipoIngresoIdOfIngresoListIngreso.getIngresoList().remove(ingresoListIngreso);
                    oldTipoIngresoIdOfIngresoListIngreso = em.merge(oldTipoIngresoIdOfIngresoListIngreso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoIngreso tipoIngreso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoIngreso persistentTipoIngreso = em.find(TipoIngreso.class, tipoIngreso.getId());
            List<Ingreso> ingresoListOld = persistentTipoIngreso.getIngresoList();
            List<Ingreso> ingresoListNew = tipoIngreso.getIngresoList();
            List<String> illegalOrphanMessages = null;
            for (Ingreso ingresoListOldIngreso : ingresoListOld) {
                if (!ingresoListNew.contains(ingresoListOldIngreso)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ingreso " + ingresoListOldIngreso + " since its tipoIngresoId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Ingreso> attachedIngresoListNew = new ArrayList<Ingreso>();
            for (Ingreso ingresoListNewIngresoToAttach : ingresoListNew) {
                ingresoListNewIngresoToAttach = em.getReference(ingresoListNewIngresoToAttach.getClass(), ingresoListNewIngresoToAttach.getId());
                attachedIngresoListNew.add(ingresoListNewIngresoToAttach);
            }
            ingresoListNew = attachedIngresoListNew;
            tipoIngreso.setIngresoList(ingresoListNew);
            tipoIngreso = em.merge(tipoIngreso);
            for (Ingreso ingresoListNewIngreso : ingresoListNew) {
                if (!ingresoListOld.contains(ingresoListNewIngreso)) {
                    TipoIngreso oldTipoIngresoIdOfIngresoListNewIngreso = ingresoListNewIngreso.getTipoIngresoId();
                    ingresoListNewIngreso.setTipoIngresoId(tipoIngreso);
                    ingresoListNewIngreso = em.merge(ingresoListNewIngreso);
                    if (oldTipoIngresoIdOfIngresoListNewIngreso != null && !oldTipoIngresoIdOfIngresoListNewIngreso.equals(tipoIngreso)) {
                        oldTipoIngresoIdOfIngresoListNewIngreso.getIngresoList().remove(ingresoListNewIngreso);
                        oldTipoIngresoIdOfIngresoListNewIngreso = em.merge(oldTipoIngresoIdOfIngresoListNewIngreso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoIngreso.getId();
                if (findTipoIngreso(id) == null) {
                    throw new NonexistentEntityException("The tipoIngreso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoIngreso tipoIngreso;
            try {
                tipoIngreso = em.getReference(TipoIngreso.class, id);
                tipoIngreso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoIngreso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Ingreso> ingresoListOrphanCheck = tipoIngreso.getIngresoList();
            for (Ingreso ingresoListOrphanCheckIngreso : ingresoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoIngreso (" + tipoIngreso + ") cannot be destroyed since the Ingreso " + ingresoListOrphanCheckIngreso + " in its ingresoList field has a non-nullable tipoIngresoId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoIngreso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoIngreso> findTipoIngresoEntities() {
        return findTipoIngresoEntities(true, -1, -1);
    }

    public List<TipoIngreso> findTipoIngresoEntities(int maxResults, int firstResult) {
        return findTipoIngresoEntities(false, maxResults, firstResult);
    }

    private List<TipoIngreso> findTipoIngresoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoIngreso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoIngreso findTipoIngreso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoIngreso.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoIngresoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoIngreso> rt = cq.from(TipoIngreso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
