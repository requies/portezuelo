/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.AlertaCambioPrecio;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Negocio;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class AlertaCambioPrecioJpaController implements Serializable {

    public AlertaCambioPrecioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AlertaCambioPrecio alertaCambioPrecio) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto barcode = alertaCambioPrecio.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                alertaCambioPrecio.setBarcode(barcode);
            }
            Negocio negocioId = alertaCambioPrecio.getNegocioId();
            if (negocioId != null) {
                negocioId = em.getReference(negocioId.getClass(), negocioId.getId());
                alertaCambioPrecio.setNegocioId(negocioId);
            }
            em.persist(alertaCambioPrecio);
            if (barcode != null) {
                barcode.getAlertaCambioPrecioList().add(alertaCambioPrecio);
                barcode = em.merge(barcode);
            }
            if (negocioId != null) {
                negocioId.getAlertaCambioPrecioList().add(alertaCambioPrecio);
                negocioId = em.merge(negocioId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AlertaCambioPrecio alertaCambioPrecio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AlertaCambioPrecio persistentAlertaCambioPrecio = em.find(AlertaCambioPrecio.class, alertaCambioPrecio.getId());
            Producto barcodeOld = persistentAlertaCambioPrecio.getBarcode();
            Producto barcodeNew = alertaCambioPrecio.getBarcode();
            Negocio negocioIdOld = persistentAlertaCambioPrecio.getNegocioId();
            Negocio negocioIdNew = alertaCambioPrecio.getNegocioId();
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                alertaCambioPrecio.setBarcode(barcodeNew);
            }
            if (negocioIdNew != null) {
                negocioIdNew = em.getReference(negocioIdNew.getClass(), negocioIdNew.getId());
                alertaCambioPrecio.setNegocioId(negocioIdNew);
            }
            alertaCambioPrecio = em.merge(alertaCambioPrecio);
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getAlertaCambioPrecioList().remove(alertaCambioPrecio);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getAlertaCambioPrecioList().add(alertaCambioPrecio);
                barcodeNew = em.merge(barcodeNew);
            }
            if (negocioIdOld != null && !negocioIdOld.equals(negocioIdNew)) {
                negocioIdOld.getAlertaCambioPrecioList().remove(alertaCambioPrecio);
                negocioIdOld = em.merge(negocioIdOld);
            }
            if (negocioIdNew != null && !negocioIdNew.equals(negocioIdOld)) {
                negocioIdNew.getAlertaCambioPrecioList().add(alertaCambioPrecio);
                negocioIdNew = em.merge(negocioIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = alertaCambioPrecio.getId();
                if (findAlertaCambioPrecio(id) == null) {
                    throw new NonexistentEntityException("The alertaCambioPrecio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AlertaCambioPrecio alertaCambioPrecio;
            try {
                alertaCambioPrecio = em.getReference(AlertaCambioPrecio.class, id);
                alertaCambioPrecio.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The alertaCambioPrecio with id " + id + " no longer exists.", enfe);
            }
            Producto barcode = alertaCambioPrecio.getBarcode();
            if (barcode != null) {
                barcode.getAlertaCambioPrecioList().remove(alertaCambioPrecio);
                barcode = em.merge(barcode);
            }
            Negocio negocioId = alertaCambioPrecio.getNegocioId();
            if (negocioId != null) {
                negocioId.getAlertaCambioPrecioList().remove(alertaCambioPrecio);
                negocioId = em.merge(negocioId);
            }
            em.remove(alertaCambioPrecio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AlertaCambioPrecio> findAlertaCambioPrecioEntities() {
        return findAlertaCambioPrecioEntities(true, -1, -1);
    }

    public List<AlertaCambioPrecio> findAlertaCambioPrecioEntities(int maxResults, int firstResult) {
        return findAlertaCambioPrecioEntities(false, maxResults, firstResult);
    }

    private List<AlertaCambioPrecio> findAlertaCambioPrecioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AlertaCambioPrecio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AlertaCambioPrecio findAlertaCambioPrecio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AlertaCambioPrecio.class, id);
        } finally {
            em.close();
        }
    }

    public int getAlertaCambioPrecioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AlertaCambioPrecio> rt = cq.from(AlertaCambioPrecio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getCantidadAlertasPendientes() {
        EntityManager em;
        em = emf.createEntityManager();
        Query q = em.createNamedQuery("AlertaCambioPrecio.countPendientes", Long.class);
        try {
            Long cantidad = ((Long)q.getSingleResult());
            if (cantidad == null) {
                return 0;
            }
            else {
                return cantidad.intValue();
            }
        }
        catch (Exception e) {
            return 0;
        }
        finally {
            em.close();
        }
    }
    
    public void marcarAlertaComoLeida(int id) {
        EntityManager em;
        em = emf.createEntityManager();
        em.getTransaction().begin();
        Query q = em.createNamedQuery("AlertaCambioPrecio.marcarComoLeida");
        q.setParameter("id", id);
        try {
            q.executeUpdate();
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            throw ex;
        }
        finally {
            em.close();
        }
    }
    
}
