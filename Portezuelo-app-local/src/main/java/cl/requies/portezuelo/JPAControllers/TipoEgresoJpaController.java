/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Egreso;
import cl.requies.portezuelo.entities.TipoEgreso;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class TipoEgresoJpaController implements Serializable {

    public static final int EGRESO_POR_COMPRA = 1;
    public static final int EGRESO_POR_RETIRO = 2;
    public static final int EGRESO_POR_PERDIDA = 3;
    public static final int EGRESO_POR_GASTOS_CORRIENTES = 4;
    public static final int EGRESO_POR_DEVOLUCION_ENVASES = 5;
    public static final int EGRESO_POR_VENTA_ANULADA = 6;
    public static final int EGRESO_POR_CIERRE = 7;
    
    public TipoEgresoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoEgreso tipoEgreso) {
        if (tipoEgreso.getEgresoList() == null) {
            tipoEgreso.setEgresoList(new ArrayList<Egreso>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Egreso> attachedEgresoList = new ArrayList<Egreso>();
            for (Egreso egresoListEgresoToAttach : tipoEgreso.getEgresoList()) {
                egresoListEgresoToAttach = em.getReference(egresoListEgresoToAttach.getClass(), egresoListEgresoToAttach.getId());
                attachedEgresoList.add(egresoListEgresoToAttach);
            }
            tipoEgreso.setEgresoList(attachedEgresoList);
            em.persist(tipoEgreso);
            for (Egreso egresoListEgreso : tipoEgreso.getEgresoList()) {
                TipoEgreso oldTipoEgresoIdOfEgresoListEgreso = egresoListEgreso.getTipoEgresoId();
                egresoListEgreso.setTipoEgresoId(tipoEgreso);
                egresoListEgreso = em.merge(egresoListEgreso);
                if (oldTipoEgresoIdOfEgresoListEgreso != null) {
                    oldTipoEgresoIdOfEgresoListEgreso.getEgresoList().remove(egresoListEgreso);
                    oldTipoEgresoIdOfEgresoListEgreso = em.merge(oldTipoEgresoIdOfEgresoListEgreso);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoEgreso tipoEgreso) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoEgreso persistentTipoEgreso = em.find(TipoEgreso.class, tipoEgreso.getId());
            List<Egreso> egresoListOld = persistentTipoEgreso.getEgresoList();
            List<Egreso> egresoListNew = tipoEgreso.getEgresoList();
            List<Egreso> attachedEgresoListNew = new ArrayList<Egreso>();
            for (Egreso egresoListNewEgresoToAttach : egresoListNew) {
                egresoListNewEgresoToAttach = em.getReference(egresoListNewEgresoToAttach.getClass(), egresoListNewEgresoToAttach.getId());
                attachedEgresoListNew.add(egresoListNewEgresoToAttach);
            }
            egresoListNew = attachedEgresoListNew;
            tipoEgreso.setEgresoList(egresoListNew);
            tipoEgreso = em.merge(tipoEgreso);
            for (Egreso egresoListOldEgreso : egresoListOld) {
                if (!egresoListNew.contains(egresoListOldEgreso)) {
                    egresoListOldEgreso.setTipoEgresoId(null);
                    egresoListOldEgreso = em.merge(egresoListOldEgreso);
                }
            }
            for (Egreso egresoListNewEgreso : egresoListNew) {
                if (!egresoListOld.contains(egresoListNewEgreso)) {
                    TipoEgreso oldTipoEgresoIdOfEgresoListNewEgreso = egresoListNewEgreso.getTipoEgresoId();
                    egresoListNewEgreso.setTipoEgresoId(tipoEgreso);
                    egresoListNewEgreso = em.merge(egresoListNewEgreso);
                    if (oldTipoEgresoIdOfEgresoListNewEgreso != null && !oldTipoEgresoIdOfEgresoListNewEgreso.equals(tipoEgreso)) {
                        oldTipoEgresoIdOfEgresoListNewEgreso.getEgresoList().remove(egresoListNewEgreso);
                        oldTipoEgresoIdOfEgresoListNewEgreso = em.merge(oldTipoEgresoIdOfEgresoListNewEgreso);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoEgreso.getId();
                if (findTipoEgreso(id) == null) {
                    throw new NonexistentEntityException("The tipoEgreso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoEgreso tipoEgreso;
            try {
                tipoEgreso = em.getReference(TipoEgreso.class, id);
                tipoEgreso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoEgreso with id " + id + " no longer exists.", enfe);
            }
            List<Egreso> egresoList = tipoEgreso.getEgresoList();
            for (Egreso egresoListEgreso : egresoList) {
                egresoListEgreso.setTipoEgresoId(null);
                egresoListEgreso = em.merge(egresoListEgreso);
            }
            em.remove(tipoEgreso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoEgreso> findTipoEgresoEntities() {
        return findTipoEgresoEntities(true, -1, -1);
    }

    public List<TipoEgreso> findTipoEgresoEntities(int maxResults, int firstResult) {
        return findTipoEgresoEntities(false, maxResults, firstResult);
    }

    private List<TipoEgreso> findTipoEgresoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoEgreso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoEgreso findTipoEgreso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoEgreso.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoEgresoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoEgreso> rt = cq.from(TipoEgreso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
