/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Traspaso;
import cl.requies.portezuelo.entities.TraspasoDetalle;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class TraspasoJpaController implements Serializable {

    public TraspasoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Traspaso traspaso) {
        if (traspaso.getTraspasoDetalleList() == null) {
            traspaso.setTraspasoDetalleList(new ArrayList<TraspasoDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = traspaso.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                traspaso.setUserId(userId);
            }
            Negocio destinoId = traspaso.getDestinoId();
            if (destinoId != null) {
                destinoId = em.getReference(destinoId.getClass(), destinoId.getId());
                traspaso.setDestinoId(destinoId);
            }
            Negocio origenId = traspaso.getOrigenId();
            if (origenId != null) {
                origenId = em.getReference(origenId.getClass(), origenId.getId());
                traspaso.setOrigenId(origenId);
            }
            List<TraspasoDetalle> attachedTraspasoDetalleList = new ArrayList<TraspasoDetalle>();
            for (TraspasoDetalle traspasoDetalleListTraspasoDetalleToAttach : traspaso.getTraspasoDetalleList()) {
                traspasoDetalleListTraspasoDetalleToAttach = em.getReference(traspasoDetalleListTraspasoDetalleToAttach.getClass(), traspasoDetalleListTraspasoDetalleToAttach.getId());
                attachedTraspasoDetalleList.add(traspasoDetalleListTraspasoDetalleToAttach);
            }
            traspaso.setTraspasoDetalleList(attachedTraspasoDetalleList);
            em.persist(traspaso);
            if (userId != null) {
                userId.getTraspasoList().add(traspaso);
                userId = em.merge(userId);
            }
            if (destinoId != null) {
                destinoId.getTraspasoList().add(traspaso);
                destinoId = em.merge(destinoId);
            }
            if (origenId != null) {
                origenId.getTraspasoList().add(traspaso);
                origenId = em.merge(origenId);
            }
            for (TraspasoDetalle traspasoDetalleListTraspasoDetalle : traspaso.getTraspasoDetalleList()) {
                Traspaso oldTraspasoIdOfTraspasoDetalleListTraspasoDetalle = traspasoDetalleListTraspasoDetalle.getTraspasoId();
                traspasoDetalleListTraspasoDetalle.setTraspasoId(traspaso);
                traspasoDetalleListTraspasoDetalle = em.merge(traspasoDetalleListTraspasoDetalle);
                if (oldTraspasoIdOfTraspasoDetalleListTraspasoDetalle != null) {
                    oldTraspasoIdOfTraspasoDetalleListTraspasoDetalle.getTraspasoDetalleList().remove(traspasoDetalleListTraspasoDetalle);
                    oldTraspasoIdOfTraspasoDetalleListTraspasoDetalle = em.merge(oldTraspasoIdOfTraspasoDetalleListTraspasoDetalle);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Traspaso traspaso) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Traspaso persistentTraspaso = em.find(Traspaso.class, traspaso.getId());
            Users userIdOld = persistentTraspaso.getUserId();
            Users userIdNew = traspaso.getUserId();
            Negocio destinoIdOld = persistentTraspaso.getDestinoId();
            Negocio destinoIdNew = traspaso.getDestinoId();
            Negocio origenIdOld = persistentTraspaso.getOrigenId();
            Negocio origenIdNew = traspaso.getOrigenId();
            List<TraspasoDetalle> traspasoDetalleListOld = persistentTraspaso.getTraspasoDetalleList();
            List<TraspasoDetalle> traspasoDetalleListNew = traspaso.getTraspasoDetalleList();
            List<String> illegalOrphanMessages = null;
            for (TraspasoDetalle traspasoDetalleListOldTraspasoDetalle : traspasoDetalleListOld) {
                if (!traspasoDetalleListNew.contains(traspasoDetalleListOldTraspasoDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain TraspasoDetalle " + traspasoDetalleListOldTraspasoDetalle + " since its traspasoId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                traspaso.setUserId(userIdNew);
            }
            if (destinoIdNew != null) {
                destinoIdNew = em.getReference(destinoIdNew.getClass(), destinoIdNew.getId());
                traspaso.setDestinoId(destinoIdNew);
            }
            if (origenIdNew != null) {
                origenIdNew = em.getReference(origenIdNew.getClass(), origenIdNew.getId());
                traspaso.setOrigenId(origenIdNew);
            }
            List<TraspasoDetalle> attachedTraspasoDetalleListNew = new ArrayList<TraspasoDetalle>();
            for (TraspasoDetalle traspasoDetalleListNewTraspasoDetalleToAttach : traspasoDetalleListNew) {
                traspasoDetalleListNewTraspasoDetalleToAttach = em.getReference(traspasoDetalleListNewTraspasoDetalleToAttach.getClass(), traspasoDetalleListNewTraspasoDetalleToAttach.getId());
                attachedTraspasoDetalleListNew.add(traspasoDetalleListNewTraspasoDetalleToAttach);
            }
            traspasoDetalleListNew = attachedTraspasoDetalleListNew;
            traspaso.setTraspasoDetalleList(traspasoDetalleListNew);
            traspaso = em.merge(traspaso);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getTraspasoList().remove(traspaso);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getTraspasoList().add(traspaso);
                userIdNew = em.merge(userIdNew);
            }
            if (destinoIdOld != null && !destinoIdOld.equals(destinoIdNew)) {
                destinoIdOld.getTraspasoList().remove(traspaso);
                destinoIdOld = em.merge(destinoIdOld);
            }
            if (destinoIdNew != null && !destinoIdNew.equals(destinoIdOld)) {
                destinoIdNew.getTraspasoList().add(traspaso);
                destinoIdNew = em.merge(destinoIdNew);
            }
            if (origenIdOld != null && !origenIdOld.equals(origenIdNew)) {
                origenIdOld.getTraspasoList().remove(traspaso);
                origenIdOld = em.merge(origenIdOld);
            }
            if (origenIdNew != null && !origenIdNew.equals(origenIdOld)) {
                origenIdNew.getTraspasoList().add(traspaso);
                origenIdNew = em.merge(origenIdNew);
            }
            for (TraspasoDetalle traspasoDetalleListNewTraspasoDetalle : traspasoDetalleListNew) {
                if (!traspasoDetalleListOld.contains(traspasoDetalleListNewTraspasoDetalle)) {
                    Traspaso oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle = traspasoDetalleListNewTraspasoDetalle.getTraspasoId();
                    traspasoDetalleListNewTraspasoDetalle.setTraspasoId(traspaso);
                    traspasoDetalleListNewTraspasoDetalle = em.merge(traspasoDetalleListNewTraspasoDetalle);
                    if (oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle != null && !oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle.equals(traspaso)) {
                        oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle.getTraspasoDetalleList().remove(traspasoDetalleListNewTraspasoDetalle);
                        oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle = em.merge(oldTraspasoIdOfTraspasoDetalleListNewTraspasoDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = traspaso.getId();
                if (findTraspaso(id) == null) {
                    throw new NonexistentEntityException("The traspaso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Traspaso traspaso;
            try {
                traspaso = em.getReference(Traspaso.class, id);
                traspaso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The traspaso with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<TraspasoDetalle> traspasoDetalleListOrphanCheck = traspaso.getTraspasoDetalleList();
            for (TraspasoDetalle traspasoDetalleListOrphanCheckTraspasoDetalle : traspasoDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Traspaso (" + traspaso + ") cannot be destroyed since the TraspasoDetalle " + traspasoDetalleListOrphanCheckTraspasoDetalle + " in its traspasoDetalleList field has a non-nullable traspasoId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = traspaso.getUserId();
            if (userId != null) {
                userId.getTraspasoList().remove(traspaso);
                userId = em.merge(userId);
            }
            Negocio destinoId = traspaso.getDestinoId();
            if (destinoId != null) {
                destinoId.getTraspasoList().remove(traspaso);
                destinoId = em.merge(destinoId);
            }
            Negocio origenId = traspaso.getOrigenId();
            if (origenId != null) {
                origenId.getTraspasoList().remove(traspaso);
                origenId = em.merge(origenId);
            }
            em.remove(traspaso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Traspaso> findTraspasoEntities() {
        return findTraspasoEntities(true, -1, -1);
    }

    public List<Traspaso> findTraspasoEntities(int maxResults, int firstResult) {
        return findTraspasoEntities(false, maxResults, firstResult);
    }

    private List<Traspaso> findTraspasoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Traspaso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Traspaso findTraspaso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Traspaso.class, id);
        } finally {
            em.close();
        }
    }

    public int getTraspasoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Traspaso> rt = cq.from(Traspaso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
