/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Venta;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.VentaDetalle;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class VentaDetalleJpaController implements Serializable {

    public VentaDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VentaDetalle ventaDetalle) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta venta = ventaDetalle.getVenta();
            if (venta != null) {
                venta = em.getReference(venta.getClass(), venta.getId());
                ventaDetalle.setVenta(venta);
            }
            Producto barcode = ventaDetalle.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                ventaDetalle.setBarcode(barcode);
            }
            em.persist(ventaDetalle);
            if (venta != null) {
                venta.getVentaDetalleList().add(ventaDetalle);
                venta = em.merge(venta);
            }
            if (barcode != null) {
                barcode.getVentaDetalleList().add(ventaDetalle);
                barcode = em.merge(barcode);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VentaDetalle ventaDetalle) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaDetalle persistentVentaDetalle = em.find(VentaDetalle.class, ventaDetalle.getId());
            Venta ventaOld = persistentVentaDetalle.getVenta();
            Venta ventaNew = ventaDetalle.getVenta();
            Producto barcodeOld = persistentVentaDetalle.getBarcode();
            Producto barcodeNew = ventaDetalle.getBarcode();
            if (ventaNew != null) {
                ventaNew = em.getReference(ventaNew.getClass(), ventaNew.getId());
                ventaDetalle.setVenta(ventaNew);
            }
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                ventaDetalle.setBarcode(barcodeNew);
            }
            ventaDetalle = em.merge(ventaDetalle);
            if (ventaOld != null && !ventaOld.equals(ventaNew)) {
                ventaOld.getVentaDetalleList().remove(ventaDetalle);
                ventaOld = em.merge(ventaOld);
            }
            if (ventaNew != null && !ventaNew.equals(ventaOld)) {
                ventaNew.getVentaDetalleList().add(ventaDetalle);
                ventaNew = em.merge(ventaNew);
            }
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getVentaDetalleList().remove(ventaDetalle);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getVentaDetalleList().add(ventaDetalle);
                barcodeNew = em.merge(barcodeNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ventaDetalle.getId();
                if (findVentaDetalle(id) == null) {
                    throw new NonexistentEntityException("The ventaDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaDetalle ventaDetalle;
            try {
                ventaDetalle = em.getReference(VentaDetalle.class, id);
                ventaDetalle.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventaDetalle with id " + id + " no longer exists.", enfe);
            }
            Venta venta = ventaDetalle.getVenta();
            if (venta != null) {
                venta.getVentaDetalleList().remove(ventaDetalle);
                venta = em.merge(venta);
            }
            Producto barcode = ventaDetalle.getBarcode();
            if (barcode != null) {
                barcode.getVentaDetalleList().remove(ventaDetalle);
                barcode = em.merge(barcode);
            }
            em.remove(ventaDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VentaDetalle> findVentaDetalleEntities() {
        return findVentaDetalleEntities(true, -1, -1);
    }

    public List<VentaDetalle> findVentaDetalleEntities(int maxResults, int firstResult) {
        return findVentaDetalleEntities(false, maxResults, firstResult);
    }

    private List<VentaDetalle> findVentaDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VentaDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VentaDetalle findVentaDetalle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VentaDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VentaDetalle> rt = cq.from(VentaDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
