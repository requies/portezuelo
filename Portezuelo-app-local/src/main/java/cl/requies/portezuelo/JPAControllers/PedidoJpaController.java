/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Pedido;
import cl.requies.portezuelo.entities.PedidoDetalle;
import cl.requies.portezuelo.entities.PedidoDetallePK;
import cl.requies.portezuelo.entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;

/**
 *
 * @author victor
 */
public class PedidoJpaController implements Serializable {

    public PedidoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pedido pedido) {
        if (pedido.getPedidoDetalleList() == null) {
            pedido.setPedidoDetalleList(new ArrayList<PedidoDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = pedido.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                pedido.setUserId(userId);
            }
            Negocio negocioId = pedido.getNegocioId();
            if (negocioId != null) {
                negocioId = em.getReference(negocioId.getClass(), negocioId.getId());
                pedido.setNegocioId(negocioId);
            }
            List<PedidoDetalle> detallesPedidoList = pedido.getPedidoDetalleList();
            pedido.setPedidoDetalleList(new ArrayList<PedidoDetalle>());
            em.persist(pedido);
            em.flush();
            
            if (userId != null) {
                userId.getPedidoList().add(pedido);
                userId = em.merge(userId);
            }
            if (negocioId != null) {
                negocioId.getPedidoList().add(pedido);
                negocioId = em.merge(negocioId);
            }
            //Se actualiza el stock de los productos
            Producto prodTemp;
            for (PedidoDetalle pedidoDetalleTemp : detallesPedidoList) {
                prodTemp = pedidoDetalleTemp.getProducto();
                //prodTemp.setStock(pedidoDetalleTemp.getCantidad() + prodTemp.getStock());
                pedidoDetalleTemp.setPedidoDetallePK(new PedidoDetallePK(prodTemp.getBarcode(), pedido.getId()));
                em.persist(pedidoDetalleTemp);
                //em.merge(prodTemp);
            }
            pedido.setPedidoDetalleList(detallesPedidoList);
            em.merge(pedido);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pedido pedido) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido persistentPedido = em.find(Pedido.class, pedido.getId());
            Users userIdOld = persistentPedido.getUserId();
            Users userIdNew = pedido.getUserId();
            Negocio negocioIdOld = persistentPedido.getNegocioId();
            Negocio negocioIdNew = pedido.getNegocioId();
            List<PedidoDetalle> productoDetalleListOld = persistentPedido.getPedidoDetalleList();
            List<PedidoDetalle> productoDetalleListNew = pedido.getPedidoDetalleList();
            List<String> illegalOrphanMessages = null;
            for (PedidoDetalle productoDetalleListOldProductoDetalle : productoDetalleListOld) {
                if (!productoDetalleListNew.contains(productoDetalleListOldProductoDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProductoDetalle " + productoDetalleListOldProductoDetalle + " since its pedido field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                pedido.setUserId(userIdNew);
            }
            if (negocioIdNew != null) {
                negocioIdNew = em.getReference(negocioIdNew.getClass(), negocioIdNew.getId());
                pedido.setNegocioId(negocioIdNew);
            }
            /*
            List<PedidoDetalle> attachedProductoDetalleListNew = new ArrayList<PedidoDetalle>();
            for (PedidoDetalle pedidoDetalleListNewPedidoDetalleToAttach : productoDetalleListNew) {
                pedidoDetalleListNewPedidoDetalleToAttach = em.getReference(pedidoDetalleListNewPedidoDetalleToAttach.getClass(), pedidoDetalleListNewPedidoDetalleToAttach.getPedidoDetallePK());
                attachedProductoDetalleListNew.add(pedidoDetalleListNewPedidoDetalleToAttach);
            }
            productoDetalleListNew = attachedProductoDetalleListNew;
            pedido.setPedidoDetalleList(productoDetalleListNew);
            */
            
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getPedidoList().remove(pedido);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getPedidoList().add(pedido);
                userIdNew = em.merge(userIdNew);
            }
            if (negocioIdOld != null && !negocioIdOld.equals(negocioIdNew)) {
                negocioIdOld.getPedidoList().remove(pedido);
                negocioIdOld = em.merge(negocioIdOld);
            }
            if (negocioIdNew != null && !negocioIdNew.equals(negocioIdOld)) {
                negocioIdNew.getPedidoList().add(pedido);
                negocioIdNew = em.merge(negocioIdNew);
            }
            Producto prod;
            Double cantidadRecibidaOld;
            double cantidadAumentoProducto;
            boolean estaba;
            for (PedidoDetalle productoDetalleListNewProductoDetalle : productoDetalleListNew) {
                estaba = false;
                cantidadAumentoProducto = 0;
                Pedido oldPedidoOfProductoDetalleListNewProductoDetalle = productoDetalleListNewProductoDetalle.getPedido();
                productoDetalleListNewProductoDetalle.setPedido(pedido);

                for (PedidoDetalle pedDetOld : productoDetalleListOld) {
                    if (pedDetOld.getPedidoDetallePK().equals(productoDetalleListNewProductoDetalle.getPedidoDetallePK())) {
                        estaba = true;
                        cantidadRecibidaOld = pedDetOld.getCantidadRecibida();
                        if (cantidadRecibidaOld != null) {
                            if (cantidadRecibidaOld < productoDetalleListNewProductoDetalle.getCantidadRecibida()) {
                                cantidadAumentoProducto = productoDetalleListNewProductoDetalle.getCantidadRecibida() - cantidadRecibidaOld;
                            }
                        }
                        else if (productoDetalleListNewProductoDetalle.getCantidadRecibida() != null) { //vale null
                            cantidadAumentoProducto = productoDetalleListNewProductoDetalle.getCantidadRecibida();
                        }
                        else {
                            cantidadAumentoProducto = 0;
                        }
                        break;
                    }
                }
                prod = productoDetalleListNewProductoDetalle.getProducto();
                prod = em.getReference(prod.getClass(), prod.getBarcode());
                em.lock(prod, LockModeType.PESSIMISTIC_WRITE);
                prod.setStock(prod.getStock()+cantidadAumentoProducto);
                em.merge(prod);
                if (estaba) {
                    productoDetalleListNewProductoDetalle = em.merge(productoDetalleListNewProductoDetalle);
                    if (oldPedidoOfProductoDetalleListNewProductoDetalle != null && !oldPedidoOfProductoDetalleListNewProductoDetalle.equals(pedido)) {
                        oldPedidoOfProductoDetalleListNewProductoDetalle.getPedidoDetalleList().remove(productoDetalleListNewProductoDetalle);
                        oldPedidoOfProductoDetalleListNewProductoDetalle = em.merge(oldPedidoOfProductoDetalleListNewProductoDetalle);
                    }
                }
                else {
                    em.persist(productoDetalleListNewProductoDetalle);
                }
            }
            pedido = em.merge(pedido);
            
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pedido.getId();
                if (findPedido(id) == null) {
                    throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido pedido;
            try {
                pedido = em.getReference(Pedido.class, id);
                pedido.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PedidoDetalle> productoDetalleListOrphanCheck = pedido.getPedidoDetalleList();
            for (PedidoDetalle productoDetalleListOrphanCheckProductoDetalle : productoDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pedido (" + pedido + ") cannot be destroyed since the ProductoDetalle " + productoDetalleListOrphanCheckProductoDetalle + " in its productoDetalleList field has a non-nullable pedido field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = pedido.getUserId();
            if (userId != null) {
                userId.getPedidoList().remove(pedido);
                userId = em.merge(userId);
            }
            Negocio negocioId = pedido.getNegocioId();
            if (negocioId != null) {
                negocioId.getPedidoList().remove(pedido);
                negocioId = em.merge(negocioId);
            }
            em.remove(pedido);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pedido> findPedidoEntities() {
        return findPedidoEntities(true, -1, -1);
    }

    public List<Pedido> findPedidoEntities(int maxResults, int firstResult) {
        return findPedidoEntities(false, maxResults, firstResult);
    }

    private List<Pedido> findPedidoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pedido.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pedido findPedido(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pedido.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pedido> rt = cq.from(Pedido.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Pedido> findPedidoEntitiesBySearch(String txt, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Pedido.findByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public long getPedidoCountBySearch(String txt) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Pedido.countByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            return (Long)q.getSingleResult();
        } finally {
            em.close();
        }
    }
}
