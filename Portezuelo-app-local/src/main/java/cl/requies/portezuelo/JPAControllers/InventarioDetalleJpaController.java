/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Inventario;
import cl.requies.portezuelo.entities.InventarioDetalle;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class InventarioDetalleJpaController implements Serializable {

    public InventarioDetalleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InventarioDetalle inventarioDetalle) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto barcode = inventarioDetalle.getBarcode();
            if (barcode != null) {
                barcode = em.getReference(barcode.getClass(), barcode.getBarcode());
                inventarioDetalle.setBarcode(barcode);
            }
            Inventario inventarioId = inventarioDetalle.getInventarioId();
            if (inventarioId != null) {
                inventarioId = em.getReference(inventarioId.getClass(), inventarioId.getId());
                inventarioDetalle.setInventarioId(inventarioId);
            }
            em.persist(inventarioDetalle);
            if (barcode != null) {
                barcode.getInventarioDetalleList().add(inventarioDetalle);
                barcode = em.merge(barcode);
            }
            if (inventarioId != null) {
                inventarioId.getInventarioDetalleList().add(inventarioDetalle);
                inventarioId = em.merge(inventarioId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InventarioDetalle inventarioDetalle) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InventarioDetalle persistentInventarioDetalle = em.find(InventarioDetalle.class, inventarioDetalle.getId());
            Producto barcodeOld = persistentInventarioDetalle.getBarcode();
            Producto barcodeNew = inventarioDetalle.getBarcode();
            Inventario inventarioIdOld = persistentInventarioDetalle.getInventarioId();
            Inventario inventarioIdNew = inventarioDetalle.getInventarioId();
            if (barcodeNew != null) {
                barcodeNew = em.getReference(barcodeNew.getClass(), barcodeNew.getBarcode());
                inventarioDetalle.setBarcode(barcodeNew);
            }
            if (inventarioIdNew != null) {
                inventarioIdNew = em.getReference(inventarioIdNew.getClass(), inventarioIdNew.getId());
                inventarioDetalle.setInventarioId(inventarioIdNew);
            }
            inventarioDetalle = em.merge(inventarioDetalle);
            if (barcodeOld != null && !barcodeOld.equals(barcodeNew)) {
                barcodeOld.getInventarioDetalleList().remove(inventarioDetalle);
                barcodeOld = em.merge(barcodeOld);
            }
            if (barcodeNew != null && !barcodeNew.equals(barcodeOld)) {
                barcodeNew.getInventarioDetalleList().add(inventarioDetalle);
                barcodeNew = em.merge(barcodeNew);
            }
            if (inventarioIdOld != null && !inventarioIdOld.equals(inventarioIdNew)) {
                inventarioIdOld.getInventarioDetalleList().remove(inventarioDetalle);
                inventarioIdOld = em.merge(inventarioIdOld);
            }
            if (inventarioIdNew != null && !inventarioIdNew.equals(inventarioIdOld)) {
                inventarioIdNew.getInventarioDetalleList().add(inventarioDetalle);
                inventarioIdNew = em.merge(inventarioIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = inventarioDetalle.getId();
                if (findInventarioDetalle(id) == null) {
                    throw new NonexistentEntityException("The inventarioDetalle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InventarioDetalle inventarioDetalle;
            try {
                inventarioDetalle = em.getReference(InventarioDetalle.class, id);
                inventarioDetalle.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inventarioDetalle with id " + id + " no longer exists.", enfe);
            }
            Producto barcode = inventarioDetalle.getBarcode();
            if (barcode != null) {
                barcode.getInventarioDetalleList().remove(inventarioDetalle);
                barcode = em.merge(barcode);
            }
            Inventario inventarioId = inventarioDetalle.getInventarioId();
            if (inventarioId != null) {
                inventarioId.getInventarioDetalleList().remove(inventarioDetalle);
                inventarioId = em.merge(inventarioId);
            }
            em.remove(inventarioDetalle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InventarioDetalle> findInventarioDetalleEntities() {
        return findInventarioDetalleEntities(true, -1, -1);
    }

    public List<InventarioDetalle> findInventarioDetalleEntities(int maxResults, int firstResult) {
        return findInventarioDetalleEntities(false, maxResults, firstResult);
    }

    private List<InventarioDetalle> findInventarioDetalleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InventarioDetalle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InventarioDetalle findInventarioDetalle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InventarioDetalle.class, id);
        } finally {
            em.close();
        }
    }

    public int getInventarioDetalleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InventarioDetalle> rt = cq.from(InventarioDetalle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
