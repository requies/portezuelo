/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Inventario;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.InventarioDetalle;
import cl.requies.portezuelo.entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class InventarioJpaController implements Serializable {

    public InventarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inventario inventario) {
        if (inventario.getInventarioDetalleList() == null) {
            inventario.setInventarioDetalleList(new ArrayList<InventarioDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = inventario.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                inventario.setUserId(userId);
            }
            Negocio negocioId = inventario.getNegocioId();
            if (negocioId != null) {
                negocioId = em.getReference(negocioId.getClass(), negocioId.getId());
                inventario.setNegocioId(negocioId);
            }
            
            
            
            em.persist(inventario);
            if (userId != null) {
                userId.getInventarioList().add(inventario);
                userId = em.merge(userId);
            }
            if (negocioId != null) {
                negocioId.getInventarioList().add(inventario);
                negocioId = em.merge(negocioId);
            }
            /*
            for (InventarioDetalle inventarioDetalleListInventarioDetalle : inventario.getInventarioDetalleList()) {
                Inventario oldInventarioIdOfInventarioDetalleListInventarioDetalle = inventarioDetalleListInventarioDetalle.getInventarioId();
                inventarioDetalleListInventarioDetalle.setInventarioId(inventario);
                inventarioDetalleListInventarioDetalle = em.merge(inventarioDetalleListInventarioDetalle);
                if (oldInventarioIdOfInventarioDetalleListInventarioDetalle != null) {
                    oldInventarioIdOfInventarioDetalleListInventarioDetalle.getInventarioDetalleList().remove(inventarioDetalleListInventarioDetalle);
                    oldInventarioIdOfInventarioDetalleListInventarioDetalle = em.merge(oldInventarioIdOfInventarioDetalleListInventarioDetalle);
                }
            }
            */
            
            //Se actualiza el stock de los productos
            Producto prodTemp;
            for (InventarioDetalle inventarioDetalleTemp : inventario.getInventarioDetalleList()) {
                prodTemp = inventarioDetalleTemp.getBarcode();
                prodTemp.setStock(inventarioDetalleTemp.getCantidad());
                em.merge(prodTemp);
            }
            
            
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inventario inventario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inventario persistentInventario = em.find(Inventario.class, inventario.getId());
            Users userIdOld = persistentInventario.getUserId();
            Users userIdNew = inventario.getUserId();
            Negocio negocioIdOld = persistentInventario.getNegocioId();
            Negocio negocioIdNew = inventario.getNegocioId();
            List<InventarioDetalle> inventarioDetalleListOld = persistentInventario.getInventarioDetalleList();
            List<InventarioDetalle> inventarioDetalleListNew = inventario.getInventarioDetalleList();
            List<String> illegalOrphanMessages = null;
            for (InventarioDetalle inventarioDetalleListOldInventarioDetalle : inventarioDetalleListOld) {
                if (!inventarioDetalleListNew.contains(inventarioDetalleListOldInventarioDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InventarioDetalle " + inventarioDetalleListOldInventarioDetalle + " since its inventarioId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                inventario.setUserId(userIdNew);
            }
            if (negocioIdNew != null) {
                negocioIdNew = em.getReference(negocioIdNew.getClass(), negocioIdNew.getId());
                inventario.setNegocioId(negocioIdNew);
            }
            List<InventarioDetalle> attachedInventarioDetalleListNew = new ArrayList<InventarioDetalle>();
            for (InventarioDetalle inventarioDetalleListNewInventarioDetalleToAttach : inventarioDetalleListNew) {
                inventarioDetalleListNewInventarioDetalleToAttach = em.getReference(inventarioDetalleListNewInventarioDetalleToAttach.getClass(), inventarioDetalleListNewInventarioDetalleToAttach.getId());
                attachedInventarioDetalleListNew.add(inventarioDetalleListNewInventarioDetalleToAttach);
            }
            inventarioDetalleListNew = attachedInventarioDetalleListNew;
            inventario.setInventarioDetalleList(inventarioDetalleListNew);
            inventario = em.merge(inventario);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getInventarioList().remove(inventario);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getInventarioList().add(inventario);
                userIdNew = em.merge(userIdNew);
            }
            if (negocioIdOld != null && !negocioIdOld.equals(negocioIdNew)) {
                negocioIdOld.getInventarioList().remove(inventario);
                negocioIdOld = em.merge(negocioIdOld);
            }
            if (negocioIdNew != null && !negocioIdNew.equals(negocioIdOld)) {
                negocioIdNew.getInventarioList().add(inventario);
                negocioIdNew = em.merge(negocioIdNew);
            }
            for (InventarioDetalle inventarioDetalleListNewInventarioDetalle : inventarioDetalleListNew) {
                if (!inventarioDetalleListOld.contains(inventarioDetalleListNewInventarioDetalle)) {
                    Inventario oldInventarioIdOfInventarioDetalleListNewInventarioDetalle = inventarioDetalleListNewInventarioDetalle.getInventarioId();
                    inventarioDetalleListNewInventarioDetalle.setInventarioId(inventario);
                    inventarioDetalleListNewInventarioDetalle = em.merge(inventarioDetalleListNewInventarioDetalle);
                    if (oldInventarioIdOfInventarioDetalleListNewInventarioDetalle != null && !oldInventarioIdOfInventarioDetalleListNewInventarioDetalle.equals(inventario)) {
                        oldInventarioIdOfInventarioDetalleListNewInventarioDetalle.getInventarioDetalleList().remove(inventarioDetalleListNewInventarioDetalle);
                        oldInventarioIdOfInventarioDetalleListNewInventarioDetalle = em.merge(oldInventarioIdOfInventarioDetalleListNewInventarioDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = inventario.getId();
                if (findInventario(id) == null) {
                    throw new NonexistentEntityException("The inventario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inventario inventario;
            try {
                inventario = em.getReference(Inventario.class, id);
                inventario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inventario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<InventarioDetalle> inventarioDetalleListOrphanCheck = inventario.getInventarioDetalleList();
            for (InventarioDetalle inventarioDetalleListOrphanCheckInventarioDetalle : inventarioDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Inventario (" + inventario + ") cannot be destroyed since the InventarioDetalle " + inventarioDetalleListOrphanCheckInventarioDetalle + " in its inventarioDetalleList field has a non-nullable inventarioId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = inventario.getUserId();
            if (userId != null) {
                userId.getInventarioList().remove(inventario);
                userId = em.merge(userId);
            }
            Negocio negocioId = inventario.getNegocioId();
            if (negocioId != null) {
                negocioId.getInventarioList().remove(inventario);
                negocioId = em.merge(negocioId);
            }
            em.remove(inventario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inventario> findInventarioEntities() {
        return findInventarioEntities(true, -1, -1);
    }

    public List<Inventario> findInventarioEntities(int maxResults, int firstResult) {
        return findInventarioEntities(false, maxResults, firstResult);
    }

    private List<Inventario> findInventarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inventario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inventario findInventario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inventario.class, id);
        } finally {
            em.close();
        }
    }

    public int getInventarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inventario> rt = cq.from(Inventario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Inventario> findInventarioEntitiesBySearch(String txt, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Inventario.findByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public long getInventarioCountBySearch(String txt) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Inventario.countByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            return (Long)q.getSingleResult();
        } finally {
            em.close();
        }
    }
}
