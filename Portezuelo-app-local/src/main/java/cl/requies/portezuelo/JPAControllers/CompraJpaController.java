/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.JPAControllers;

import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.JPAControllers.exceptions.PreexistingEntityException;
import cl.requies.portezuelo.entities.Compra;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.CompraDetalle;
import cl.requies.portezuelo.entities.CompraDetallePK;
import cl.requies.portezuelo.entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;

/**
 *
 * @author victor
 */
public class CompraJpaController implements Serializable {

    public CompraJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Compra compra) throws PreexistingEntityException, Exception {
        if (compra.getCompraDetalleList() == null) {
            compra.setCompraDetalleList(new ArrayList<CompraDetalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = compra.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                compra.setUserId(userId);
            }
            Negocio negocioId = compra.getNegocioId();
            if (negocioId != null) {
                negocioId = em.getReference(negocioId.getClass(), negocioId.getId());
                compra.setNegocioId(negocioId);
            }
            
            List<CompraDetalle> detallesCompraList = compra.getCompraDetalleList();
            compra.setCompraDetalleList(new ArrayList<CompraDetalle>());
            em.persist(compra);
            em.flush();
            
            if (userId != null) {
                userId.getCompraList().add(compra);
                userId = em.merge(userId);
            }
            if (negocioId != null) {
                negocioId.getCompraList().add(compra);
                negocioId = em.merge(negocioId);
            }
            //Se actualiza el stock de los productos
            Producto prodTemp;
            for (CompraDetalle compraDetalleTemp : detallesCompraList) {
                prodTemp = em.getReference(Producto.class, compraDetalleTemp.getProducto().getBarcode());
                em.lock(prodTemp, LockModeType.PESSIMISTIC_WRITE); //GENERA SECCIÓN CRÍTICA EN LA TRANSACCIÓN
                prodTemp.setStock(compraDetalleTemp.getCantidad() + prodTemp.getStock());
                compraDetalleTemp.setCompraDetallePK(new CompraDetallePK(prodTemp.getBarcode(), compra.getId()));
                em.persist(compraDetalleTemp);
                em.merge(prodTemp);
            }
            compra.setCompraDetalleList(detallesCompraList);
            em.merge(compra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCompra(compra.getId()) != null) {
                throw new PreexistingEntityException("Compra " + compra + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Compra compra) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Compra persistentCompra = em.find(Compra.class, compra.getId());
            Users userIdOld = persistentCompra.getUserId();
            Users userIdNew = compra.getUserId();
            Negocio negocioIdOld = persistentCompra.getNegocioId();
            Negocio negocioIdNew = compra.getNegocioId();
            List<CompraDetalle> compraDetalleListOld = persistentCompra.getCompraDetalleList();
            List<CompraDetalle> compraDetalleListNew = compra.getCompraDetalleList();
            List<String> illegalOrphanMessages = null;
            for (CompraDetalle compraDetalleListOldCompraDetalle : compraDetalleListOld) {
                if (!compraDetalleListNew.contains(compraDetalleListOldCompraDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CompraDetalle " + compraDetalleListOldCompraDetalle + " since its compra field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                compra.setUserId(userIdNew);
            }
            if (negocioIdNew != null) {
                negocioIdNew = em.getReference(negocioIdNew.getClass(), negocioIdNew.getId());
                compra.setNegocioId(negocioIdNew);
            }
            List<CompraDetalle> attachedCompraDetalleListNew = new ArrayList<CompraDetalle>();
            for (CompraDetalle compraDetalleListNewCompraDetalleToAttach : compraDetalleListNew) {
                compraDetalleListNewCompraDetalleToAttach = em.getReference(compraDetalleListNewCompraDetalleToAttach.getClass(), compraDetalleListNewCompraDetalleToAttach.getCompraDetallePK());
                attachedCompraDetalleListNew.add(compraDetalleListNewCompraDetalleToAttach);
            }
            compraDetalleListNew = attachedCompraDetalleListNew;
            compra.setCompraDetalleList(compraDetalleListNew);
            compra = em.merge(compra);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getCompraList().remove(compra);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getCompraList().add(compra);
                userIdNew = em.merge(userIdNew);
            }
            if (negocioIdOld != null && !negocioIdOld.equals(negocioIdNew)) {
                negocioIdOld.getCompraList().remove(compra);
                negocioIdOld = em.merge(negocioIdOld);
            }
            if (negocioIdNew != null && !negocioIdNew.equals(negocioIdOld)) {
                negocioIdNew.getCompraList().add(compra);
                negocioIdNew = em.merge(negocioIdNew);
            }
            for (CompraDetalle compraDetalleListNewCompraDetalle : compraDetalleListNew) {
                if (!compraDetalleListOld.contains(compraDetalleListNewCompraDetalle)) {
                    Compra oldCompraOfCompraDetalleListNewCompraDetalle = compraDetalleListNewCompraDetalle.getCompra();
                    compraDetalleListNewCompraDetalle.setCompra(compra);
                    compraDetalleListNewCompraDetalle = em.merge(compraDetalleListNewCompraDetalle);
                    if (oldCompraOfCompraDetalleListNewCompraDetalle != null && !oldCompraOfCompraDetalleListNewCompraDetalle.equals(compra)) {
                        oldCompraOfCompraDetalleListNewCompraDetalle.getCompraDetalleList().remove(compraDetalleListNewCompraDetalle);
                        oldCompraOfCompraDetalleListNewCompraDetalle = em.merge(oldCompraOfCompraDetalleListNewCompraDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = compra.getId();
                if (findCompra(id) == null) {
                    throw new NonexistentEntityException("The compra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Compra compra;
            try {
                compra = em.getReference(Compra.class, id);
                compra.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The compra with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CompraDetalle> compraDetalleListOrphanCheck = compra.getCompraDetalleList();
            for (CompraDetalle compraDetalleListOrphanCheckCompraDetalle : compraDetalleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Compra (" + compra + ") cannot be destroyed since the CompraDetalle " + compraDetalleListOrphanCheckCompraDetalle + " in its compraDetalleList field has a non-nullable compra field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = compra.getUserId();
            if (userId != null) {
                userId.getCompraList().remove(compra);
                userId = em.merge(userId);
            }
            Negocio negocioId = compra.getNegocioId();
            if (negocioId != null) {
                negocioId.getCompraList().remove(compra);
                negocioId = em.merge(negocioId);
            }
            em.remove(compra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Compra> findCompraEntities() {
        return findCompraEntities(true, -1, -1);
    }

    public List<Compra> findCompraEntities(int maxResults, int firstResult) {
        return findCompraEntities(false, maxResults, firstResult);
    }

    private List<Compra> findCompraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Compra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Compra findCompra(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Compra.class, id);
        } finally {
            em.close();
        }
    }

    public int getCompraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Compra> rt = cq.from(Compra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Compra> findCompraEntitiesBySearch(String txt, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Compra.findByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public long getCompraCountBySearch(String txt) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Compra.countByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            return (Long)q.getSingleResult();
        } finally {
            em.close();
        }
    }
}
