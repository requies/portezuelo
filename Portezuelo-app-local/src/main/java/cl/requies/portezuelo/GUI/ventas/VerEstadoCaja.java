/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI.ventas;

import cl.requies.portezuelo.JPAControllers.CajaJpaController;
import cl.requies.portezuelo.controllers.LoginController;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Venta;
import cl.requies.portezuelo.others.Utils;
import java.util.List;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class VerEstadoCaja extends javax.swing.JDialog {

    /**
     * Creates new form VerEstadoCaja
     */
    public VerEstadoCaja(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarDatosCaja();
        this.getRootPane().setDefaultButton(this.jbtn_cerrar);
    }
    
    private void cargarDatosCaja() {
        Caja c = LoginController.getInstance().getCajaActual();
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        CajaJpaController controller = new CajaJpaController(emf);
        if (c == null) {
            
            return;
        }
        c = controller.findCaja(c.getId());
        
        jlabel_cajero.setText(c.getUserId().toString());
        jlabel_nombreCaja.setText(c.getMaquinaId().toString());
        jlabel_fechaHora.setText(c.getFechaInicio().toString());
        jlabel_dineroInicial.setText(c.getMontoInicio().toString());
        jlabel_dineroActual.setText(c.getMontoTermino().toString());
        
        List<Venta> ventas = c.getVentaList();
        int cantidadVentas = ventas.size();
        int dineroVentas = 0;
        for (Venta v : ventas) {
            if (v.getVentaAnulada() == null) {
                dineroVentas += v.getMonto();
            }
        }
        jlabel_cantidadVentas.setText(Integer.toString(cantidadVentas));
        jlabel_dineroVentas.setText(Integer.toString(dineroVentas));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jbtn_cerrar = new javax.swing.JButton();
        jlabel_fechaHora = new javax.swing.JLabel();
        jlabel_cajero = new javax.swing.JLabel();
        jlabel_cantidadVentas = new javax.swing.JLabel();
        jlabel_dineroVentas = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jlabel_dineroActual = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jlabel_dineroInicial = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jlabel_nombreCaja = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Estado de caja");

        jLabel1.setText("Dinero estimado en caja:");

        jLabel2.setText("Ingresos por ventas: ");

        jLabel3.setText("Cantidad de ventas: ");

        jLabel4.setText("Cajero actual: ");

        jbtn_cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-close.png"))); // NOI18N
        jbtn_cerrar.setText("Cerrar");
        jbtn_cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_cerrarActionPerformed(evt);
            }
        });

        jlabel_fechaHora.setText("FECHA_HORA");

        jlabel_cajero.setText("CAJERO");

        jlabel_cantidadVentas.setText("100");

        jlabel_dineroVentas.setText("$1000");

        jLabel9.setText("Inicio de caja: ");

        jlabel_dineroActual.setText("$11000");

        jLabel5.setText("Dinero inicial en caja: ");

        jlabel_dineroInicial.setText("$500");

        jLabel6.setText("Caja usada: ");

        jlabel_nombreCaja.setText("NOMBRE_CAJA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtn_cerrar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlabel_dineroVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlabel_dineroInicial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlabel_dineroActual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jlabel_nombreCaja)
                                .addGap(0, 131, Short.MAX_VALUE))
                            .addComponent(jlabel_cantidadVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlabel_fechaHora, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlabel_cajero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jlabel_cajero))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jlabel_nombreCaja))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlabel_fechaHora)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jlabel_dineroInicial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jlabel_dineroActual))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jlabel_dineroVentas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jlabel_cantidadVentas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_cerrar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtn_cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_cerrarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jbtn_cerrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton jbtn_cerrar;
    private javax.swing.JLabel jlabel_cajero;
    private javax.swing.JLabel jlabel_cantidadVentas;
    private javax.swing.JLabel jlabel_dineroActual;
    private javax.swing.JLabel jlabel_dineroInicial;
    private javax.swing.JLabel jlabel_dineroVentas;
    private javax.swing.JLabel jlabel_fechaHora;
    private javax.swing.JLabel jlabel_nombreCaja;
    // End of variables declaration//GEN-END:variables
}
