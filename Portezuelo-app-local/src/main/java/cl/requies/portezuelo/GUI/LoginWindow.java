/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI;

import cl.requies.portezuelo.GUI.ventas.VentasWindow;
import cl.requies.portezuelo.GUI.admin.AdminWindow;
import cl.requies.portezuelo.GUI.comprasIngresosBodega.PedidosIngresosBodegaProdutosWindow;
import cl.requies.portezuelo.JPAControllers.CajaJpaController;
import cl.requies.portezuelo.controllers.LoginController;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Maquina;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import cl.requies.portezuelo.others.AnalizadorRut;
import cl.requies.portezuelo.others.Utils;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretListener;

/**
 *
 * @author victor
 */
public class LoginWindow extends javax.swing.JFrame {
    
    public static final String VENDER = "Vender";
    public static final String PEDIDOS_INGRESOSBODEGA_PRODUCTOS = "Administrar inventario, pedidos y productos";
    public static final String ADMINISTRAR = "Administrar";
    public static final String INFORMES = "Informes";
    
    
    LoginController lc;
    
    AnalizadorRut analizadorRut;
    
    /**
     * Creates new form LoginWindow
     */
    public LoginWindow() {
        analizadorRut = new AnalizadorRut();
        initComponents();
        lc = LoginController.getInstance();
        this.getRootPane().setDefaultButton(this.login_btn);
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        lc = null;
        analizadorRut = null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cb_what_window = new javax.swing.JComboBox();
        jtf_username = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jtf_password = new javax.swing.JPasswordField();
        exit_btn = new javax.swing.JButton();
        login_btn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Autenticación de Usuario");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Autenticación");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Rut:");

        jLabel3.setText("Contraseña:");

        cb_what_window.setModel(new javax.swing.DefaultComboBoxModel(new String[] { VENDER, PEDIDOS_INGRESOSBODEGA_PRODUCTOS, ADMINISTRAR}));

        jtf_username.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jtf_usernameCaretUpdate(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/system-lock-screen.png"))); // NOI18N

        jLabel5.setText("¿Qué desea hacer?");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cb_what_window, 0, 246, Short.MAX_VALUE)
                    .addComponent(jtf_password)
                    .addComponent(jtf_username))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtf_username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jtf_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(cb_what_window, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        exit_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/application-exit.png"))); // NOI18N
        exit_btn.setText("Salir");
        exit_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exit_btnActionPerformed(evt);
            }
        });

        login_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-ok.png"))); // NOI18N
        login_btn.setText("Iniciar sesión");
        login_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                login_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(exit_btn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 247, Short.MAX_VALUE)
                        .addComponent(login_btn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exit_btn)
                    .addComponent(login_btn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exit_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exit_btnActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exit_btnActionPerformed

    private void login_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_login_btnActionPerformed
        String rutStr = jtf_username.getText().trim().replaceAll("[.]", "").replaceAll("-", "");
        String rutStrSinDv;
        try {
            analizadorRut.StringToIntFormat(rutStr);
        }
        catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, 
                    "El rut ingresado no es válido", 
                    "Rut ingresado no válido", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            if (!analizadorRut.validarRut(rutStr)) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, 
                    "El dígito verificador del rut no es válido", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        boolean result = lc.login(rutStr, jtf_password.getText());
        if (result) {
            
            if (VENDER.equals(cb_what_window.getSelectedItem())) {
                InicioCaja dialog = new InicioCaja(this, true);
                dialog.setLocationRelativeTo(this);
                boolean iniciarCaja = dialog.showDialog();
                
                
                if (!iniciarCaja) {
                    lc.logout();
                    return; //No se inicia sesión
                }
                this.setVisible(false);
                
                Maquina maquinaSeleccionada = dialog.getMaquinaSeleccionada();
                int montoInicial = dialog.getMontoInicial();
                
                EntityManagerFactory emf = Utils.getEntityManagerFactory();
                CajaJpaController controller = new CajaJpaController(emf);


                Caja u = new Caja();
                u.setFechaInicio(new Date());
                Users userLogueado = lc.getUsuarioLogueado();
                u.setUserId(userLogueado);
                u.setMontoInicio(montoInicial);
                u.setMontoTermino(montoInicial);
                u.setMaquinaId(maquinaSeleccionada);
                try {
                    controller.create(u);
                    lc.setCajaActual(u);
                } catch (Exception ex) {
                    Logger.getLogger(LoginWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        VentasWindow w = new VentasWindow();
                        w.setVisible(true);
                    }
                });
            }
            else if (ADMINISTRAR.equals(cb_what_window.getSelectedItem())) {
                this.setVisible(false);
                if (lc.getUsuarioLogueado().getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
                    java.awt.EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            AdminWindow w = new AdminWindow();
                            w.setVisible(true);
                        }
                    });
                }
                else {
                    this.setVisible(true);
                    JOptionPane.showMessageDialog(this, 
                    "El usuario ingresado no tiene permisos para realizar esta acción, contacte con un administrador", 
                    "Permisos no suficientes", JOptionPane.ERROR_MESSAGE);
                }
            }
            else if (PEDIDOS_INGRESOSBODEGA_PRODUCTOS.equals(cb_what_window.getSelectedItem())) {
                this.setVisible(false);
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        PedidosIngresosBodegaProdutosWindow w = new PedidosIngresosBodegaProdutosWindow();
                        w.setVisible(true);
                    }
                });
            }
        }
        else {
            JOptionPane.showMessageDialog(this, 
                    "El nombre de usuario y contraseña son incorrectos", 
                    "Login incorrecto", JOptionPane.ERROR_MESSAGE);
            jtf_password.requestFocus();
            jtf_password.selectAll();
        }
    }//GEN-LAST:event_login_btnActionPerformed

    private void jtf_usernameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jtf_usernameCaretUpdate
        String rutEscrito = jtf_username.getText();
        String rutRes = analizadorRut.formateaRut(rutEscrito);
        if (!rutEscrito.equals(rutRes)) {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    String rutEscrito = jtf_username.getText();
                    String rutRes = analizadorRut.formateaRut(rutEscrito);
                    jtf_username.setText(rutRes);
                }
            });
            
        }
    }//GEN-LAST:event_jtf_usernameCaretUpdate


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cb_what_window;
    private javax.swing.JButton exit_btn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField jtf_password;
    private javax.swing.JTextField jtf_username;
    private javax.swing.JButton login_btn;
    // End of variables declaration//GEN-END:variables
}
