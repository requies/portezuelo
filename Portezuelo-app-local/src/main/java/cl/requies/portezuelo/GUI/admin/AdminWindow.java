/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI.admin;

import cl.requies.portezuelo.JPAControllers.ImpuestoJpaController;
import cl.requies.portezuelo.JPAControllers.MaquinaJpaController;
import cl.requies.portezuelo.JPAControllers.NegocioJpaController;
import cl.requies.portezuelo.JPAControllers.UsersJpaController;
import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.controllers.LoginController;
import cl.requies.portezuelo.entities.Impuesto;
import cl.requies.portezuelo.entities.Maquina;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import cl.requies.portezuelo.others.Utils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author victor
 */
public class AdminWindow extends javax.swing.JFrame {
    private static final int INDEX_TAB_USUARIOS = 0;
    private static final int INDEX_TAB_IMPUESTOS = 1;
    private static final int INDEX_TAB_DATOS_NEGOCIO = 2;
    private static final int INDEX_TAB_LOCALES = 3;
    
    /**
     * Creates new form AdminWindow
     */
    public AdminWindow() {
        initComponents();
        cargarUsuarios();
        cargarImpuestos();
        cargarDatosNegocio();
        cargarPuntosVenta();
        jbtn_changePassword.setVisible(false);
        jbtn_deleteUser.setVisible(false);
        jbtn_editarImpuesto.setVisible(false);
        jbtn_deleteImpuesto.setVisible(false);
    }
    
    private void cargarImpuestos() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        ImpuestoJpaController controller = new ImpuestoJpaController(emf);
        List<Impuesto> r = controller.findImpuestoEntities();
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_impuestos.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            for (Impuesto u : r) {
                row = new String[] {u.getId().toString(), u.getDescripcion(), 
                    Double.toString(u.getMonto()).concat("%")};
                tm.addRow(row);
            }
        }
    }
    
    private void cargarUsuarios() {
        EntityManagerFactory emf =Utils.getEntityManagerFactory();
        UsersJpaController controller = new UsersJpaController(emf);
        List<Users> r = controller.findUsersEntities();
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_users.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            String activado;
            for (Users u : r) {
                if (u.isActivo()) {
                    activado = "Si";
                }
                else {
                    activado = "No";
                }
                row = new String[] {u.getNombre(), u.getApellP(), 
                    u.getRut(), TipoUsuario.TIPOS_USUARIOS[u.getTipoUsuario()], activado};
                tm.addRow(row);
            }
        }
    }
    
    private void cargarDatosNegocio() {
        LoginController lc = LoginController.getInstance();
        Negocio neg = lc.getNegocio();
        if (neg == null)
            return;
        jtf_rut.setText(neg.getRut());
        jtf_ciudad.setText(neg.getCiudad());
        jtf_comuna.setText(neg.getComuna());
        jtf_direccion.setText(neg.getDireccion());
        jtf_fono.setText(neg.getFono());
        jtf_razonSocial.setText(neg.getRazonSocial());
        jtf_nombreFantasia.setText(neg.getNombre());
    }
    
    private void cargarPuntosVenta() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        MaquinaJpaController controller = new MaquinaJpaController(emf);
        List<Maquina> r = controller.findMaquinaEntities();
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_puntosVenta.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            String habilitadaStr;
            for (Maquina u : r) {
                if (u.isHabilitada())
                    habilitadaStr = "Si";
                else
                    habilitadaStr = "No";
                row = new String[] {u.getId().toString(), u.getCodigoMaquina(), 
                    u.getNombreMaqina(), habilitadaStr};
                tm.addRow(row);
            }
        }
    }
    
    @Deprecated
    private void eliminarUsuario() {
        DefaultTableModel model = (DefaultTableModel)jtbl_users.getModel();
        int selectedRow = jtbl_users.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay usuarios registrados en el sistema para eliminar", 
                    "No es posible eliminar usuarios", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un usuario para eliminar", 
                    "No es posible eliminar usuarios", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_RUT = 2;
            String rutSeleccionado = (String)model.getValueAt(selectedRow, INDEX_RUT);
            
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            UsersJpaController controller = new UsersJpaController(emf);
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea eliminar el usuario seleccionado del sistema?", 
                    "Confirmación para eliminar usuario", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                Users u = controller.findUserByRut(rutSeleccionado);
                if (u == null) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido eliminar el usuario con rut \"".concat(rutSeleccionado).concat("\""), 
                    "Error al eliminar un usuario", JOptionPane.ERROR_MESSAGE);
                }
                else if (u.getTipoUsuario() == TipoUsuario.ADMINISTRADOR) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido eliminar el usuario con rut \"".concat(rutSeleccionado).concat("\", un usuario administrador no puede ser eliminado del sistema"), 
                    "Error al eliminar un usuario", JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                    try {
                        //Intento eliminar el usuario
                        controller.destroy(u.getId());
                        JOptionPane.showMessageDialog(this, 
                        "Se ha eliminado el usuario con rut \"".concat(rutSeleccionado).concat("\""), 
                        "Se ha eliminado un usuario", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch (IllegalOrphanException | NonexistentEntityException e) {
                        try {
                            //Deshabilito el usuario en lugar de eliminarlo
                            u.setActivo(false);
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this, 
                                "No ha sido posible eliminar el usuario con rut \"".concat(rutSeleccionado).concat("\", pero se ha deshabilitado"), 
                                "Se ha deshabilitado un usuario", JOptionPane.INFORMATION_MESSAGE);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            cargarUsuarios();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jmenu_habilitar_clickSecundario = new javax.swing.JPopupMenu();
        jmenu_habilitar = new javax.swing.JMenuItem();
        vbox1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2_usuarios = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jbtn_addUser = new javax.swing.JButton();
        jbtn_deleteUser = new javax.swing.JButton();
        jbtn_changePassword = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtbl_users = new javax.swing.JTable();
        jPanel3_impuestos = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtbl_impuestos = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jbtn_addImpuesto = new javax.swing.JButton();
        jbtn_editarImpuesto = new javax.swing.JButton();
        jbtn_deleteImpuesto = new javax.swing.JButton();
        jPanel4_datosNegocio = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jtf_rut = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jtf_razonSocial = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jtf_nombreFantasia = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jtf_direccion = new javax.swing.JTextField();
        jPanel17 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jtf_comuna = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jtf_ciudad = new javax.swing.JTextField();
        jPanel21 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jtf_fono = new javax.swing.JTextField();
        jbtn_guardarDatosNegocio = new javax.swing.JButton();
        jPanel8_puntosVenta = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jbtn_addPuntoVenta = new javax.swing.JButton();
        jbtn_editarPuntoVenta = new javax.swing.JButton();
        jbtn_deletePuntoVenta = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtbl_puntosVenta = new javax.swing.JTable();
        jPanel_salir = new javax.swing.JPanel();
        salir_btn = new javax.swing.JButton();

        jmenu_habilitar.setText("Habilitar");
        jmenu_habilitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenu_habilitarActionPerformed(evt);
            }
        });
        jmenu_habilitar_clickSecundario.add(jmenu_habilitar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administración");
        setMinimumSize(new java.awt.Dimension(640, 430));

        vbox1.setMinimumSize(new java.awt.Dimension(618, 396));

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jbtn_addUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add-user.png"))); // NOI18N
        jbtn_addUser.setText("Agregar");
        jbtn_addUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_addUserActionPerformed(evt);
            }
        });

        jbtn_deleteUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/user-group-delete.png"))); // NOI18N
        jbtn_deleteUser.setText("Deshabilitar");
        jbtn_deleteUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_deleteUserActionPerformed(evt);
            }
        });

        jbtn_changePassword.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/preferences-desktop-user-password.png"))); // NOI18N
        jbtn_changePassword.setText("Cambiar contraseña");
        jbtn_changePassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_changePasswordActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_changePassword)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_deleteUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_addUser)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_addUser)
                    .addComponent(jbtn_deleteUser)
                    .addComponent(jbtn_changePassword))
                .addContainerGap())
        );

        jtbl_users.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Apellido", "Rut", "Tipo", "habilitado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtbl_users.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtbl_usersMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jtbl_users);
        if (jtbl_users.getColumnModel().getColumnCount() > 0) {
            jtbl_users.getColumnModel().getColumn(4).setResizable(false);
            jtbl_users.getColumnModel().getColumn(4).setPreferredWidth(30);
        }

        javax.swing.GroupLayout jPanel2_usuariosLayout = new javax.swing.GroupLayout(jPanel2_usuarios);
        jPanel2_usuarios.setLayout(jPanel2_usuariosLayout);
        jPanel2_usuariosLayout.setHorizontalGroup(
            jPanel2_usuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2_usuariosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2_usuariosLayout.setVerticalGroup(
            jPanel2_usuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2_usuariosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("Usuarios", jPanel2_usuarios);

        jtbl_impuestos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Tasa %"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jtbl_impuestos);
        if (jtbl_impuestos.getColumnModel().getColumnCount() > 0) {
            jtbl_impuestos.getColumnModel().getColumn(0).setResizable(false);
            jtbl_impuestos.getColumnModel().getColumn(0).setPreferredWidth(30);
            jtbl_impuestos.getColumnModel().getColumn(2).setPreferredWidth(40);
        }

        jbtn_addImpuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_addImpuesto.setText("Agregar");
        jbtn_addImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_addImpuestoActionPerformed(evt);
            }
        });

        jbtn_editarImpuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/document-edit.png"))); // NOI18N
        jbtn_editarImpuesto.setText("Editar");
        jbtn_editarImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_editarImpuestoActionPerformed(evt);
            }
        });

        jbtn_deleteImpuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-remove.png"))); // NOI18N
        jbtn_deleteImpuesto.setText("Eliminar");
        jbtn_deleteImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_deleteImpuestoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_deleteImpuesto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_editarImpuesto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_addImpuesto)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_addImpuesto)
                    .addComponent(jbtn_deleteImpuesto)
                    .addComponent(jbtn_editarImpuesto))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3_impuestosLayout = new javax.swing.GroupLayout(jPanel3_impuestos);
        jPanel3_impuestos.setLayout(jPanel3_impuestosLayout);
        jPanel3_impuestosLayout.setHorizontalGroup(
            jPanel3_impuestosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3_impuestosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3_impuestosLayout.setVerticalGroup(
            jPanel3_impuestosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3_impuestosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("Impuestos", jPanel3_impuestos);

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.Y_AXIS));

        jPanel4.setLayout(new java.awt.GridLayout(1, 2));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("*Rut: ");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel4.add(jLabel1);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jtf_rut, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jtf_rut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel4.add(jPanel10);

        jPanel3.add(jPanel4);

        jPanel11.setLayout(new java.awt.GridLayout(1, 2));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Razón Social: ");
        jLabel7.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel11.add(jLabel7);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jtf_razonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jtf_razonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel11.add(jPanel12);

        jPanel3.add(jPanel11);

        jPanel13.setLayout(new java.awt.GridLayout(1, 2));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("*Nombre del negocio: ");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel13.add(jLabel8);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jtf_nombreFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jtf_nombreFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel13.add(jPanel14);

        jPanel3.add(jPanel13);

        jPanel15.setLayout(new java.awt.GridLayout(1, 2));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Dirección: ");
        jLabel9.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel15.add(jLabel9);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jtf_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jtf_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel15.add(jPanel16);

        jPanel3.add(jPanel15);

        jPanel17.setLayout(new java.awt.GridLayout(1, 2));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Comuna: ");
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel17.add(jLabel10);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jtf_comuna, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jtf_comuna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel17.add(jPanel18);

        jPanel3.add(jPanel17);

        jPanel19.setLayout(new java.awt.GridLayout(1, 2));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Ciudad: ");
        jLabel11.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel19.add(jLabel11);

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jtf_ciudad, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jtf_ciudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel19.add(jPanel20);

        jPanel3.add(jPanel19);

        jPanel21.setLayout(new java.awt.GridLayout(1, 2));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Fono: ");
        jLabel12.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel21.add(jLabel12);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addComponent(jtf_fono, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addComponent(jtf_fono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel21.add(jPanel22);

        jPanel3.add(jPanel21);

        jbtn_guardarDatosNegocio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/document-save.png"))); // NOI18N
        jbtn_guardarDatosNegocio.setText("Guardar");
        jbtn_guardarDatosNegocio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_guardarDatosNegocioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4_datosNegocioLayout = new javax.swing.GroupLayout(jPanel4_datosNegocio);
        jPanel4_datosNegocio.setLayout(jPanel4_datosNegocioLayout);
        jPanel4_datosNegocioLayout.setHorizontalGroup(
            jPanel4_datosNegocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4_datosNegocioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4_datosNegocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4_datosNegocioLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtn_guardarDatosNegocio)))
                .addContainerGap())
        );
        jPanel4_datosNegocioLayout.setVerticalGroup(
            jPanel4_datosNegocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4_datosNegocioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(jbtn_guardarDatosNegocio)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Datos del negocio", jPanel4_datosNegocio);

        jbtn_addPuntoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_addPuntoVenta.setText("Agregar");
        jbtn_addPuntoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_addPuntoVentaActionPerformed(evt);
            }
        });

        jbtn_editarPuntoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/document-edit.png"))); // NOI18N
        jbtn_editarPuntoVenta.setText("Editar");
        jbtn_editarPuntoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_editarPuntoVentaActionPerformed(evt);
            }
        });

        jbtn_deletePuntoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-remove.png"))); // NOI18N
        jbtn_deletePuntoVenta.setText("Eliminar");
        jbtn_deletePuntoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_deletePuntoVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_deletePuntoVenta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_editarPuntoVenta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_addPuntoVenta)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_addPuntoVenta)
                    .addComponent(jbtn_deletePuntoVenta)
                    .addComponent(jbtn_editarPuntoVenta))
                .addContainerGap())
        );

        jtbl_puntosVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Código máquina", "Nombre punto de venta", "Habilitada"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jtbl_puntosVenta);
        if (jtbl_puntosVenta.getColumnModel().getColumnCount() > 0) {
            jtbl_puntosVenta.getColumnModel().getColumn(0).setPreferredWidth(30);
            jtbl_puntosVenta.getColumnModel().getColumn(2).setPreferredWidth(40);
            jtbl_puntosVenta.getColumnModel().getColumn(3).setPreferredWidth(20);
        }

        javax.swing.GroupLayout jPanel8_puntosVentaLayout = new javax.swing.GroupLayout(jPanel8_puntosVenta);
        jPanel8_puntosVenta.setLayout(jPanel8_puntosVentaLayout);
        jPanel8_puntosVentaLayout.setHorizontalGroup(
            jPanel8_puntosVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8_puntosVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8_puntosVentaLayout.setVerticalGroup(
            jPanel8_puntosVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8_puntosVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("Puntos de venta", jPanel8_puntosVenta);

        salir_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/application-exit.png"))); // NOI18N
        salir_btn.setText("Salir");
        salir_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salir_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel_salirLayout = new javax.swing.GroupLayout(jPanel_salir);
        jPanel_salir.setLayout(jPanel_salirLayout);
        jPanel_salirLayout.setHorizontalGroup(
            jPanel_salirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_salirLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(salir_btn)
                .addContainerGap())
        );
        jPanel_salirLayout.setVerticalGroup(
            jPanel_salirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_salirLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(salir_btn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout vbox1Layout = new javax.swing.GroupLayout(vbox1);
        vbox1.setLayout(vbox1Layout);
        vbox1Layout.setHorizontalGroup(
            vbox1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, vbox1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
            .addComponent(jPanel_salir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        vbox1Layout.setVerticalGroup(
            vbox1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vbox1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_salir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(vbox1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(vbox1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void salir_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salir_btnActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salir_btnActionPerformed

    private void jbtn_addImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_addImpuestoActionPerformed
        JDialog dialog = new AddImpuesto(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        this.cargarImpuestos();
    }//GEN-LAST:event_jbtn_addImpuestoActionPerformed

    private void jbtn_addUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_addUserActionPerformed
        JDialog dialog = new AddUser(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarUsuarios();
    }//GEN-LAST:event_jbtn_addUserActionPerformed

    private void jbtn_deleteUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_deleteUserActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_users.getModel();
        int selectedRow = jtbl_users.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay usuarios registrados en el sistema para deshabilitar", 
                    "No es posible deshabilitar usuarios", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un usuario para deshabilitar", 
                    "No es posible deshabilitar usuarios", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_RUT = 2;
            String rutSeleccionado = (String)model.getValueAt(selectedRow, INDEX_RUT);
            
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            UsersJpaController controller = new UsersJpaController(emf);
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea deshabilitar el usuario seleccionado del sistema?", 
                    "Confirmación para deshabilitar usuario", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                Users u = controller.findUserByRut(rutSeleccionado);
                if (u == null) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido deshabilitar el usuario con rut \"".concat(rutSeleccionado).concat("\""), 
                    "Error al deshabilitar un usuario", JOptionPane.ERROR_MESSAGE);
                }
                else if (u.getTipoUsuario() == TipoUsuario.ADMINISTRADOR) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido deshabilitar el usuario con rut \"".concat(rutSeleccionado).concat("\", un usuario administrador no puede ser deshabilitar del sistema"), 
                    "Error al deshabilitar un usuario", JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                    try {
                        //Deshabilito el usuario en lugar de eliminarlo
                        u.setActivo(false);
                        controller.edit(u);
                        JOptionPane.showMessageDialog(this, 
                            "Se ha deshabilitado el usuario con rut \"".concat(rutSeleccionado).concat("\""), 
                            "Se ha deshabilitado un usuario", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch (NonexistentEntityException ex) {
                        Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    catch (Exception ex) {
                        Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            cargarUsuarios();
        }
    }//GEN-LAST:event_jbtn_deleteUserActionPerformed

    private void jbtn_changePasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_changePasswordActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_users.getModel();
        int selectedRow = jtbl_users.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay usuarios registrados en el sistema para modificar su contraseña", 
                    "No es posible cambiar la contraseña", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un usuario para modificar su contraseña", 
                    "No es posible  cambiar la contraseña", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_RUT = 2;
            String rutSeleccionado = (String)model.getValueAt(selectedRow, INDEX_RUT);
            
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            UsersJpaController controller = new UsersJpaController(emf);
            Users u = controller.findUserByRut(rutSeleccionado);
            if (u == null) {
                JOptionPane.showMessageDialog(this, 
                "No se ha podido encontrar el usuario con rut\"".concat(rutSeleccionado).concat("\""), 
                "Error interno al cambiar contraseña", JOptionPane.ERROR_MESSAGE);
            }
            else {
                javax.swing.JLabel usernameText = new javax.swing.JLabel("Cambie la contraseña para el usuario\"".concat(u.getRut().concat("\"")));
                javax.swing.JLabel jPassword1 = new javax.swing.JLabel("Contraseña nueva");
                javax.swing.JPasswordField password = new javax.swing.JPasswordField();
                javax.swing.JLabel jPassword2 = new javax.swing.JLabel("Confirmar contraseña nueva");
                javax.swing.JPasswordField passwordCopia = new javax.swing.JPasswordField();
                Object[] ob = {usernameText, jPassword1, password, jPassword2, passwordCopia};
                int result = JOptionPane.showConfirmDialog(null, ob, "Introduzca la nueva contraseña", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    if (!password.getText().equals(passwordCopia.getText())) {
                        JOptionPane.showMessageDialog(this, 
                            "Las contraseñas escritas no coinciden entre si, vuelva a intentarlo", 
                            "Error al cambiar la contraseña", JOptionPane.ERROR_MESSAGE);
                    }
                    else {
                        //System.out.println("Cambiando contraseña a: "+password.getText());
                        u.setPasswd(Utils.convertToMD5(password.getText()));
                        try {
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this, 
                            "Se ha cambiado la contraseña del usuario con rut \"".concat(u.getRut()).concat("\""), 
                            "Contraseña cambiada", JOptionPane.INFORMATION_MESSAGE);
                        } catch (Exception ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                            JOptionPane.showMessageDialog(this, 
                            "Error al cambiar la contraseña del usuario con rut \"".concat(u.getRut()).concat("\""), 
                            "Error interno", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_jbtn_changePasswordActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (jTabbedPane1.getSelectedIndex() == INDEX_TAB_USUARIOS) {
            
        }
        else if (jTabbedPane1.getSelectedIndex() == INDEX_TAB_IMPUESTOS) {
            
        }
        else if (jTabbedPane1.getSelectedIndex() == INDEX_TAB_DATOS_NEGOCIO) {
            this.getRootPane().setDefaultButton(this.jbtn_guardarDatosNegocio);
            cargarDatosNegocio();
        }
        else if (jTabbedPane1.getSelectedIndex() == INDEX_TAB_LOCALES) {
            
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jbtn_guardarDatosNegocioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_guardarDatosNegocioActionPerformed
        LoginController lc = LoginController.getInstance();
        Negocio neg = lc.getNegocio();
        if (neg == null) {
            //Error, DEBE HABER UN NEGOCIO
            
            return;
        }
        
        String nombre = jtf_nombreFantasia.getText().trim();
        if (nombre.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
            "El nombre del negocio no puede ser vacío", 
            "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        neg.setNombre(nombre);
        neg.setRut(jtf_rut.getText().trim());
        neg.setCiudad(jtf_ciudad.getText().trim());
        neg.setComuna(jtf_comuna.getText().trim());
        neg.setDireccion(jtf_direccion.getText().trim());
        neg.setFono(jtf_fono.getText().trim());
        neg.setRazonSocial(jtf_razonSocial.getText().trim());
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        NegocioJpaController controller = new NegocioJpaController(emf);
        try {
            controller.edit(neg);
            JOptionPane.showMessageDialog(this, 
            "Se han guardado los datos del negocio", 
            "Cambios guardados", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, 
            "Error al modificar los datos del negocio, intente más tarde", 
            "Error interno", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jbtn_guardarDatosNegocioActionPerformed

    private void jbtn_editarImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_editarImpuestoActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_impuestos.getModel();
        int selectedRow = jtbl_impuestos.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay impuestos agregados al sistema para poder modificar", 
                    "No es posible modificar el impuesto", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un impuesto para modificar", 
                    "No es posible modificar el impuesto", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_ID = 0;
            String idStr = (String)model.getValueAt(selectedRow, INDEX_ID);
            int id;
            try {
                id = Integer.parseInt(idStr);
            }
            catch (NumberFormatException nfe) {
                return;
            }
            JDialog dialog = new EditImpuesto(this, true, id);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            cargarImpuestos();
        }
    }//GEN-LAST:event_jbtn_editarImpuestoActionPerformed

    private void jbtn_deleteImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_deleteImpuestoActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_impuestos.getModel();
        int selectedRow = jtbl_impuestos.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay impuestos agregados al sistema para poder modificar", 
                    "No es posible modificar el impuesto", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un impuesto para modificar", 
                    "No es posible modificar el impuesto", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_ID = 0;
            String idStr = (String)model.getValueAt(selectedRow, INDEX_ID);
            int id;
            try {
                id = Integer.parseInt(idStr);
            }
            catch (NumberFormatException nfe) {
                return;
            }
            
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea eliminar el impuesto seleccionado del sistema?", 
                    "Confirmación para eliminar impuesto", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                EntityManagerFactory emf = Utils.getEntityManagerFactory();
                ImpuestoJpaController controller = new ImpuestoJpaController(emf);
                try {
                    controller.destroy(id);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        cargarImpuestos();
    }//GEN-LAST:event_jbtn_deleteImpuestoActionPerformed

    private void jmenu_habilitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenu_habilitarActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_users.getModel();
        int selectedRow = jtbl_users.getSelectedRow();
        if (selectedRow >= 0) {
            int INDEX_RUT = 2;
            String rutSeleccionado = (String)model.getValueAt(selectedRow, INDEX_RUT);
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            UsersJpaController controller = new UsersJpaController(emf);
            Users u = controller.findUserByRut(rutSeleccionado);
            if (u != null) {
                if (!u.isActivo()) {
                    int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea habilitar el usuario seleccionado del sistema?", 
                    "Confirmación para habilitar usuario", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcion == JOptionPane.YES_OPTION) {
                        try {
                            //Deshabilito el usuario en lugar de eliminarlo
                            u.setActivo(true);
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this, 
                                "Se ha habilitado el usuario con rut \"".concat(rutSeleccionado).concat("\""), 
                                "Se ha habilitado un usuario", JOptionPane.INFORMATION_MESSAGE);
                        }
                        catch (NonexistentEntityException ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        catch (Exception ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            cargarUsuarios();
        }
    }//GEN-LAST:event_jmenu_habilitarActionPerformed

    private void jtbl_usersMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtbl_usersMouseReleased
        if (evt.getButton() != java.awt.event.MouseEvent.BUTTON3) {
            return; //si no es click secundario, hace nada
        }
        int r = jtbl_users.rowAtPoint(evt.getPoint());
        if (r >= 0 && r < jtbl_users.getRowCount()) {
            jtbl_users.setRowSelectionInterval(r, r);
        } else {
            jtbl_users.clearSelection();
        }

        int rowindex = jtbl_users.getSelectedRow();
        if (rowindex < 0)
            return;
        int INDEX_RUT = 2;
        DefaultTableModel model = (DefaultTableModel)jtbl_users.getModel();
        String rutSeleccionado = (String)model.getValueAt(rowindex, INDEX_RUT);
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        UsersJpaController controller = new UsersJpaController(emf);
        Users u = controller.findUserByRut(rutSeleccionado);
        
        if (u != null) {
            if (!u.isActivo()) {
                if (evt.isPopupTrigger() && evt.getComponent() instanceof JTable ) {
                    JPopupMenu popup = jmenu_habilitar_clickSecundario;
                    popup.show(evt.getComponent(), evt.getX(), evt.getY());
                }
            }
        }
    }//GEN-LAST:event_jtbl_usersMouseReleased

    private void jbtn_addPuntoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_addPuntoVentaActionPerformed
        JDialog dialog = new AddPuntoVenta(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarPuntosVenta();
    }//GEN-LAST:event_jbtn_addPuntoVentaActionPerformed

    private void jbtn_editarPuntoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_editarPuntoVentaActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_puntosVenta.getModel();
        int selectedRow = jtbl_puntosVenta.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay puntos de venta agregados al sistema para poder modificar", 
                    "No es posible modificar el punto de venta", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un punto de venta para modificar", 
                    "No es posible modificar el punto de venta", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_ID = 0;
            String idStr = (String)model.getValueAt(selectedRow, INDEX_ID);
            int id;
            try {
                id = Integer.parseInt(idStr);
            }
            catch (NumberFormatException nfe) {
                return;
            }
            JDialog dialog = new EditPuntoVenta(this, true, id);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            cargarPuntosVenta();
        }
    }//GEN-LAST:event_jbtn_editarPuntoVentaActionPerformed

    private void jbtn_deletePuntoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_deletePuntoVentaActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_puntosVenta.getModel();
        int selectedRow = jtbl_puntosVenta.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay puntos de venta registrados en el sistema para eliminar", 
                    "No es posible eliminar puntos de venta", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un punto de venta para eliminar", 
                    "No es posible eliminar puntos de venta", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_ID = 0;
            String idSeleccionado = (String)model.getValueAt(selectedRow, INDEX_ID);
            
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            MaquinaJpaController controller = new MaquinaJpaController(emf);
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea eliminar el punto de venta del sistema?", 
                    "Confirmación para eliminar punto de venta", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                Maquina u = controller.findMaquina(Integer.parseInt(idSeleccionado));
                if (u == null) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido eliminar el punto de venta, error interno", 
                    "Error al eliminar un punto de venta", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    try {
                        //Intento eliminar el punto de venta
                        controller.destroy(u.getId());
                        JOptionPane.showMessageDialog(this, 
                        "Se ha eliminado el punto de venta", 
                        "Se ha eliminado un punto de venta", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch (Exception e) {
                        try {
                            //Deshabilito el usuario en lugar de eliminarlo
                            u.setHabilitada(false);
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this, 
                                "No ha sido posible eliminar el punto de venta, pero se ha deshabilitado", 
                                "Se ha deshabilitado un usuario", JOptionPane.INFORMATION_MESSAGE);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(AdminWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            cargarPuntosVenta();
        }
    }//GEN-LAST:event_jbtn_deletePuntoVentaActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel2_usuarios;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel3_impuestos;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel4_datosNegocio;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8_puntosVenta;
    private javax.swing.JPanel jPanel_salir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton jbtn_addImpuesto;
    private javax.swing.JButton jbtn_addPuntoVenta;
    private javax.swing.JButton jbtn_addUser;
    private javax.swing.JButton jbtn_changePassword;
    private javax.swing.JButton jbtn_deleteImpuesto;
    private javax.swing.JButton jbtn_deletePuntoVenta;
    private javax.swing.JButton jbtn_deleteUser;
    private javax.swing.JButton jbtn_editarImpuesto;
    private javax.swing.JButton jbtn_editarPuntoVenta;
    private javax.swing.JButton jbtn_guardarDatosNegocio;
    private javax.swing.JMenuItem jmenu_habilitar;
    private javax.swing.JPopupMenu jmenu_habilitar_clickSecundario;
    private javax.swing.JTable jtbl_impuestos;
    private javax.swing.JTable jtbl_puntosVenta;
    private javax.swing.JTable jtbl_users;
    private javax.swing.JTextField jtf_ciudad;
    private javax.swing.JTextField jtf_comuna;
    private javax.swing.JTextField jtf_direccion;
    private javax.swing.JTextField jtf_fono;
    private javax.swing.JTextField jtf_nombreFantasia;
    private javax.swing.JTextField jtf_razonSocial;
    private javax.swing.JTextField jtf_rut;
    private javax.swing.JButton salir_btn;
    private javax.swing.JPanel vbox1;
    // End of variables declaration//GEN-END:variables
}
