/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI.comprasIngresosBodega;

import cl.requies.portezuelo.JPAControllers.CompraJpaController;
import cl.requies.portezuelo.JPAControllers.EgresoJpaController;
import cl.requies.portezuelo.JPAControllers.ProductoJpaController;
import cl.requies.portezuelo.JPAControllers.TipoEgresoJpaController;
import cl.requies.portezuelo.controllers.BusquedaController;
import cl.requies.portezuelo.controllers.LoginController;
import cl.requies.portezuelo.entities.Compra;
import cl.requies.portezuelo.entities.CompraDetalle;
import cl.requies.portezuelo.entities.Egreso;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.others.Utils;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author victor
 */
public class AddCompra extends javax.swing.JDialog {
    private static final int COD_NOMBREPRODUCTO = 0;
    private static final int COL_CODIGOBARRAS = 1;
    private static final int COL_CODIGO_CORTO = 2;
    private static final int COL_COSTO_UNITARIO = 3;
    private static final int COL_CANTIDAD = 4;
    private static final int COL_COSTO_TOTAL = 5;
    
    private javax.swing.JTextField ultimoModificado;
    private javax.swing.JTextField penultimoModificado;
    
    /**
     * Creates new form AddOperacionInventario
     */
    public AddCompra(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        limpiarCampos();
    }
    
    private void limpiarCampos() {
        jtf_buscarProducto.selectAll();
        jtf_cantidadComprada.setText("");
        jtf_buscarProducto.requestFocus();
        jtf_costoTotal.setText("");
        jtf_costoUnitario.setText("");
        ultimoModificado = null;
        penultimoModificado = null;
        this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
    }
    
    private void cargarDatosProducto(Producto p) {
        if (p != null) {
            jlabel_producto.setText(p.toString());
            //jtf_cantidadComprada.setText(p.getStock().toString());
        }
        else {
            jlabel_producto.setText("");
            //jtf_cantidadComprada.setText("");
        }
        jtf_cantidadComprada.setText("");
    }
    
    private void calculoCostos() {
        if ((ultimoModificado == jtf_costoTotal && penultimoModificado == jtf_costoUnitario) || 
                (penultimoModificado == jtf_costoTotal && ultimoModificado == jtf_costoUnitario)) {
            jtf_cantidadComprada.setText("");
        }
        if ((ultimoModificado == jtf_cantidadComprada && penultimoModificado == jtf_costoUnitario) || 
                (penultimoModificado == jtf_cantidadComprada && ultimoModificado == jtf_costoUnitario)) {
            jtf_costoTotal.setText("");
        }
        if ((ultimoModificado == jtf_costoTotal && penultimoModificado == jtf_cantidadComprada) || 
                (penultimoModificado == jtf_costoTotal && ultimoModificado == jtf_cantidadComprada)) {
            jtf_costoUnitario.setText("");
        }
        
        
        String cantidadStr = jtf_cantidadComprada.getText().trim();
        String costoUnitarioStr = jtf_costoUnitario.getText().trim();
        String costoTotalStr = jtf_costoTotal.getText().trim();
        Double cantidad;
        Integer costoUnitario;
        Integer costoTotal;
        try {
            cantidad = Double.parseDouble(cantidadStr);
            if (cantidad <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            cantidad = null;
        }
        try {
            costoUnitario = Integer.parseInt(costoUnitarioStr);
            if (costoUnitario <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            costoUnitario = null;
        }
        try {
            costoTotal = Integer.parseInt(costoTotalStr);
            if (costoTotal <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            costoTotal = null;
        }
        if (cantidad != null && costoUnitario != null && costoTotal == null) {
            costoTotal = (int)(cantidad*costoUnitario);
            jtf_costoTotal.setText(costoTotal.toString());
        }
        else if (cantidad != null && costoUnitario == null && costoTotal != null) {
            costoUnitario = (int)(costoTotal/cantidad);
            jtf_costoUnitario.setText(costoUnitario.toString());
        }
        else if (cantidad == null && costoUnitario != null && costoTotal != null) {
            cantidad = costoTotal/(double)costoUnitario;
            jtf_cantidadComprada.setText(cantidad.toString());
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jmenu_clickSecundarioProductos = new javax.swing.JPopupMenu();
        jmenu_eliminarProductoDeCompra = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        jtf_buscarProducto = new javax.swing.JTextField();
        jbtn_buscarProducto = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jl_productosEncontrados = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        jtf_cantidadComprada = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jlabel_producto = new javax.swing.JLabel();
        jbtn_agregarProductoToCompra = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jtf_costoUnitario = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jtf_costoTotal = new javax.swing.JTextField();
        jbtn_guardar = new javax.swing.JButton();
        jbtn_cancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtbl_productosComprados = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jtf_descripcionCompra = new javax.swing.JTextField();

        jmenu_eliminarProductoDeCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-remove.png"))); // NOI18N
        jmenu_eliminarProductoDeCompra.setText("Quitar");
        jmenu_eliminarProductoDeCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenu_eliminarProductoDeCompraActionPerformed(evt);
            }
        });
        jmenu_clickSecundarioProductos.add(jmenu_eliminarProductoDeCompra);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agregar compra de productos");
        setMinimumSize(new java.awt.Dimension(540, 470));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Agregar producto a la compra"));
        jPanel2.setPreferredSize(new java.awt.Dimension(520, 265));

        jtf_buscarProducto.setToolTipText("Escriba aquí el código del producto o parte de su nombre");
        jtf_buscarProducto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_buscarProductoFocusGained(evt);
            }
        });

        jbtn_buscarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find_16.png"))); // NOI18N
        jbtn_buscarProducto.setText("Buscar");
        jbtn_buscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscarProductoActionPerformed(evt);
            }
        });

        jLabel1.setText("Resultado de búsqueda");

        jl_productosEncontrados.setModel(new DefaultListModel());
        jl_productosEncontrados.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jl_productosEncontrados.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jl_productosEncontradosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jl_productosEncontrados);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Información del producto"));

        jtf_cantidadComprada.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_cantidadCompradaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_cantidadCompradaFocusLost(evt);
            }
        });

        jLabel2.setText("Producto: ");

        jLabel3.setText("Cantidad comprada: ");

        jlabel_producto.setText(" ");

        jbtn_agregarProductoToCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_agregarProductoToCompra.setText("Agregar producto a la compra");
        jbtn_agregarProductoToCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_agregarProductoToCompraActionPerformed(evt);
            }
        });

        jLabel4.setText("Costo unitario: ");

        jtf_costoUnitario.setToolTipText("Costo por cada unidad comprada");
        jtf_costoUnitario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_costoUnitarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_costoUnitarioFocusLost(evt);
            }
        });

        jLabel5.setText("Costo total: ");

        jtf_costoTotal.setToolTipText("Costo total de ese producto, es decir, costo unitario por cantidad");
        jtf_costoTotal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_costoTotalFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_costoTotalFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtf_costoUnitario, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(jtf_costoTotal)
                            .addComponent(jtf_cantidadComprada))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jbtn_agregarProductoToCompra)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlabel_producto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jlabel_producto))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jtf_cantidadComprada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jtf_costoUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jtf_costoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jbtn_agregarProductoToCompra)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jtf_buscarProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscarProducto))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtf_buscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_buscarProducto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jbtn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/document-save.png"))); // NOI18N
        jbtn_guardar.setText("Guardar");
        jbtn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_guardarActionPerformed(evt);
            }
        });

        jbtn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-cancel.png"))); // NOI18N
        jbtn_cancelar.setText("Cancelar");
        jbtn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_cancelarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Productos de la compra"));

        jtbl_productosComprados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Código de barras", "Código corto", "Costo unitario", "Cantidad", "Costo total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtbl_productosComprados.setComponentPopupMenu(jmenu_clickSecundarioProductos);
        jScrollPane3.setViewportView(jtbl_productosComprados);
        if (jtbl_productosComprados.getColumnModel().getColumnCount() > 0) {
            jtbl_productosComprados.getColumnModel().getColumn(1).setResizable(false);
            jtbl_productosComprados.getColumnModel().getColumn(1).setPreferredWidth(120);
            jtbl_productosComprados.getColumnModel().getColumn(2).setResizable(false);
            jtbl_productosComprados.getColumnModel().getColumn(2).setPreferredWidth(80);
            jtbl_productosComprados.getColumnModel().getColumn(3).setResizable(false);
            jtbl_productosComprados.getColumnModel().getColumn(3).setPreferredWidth(80);
            jtbl_productosComprados.getColumnModel().getColumn(4).setResizable(false);
            jtbl_productosComprados.getColumnModel().getColumn(4).setPreferredWidth(50);
            jtbl_productosComprados.getColumnModel().getColumn(5).setResizable(false);
            jtbl_productosComprados.getColumnModel().getColumn(5).setPreferredWidth(80);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
        );

        jLabel6.setText("Breve descripción de la compra: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtn_cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbtn_guardar))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtf_descripcionCompra)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jtf_descripcionCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_cancelar)
                    .addComponent(jbtn_guardar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtn_buscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscarProductoActionPerformed
        String textoBusqueda = jtf_buscarProducto.getText().trim();
        if (textoBusqueda.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "No ha escrito nada para buscar", 
                    "Error en la búsqueda", JOptionPane.WARNING_MESSAGE);
            return;
        }
        BusquedaController controllerBusqueda = new BusquedaController();
        DefaultListModel lm = (DefaultListModel)jl_productosEncontrados.getModel();
        lm.removeAllElements();
        List<Producto> productosEncontrados = controllerBusqueda.findProductos(textoBusqueda);
        for (Producto p : productosEncontrados) {
            lm.addElement(p);
        }
        //Si se obtuvo un resultado, lo selecciono automáticamente
        if (lm.size() == 1) {
            jl_productosEncontrados.setSelectedIndex(0);
            cargarDatosProducto((Producto)jl_productosEncontrados.getSelectedValue());
            this.getRootPane().setDefaultButton(this.jbtn_agregarProductoToCompra);
            jtf_cantidadComprada.requestFocus();
            jtf_cantidadComprada.selectAll();
        }
        if (lm.isEmpty()) {
            jtf_buscarProducto.requestFocus();
            jtf_buscarProducto.selectAll();
            this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
        } 
        
    }//GEN-LAST:event_jbtn_buscarProductoActionPerformed

    private void jl_productosEncontradosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jl_productosEncontradosValueChanged
        //Se ha seleccionado un elemento de la lista de resultados de búsqueda
        Producto productoSeleccionado = (Producto)jl_productosEncontrados.getSelectedValue();
        cargarDatosProducto(productoSeleccionado);
        if (productoSeleccionado != null) {
            jtf_cantidadComprada.requestFocus();
            jtf_cantidadComprada.selectAll();
        }
    }//GEN-LAST:event_jl_productosEncontradosValueChanged

    private void jbtn_agregarProductoToCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_agregarProductoToCompraActionPerformed
        calculoCostos();
        //Se toma el producto seleccionado y se guarda en la lista de productos inventariados
        Producto productoSeleccionado = (Producto)jl_productosEncontrados.getSelectedValue();
        if (productoSeleccionado == null) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un producto para agregar a la compra", 
                    "Error en la validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        boolean posible =true;
        if (productoSeleccionado.getStock() == null) {
            posible = false;
        }
        else if (productoSeleccionado.getStock() < 0) {
            posible = false;
        }
        if (!posible) {
            JOptionPane.showMessageDialog(this, 
                    "El producto seleccionado no es posible de comprar debido a que no maneja stock", 
                    "Error en la validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        String cantidadStr = jtf_cantidadComprada.getText();
        Double cantidad;
        try {
            cantidad = Double.parseDouble(cantidadStr);
            if (cantidad <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            Logger.getLogger(AddCompra.class.getName()).log(Level.WARNING, null, nfe);
            JOptionPane.showMessageDialog(this, 
                    "No ha escrito un número en el formato correcto para la cantidad comprada", 
                    "Error en la validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        String costoUnitarioStr = jtf_costoUnitario.getText();
        Integer costoUnitario;
        try {
            costoUnitario = Integer.parseInt(costoUnitarioStr);
            if (costoUnitario <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            Logger.getLogger(AddCompra.class.getName()).log(Level.WARNING, null, nfe);
            JOptionPane.showMessageDialog(this, 
                    "No ha escrito un número en el formato correcto para el costo unitario", 
                    "Error en la validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        
        DefaultTableModel tm = (DefaultTableModel)jtbl_productosComprados.getModel();
        //Se verifica que el producto no esté en la tabla
        String codBarrasTempStr;
        long codBarrasTemp;
        boolean agregarNuevo = true;
        for (int i = 0; i < tm.getRowCount(); i++) {
            codBarrasTempStr = (String)tm.getValueAt(i, COL_CODIGOBARRAS);
            codBarrasTemp = Long.parseLong(codBarrasTempStr);
            if (codBarrasTemp == (productoSeleccionado.getBarcode())) {
                int opcion = JOptionPane.showConfirmDialog(this, 
                        "El producto que intenta agregar ya se encuentra en la compra, desea modificarlo?", 
                        "Producto ya agregado, ¿modificar?", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                
                
                
                if (opcion == JOptionPane.YES_OPTION) {
                    //Modifico el valor del stock real de ese producto
                    tm.setValueAt(cantidad.toString(), i, COL_CANTIDAD);
                    tm.setValueAt(costoUnitario.toString(), i, COL_COSTO_UNITARIO);
                    tm.setValueAt((Integer.toString((int)(costoUnitario*cantidad))), i, COL_COSTO_TOTAL);
                    agregarNuevo = false;
                    break;
                }
                jtf_buscarProducto.requestFocus();
                jtf_buscarProducto.selectAll();
                return;
            }
        }
        
        if (agregarNuevo){
            //Se agrega el producto y sus stock contabilizados a la tabla
            String row[] = new String[] {productoSeleccionado.toString(), 
                                    Long.toString(productoSeleccionado.getBarcode()), 
                                    productoSeleccionado.getCodigoCorto(), 
                                    costoUnitarioStr, 
                                    cantidadStr, 
                                    (Integer.toString((int)(costoUnitario*cantidad)))};
            tm.addRow(row);
        }
        limpiarCampos();
        jtf_buscarProducto.requestFocus();
        jtf_buscarProducto.setText("");
        ((DefaultListModel)jl_productosEncontrados.getModel()).removeAllElements();
        this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
    }//GEN-LAST:event_jbtn_agregarProductoToCompraActionPerformed

    private void jbtn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_cancelarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jbtn_cancelarActionPerformed

    private void jbtn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_guardarActionPerformed
        //Se debe guardar la compra y su detalle en la DB
        DefaultTableModel tm = (DefaultTableModel)jtbl_productosComprados.getModel();
        if (tm.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha agregado productos a la compra", 
                    "Error en la validación", JOptionPane.ERROR_MESSAGE);
                    jtf_buscarProducto.requestFocus();
                    jtf_buscarProducto.selectAll();
            return;
        }
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        CompraJpaController controller = new CompraJpaController(emf);
        ProductoJpaController controllerProd = new ProductoJpaController(emf);
        
        
        //Se crea el objeto compra
        Compra compr = new Compra();
        compr.setFecha(new Date());
        compr.setDescripcion(jtf_descripcionCompra.getText());
        compr.setUserId(LoginController.getInstance().getUsuarioLogueado()); //Posible queso con NULL acá
        Negocio negocio = LoginController.getInstance().getNegocio();
        compr.setNegocioId(negocio);
        compr.setCompraDetalleList(new ArrayList<CompraDetalle>());
        
        //Se crean los detalles de la compra y se agregan a la compra
        String codBarrasTempStr;
        long codBarrasTemp;
        CompraDetalle comprDetNvo;
        int totalCompra = 0, tempCompra;
        double cantidad;
        for (int i = 0; i < tm.getRowCount(); i++) {
            codBarrasTempStr = (String)tm.getValueAt(i, COL_CODIGOBARRAS);
            codBarrasTemp = Long.parseLong(codBarrasTempStr);
            comprDetNvo = new CompraDetalle();
            comprDetNvo.setProducto(controllerProd.findProducto(codBarrasTemp));
            cantidad = Double.parseDouble((String)tm.getValueAt(i, COL_CANTIDAD));
            comprDetNvo.setCantidad(cantidad);
            comprDetNvo.setCompra(compr);
            tempCompra = Integer.parseInt((String)tm.getValueAt(i, COL_COSTO_UNITARIO));
            totalCompra += (int)(tempCompra*cantidad);
            comprDetNvo.setCosto(tempCompra);
            
            compr.getCompraDetalleList().add(comprDetNvo);
        }
        
        //Acá en adelante se crea la compra en la base de datos
        try {
            controller.create(compr);
            /*
            JOptionPane.showMessageDialog(this, 
                    "Se ha registrado la compra en el sistema", 
                    "Compra guardada", JOptionPane.INFORMATION_MESSAGE);
            */
            int opcion = JOptionPane.showConfirmDialog(this, 
                        "Se ha registrado la compra por $".concat(Integer.toString(totalCompra)).concat(" ¿Desea además realizar un egreso de dinero de la caja?"), 
                        "Compra guardada, ¿Desea registrar egreso?", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);  
            if (opcion == JOptionPane.YES_OPTION) {
                EgresoJpaController egController = new EgresoJpaController(emf);
                Egreso eg = new Egreso();
                eg.setFecha(new Date());
                eg.setUserId(compr.getUserId());
                eg.setMonto(totalCompra);
                TipoEgresoJpaController controllerTipoIngreso = new TipoEgresoJpaController(emf);
                eg.setTipoEgresoId(controllerTipoIngreso.findTipoEgreso(TipoEgresoJpaController.EGRESO_POR_COMPRA));
                try {
                    egController.create(eg);
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al crear el egreso de dinero por la compra, intente más tarde", 
                    "Error al guardar el egreso de dinero", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddCompra.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al crear la compra, intente más tarde", 
                    "Error al guardar la compra", JOptionPane.ERROR_MESSAGE);
            
        }
        this.setVisible(false);
    }//GEN-LAST:event_jbtn_guardarActionPerformed

    private void jmenu_eliminarProductoDeCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenu_eliminarProductoDeCompraActionPerformed
        int prodSeleccionado =jtbl_productosComprados.getSelectedRow();
        if (prodSeleccionado == -1) {
            
            return;
        }
        DefaultTableModel tm = (DefaultTableModel)jtbl_productosComprados.getModel();
        tm.removeRow(prodSeleccionado);
        
        
    }//GEN-LAST:event_jmenu_eliminarProductoDeCompraActionPerformed

    private void jtf_buscarProductoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_buscarProductoFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
    }//GEN-LAST:event_jtf_buscarProductoFocusGained

    private void jtf_cantidadCompradaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_cantidadCompradaFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_agregarProductoToCompra);
    }//GEN-LAST:event_jtf_cantidadCompradaFocusGained

    private void jtf_cantidadCompradaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_cantidadCompradaFocusLost
        if (!jtf_cantidadComprada.getText().trim().isEmpty()) {
            penultimoModificado = ultimoModificado;
            ultimoModificado = jtf_cantidadComprada;
        }
        calculoCostos();
    }//GEN-LAST:event_jtf_cantidadCompradaFocusLost

    private void jtf_costoUnitarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_costoUnitarioFocusLost
        if (!jtf_costoUnitario.getText().trim().isEmpty()) {
            penultimoModificado = ultimoModificado;
            ultimoModificado = jtf_costoUnitario;
        }
        calculoCostos();
    }//GEN-LAST:event_jtf_costoUnitarioFocusLost

    private void jtf_costoTotalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_costoTotalFocusLost
        if (!jtf_costoTotal.getText().trim().isEmpty()) {
            penultimoModificado = ultimoModificado;
            ultimoModificado = jtf_costoTotal;
        }
        calculoCostos();
    }//GEN-LAST:event_jtf_costoTotalFocusLost

    private void jtf_costoUnitarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_costoUnitarioFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_agregarProductoToCompra);
    }//GEN-LAST:event_jtf_costoUnitarioFocusGained

    private void jtf_costoTotalFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_costoTotalFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_agregarProductoToCompra);
    }//GEN-LAST:event_jtf_costoTotalFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton jbtn_agregarProductoToCompra;
    private javax.swing.JButton jbtn_buscarProducto;
    private javax.swing.JButton jbtn_cancelar;
    private javax.swing.JButton jbtn_guardar;
    private javax.swing.JList jl_productosEncontrados;
    private javax.swing.JLabel jlabel_producto;
    private javax.swing.JPopupMenu jmenu_clickSecundarioProductos;
    private javax.swing.JMenuItem jmenu_eliminarProductoDeCompra;
    private javax.swing.JTable jtbl_productosComprados;
    private javax.swing.JTextField jtf_buscarProducto;
    private javax.swing.JTextField jtf_cantidadComprada;
    private javax.swing.JTextField jtf_costoTotal;
    private javax.swing.JTextField jtf_costoUnitario;
    private javax.swing.JTextField jtf_descripcionCompra;
    // End of variables declaration//GEN-END:variables
}
