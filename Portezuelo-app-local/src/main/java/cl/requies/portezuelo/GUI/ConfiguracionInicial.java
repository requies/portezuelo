/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI;

import ConexionRemoteServer.NegocioRemote;
import cl.requies.portezuelo.JPAControllers.NegocioJpaController;
import cl.requies.portezuelo.JPAControllers.UsersJpaController;
import cl.requies.portezuelo.controllers.PortezueloMain;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import cl.requies.portezuelo.others.AnalizadorRut;
import cl.requies.portezuelo.others.Utils;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author victor
 */
public class ConfiguracionInicial extends javax.swing.JDialog {

    private boolean configurado = false;
    private int pasoActual;
    
    String rut;
    String pass;
    String nombre;
    String direccion;
    String comuna;
    String ciudad;
    String fono;
    
    int idLocalVentasCreado;
    
    boolean existeAdmin = false;
    boolean existeNegocio = false;
    Users adminEncontrado;
    Negocio negocioEncontrado;
    
    AnalizadorRut analizadorRut;
    List<Negocio> resultadoRest;
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        analizadorRut = null;
    }
    
    /**
     * Creates new form ConfiguracionInicial
     */
    public ConfiguracionInicial(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        analizadorRut = new AnalizadorRut();
        this.getRootPane().setDefaultButton(this.jbtn_next1);
        setPaso(1, true);
    }
    
    private void setPaso(int paso, boolean avanzando) {
        if (paso == 1) {
            pasoActual = 1;
            jp_paso1.setVisible(true);
            jp_paso2.setVisible(false);
            jp_paso3.setVisible(false);
        }
        else if (paso == 2) {
            pasoActual = 2;
            jp_paso1.setVisible(false);
            jp_paso2.setVisible(true);
            jp_paso3.setVisible(false);
            adminEncontrado = findUsuarioAdminInDb();
            Negocio negocioTemp = findNegocioInDb();
            existeNegocio = false;
            existeAdmin = false;
            if (negocioTemp != null) {
                negocioEncontrado = negocioTemp;
                existeNegocio = true;
                //selecciono el negocio que está actualmente en la DB
                if (resultadoRest != null) {
                    for (Negocio neg : resultadoRest) {
                        if (neg.getId() == negocioEncontrado.getId().intValue()) {
                            //Si ya está ocupado, lo agrego al jcombobox porque está ocupado en ÉSTA DB LOCAL
                            DefaultComboBoxModel tm = (DefaultComboBoxModel) jcb_negocios.getModel();
                            if (neg.getSeleccionado()) {
                                tm.addElement(neg);
                            }
                            tm.setSelectedItem(neg);
                            idLocalVentasCreado = neg.getId();
                            break;
                        }
                    }
                }
            }
            if (adminEncontrado != null) {
                existeAdmin = true;
                jtf_adminRut.setText(adminEncontrado.getRut());
                jtf_adminPasswd.setText("");
                if (avanzando)
                    setPaso(paso+1, true); //Se salta la config del usuario admin
                else
                    setPaso(paso-1, false); //Se salta la config del usuario admin
            }
            
        }
        else if (paso == 3) {
            pasoActual = 3;
            jp_paso1.setVisible(false);
            jp_paso2.setVisible(false);
            jp_paso3.setVisible(true);
        }
        else {
            setPaso(1, true); //En caso de error
        }
        this.setSize(510, 260);
    }
    
    private Users findUsuarioAdminInDb() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        UsersJpaController controller = new UsersJpaController(emf);
        return controller.findUserAdmin();
    }
    
    private Negocio findNegocioInDb() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        NegocioJpaController controller = new NegocioJpaController(emf);
        List<Negocio> l = controller.findNegocioEntities();
        if (l.isEmpty())
            return null;
        return l.get(0); //Devuelvo el primero si hay resultados, SOLO DEBE HABER UNO
    }
    
    public boolean isConfigurado() {
        return configurado;
    }
    
    private boolean validarDatosUserAdmin() {
        rut = jtf_adminRut.getText().trim();
        pass = jtf_adminPasswd.getText().trim();
        if (rut.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "El rut del usuario administrador no puede ser vacío", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        rut = rut.replaceAll("[.]", "").replaceAll("-", "");
        try {
            analizadorRut.StringToIntFormat(rut);
        }
        catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, 
                    "El rut del usuario administrador no sigue un formato válido", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        try {
            if (!analizadorRut.validarRut(rut)) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(this, 
                    "El dígito verificador del rut no es válido", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (pass.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "La contraseña del usuario administrador no puede ser vacío", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        
        return true;
    }
    
    private boolean validarDatosNegocio() {
        nombre = jtf_nombreFantasia.getText().trim();
        direccion = jtf_direccion.getText().trim();
        comuna = jtf_comuna.getText().trim();
        ciudad = jtf_ciudad.getText().trim();
        fono = jtf_fono.getText().trim();
        
        if (idLocalVentasCreado < 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un negocio para asignar a este equipo, contacte con el administrador del servidor para crear negocios en caso que este sea la primera vez que se configura el sistema para este local", 
                    "Error al asignar negocio", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        if (nombre.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "El nombre del negocio/local no puede ser vacío", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    private void saveUserAdmin() {
        if (existeAdmin) {
            return; //si ya existe el usuario, no hace nada
        }
        Users u = new Users(1, rut, Utils.convertToMD5(pass), "Administrador", "");
        u.setApellM("");
        u.setFechaIngreso(new Date());
        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
        u.setActivo(true);
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        boolean updatear = false;
        try {
            em.getTransaction().begin();
            em.persist(u);
            em.getTransaction().commit();
            updatear = false;
        } catch (Exception ex) {
            //Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, 
                    "El usuario administrador ya existía, se modificarán los datos del administrador existente", 
                    "Administrador ya existente", JOptionPane.WARNING_MESSAGE);
            updatear = true;
        }finally {
            em.close();
        }
        if (updatear) {
            em = emf.createEntityManager();
            try {
                em.getTransaction().begin();
                em.merge(u);
                em.getTransaction().commit();
            }
            catch (Exception ex) {
                Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                em.close();
            }
        }
    }
    
    private void saveNegocio() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        NegocioJpaController controller = new NegocioJpaController(emf);
        Negocio neg;
        if (!existeNegocio) {
            neg = new Negocio();
        }
        else {
            neg = negocioEncontrado;
        }
        if (negocioEncontrado == null) {
            JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al guardar el negocio en la base de datos, no se ha seleccionado ninguno", 
                    "Error al guardar negocio", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        neg.setId(idLocalVentasCreado); //MUY IMPORTANTE!!!
        neg.setRut(negocioEncontrado.getRut());
        neg.setNombre(nombre);
        neg.setRazonSocial(negocioEncontrado.getRazonSocial());
        neg.setDireccion(direccion);
        neg.setCiudad(ciudad);
        neg.setComuna(comuna);
        neg.setGiro(negocioEncontrado.getGiro());
        neg.setFax(fono);
        neg.setFono(fono);
        neg.setSeleccionado(true);
        
        if (!existeNegocio) { //No existe
            try {
                controller.create(neg);
            }
            catch (Exception ex) {
                Logger.getLogger(ConfiguracionInicial.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al guardar el negocio en la base de datos", 
                    "Error al crear negocio", JOptionPane.ERROR_MESSAGE);
            }
        }
        else {
            try {
                controller.edit(neg);
            } catch (Exception ex) {
                Logger.getLogger(ConfiguracionInicial.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al guardar el negocio en la base de datos", 
                    "Error al editar el negocio", JOptionPane.ERROR_MESSAGE);
            }
        } 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_paso1 = new javax.swing.JPanel();
        jPanel_conexionDatabase = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jtf_db_url = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jtf_db_usuario = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jtf_db_password = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jtf_server = new javax.swing.JTextField();
        jbtn_next1 = new javax.swing.JButton();
        jbtn_cancelar = new javax.swing.JButton();
        jp_paso2 = new javax.swing.JPanel();
        jPanel_datosAdmin = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jtf_adminRut = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jtf_adminPasswd = new javax.swing.JTextField();
        jbtn_previous2 = new javax.swing.JButton();
        jbtn_next2 = new javax.swing.JButton();
        jp_paso3 = new javax.swing.JPanel();
        jPanel_localVentas = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jcb_negocios = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jtf_nombreFantasia = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jtf_direccion = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jtf_comuna = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jtf_ciudad = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jtf_fono = new javax.swing.JTextField();
        jbtn_previous3 = new javax.swing.JButton();
        jbtn_finalizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Configuración inicial");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel_conexionDatabase.setBorder(javax.swing.BorderFactory.createTitledBorder("Conexión a base de datos"));
        jPanel_conexionDatabase.setLayout(new java.awt.GridLayout(0, 2));

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Url de conexión JDBC: ");
        jLabel16.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_conexionDatabase.add(jLabel16);

        jtf_db_url.setText("jdbc:postgresql://127.0.0.1:5432/portezuelo");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_db_url, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jtf_db_url, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_conexionDatabase.add(jPanel21);

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Usuario: ");
        jLabel17.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_conexionDatabase.add(jLabel17);

        jtf_db_usuario.setText("root");

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_db_usuario, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addComponent(jtf_db_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_conexionDatabase.add(jPanel23);

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Contraseña: ");
        jLabel18.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_conexionDatabase.add(jLabel18);

        jtf_db_password.setText("root");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_db_password, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addComponent(jtf_db_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel_conexionDatabase.add(jPanel24);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Url de servidor de sincronización: ");
        jLabel20.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_conexionDatabase.add(jLabel20);

        jtf_server.setText("http://portezuelo.requies.cl");

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_server, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addComponent(jtf_server, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel_conexionDatabase.add(jPanel26);

        jbtn_next1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_next1.setText("Siguiente");
        jbtn_next1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_next1ActionPerformed(evt);
            }
        });

        jbtn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-cancel.png"))); // NOI18N
        jbtn_cancelar.setText("Cancelar");
        jbtn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jp_paso1Layout = new javax.swing.GroupLayout(jp_paso1);
        jp_paso1.setLayout(jp_paso1Layout);
        jp_paso1Layout.setHorizontalGroup(
            jp_paso1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jp_paso1Layout.createSequentialGroup()
                .addContainerGap(276, Short.MAX_VALUE)
                .addComponent(jbtn_cancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_next1)
                .addContainerGap())
            .addGroup(jp_paso1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_conexionDatabase, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jp_paso1Layout.setVerticalGroup(
            jp_paso1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jp_paso1Layout.createSequentialGroup()
                .addContainerGap(178, Short.MAX_VALUE)
                .addGroup(jp_paso1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jbtn_cancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jbtn_next1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jp_paso1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_conexionDatabase, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(69, Short.MAX_VALUE)))
        );

        jPanel_datosAdmin.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario administrador"));
        jPanel_datosAdmin.setToolTipText("");
        jPanel_datosAdmin.setLayout(new java.awt.GridLayout(0, 2));

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Rut: ");
        jLabel13.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_datosAdmin.add(jLabel13);

        jtf_adminRut.setText("1-9");
        jtf_adminRut.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jtf_adminRutCaretUpdate(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_adminRut, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addComponent(jtf_adminRut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_datosAdmin.add(jPanel15);

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Contraseña: ");
        jLabel14.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_datosAdmin.add(jLabel14);

        jtf_adminPasswd.setText("admin");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_adminPasswd, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addComponent(jtf_adminPasswd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_datosAdmin.add(jPanel17);

        jbtn_previous2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_previous2.setText("Anterior");
        jbtn_previous2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_previous2ActionPerformed(evt);
            }
        });

        jbtn_next2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_next2.setText("Siguiente");
        jbtn_next2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_next2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jp_paso2Layout = new javax.swing.GroupLayout(jp_paso2);
        jp_paso2.setLayout(jp_paso2Layout);
        jp_paso2Layout.setHorizontalGroup(
            jp_paso2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jp_paso2Layout.createSequentialGroup()
                .addContainerGap(286, Short.MAX_VALUE)
                .addComponent(jbtn_previous2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_next2)
                .addContainerGap())
            .addGroup(jp_paso2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_datosAdmin, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jp_paso2Layout.setVerticalGroup(
            jp_paso2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jp_paso2Layout.createSequentialGroup()
                .addContainerGap(175, Short.MAX_VALUE)
                .addGroup(jp_paso2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_next2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_previous2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jp_paso2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_datosAdmin, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        jPanel_localVentas.setBorder(javax.swing.BorderFactory.createTitledBorder("Local de ventas"));
        jPanel_localVentas.setToolTipText("");
        jPanel_localVentas.setLayout(new java.awt.GridLayout(0, 2));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Local de ventas/sucursal: ");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel8);

        jcb_negocios.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcb_negociosItemStateChanged(evt);
            }
        });
        jPanel_localVentas.add(jcb_negocios);

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Nombre: ");
        jLabel15.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel15);

        jtf_nombreFantasia.setText("Portezuelo sucursal 1");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_nombreFantasia, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jtf_nombreFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_localVentas.add(jPanel14);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Dirección: ");
        jLabel9.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel9);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_direccion, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jtf_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel_localVentas.add(jPanel16);

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Comuna: ");
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel10);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_comuna, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jtf_comuna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel_localVentas.add(jPanel18);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Ciudad: ");
        jLabel11.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel11);

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_ciudad, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jtf_ciudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel_localVentas.add(jPanel20);

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Fono: ");
        jLabel12.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_localVentas.add(jLabel12);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtf_fono, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addComponent(jtf_fono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel_localVentas.add(jPanel22);

        jbtn_previous3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_previous3.setText("Anterior");
        jbtn_previous3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_previous3ActionPerformed(evt);
            }
        });

        jbtn_finalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-ok.png"))); // NOI18N
        jbtn_finalizar.setText("Finalizar");
        jbtn_finalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_finalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jp_paso3Layout = new javax.swing.GroupLayout(jp_paso3);
        jp_paso3.setLayout(jp_paso3Layout);
        jp_paso3Layout.setHorizontalGroup(
            jp_paso3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jp_paso3Layout.createSequentialGroup()
                .addContainerGap(286, Short.MAX_VALUE)
                .addComponent(jbtn_previous3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_finalizar)
                .addContainerGap())
            .addGroup(jp_paso3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_localVentas, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jp_paso3Layout.setVerticalGroup(
            jp_paso3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jp_paso3Layout.createSequentialGroup()
                .addContainerGap(178, Short.MAX_VALUE)
                .addGroup(jp_paso3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_previous3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_finalizar))
                .addContainerGap())
            .addGroup(jp_paso3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jp_paso3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel_localVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(49, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jp_paso1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jp_paso2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jp_paso3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jp_paso1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jp_paso2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jp_paso3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbtn_next1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_next1ActionPerformed
        String DB_URL = jtf_db_url.getText().trim();
        if (DB_URL.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "La url de conexión a la base de datos no puede ser vacía", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String DB_USERNAME = jtf_db_usuario.getText().trim();
        if (DB_USERNAME.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "El nombre de usuario de conexión a la base de datos no puede ser vacío", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String DB_PASSWORD = jtf_db_password.getText().trim();
        if (DB_PASSWORD.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "La contraseña de conexión a la base de datos no puede ser vacía", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String DB_DRIVER = "org.postgresql.Driver"; //Antes se pedía en un jtexfield
        if (DB_DRIVER.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "El driver de conexión a la base de datos no puede ser vacío", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String URL_SERVER = jtf_server.getText().trim();
        if (URL_SERVER.isEmpty()) {
            JOptionPane.showMessageDialog(this, 
                    "La URL de conexión al servidor de sincronización no puede ser vacía", 
                    "Error de validación", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        NegocioRemote restClient;
        try {
            restClient = new NegocioRemote(URL_SERVER);
            resultadoRest = restClient.getJson();
            DefaultComboBoxModel tm = (DefaultComboBoxModel) jcb_negocios.getModel();
            tm.removeAllElements();
            
            for (Negocio u : resultadoRest) {
                if (!u.getSeleccionado()) {
                    tm.addElement(u);
                }
            }
            if (jcb_negocios.getSelectedIndex() >= 0) {
                negocioEncontrado = (Negocio)tm.getSelectedItem();
            }
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(this, 
                    "No se ha posido establecer conexión al servidor central, revise la URL", 
                    "Error de conexión", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        boolean res = Utils.setConfigInicial(DB_URL, DB_USERNAME, DB_PASSWORD, DB_DRIVER, URL_SERVER, false);
        configurado = res;
        if (Utils.probarConfigDB()) {
            setPaso(pasoActual+1, true);
        }
        else {
            JOptionPane.showMessageDialog(this, 
                    "No se ha posido establecer conexión a la base de datos", 
                    "Error de base de datos", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
    }//GEN-LAST:event_jbtn_next1ActionPerformed

    private void jbtn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_cancelarActionPerformed
        configurado = false;
        this.setVisible(false);
    }//GEN-LAST:event_jbtn_cancelarActionPerformed

    private void jbtn_previous2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_previous2ActionPerformed
        
        
        
        setPaso(pasoActual-1, false);
    }//GEN-LAST:event_jbtn_previous2ActionPerformed

    private void jbtn_previous3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_previous3ActionPerformed
        
        
        
        setPaso(pasoActual-1, false);
    }//GEN-LAST:event_jbtn_previous3ActionPerformed

    private void jbtn_finalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_finalizarActionPerformed
        if (!validarDatosNegocio()) {
            return;
        }
        saveUserAdmin();
        saveNegocio(); //Dentro esta función verifica si debe crear o editar dependiendo si el negocio existía o no
        
        boolean res = Utils.saveConfigInicial();
        if (!res) {
            JOptionPane.showMessageDialog(this, 
                    "No ha sido posible escribir en el archivo de configuración, revise si tiene permisos y si hay espacio en disco", 
                    "Error al guardar configuraciones", JOptionPane.ERROR_MESSAGE);
            configurado = false;
        }
        else {
            configurado = true;
        }
        this.setVisible(false);
    }//GEN-LAST:event_jbtn_finalizarActionPerformed

    private void jbtn_next2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_next2ActionPerformed
        if (existeAdmin) {
            return;
        }
        if (!validarDatosUserAdmin()) {
            return;
        }
        setPaso(pasoActual+1, true);
    }//GEN-LAST:event_jbtn_next2ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        configurado = false;
    }//GEN-LAST:event_formWindowClosing

    private void jcb_negociosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcb_negociosItemStateChanged
        Negocio neg;
        if (jcb_negocios.getSelectedIndex() < 0) {
            idLocalVentasCreado = -1;
            return;
        }
        neg = (Negocio)jcb_negocios.getSelectedItem();
        jtf_direccion.setText(neg.getDireccion());
        jtf_comuna.setText(neg.getComuna());
        jtf_ciudad.setText(neg.getCiudad());
        jtf_nombreFantasia.setText(neg.getNombre());
        jtf_fono.setText(neg.getFono());
        idLocalVentasCreado = neg.getId();
    }//GEN-LAST:event_jcb_negociosItemStateChanged

    private void jtf_adminRutCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jtf_adminRutCaretUpdate
        String rutEscrito = jtf_adminRut.getText();
        String rutRes = analizadorRut.formateaRut(rutEscrito);
        if (!rutEscrito.equals(rutRes)) {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    String rutEscrito = jtf_adminRut.getText();
                    String rutRes = analizadorRut.formateaRut(rutEscrito);
                    jtf_adminRut.setText(rutRes);
                }
            });
        }
    }//GEN-LAST:event_jtf_adminRutCaretUpdate


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel_conexionDatabase;
    private javax.swing.JPanel jPanel_datosAdmin;
    private javax.swing.JPanel jPanel_localVentas;
    private javax.swing.JButton jbtn_cancelar;
    private javax.swing.JButton jbtn_finalizar;
    private javax.swing.JButton jbtn_next1;
    private javax.swing.JButton jbtn_next2;
    private javax.swing.JButton jbtn_previous2;
    private javax.swing.JButton jbtn_previous3;
    private javax.swing.JComboBox jcb_negocios;
    private javax.swing.JPanel jp_paso1;
    private javax.swing.JPanel jp_paso2;
    private javax.swing.JPanel jp_paso3;
    private javax.swing.JTextField jtf_adminPasswd;
    private javax.swing.JTextField jtf_adminRut;
    private javax.swing.JTextField jtf_ciudad;
    private javax.swing.JTextField jtf_comuna;
    private javax.swing.JTextField jtf_db_password;
    private javax.swing.JTextField jtf_db_url;
    private javax.swing.JTextField jtf_db_usuario;
    private javax.swing.JTextField jtf_direccion;
    private javax.swing.JTextField jtf_fono;
    private javax.swing.JTextField jtf_nombreFantasia;
    private javax.swing.JTextField jtf_server;
    // End of variables declaration//GEN-END:variables
}
