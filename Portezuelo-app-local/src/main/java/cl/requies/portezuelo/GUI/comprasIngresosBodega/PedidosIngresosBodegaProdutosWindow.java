/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI.comprasIngresosBodega;

import cl.requies.portezuelo.JPAControllers.CompraJpaController;
import cl.requies.portezuelo.JPAControllers.InventarioJpaController;
import cl.requies.portezuelo.JPAControllers.PedidoJpaController;
import cl.requies.portezuelo.JPAControllers.ProductoJpaController;
import cl.requies.portezuelo.JPAControllers.exceptions.IllegalOrphanException;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.entities.Compra;
import cl.requies.portezuelo.entities.CompraDetalle;
import cl.requies.portezuelo.entities.Inventario;
import cl.requies.portezuelo.entities.InventarioDetalle;
import cl.requies.portezuelo.entities.Pedido;
import cl.requies.portezuelo.entities.PedidoDetalle;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.enums.EstadoPedido;
import cl.requies.portezuelo.others.Utils;
import java.awt.event.KeyEvent;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author victor
 */
public class PedidosIngresosBodegaProdutosWindow extends javax.swing.JFrame {
    private static final int ROWS_PER_PAGE = 20;
    private static final int COL_ID = 0;
    
    
    private static final int INDEX_TAB_PEDIDOS = 0;
    private static final int INDEX_TAB_PRODUCTOS = 1;
    private static final int INDEX_TAB_OPERACIONES_INVENTARIO = 2;
    private static final int INDEX_TAB_COMPRAS = 3;

    
    
    private int paginaActualProductos;
    private int paginaActualOperacionesInventario;
    private int paginaActualPedidos;
    private int paginaActualCompras;
    
    /**
     * Creates new form ComprasIngresosBodegaWindow
     */
    public PedidosIngresosBodegaProdutosWindow() {
        initComponents();
        paginaActualProductos = 1;
        paginaActualOperacionesInventario = 1;
        paginaActualPedidos = 1;
        paginaActualCompras = 1;
        cargarProductos();
        cargarOperacionesInventario();
        cargarCompras();
        cargarPedidos();
        add_producto_btn1.setVisible(false); //No se usa por ahora el botón "crear oferta"
    }
    
    private void cargarOperacionesInventario() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        InventarioJpaController controller = new InventarioJpaController(emf);
        List<Inventario> r;
        
        String textoBusqueda = jtf_buscar_inventario.getText().trim();
        int cantidadEncontrada, cantidadPaginas;
        if (textoBusqueda.isEmpty()) {
            r = controller.findInventarioEntities(ROWS_PER_PAGE, (paginaActualOperacionesInventario-1)*ROWS_PER_PAGE);
            cantidadEncontrada = controller.getInventarioCount();
        }
        else {
            r = controller.findInventarioEntitiesBySearch(textoBusqueda, ROWS_PER_PAGE, (paginaActualOperacionesInventario-1)*ROWS_PER_PAGE);
            cantidadEncontrada = (int)controller.getInventarioCountBySearch(textoBusqueda);
        }
        
        cantidadPaginas = (cantidadEncontrada-1)/ROWS_PER_PAGE + 1;
        if (cantidadPaginas <= 0) {
            cantidadPaginas = 1;
        }
        if (paginaActualOperacionesInventario > cantidadPaginas) {
            paginaActualOperacionesInventario = cantidadPaginas;
        }
        if (paginaActualOperacionesInventario < 1) {
            paginaActualOperacionesInventario = 1;
        }
        
        jlabel_numTotalPag_inventario.setText(Integer.toString(cantidadPaginas));
        jtf_numPag_inventario.setText(Integer.toString(paginaActualOperacionesInventario));
        jbtn_nextPag_inventario.setEnabled(paginaActualOperacionesInventario < cantidadPaginas);
        jbtn_prevPag_inventario.setEnabled(paginaActualOperacionesInventario > 1);
        
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_operacionesInventario.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            String negocioStr;
            List<InventarioDetalle> listaElementos;
            int cantidadAjustes;
            for (Inventario u : r) {
                negocioStr = u.getNegocioId().getNombre().concat(" ")
                        .concat(u.getNegocioId().getDireccion()).concat(" ")
                        .concat(u.getNegocioId().getComuna());
                listaElementos = u.getInventarioDetalleList();
                cantidadAjustes = 0;
                for (InventarioDetalle invDet : listaElementos) {
                    if (invDet.getCantidad() != invDet.getCantidadEsperada().doubleValue()) {
                        cantidadAjustes++;
                    }
                }
                row = new String[] {u.getId().toString(), u.getFecha().toString(), 
                    negocioStr, u.getUserId().toString(), 
                    Integer.toString(listaElementos.size()), 
                    Integer.toString(cantidadAjustes)};
                tm.addRow(row);
            }
        }
    }
    
    private void cargarProductos() {
        String textoBusqueda = jtf_buscar_productos.getText().trim();
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        ProductoJpaController controller = new ProductoJpaController(emf);
        
        int cantidadEncontrada, cantidadPaginas;
        List<Producto> r;
        if (textoBusqueda.isEmpty()) {
            r = controller.findProductoEntities(ROWS_PER_PAGE, (paginaActualProductos-1)*ROWS_PER_PAGE);
            cantidadEncontrada = controller.getProductoCount();
        }
        else {
            r = controller.findProductoEntitiesBySearch(textoBusqueda, ROWS_PER_PAGE, (paginaActualProductos-1)*ROWS_PER_PAGE);
            cantidadEncontrada = (int)controller.getProductoCountBySearch(textoBusqueda);
        }
        
        
        cantidadPaginas = (cantidadEncontrada-1)/ROWS_PER_PAGE + 1;
        if (cantidadPaginas <= 0) {
            cantidadPaginas = 1;
        }
        if (paginaActualProductos > cantidadPaginas) {
            paginaActualProductos = cantidadPaginas;
        }
        if (paginaActualProductos < 1) {
            paginaActualProductos = 1;
        }
        
        jlabel_numTotalPag_productos.setText(Integer.toString(cantidadPaginas));
        jtf_numPag_productos.setText(Integer.toString(paginaActualProductos));
        jbtn_nextPag_productos.setEnabled(paginaActualProductos < cantidadPaginas);
        jbtn_prevPag_productos.setEnabled(paginaActualProductos > 1);
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_productos.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            Double stock;
            String stockStr;
            for (Producto u : r) {
                stock = u.getStock();
                stockStr = "";
                if (stock != null) {
                    if (stock >= 0) {
                        stockStr = stock.toString();
                        if (u.getEsVentaFraccionada() != null) {
                            if (!u.getEsVentaFraccionada()) {
                                stockStr = Integer.toString(stock.intValue());
                            }
                        }
                    }
                }
                row = new String[] {u.getBarcode().toString(), 
                    u.getCodigoCorto(), 
                    u.getDescripcion(), 
                    u.getMarca(), 
                    u.getContenido().concat(" ").concat(u.getUnidad()), 
                    "$".concat(Integer.toString(u.getPrecio().intValue())), 
                    stockStr};
                tm.addRow(row);
            }
        }
    }
    
    private void cargarCompras() {
        String textoBusqueda = jtf_buscar_compra.getText().trim();
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        CompraJpaController controller = new CompraJpaController(emf);
        
        int cantidadEncontrada, cantidadPaginas;
        List<Compra> r;
        if (textoBusqueda.isEmpty()) {
            r = controller.findCompraEntities(ROWS_PER_PAGE, (paginaActualCompras-1)*ROWS_PER_PAGE);
            cantidadEncontrada = controller.getCompraCount();
        }
        else {
            r = controller.findCompraEntitiesBySearch(textoBusqueda, ROWS_PER_PAGE, (paginaActualCompras-1)*ROWS_PER_PAGE);
            cantidadEncontrada = (int)controller.getCompraCountBySearch(textoBusqueda);
        }
        
        
        cantidadPaginas = (cantidadEncontrada-1)/ROWS_PER_PAGE + 1;
        if (cantidadPaginas <= 0) {
            cantidadPaginas = 1;
        }
        if (paginaActualCompras > cantidadPaginas) {
            paginaActualCompras = cantidadPaginas;
        }
        if (paginaActualCompras < 1) {
            paginaActualCompras = 1;
        }
        
        jlabel_numTotalPag_compras.setText(Integer.toString(cantidadPaginas));
        jtf_numPag_compras.setText(Integer.toString(paginaActualCompras));
        jbtn_nextPag_compras.setEnabled(paginaActualCompras < cantidadPaginas);
        jbtn_prevPag_compras.setEnabled(paginaActualCompras > 1);
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_compras.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            String negocioStr;
            List<CompraDetalle> listaElementos;
            double montoTotal;
            for (Compra u : r) {
                negocioStr = u.getNegocioId().getNombre().concat(" ")
                        .concat(u.getNegocioId().getDireccion()).concat(" ")
                        .concat(u.getNegocioId().getComuna());
                listaElementos = u.getCompraDetalleList();
                montoTotal = 0;
                for (CompraDetalle det : listaElementos) {
                    montoTotal += det.getCosto()*det.getCantidad();
                }
                row = new String[] {u.getId().toString(), u.getFecha().toString(), 
                    negocioStr, u.getUserId().toString(), 
                    Integer.toString(listaElementos.size()), 
                    "$".concat(Integer.toString((int)montoTotal))};
                tm.addRow(row);
            }
        }
    }
    
    
    private void cargarPedidos() {
        String textoBusqueda = jtf_buscar_compra.getText().trim();
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        PedidoJpaController controller = new PedidoJpaController(emf);
        
        int cantidadEncontrada, cantidadPaginas;
        List<Pedido> r;
        if (textoBusqueda.isEmpty()) {
            r = controller.findPedidoEntities(ROWS_PER_PAGE, (paginaActualPedidos-1)*ROWS_PER_PAGE);
            cantidadEncontrada = controller.getPedidoCount();
        }
        else {
            r = controller.findPedidoEntitiesBySearch(textoBusqueda, ROWS_PER_PAGE, (paginaActualPedidos-1)*ROWS_PER_PAGE);
            cantidadEncontrada = (int)controller.getPedidoCountBySearch(textoBusqueda);
        }
        
        
        cantidadPaginas = (cantidadEncontrada-1)/ROWS_PER_PAGE + 1;
        if (cantidadPaginas <= 0) {
            cantidadPaginas = 1;
        }
        if (paginaActualPedidos > cantidadPaginas) {
            paginaActualPedidos = cantidadPaginas;
        }
        if (paginaActualPedidos < 1) {
            paginaActualPedidos = 1;
        }
        
        jlabel_numTotalPag_pedido.setText(Integer.toString(cantidadPaginas));
        jtf_numPag_pedido.setText(Integer.toString(paginaActualPedidos));
        jbtn_nextPag_pedido.setEnabled(paginaActualPedidos < cantidadPaginas);
        jbtn_prevPag_pedido.setEnabled(paginaActualPedidos > 1);
        
        if (r!= null) {
            DefaultTableModel tm = (DefaultTableModel)jtbl_pedidos.getModel();
            while (tm.getRowCount() > 0)
                tm.removeRow(0);
            String row[];
            String negocioStr;
            List<PedidoDetalle> listaElementos;
            for (Pedido u : r) {
                negocioStr = u.getNegocioId().getNombre().concat(" ")
                        .concat(u.getNegocioId().getDireccion()).concat(" ")
                        .concat(u.getNegocioId().getComuna());
                listaElementos = u.getPedidoDetalleList();
                row = new String[] {u.getId().toString(), u.getFecha().toString(), 
                    negocioStr, u.getUserId().toString(), 
                    Integer.toString(listaElementos.size()), 
                    EstadoPedido.ESTADOS_PEDIDO[u.getEstado()]};
                tm.addRow(row);
            }
        }
    }
    
    @Deprecated
    private void eliminarProductoSeleccionado() {
        DefaultTableModel model = (DefaultTableModel)jtbl_productos.getModel();
        int selectedRow = jtbl_productos.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay producto registrados en el sistema para eliminar", 
                    "No es posible eliminar productos", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un producto para eliminar", 
                    "No es posible eliminar productos", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int INDEX_CODIGO = 0;
            String codigoSeleccionado = (String)model.getValueAt(selectedRow, INDEX_CODIGO);
            
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            ProductoJpaController controller = new ProductoJpaController(emf);
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea eliminar el producto seleccionado del sistema?", 
                    "Confirmación para eliminar producto", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                Producto u = controller.findProducto(Long.parseLong(codigoSeleccionado));
                if (u == null) {
                    JOptionPane.showMessageDialog(this, 
                    "No se ha podido eliminar el producto con código \"".concat(codigoSeleccionado).concat("\""), 
                    "Error al eliminar un producto", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    try {
                        //Intento eliminar el usuario
                        controller.destroy(u.getBarcode());
                        JOptionPane.showMessageDialog(this, 
                        "Se ha eliminado el producto con código \"".concat(codigoSeleccionado).concat("\""), 
                        "Se ha eliminado un producto", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch (IllegalOrphanException | NonexistentEntityException e) {
                        try {
                            //Deshabilito el usuario en lugar de eliminarlo
                            u.setEstado(false);
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this, 
                                "No ha sido posible eliminar el producto con código \"".concat(codigoSeleccionado).concat("\", pero se ha deshabilitado"), 
                                "Se ha deshabilitado un producto", JOptionPane.INFORMATION_MESSAGE);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(PedidosIngresosBodegaProdutosWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(PedidosIngresosBodegaProdutosWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
            }
            cargarProductos();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jmenu_cancelarPedido_clickSecundario = new javax.swing.JPopupMenu();
        jmenu_cancelarPedido = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        tab1 = new javax.swing.JTabbedPane();
        jPanel_pedidos = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jbtn_agregarPedido = new javax.swing.JButton();
        jbtn_verDetallesPedido = new javax.swing.JButton();
        jbtn_recibirPedido = new javax.swing.JButton();
        jScrollPane_listaPedidos = new javax.swing.JScrollPane();
        jtbl_pedidos = new javax.swing.JTable();
        jPanel14 = new javax.swing.JPanel();
        jbtn_nextPag_pedido = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jtf_numPag_pedido = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jbtn_prevPag_pedido = new javax.swing.JButton();
        jlabel_numTotalPag_pedido = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jbtn_buscar_pedido = new javax.swing.JButton();
        jtf_buscar_pedido = new javax.swing.JTextField();
        jPanel_productos = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        add_producto_btn = new javax.swing.JButton();
        add_producto_btn1 = new javax.swing.JButton();
        jScrollPane_listaProductos = new javax.swing.JScrollPane();
        jtbl_productos = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jbtn_buscar_productos = new javax.swing.JButton();
        jtf_buscar_productos = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jbtn_nextPag_productos = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jtf_numPag_productos = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jbtn_prevPag_productos = new javax.swing.JButton();
        jlabel_numTotalPag_productos = new javax.swing.JLabel();
        jPanel_inventario = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jbtn_realizarOperacionInventario = new javax.swing.JButton();
        jbtn_verOperacionInventario = new javax.swing.JButton();
        jScrollPane_listaOperacionesInventario = new javax.swing.JScrollPane();
        jtbl_operacionesInventario = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jbtn_nextPag_inventario = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jtf_numPag_inventario = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jbtn_prevPag_inventario = new javax.swing.JButton();
        jlabel_numTotalPag_inventario = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jbtn_buscar_inventario = new javax.swing.JButton();
        jtf_buscar_inventario = new javax.swing.JTextField();
        jPanel_comprasLocal = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jbtn_agregarCompra = new javax.swing.JButton();
        jbtn_verCompra = new javax.swing.JButton();
        jScrollPane_listaCompras = new javax.swing.JScrollPane();
        jtbl_compras = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        jbtn_nextPag_compras = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jtf_numPag_compras = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jbtn_prevPag_compras = new javax.swing.JButton();
        jlabel_numTotalPag_compras = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jbtn_buscar_compra = new javax.swing.JButton();
        jtf_buscar_compra = new javax.swing.JTextField();
        exit_btn = new javax.swing.JButton();

        jmenu_cancelarPedido.setText("Cancelar");
        jmenu_cancelarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmenu_cancelarPedidoActionPerformed(evt);
            }
        });
        jmenu_cancelarPedido_clickSecundario.add(jmenu_cancelarPedido);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(634, 503));

        tab1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tab1StateChanged(evt);
            }
        });

        jbtn_agregarPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_agregarPedido.setText("Agregar pedido");
        jbtn_agregarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_agregarPedidoActionPerformed(evt);
            }
        });

        jbtn_verDetallesPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/view-list-details.png"))); // NOI18N
        jbtn_verDetallesPedido.setText("Ver detalles pedido");
        jbtn_verDetallesPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_verDetallesPedidoActionPerformed(evt);
            }
        });

        jbtn_recibirPedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/arrow-down-double.png"))); // NOI18N
        jbtn_recibirPedido.setText("Recibir pedido");
        jbtn_recibirPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_recibirPedidoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_recibirPedido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_verDetallesPedido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_agregarPedido)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_agregarPedido)
                    .addComponent(jbtn_verDetallesPedido)
                    .addComponent(jbtn_recibirPedido))
                .addContainerGap())
        );

        jtbl_pedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Fecha creación", "Local", "Usuario realizador", "Productos pedidos", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtbl_pedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtbl_pedidosMouseReleased(evt);
            }
        });
        jScrollPane_listaPedidos.setViewportView(jtbl_pedidos);
        if (jtbl_pedidos.getColumnModel().getColumnCount() > 0) {
            jtbl_pedidos.getColumnModel().getColumn(0).setResizable(false);
            jtbl_pedidos.getColumnModel().getColumn(0).setPreferredWidth(30);
            jtbl_pedidos.getColumnModel().getColumn(1).setPreferredWidth(90);
            jtbl_pedidos.getColumnModel().getColumn(3).setPreferredWidth(90);
            jtbl_pedidos.getColumnModel().getColumn(4).setPreferredWidth(90);
            jtbl_pedidos.getColumnModel().getColumn(5).setResizable(false);
        }

        jbtn_nextPag_pedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_nextPag_pedido.setText("Siguiente");
        jbtn_nextPag_pedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_nextPag_pedidoActionPerformed(evt);
            }
        });

        jLabel7.setText("/");

        jtf_numPag_pedido.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_numPag_pedidoFocusLost(evt);
            }
        });
        jtf_numPag_pedido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtf_numPag_pedidoKeyReleased(evt);
            }
        });

        jLabel8.setText("Página: ");

        jbtn_prevPag_pedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_prevPag_pedido.setText("Anterior");
        jbtn_prevPag_pedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_prevPag_pedidoActionPerformed(evt);
            }
        });

        jlabel_numTotalPag_pedido.setText("10");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_prevPag_pedido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_numPag_pedido, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_numTotalPag_pedido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_nextPag_pedido)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jbtn_nextPag_pedido)
                    .addComponent(jLabel7)
                    .addComponent(jtf_numPag_pedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_prevPag_pedido)
                    .addComponent(jlabel_numTotalPag_pedido))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jbtn_buscar_pedido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find_16.png"))); // NOI18N
        jbtn_buscar_pedido.setText("Buscar");
        jbtn_buscar_pedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscar_pedidoActionPerformed(evt);
            }
        });

        jtf_buscar_pedido.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_buscar_pedidoFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jtf_buscar_pedido, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscar_pedido, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_buscar_pedido)
                    .addComponent(jtf_buscar_pedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel_pedidosLayout = new javax.swing.GroupLayout(jPanel_pedidos);
        jPanel_pedidos.setLayout(jPanel_pedidosLayout);
        jPanel_pedidosLayout.setHorizontalGroup(
            jPanel_pedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_pedidosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_pedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_pedidosLayout.createSequentialGroup()
                        .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane_listaPedidos, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel_pedidosLayout.setVerticalGroup(
            jPanel_pedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_pedidosLayout.createSequentialGroup()
                .addGroup(jPanel_pedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane_listaPedidos, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tab1.addTab("Pedidos", jPanel_pedidos);

        add_producto_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        add_producto_btn.setText("Agregar producto");
        add_producto_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_producto_btnActionPerformed(evt);
            }
        });

        add_producto_btn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add-user.png"))); // NOI18N
        add_producto_btn1.setText("Agregar oferta");
        add_producto_btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_producto_btn1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(add_producto_btn1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(add_producto_btn)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(add_producto_btn)
                    .addComponent(add_producto_btn1))
                .addContainerGap())
        );

        jtbl_productos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Código corto", "Descripción", "Marca", "Cantidad", "Precio venta", "stock"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane_listaProductos.setViewportView(jtbl_productos);
        if (jtbl_productos.getColumnModel().getColumnCount() > 0) {
            jtbl_productos.getColumnModel().getColumn(0).setResizable(false);
            jtbl_productos.getColumnModel().getColumn(1).setResizable(false);
            jtbl_productos.getColumnModel().getColumn(1).setPreferredWidth(70);
            jtbl_productos.getColumnModel().getColumn(2).setPreferredWidth(170);
            jtbl_productos.getColumnModel().getColumn(4).setPreferredWidth(40);
            jtbl_productos.getColumnModel().getColumn(6).setResizable(false);
            jtbl_productos.getColumnModel().getColumn(6).setPreferredWidth(20);
        }

        jbtn_buscar_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find_16.png"))); // NOI18N
        jbtn_buscar_productos.setText("Buscar");
        jbtn_buscar_productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscar_productosActionPerformed(evt);
            }
        });

        jtf_buscar_productos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_buscar_productosFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jtf_buscar_productos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscar_productos, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_buscar_productos)
                    .addComponent(jtf_buscar_productos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jbtn_nextPag_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_nextPag_productos.setText("Siguiente");
        jbtn_nextPag_productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_nextPag_productosActionPerformed(evt);
            }
        });

        jLabel3.setText("/");

        jtf_numPag_productos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_numPag_productosFocusLost(evt);
            }
        });
        jtf_numPag_productos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtf_numPag_productosKeyReleased(evt);
            }
        });

        jLabel4.setText("Página: ");

        jbtn_prevPag_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_prevPag_productos.setText("Anterior");
        jbtn_prevPag_productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_prevPag_productosActionPerformed(evt);
            }
        });

        jlabel_numTotalPag_productos.setText("10");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_prevPag_productos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_numPag_productos, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_numTotalPag_productos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_nextPag_productos)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jbtn_nextPag_productos)
                    .addComponent(jLabel3)
                    .addComponent(jtf_numPag_productos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_prevPag_productos)
                    .addComponent(jlabel_numTotalPag_productos))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel_productosLayout = new javax.swing.GroupLayout(jPanel_productos);
        jPanel_productos.setLayout(jPanel_productosLayout);
        jPanel_productosLayout.setHorizontalGroup(
            jPanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel_productosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane_listaProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                    .addGroup(jPanel_productosLayout.createSequentialGroup()
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel_productosLayout.setVerticalGroup(
            jPanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_productosLayout.createSequentialGroup()
                .addGroup(jPanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane_listaProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        tab1.addTab("Productos", jPanel_productos);

        jbtn_realizarOperacionInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_realizarOperacionInventario.setText("Realizar inventario");
        jbtn_realizarOperacionInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_realizarOperacionInventarioActionPerformed(evt);
            }
        });

        jbtn_verOperacionInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/view-list-details.png"))); // NOI18N
        jbtn_verOperacionInventario.setText("Ver detalles inventario");
        jbtn_verOperacionInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_verOperacionInventarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_verOperacionInventario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_realizarOperacionInventario)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_realizarOperacionInventario)
                    .addComponent(jbtn_verOperacionInventario))
                .addContainerGap())
        );

        jtbl_operacionesInventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Fecha", "Local", "Usuario realizador", "Productos revisados", "Productos ajustados"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane_listaOperacionesInventario.setViewportView(jtbl_operacionesInventario);
        if (jtbl_operacionesInventario.getColumnModel().getColumnCount() > 0) {
            jtbl_operacionesInventario.getColumnModel().getColumn(0).setResizable(false);
            jtbl_operacionesInventario.getColumnModel().getColumn(0).setPreferredWidth(30);
            jtbl_operacionesInventario.getColumnModel().getColumn(1).setResizable(false);
            jtbl_operacionesInventario.getColumnModel().getColumn(1).setPreferredWidth(90);
            jtbl_operacionesInventario.getColumnModel().getColumn(3).setPreferredWidth(90);
            jtbl_operacionesInventario.getColumnModel().getColumn(4).setPreferredWidth(90);
            jtbl_operacionesInventario.getColumnModel().getColumn(5).setResizable(false);
            jtbl_operacionesInventario.getColumnModel().getColumn(5).setPreferredWidth(90);
        }

        jbtn_nextPag_inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_nextPag_inventario.setText("Siguiente");
        jbtn_nextPag_inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_nextPag_inventarioActionPerformed(evt);
            }
        });

        jLabel1.setText("/");

        jtf_numPag_inventario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_numPag_inventarioFocusLost(evt);
            }
        });
        jtf_numPag_inventario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtf_numPag_inventarioKeyReleased(evt);
            }
        });

        jLabel2.setText("Página: ");

        jbtn_prevPag_inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_prevPag_inventario.setText("Anterior");
        jbtn_prevPag_inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_prevPag_inventarioActionPerformed(evt);
            }
        });

        jlabel_numTotalPag_inventario.setText("10");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_prevPag_inventario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_numPag_inventario, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_numTotalPag_inventario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_nextPag_inventario)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jbtn_nextPag_inventario)
                    .addComponent(jLabel1)
                    .addComponent(jtf_numPag_inventario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_prevPag_inventario)
                    .addComponent(jlabel_numTotalPag_inventario))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jbtn_buscar_inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find_16.png"))); // NOI18N
        jbtn_buscar_inventario.setText("Buscar");
        jbtn_buscar_inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscar_inventarioActionPerformed(evt);
            }
        });

        jtf_buscar_inventario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_buscar_inventarioFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jtf_buscar_inventario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscar_inventario, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_buscar_inventario)
                    .addComponent(jtf_buscar_inventario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel_inventarioLayout = new javax.swing.GroupLayout(jPanel_inventario);
        jPanel_inventario.setLayout(jPanel_inventarioLayout);
        jPanel_inventarioLayout.setHorizontalGroup(
            jPanel_inventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel_inventarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_inventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane_listaOperacionesInventario, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                    .addGroup(jPanel_inventarioLayout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel_inventarioLayout.setVerticalGroup(
            jPanel_inventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_inventarioLayout.createSequentialGroup()
                .addGroup(jPanel_inventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane_listaOperacionesInventario, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        tab1.addTab("Operaciones de inventario", jPanel_inventario);

        jbtn_agregarCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_agregarCompra.setText("Agregar compra");
        jbtn_agregarCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_agregarCompraActionPerformed(evt);
            }
        });

        jbtn_verCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/view-list-details.png"))); // NOI18N
        jbtn_verCompra.setText("Ver detalles compra");
        jbtn_verCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_verCompraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(361, Short.MAX_VALUE)
                .addComponent(jbtn_verCompra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_agregarCompra)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_agregarCompra)
                    .addComponent(jbtn_verCompra))
                .addContainerGap())
        );

        jtbl_compras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Fecha", "Local", "Usuario realizador", "Productos comprados", "Monto comprado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane_listaCompras.setViewportView(jtbl_compras);
        if (jtbl_compras.getColumnModel().getColumnCount() > 0) {
            jtbl_compras.getColumnModel().getColumn(0).setResizable(false);
            jtbl_compras.getColumnModel().getColumn(0).setPreferredWidth(30);
            jtbl_compras.getColumnModel().getColumn(1).setResizable(false);
            jtbl_compras.getColumnModel().getColumn(1).setPreferredWidth(90);
            jtbl_compras.getColumnModel().getColumn(3).setPreferredWidth(90);
            jtbl_compras.getColumnModel().getColumn(4).setPreferredWidth(90);
            jtbl_compras.getColumnModel().getColumn(5).setResizable(false);
            jtbl_compras.getColumnModel().getColumn(5).setPreferredWidth(90);
        }

        jbtn_nextPag_compras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-next-view.png"))); // NOI18N
        jbtn_nextPag_compras.setText("Siguiente");
        jbtn_nextPag_compras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_nextPag_comprasActionPerformed(evt);
            }
        });

        jLabel5.setText("/");

        jtf_numPag_compras.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtf_numPag_comprasFocusLost(evt);
            }
        });
        jtf_numPag_compras.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtf_numPag_comprasKeyReleased(evt);
            }
        });

        jLabel6.setText("Página: ");

        jbtn_prevPag_compras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/go-previous-view.png"))); // NOI18N
        jbtn_prevPag_compras.setText("Anterior");
        jbtn_prevPag_compras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_prevPag_comprasActionPerformed(evt);
            }
        });

        jlabel_numTotalPag_compras.setText("10");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtn_prevPag_compras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_numPag_compras, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_numTotalPag_compras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_nextPag_compras)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jbtn_nextPag_compras)
                    .addComponent(jLabel5)
                    .addComponent(jtf_numPag_compras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbtn_prevPag_compras)
                    .addComponent(jlabel_numTotalPag_compras))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jbtn_buscar_compra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find_16.png"))); // NOI18N
        jbtn_buscar_compra.setText("Buscar");
        jbtn_buscar_compra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscar_compraActionPerformed(evt);
            }
        });

        jtf_buscar_compra.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_buscar_compraFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jtf_buscar_compra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscar_compra, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtn_buscar_compra)
                    .addComponent(jtf_buscar_compra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel_comprasLocalLayout = new javax.swing.GroupLayout(jPanel_comprasLocal);
        jPanel_comprasLocal.setLayout(jPanel_comprasLocalLayout);
        jPanel_comprasLocalLayout.setHorizontalGroup(
            jPanel_comprasLocalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel_comprasLocalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_comprasLocalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_comprasLocalLayout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane_listaCompras, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel_comprasLocalLayout.setVerticalGroup(
            jPanel_comprasLocalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_comprasLocalLayout.createSequentialGroup()
                .addGroup(jPanel_comprasLocalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane_listaCompras, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        tab1.addTab("Compras en local", jPanel_comprasLocal);

        exit_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/application-exit.png"))); // NOI18N
        exit_btn.setText("Salir");
        exit_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exit_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tab1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(exit_btn)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tab1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exit_btn)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exit_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exit_btnActionPerformed
        this.setVisible(false);
        System.exit(0);
    }//GEN-LAST:event_exit_btnActionPerformed

    private void add_producto_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_producto_btnActionPerformed
        JDialog dialog = new AddProducto(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarProductos();
    }//GEN-LAST:event_add_producto_btnActionPerformed

    private void add_producto_btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_producto_btn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_add_producto_btn1ActionPerformed

    private void jbtn_realizarOperacionInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_realizarOperacionInventarioActionPerformed
        //Abre un dialogo para realizar una operación de inventario
        JDialog dialog = new AddOperacionInventario(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarOperacionesInventario();
    }//GEN-LAST:event_jbtn_realizarOperacionInventarioActionPerformed

    private void jbtn_verOperacionInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_verOperacionInventarioActionPerformed
        //Mostrar los detalles de la operación de inventario seleccionada
        DefaultTableModel tm = (DefaultTableModel)jtbl_operacionesInventario.getModel();
        int selectedRow = jtbl_operacionesInventario.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado una operación de inventario para ver los detalles", 
                    "Error al ver la operación inventario", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Integer idOp = Integer.parseInt((String)tm.getValueAt(selectedRow, COL_ID));
        
        JDialog dialog = new VerOperacionInventario(this, true, idOp);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }//GEN-LAST:event_jbtn_verOperacionInventarioActionPerformed

    private void jbtn_agregarCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_agregarCompraActionPerformed
        //Abre un dialogo para realizar una operación de inventario
        JDialog dialog = new AddCompra(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarCompras();
    }//GEN-LAST:event_jbtn_agregarCompraActionPerformed

    private void jbtn_verCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_verCompraActionPerformed
        //Mostrar los detalles de la operación de inventario seleccionada
        DefaultTableModel tm = (DefaultTableModel)jtbl_compras.getModel();
        int selectedRow = jtbl_compras.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado una compra realizada para ver los detalles", 
                    "Error al ver una compra", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Integer idCompra = Integer.parseInt((String)tm.getValueAt(selectedRow, COL_ID));

        JDialog dialog = new VerCompra(this, true, idCompra);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }//GEN-LAST:event_jbtn_verCompraActionPerformed

    private void jbtn_nextPag_productosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_nextPag_productosActionPerformed
        paginaActualProductos++;
        cargarProductos();
    }//GEN-LAST:event_jbtn_nextPag_productosActionPerformed

    private void jbtn_prevPag_productosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_prevPag_productosActionPerformed
        paginaActualProductos--;
        cargarProductos();
    }//GEN-LAST:event_jbtn_prevPag_productosActionPerformed

    private void jtf_numPag_productosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtf_numPag_productosKeyReleased
        //Si se presiona ENTER, se guarda el valor del número escrito para cargar los productos
        int key=evt.getKeyCode();
        if(evt.getSource() == jtf_numPag_productos)
        {
            if(key == KeyEvent.VK_ENTER)
            {
                try {
                    paginaActualProductos = Integer.parseInt(jtf_numPag_productos.getText());
                    if (paginaActualProductos <= 0) {
                        throw new NumberFormatException();
                    }
                }
                catch (NumberFormatException nfe) {
                    paginaActualProductos = 1;
                }
                cargarProductos();
            }
        }
    }//GEN-LAST:event_jtf_numPag_productosKeyReleased

    private void jtf_numPag_productosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_numPag_productosFocusLost
        try {
            paginaActualProductos = Integer.parseInt(jtf_numPag_productos.getText());
            if (paginaActualProductos <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            paginaActualProductos = 1;
        }
        cargarProductos();
    }//GEN-LAST:event_jtf_numPag_productosFocusLost

    private void jtf_buscar_productosFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_buscar_productosFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscar_productos);
        this.jtf_buscar_productos.selectAll();
    }//GEN-LAST:event_jtf_buscar_productosFocusGained

    private void jbtn_buscar_productosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscar_productosActionPerformed
        paginaActualProductos = 1;
        cargarProductos();
    }//GEN-LAST:event_jbtn_buscar_productosActionPerformed

    private void tab1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tab1StateChanged
        if (tab1.getSelectedIndex() == INDEX_TAB_PEDIDOS) {
            jtf_buscar_pedido.requestFocus();
        }
        else if (tab1.getSelectedIndex() == INDEX_TAB_PRODUCTOS) {
            jtf_buscar_productos.requestFocus();
        }
        else if (tab1.getSelectedIndex() == INDEX_TAB_OPERACIONES_INVENTARIO) {
            jtf_buscar_inventario.requestFocus();
        }
        else if (tab1.getSelectedIndex() == INDEX_TAB_COMPRAS) {
            jtf_buscar_compra.requestFocus();
        }
    }//GEN-LAST:event_tab1StateChanged

    private void jbtn_nextPag_inventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_nextPag_inventarioActionPerformed
        paginaActualOperacionesInventario++;
        cargarOperacionesInventario();
    }//GEN-LAST:event_jbtn_nextPag_inventarioActionPerformed

    private void jbtn_prevPag_inventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_prevPag_inventarioActionPerformed
        paginaActualOperacionesInventario--;
        cargarOperacionesInventario();
    }//GEN-LAST:event_jbtn_prevPag_inventarioActionPerformed

    private void jtf_buscar_inventarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_buscar_inventarioFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscar_inventario);
        this.jtf_buscar_inventario.selectAll();
    }//GEN-LAST:event_jtf_buscar_inventarioFocusGained

    private void jbtn_buscar_inventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscar_inventarioActionPerformed
        paginaActualOperacionesInventario = 1;
        cargarOperacionesInventario();
    }//GEN-LAST:event_jbtn_buscar_inventarioActionPerformed

    private void jtf_numPag_inventarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_numPag_inventarioFocusLost
        try {
            paginaActualOperacionesInventario = Integer.parseInt(jtf_numPag_inventario.getText());
            if (paginaActualOperacionesInventario <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            paginaActualOperacionesInventario = 1;
        }
        cargarOperacionesInventario();
    }//GEN-LAST:event_jtf_numPag_inventarioFocusLost

    private void jtf_numPag_inventarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtf_numPag_inventarioKeyReleased
        //Si se presiona ENTER, se guarda el valor del número escrito para cargar los productos
        int key=evt.getKeyCode();
        if(evt.getSource() == jtf_numPag_inventario)
        {
            if(key == KeyEvent.VK_ENTER)
            {
                try {
                    paginaActualOperacionesInventario = Integer.parseInt(jtf_numPag_inventario.getText());
                    if (paginaActualOperacionesInventario <= 0) {
                        throw new NumberFormatException();
                    }
                }
                catch (NumberFormatException nfe) {
                    paginaActualOperacionesInventario = 1;
                }
                cargarOperacionesInventario();
            }
        }
    }//GEN-LAST:event_jtf_numPag_inventarioKeyReleased

    private void jbtn_nextPag_comprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_nextPag_comprasActionPerformed
        paginaActualCompras++;
        cargarCompras();
    }//GEN-LAST:event_jbtn_nextPag_comprasActionPerformed

    private void jtf_numPag_comprasFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_numPag_comprasFocusLost
        try {
            paginaActualCompras = Integer.parseInt(jtf_numPag_compras.getText());
            if (paginaActualCompras <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            paginaActualCompras = 1;
        }
        cargarCompras();
    }//GEN-LAST:event_jtf_numPag_comprasFocusLost

    private void jtf_numPag_comprasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtf_numPag_comprasKeyReleased
        //Si se presiona ENTER, se guarda el valor del número escrito para cargar los productos
        int key=evt.getKeyCode();
        if(evt.getSource() == jtf_numPag_compras)
        {
            if(key == KeyEvent.VK_ENTER)
            {
                try {
                    paginaActualCompras = Integer.parseInt(jtf_numPag_compras.getText());
                    if (paginaActualCompras <= 0) {
                        throw new NumberFormatException();
                    }
                }
                catch (NumberFormatException nfe) {
                    paginaActualCompras = 1;
                }
                cargarCompras();
            }
        }
    }//GEN-LAST:event_jtf_numPag_comprasKeyReleased

    private void jbtn_prevPag_comprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_prevPag_comprasActionPerformed
        paginaActualCompras--;
        cargarCompras();
    }//GEN-LAST:event_jbtn_prevPag_comprasActionPerformed

    private void jbtn_buscar_compraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscar_compraActionPerformed
        paginaActualCompras = 1;
        cargarCompras();
    }//GEN-LAST:event_jbtn_buscar_compraActionPerformed

    private void jtf_buscar_compraFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_buscar_compraFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscar_compra);
        this.jtf_buscar_compra.selectAll();
    }//GEN-LAST:event_jtf_buscar_compraFocusGained

    private void jbtn_agregarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_agregarPedidoActionPerformed
        //Abre un dialogo para realizar un pedido a bodega central
        JDialog dialog = new AddPedido(this, true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarPedidos();
    }//GEN-LAST:event_jbtn_agregarPedidoActionPerformed

    private void jbtn_verDetallesPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_verDetallesPedidoActionPerformed
        DefaultTableModel tm = (DefaultTableModel)jtbl_pedidos.getModel();
        int selectedRow = jtbl_pedidos.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un pedido para ver los detalles", 
                    "Error al ver el pedido", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        int COL_ID_PEDIDO = 0;
        Integer id = Integer.parseInt((String)tm.getValueAt(selectedRow, COL_ID_PEDIDO));
        
        JDialog dialog = new VerDetallesPedido(this, true, id);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        cargarPedidos();
    }//GEN-LAST:event_jbtn_verDetallesPedidoActionPerformed

    private void jbtn_nextPag_pedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_nextPag_pedidoActionPerformed
        paginaActualPedidos++;
        cargarPedidos();
    }//GEN-LAST:event_jbtn_nextPag_pedidoActionPerformed

    private void jtf_numPag_pedidoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_numPag_pedidoFocusLost
        try {
            paginaActualPedidos = Integer.parseInt(jtf_numPag_pedido.getText());
            if (paginaActualPedidos <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException nfe) {
            paginaActualPedidos = 1;
        }
        cargarPedidos();
    }//GEN-LAST:event_jtf_numPag_pedidoFocusLost

    private void jtf_numPag_pedidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtf_numPag_pedidoKeyReleased
        //Si se presiona ENTER, se guarda el valor del número escrito para cargar los pedidos
        int key=evt.getKeyCode();
        if(evt.getSource() == jtf_numPag_pedido)
        {
            if(key == KeyEvent.VK_ENTER)
            {
                try {
                    paginaActualPedidos = Integer.parseInt(jtf_numPag_pedido.getText());
                    if (paginaActualPedidos <= 0) {
                        throw new NumberFormatException();
                    }
                }
                catch (NumberFormatException nfe) {
                    paginaActualPedidos = 1;
                }
                cargarPedidos();
            }
        }
    }//GEN-LAST:event_jtf_numPag_pedidoKeyReleased

    private void jbtn_prevPag_pedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_prevPag_pedidoActionPerformed
        paginaActualPedidos--;
        cargarPedidos();
    }//GEN-LAST:event_jbtn_prevPag_pedidoActionPerformed

    private void jbtn_buscar_pedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscar_pedidoActionPerformed
        paginaActualPedidos = 1;
        cargarPedidos();
    }//GEN-LAST:event_jbtn_buscar_pedidoActionPerformed

    private void jtf_buscar_pedidoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_buscar_pedidoFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscar_pedido);
        this.jtf_buscar_pedido.selectAll();
    }//GEN-LAST:event_jtf_buscar_pedidoFocusGained

    private void jbtn_recibirPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_recibirPedidoActionPerformed
        DefaultTableModel tm = (DefaultTableModel)jtbl_pedidos.getModel();
        int selectedRow = jtbl_pedidos.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un pedido para recibir", 
                    "Error al recibir el pedido", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        int COL_ID_PEDIDO = 0;
        int COL_ESTADO = 5;
        int id = Integer.parseInt((String)tm.getValueAt(selectedRow, COL_ID_PEDIDO));
        String estadoActual = (String)tm.getValueAt(selectedRow, COL_ESTADO);
        if (estadoActual.equals(EstadoPedido.ESTADOS_PEDIDO[EstadoPedido.ESTADO_RECIBIDO])) {
            JOptionPane.showMessageDialog(this, 
                    "El pedido seleccionado ya ha sido recibido anteriormente, no es posible de recibir de nuevo", 
                    "Pedido ya recibido", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (estadoActual.equals(EstadoPedido.ESTADOS_PEDIDO[EstadoPedido.ESTADO_CANCELADO])) {
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "El pedido seleccionado ha sido cancelado anteriormente, ¿Seguro desea recibirlo?", 
                    "Producto cancelado", 
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.NO_OPTION) {
                return;
            }
        }
        
        JDialog dialog = new RecibirPedido(this, true, id);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    
        cargarPedidos();
    }//GEN-LAST:event_jbtn_recibirPedidoActionPerformed

    private void jmenu_cancelarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmenu_cancelarPedidoActionPerformed
        DefaultTableModel model = (DefaultTableModel)jtbl_pedidos.getModel();
        int selectedRow = jtbl_pedidos.getSelectedRow();
        if (selectedRow >= 0) {
            int INDEX_ID = 0;
            String idSeleccionado = (String)model.getValueAt(selectedRow, INDEX_ID);
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            PedidoJpaController controller = new PedidoJpaController(emf);
            Pedido u = controller.findPedido(Integer.parseInt(idSeleccionado));
            if (u != null) {
                if (u.getEstado() == EstadoPedido.ESTADO_CANCELADO) {
                    
                }
                else if (u.getEstado() == EstadoPedido.ESTADO_RECIBIDO) {
                    JOptionPane.showMessageDialog(this,
                                "El pedido se encuentra actualmente recibido, no es posible de cancelar",
                                "No es posible cancelar el pedido", JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                    int opcion = JOptionPane.showConfirmDialog(this,
                        "¿Seguro que desea cancelar el pedido?, se avisará que este no debe ser enviado",
                        "Confirmación para cancelar pedido",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcion == JOptionPane.YES_OPTION) {
                        try {
                            //Deshabilito el usuario en lugar de eliminarlo
                            u.setEstado(EstadoPedido.ESTADO_CANCELADO);
                            controller.edit(u);
                            JOptionPane.showMessageDialog(this,
                                "Se ha cancelado el pedido con id \"".concat(idSeleccionado).concat("\""),
                                "Se ha cancelado un pedido", JOptionPane.INFORMATION_MESSAGE);
                        }
                        catch (NonexistentEntityException ex) {
                            Logger.getLogger(PedidosIngresosBodegaProdutosWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        catch (Exception ex) {
                            Logger.getLogger(PedidosIngresosBodegaProdutosWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            cargarPedidos();
        }
    }//GEN-LAST:event_jmenu_cancelarPedidoActionPerformed

    private void jtbl_pedidosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtbl_pedidosMouseReleased
        if (evt.getButton() != java.awt.event.MouseEvent.BUTTON3) {
            return; //si no es click secundario, hace nada
        }
        int r = jtbl_pedidos.rowAtPoint(evt.getPoint());
        if (r >= 0 && r < jtbl_pedidos.getRowCount()) {
            jtbl_pedidos.setRowSelectionInterval(r, r);
        } else {
            jtbl_pedidos.clearSelection();
        }

        int rowindex = jtbl_pedidos.getSelectedRow();
        if (rowindex < 0)
            return;
        int INDEX_ID = 0;
        DefaultTableModel model = (DefaultTableModel)jtbl_pedidos.getModel();
        String idSeleccionado = (String)model.getValueAt(rowindex, INDEX_ID);
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        PedidoJpaController controller = new PedidoJpaController(emf);
        Pedido u = controller.findPedido(Integer.parseInt(idSeleccionado));
        
        if (u != null) {
            if (u.getEstado() != EstadoPedido.ESTADO_CANCELADO && u.getEstado() != EstadoPedido.ESTADO_RECIBIDO) {
                if (evt.isPopupTrigger() && evt.getComponent() instanceof JTable ) {
                    JPopupMenu popup = jmenu_cancelarPedido_clickSecundario;
                    popup.show(evt.getComponent(), evt.getX(), evt.getY());
                }
            }
        }
    }//GEN-LAST:event_jtbl_pedidosMouseReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add_producto_btn;
    private javax.swing.JButton add_producto_btn1;
    private javax.swing.JButton exit_btn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanel_comprasLocal;
    private javax.swing.JPanel jPanel_inventario;
    private javax.swing.JPanel jPanel_pedidos;
    private javax.swing.JPanel jPanel_productos;
    private javax.swing.JScrollPane jScrollPane_listaCompras;
    private javax.swing.JScrollPane jScrollPane_listaOperacionesInventario;
    private javax.swing.JScrollPane jScrollPane_listaPedidos;
    private javax.swing.JScrollPane jScrollPane_listaProductos;
    private javax.swing.JButton jbtn_agregarCompra;
    private javax.swing.JButton jbtn_agregarPedido;
    private javax.swing.JButton jbtn_buscar_compra;
    private javax.swing.JButton jbtn_buscar_inventario;
    private javax.swing.JButton jbtn_buscar_pedido;
    private javax.swing.JButton jbtn_buscar_productos;
    private javax.swing.JButton jbtn_nextPag_compras;
    private javax.swing.JButton jbtn_nextPag_inventario;
    private javax.swing.JButton jbtn_nextPag_pedido;
    private javax.swing.JButton jbtn_nextPag_productos;
    private javax.swing.JButton jbtn_prevPag_compras;
    private javax.swing.JButton jbtn_prevPag_inventario;
    private javax.swing.JButton jbtn_prevPag_pedido;
    private javax.swing.JButton jbtn_prevPag_productos;
    private javax.swing.JButton jbtn_realizarOperacionInventario;
    private javax.swing.JButton jbtn_recibirPedido;
    private javax.swing.JButton jbtn_verCompra;
    private javax.swing.JButton jbtn_verDetallesPedido;
    private javax.swing.JButton jbtn_verOperacionInventario;
    private javax.swing.JLabel jlabel_numTotalPag_compras;
    private javax.swing.JLabel jlabel_numTotalPag_inventario;
    private javax.swing.JLabel jlabel_numTotalPag_pedido;
    private javax.swing.JLabel jlabel_numTotalPag_productos;
    private javax.swing.JMenuItem jmenu_cancelarPedido;
    private javax.swing.JPopupMenu jmenu_cancelarPedido_clickSecundario;
    private javax.swing.JTable jtbl_compras;
    private javax.swing.JTable jtbl_operacionesInventario;
    private javax.swing.JTable jtbl_pedidos;
    private javax.swing.JTable jtbl_productos;
    private javax.swing.JTextField jtf_buscar_compra;
    private javax.swing.JTextField jtf_buscar_inventario;
    private javax.swing.JTextField jtf_buscar_pedido;
    private javax.swing.JTextField jtf_buscar_productos;
    private javax.swing.JTextField jtf_numPag_compras;
    private javax.swing.JTextField jtf_numPag_inventario;
    private javax.swing.JTextField jtf_numPag_pedido;
    private javax.swing.JTextField jtf_numPag_productos;
    private javax.swing.JTabbedPane tab1;
    // End of variables declaration//GEN-END:variables
}
