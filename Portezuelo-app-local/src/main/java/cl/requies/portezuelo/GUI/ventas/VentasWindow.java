/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.GUI.ventas;

import cl.requies.portezuelo.GUI.AcercaDe;
import cl.requies.portezuelo.JPAControllers.AlertaCambioPrecioJpaController;
import cl.requies.portezuelo.JPAControllers.CajaJpaController;
import cl.requies.portezuelo.JPAControllers.ImpuestoJpaController;
import cl.requies.portezuelo.JPAControllers.ProductoJpaController;
import cl.requies.portezuelo.JPAControllers.VentaAnuladaJpaController;
import cl.requies.portezuelo.JPAControllers.VentaJpaController;
import cl.requies.portezuelo.JPAControllers.exceptions.NonexistentEntityException;
import cl.requies.portezuelo.controllers.LoginController;
import cl.requies.portezuelo.controllers.VentasController;
import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Impuesto;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.entities.Venta;
import cl.requies.portezuelo.entities.VentaAnulada;
import cl.requies.portezuelo.entities.VentaDetalle;
import cl.requies.portezuelo.others.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author victor
 */
public class VentasWindow extends javax.swing.JFrame {
    private static final int COD_NOMBREPRODUCTO = 0;
    private static final int COL_CODIGOBARRAS = 1;
    private static final int COL_CANTIDAD = 2;
    private static final int COL_PRECIOUNITARIO = 3;
    private static final int COL_VALORTOTAL = 4;
    
    public static final int LARGO_CODIGO_BARRAS = 13;
    private String codigoTemporal = null;
    
    Caja cajaActual;
    Venta ultimaVentaHecha;
    VentasController vc;
    Producto productoActual;
    double totalVenta;
    boolean ventaPagada;
    short tipoPago;
    int nroVenta;
    
    /**
     * Creates new form PortezueloVentas
     */
    public VentasWindow() {
        initComponents();
        Caja u = LoginController.getInstance().getCajaActual();
        if (u == null) {
            JOptionPane.showMessageDialog(this, 
                    "No se ha registrado una caja asociada a esta ventana", 
                    "Error interno", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        cajaActual = u;
        ultimaVentaHecha = null;
        
        vc = new VentasController();
        this.jlabel_nombreVendedor.setText(cajaActual.getUserId().getNombre().
                concat(" ").concat(cajaActual.getUserId().getApellP()));
        
        limpiarVenta();
        updateNroVenta();
        
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        cajaActual = null;
        ultimaVentaHecha = null;
        vc = null;
        productoActual = null;
    }
    
    private void updateNroVenta() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        VentaJpaController controller = new VentaJpaController(emf);
        nroVenta = controller.getNroProxVenta();
        int nroTemp = nroVenta;
        StringBuilder ceros = new StringBuilder();
        while (nroTemp < 100000000) { //Agrego ceros a la izquiera al string del N° de venta
            nroTemp = nroTemp*10;
            ceros.append('0');
        }
        ceros.append(nroVenta);
        jlabel_nroVenta.setText(ceros.toString());
    }
    
    private void actualizarValorTotal() {
        DefaultTableModel model = (DefaultTableModel)jt_productsSelecteds.getModel();
        int i, suma = 0;
        for (i = 0; i < model.getRowCount(); i++) {
            suma += Integer.parseInt((String)model.getValueAt(i, model.getColumnCount()-1));
        }
        totalVenta = suma;
        jlabel_valorTotal.setText("$".concat(Integer.toString(suma)));
    }
    
    public void exitConfirm() {
        int opcion = JOptionPane.showConfirmDialog(this, "Se cerrará la caja, ¿Está seguro(a)?", "¿Realmente desea Salir?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (opcion == JOptionPane.OK_OPTION) {
            guardarDatosCaja();
            System.exit(0);
        }
    }
    
    private void guardarDatosCaja() {
        System.out.println("Guardando datos de caja: "+cajaActual.toString());
        if (cajaActual != null) {
            EntityManagerFactory emf = Utils.getEntityManagerFactory();
            CajaJpaController controller = new CajaJpaController(emf);
            cajaActual = controller.findCaja(cajaActual.getId());
            
            cajaActual.setFechaTermino(new Date());
            
            try {
                controller.edit(cajaActual);
                /*
                JOptionPane.showMessageDialog(this, 
                    "Se guardó la información de la caja", 
                    "Saliendo...", JOptionPane.INFORMATION_MESSAGE);
                */
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(VentasWindow.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, 
                    "Error al actualizar los datos de la caja", 
                    "Error interno", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                Logger.getLogger(VentasWindow.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, 
                    "Error al actualizar los datos de la caja", 
                    "Error interno", JOptionPane.ERROR_MESSAGE);
            }
        }
        
    }
    
    private String buildNombreProducto(Producto p) {
        return p.getDescripcion().concat(" ").concat(p.getMarca()).
                    concat(" ").concat(p.getContenido()).concat(p.getUnidad());
    }
    
    private void cargarDatosProducto(Producto p) {
        if (p != null) {
            jtf_codigoBarras.setText(p.getBarcode().toString());
            jtf_codigoCorto.setText(p.getCodigoCorto());
            jlabel_nombreProducto.setText(buildNombreProducto(p));
            jtf_precio_unitario.setText(Double.toString(p.getPrecio()));
            jtf_precio_unitario.setEnabled(p.getPrecioEditable());
            Double stock = p.getStock();
            String stockStr;
            if (stock/Math.floor(stock) == 1) {
                stockStr = Integer.toString((int)(stock.doubleValue()));
            }
            else {
                stockStr = Double.toString(stock);
            }
            jlabel_stock.setText(stockStr);
        }
        else {
            jtf_codigoBarras.setText("");
            jtf_codigoCorto.setText("");
            jlabel_nombreProducto.setText("");
            jtf_precio_unitario.setText("");
            jlabel_stock.setText("");
            jtf_cantidad.setText("1");
            jtf_precio_unitario.setEnabled(false);
        }
    }
    
    private void limpiarVenta() {
        DefaultTableModel model = (DefaultTableModel)jt_productsSelecteds.getModel();
        while(model.getRowCount() > 0) {
            model.removeRow(0);
        }
        totalVenta = 0;
        ventaPagada = false;
        productoActual = null;
        
        cargarDatosProducto(null);
        actualizarValorTotal();
        cargarMensajesAlertaCambioPrecios();
    }
    
    private void cargarMensajesAlertaCambioPrecios() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        AlertaCambioPrecioJpaController controller = new AlertaCambioPrecioJpaController(emf);
        int cantidadAlertasPendientes = controller.getCantidadAlertasPendientes();
        if (cantidadAlertasPendientes <= 0)
            jLabel_alertasCambioPrecio.setText("");
        else if (cantidadAlertasPendientes == 1) {
            jLabel_alertasCambioPrecio.setText("Tiene "
                    .concat(Integer.toString(cantidadAlertasPendientes))
                    .concat(" alerta de cambio de precio sin leer"));
        }
        else {
            jLabel_alertasCambioPrecio.setText("Tiene "
                    .concat(Integer.toString(cantidadAlertasPendientes))
                    .concat(" alertas de cambio de precio sin leer"));
        }
    }
    
    private void crearVenta() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        VentaJpaController controller = new VentaJpaController(emf);
        ProductoJpaController controllerProd = new ProductoJpaController(emf);
        ImpuestoJpaController controllerImp = new ImpuestoJpaController(emf);
        
        Venta v = new Venta();
        v.setCancelada(ventaPagada);
        v.setTipoVenta(tipoPago);
        v.setDescuento(0); //POR AHORA NO HAY DESCUENTOS
        v.setFecha(new Date());
        v.setMonto((int)totalVenta);
        v.setMaquina(cajaActual.getMaquinaId());
        v.setCajaId(cajaActual);
        //v.setIdDocumento();
        //v.setTipoDocumento(0);
        //v.setTipoVenta(0);
        v.setVentaAnulada(null);
        if (v.getVentaDetalleList() == null) {
            v.setVentaDetalleList(new ArrayList<VentaDetalle>());
        }
        
        VentaDetalle vd;
        Producto p;
        Impuesto impOtro;
        Impuesto imp = controllerImp.findImpuesto(1); //1 es el id del IVA según el poblado inicial
        double iva_percent, otros_percent;
        double impuestoAplicable;
        double precio, cantidad, precio_neto, ganancia;
        String codigoBarrasStr;
        if (imp == null) {
            System.out.println("No se ha encontrado el valor el IVA, ERROR EN EL POBLADO");
            throw new NullPointerException("Impuesto IVA no encontrado en la base de datos");
        }
        iva_percent = imp.getMonto();
        for (int i = 0; i < jt_productsSelecteds.getRowCount(); i++) {
            vd = new VentaDetalle();
            codigoBarrasStr = (String)jt_productsSelecteds.getValueAt(i, COL_CODIGOBARRAS);
            
            p = controllerProd.findProducto(Long.parseLong(codigoBarrasStr));
            vd.setBarcode(p);
            cantidad = Double.parseDouble((String)jt_productsSelecteds.getValueAt(i, COL_CANTIDAD));
            vd.setCantidad(cantidad);
            vd.setVenta(v);
            
            //Se obtienen los otros impuestos
            impOtro = p.getOtrosImpuestos();
            otros_percent = 0;
            if (impOtro != null) {
                otros_percent = impOtro.getMonto()/100;
            }
            iva_percent = iva_percent/100;
            precio = Double.parseDouble((String)jt_productsSelecteds.getValueAt(i, COL_PRECIOUNITARIO));
            
            
            //Se obtiene el precio proporcional en caso de haber un descuento en el monto total de la venta
            if (v.getDescuento() > 0) {
                precio = (((cantidad * precio) - 
                        (v.getDescuento() * ( (cantidad * precio) /
                        (v.getMonto() + v.getDescuento())))) / cantidad);
            }
            vd.setPrecio(precio);
            
            
            //Se obtiene cuanto hay de IVA
            if (iva_percent != 0 && p.getAplicaIva())
              impuestoAplicable = (precio / (iva_percent+otros_percent+1) * iva_percent) * cantidad;
            else
              impuestoAplicable = 0;
            vd.setIva(impuestoAplicable);
            //Se obtiene cuanto hay de otros impuestos
            if (otros_percent != 0)
                impuestoAplicable = (precio / (iva_percent+otros_percent+1) * otros_percent) * cantidad;
            else
                impuestoAplicable = 0;
            vd.setOtros(impuestoAplicable);
            
            precio_neto = precio / (iva_percent + otros_percent + 1);
            if (p.getCostoPromedio() == null) {
                ganancia = 0;
                vd.setFifo(0);
            }
            else {
                ganancia = precio_neto - p.getCostoPromedio();
                ganancia = ganancia * cantidad;
                vd.setFifo(p.getCostoPromedio());
            }
            vd.setPrecioNeto(precio_neto);
            vd.setGanancia(ganancia);
            
            
            //En caso que el pago sea mixto entre afecto a IVA o no
            vd.setProporcionIva(1.0);
            vd.setProporcionOtros(1.0);
            vd.setOtrosResidual(0.0);
            vd.setIvaResidual(0.0);
            
              
            
            v.getVentaDetalleList().add(vd);
        }
        
        
        controller.create(v);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        hbox11 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jlabel_nombreVendedor = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jlabel_nroVenta = new javax.swing.JLabel();
        barcode_group = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jtf_codigoBarras = new javax.swing.JTextField();
        jbtn_buscarProducto = new javax.swing.JButton();
        singleCode_group = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jtf_codigoCorto = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        vbox1_product = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jlabel_nombreProducto = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jtf_precio_unitario = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jlabel_stock = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jtf_cantidad = new javax.swing.JTextField();
        jbtn_agregarProducto = new javax.swing.JButton();
        jbtn_limpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jt_productsSelecteds = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jbtn_limpiarVenta = new javax.swing.JButton();
        jbtn_quitarProducto = new javax.swing.JButton();
        jbtn_vender = new javax.swing.JButton();
        jbtn_cerrarCaja = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel_alertasCambioPrecio = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jlabel_valorTotal = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        verEstadoCaja = new javax.swing.JMenuItem();
        AnularVentaMenuItem = new javax.swing.JMenuItem();
        realizarIngresoMenuItem = new javax.swing.JMenuItem();
        realizarEgresoMenuItem = new javax.swing.JMenuItem();
        alertaCambioPrecioMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Portezuelo - Ventas");
        setMinimumSize(new java.awt.Dimension(873, 480));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Portezuelo - Ventas");
        jLabel1.setMaximumSize(new java.awt.Dimension(234, 30));
        jLabel1.setMinimumSize(new java.awt.Dimension(234, 30));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.GridLayout(3, 3));

        hbox11.setLayout(new javax.swing.BoxLayout(hbox11, javax.swing.BoxLayout.X_AXIS));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Vendedor: ");
        hbox11.add(jLabel3);

        jlabel_nombreVendedor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel_nombreVendedor.setText("NOMBRE APELLIDO");
        hbox11.add(jlabel_nombreVendedor);

        jPanel2.add(hbox11);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("Venta N°: ");

        jlabel_nroVenta.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel_nroVenta.setText("0000000");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_nroVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(179, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jlabel_nroVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2)))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel4);

        barcode_group.setLayout(new javax.swing.BoxLayout(barcode_group, javax.swing.BoxLayout.LINE_AXIS));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Código de barras: ");

        jtf_codigoBarras.setToolTipText("Introduzca aquí el código de barras del producto");
        jtf_codigoBarras.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jtf_codigoBarrasCaretUpdate(evt);
            }
        });
        jtf_codigoBarras.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_codigoBarrasFocusGained(evt);
            }
        });

        jbtn_buscarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-find.png"))); // NOI18N
        jbtn_buscarProducto.setMnemonic('B');
        jbtn_buscarProducto.setText("<html><a style='text-decoration:underline'>B</a>uscar</html>");
        jbtn_buscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_buscarProductoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_codigoBarras, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_buscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtf_codigoBarras)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jbtn_buscarProducto))
                .addContainerGap())
        );

        barcode_group.add(jPanel5);

        jPanel2.add(barcode_group);

        singleCode_group.setLayout(new javax.swing.BoxLayout(singleCode_group, javax.swing.BoxLayout.LINE_AXIS));

        jLabel16.setText(" ");
        singleCode_group.add(jLabel16);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Código corto:");

        jtf_codigoCorto.setToolTipText("Introduzca aquí el código corto del producto");
        jtf_codigoCorto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_codigoCortoFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_codigoCorto, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(174, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtf_codigoCorto, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(jLabel7))
                .addContainerGap())
        );

        singleCode_group.add(jPanel10);

        jPanel2.add(singleCode_group);

        vbox1_product.setAlignmentY(0.0F);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Producto: ");
        jLabel5.setAlignmentY(0.0F);

        jlabel_nombreProducto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlabel_nombreProducto.setText("NOMBRE_PRODUCTO");
        jlabel_nombreProducto.setAlignmentY(0.0F);

        javax.swing.GroupLayout vbox1_productLayout = new javax.swing.GroupLayout(vbox1_product);
        vbox1_product.setLayout(vbox1_productLayout);
        vbox1_productLayout.setHorizontalGroup(
            vbox1_productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vbox1_productLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_nombreProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        vbox1_productLayout.setVerticalGroup(
            vbox1_productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vbox1_productLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel5)
                .addComponent(jlabel_nombreProducto))
        );

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Precio unitario: $");

        jtf_precio_unitario.setEnabled(false);
        jtf_precio_unitario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_precio_unitarioFocusGained(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Stock:");

        jlabel_stock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlabel_stock.setText("0");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_precio_unitario, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabel_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(140, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel9)
                .addComponent(jtf_precio_unitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel14)
                .addComponent(jlabel_stock))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(vbox1_product, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(vbox1_product, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.add(jPanel7);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("Cantidad: ");

        jtf_cantidad.setText("1");
        jtf_cantidad.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtf_cantidadFocusGained(evt);
            }
        });
        jtf_cantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtf_cantidadKeyReleased(evt);
            }
        });

        jbtn_agregarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-add.png"))); // NOI18N
        jbtn_agregarProducto.setMnemonic('A');
        jbtn_agregarProducto.setText("<html><a style='text-decoration:underline'>A</a>gregar</html>");
        jbtn_agregarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_agregarProductoActionPerformed(evt);
            }
        });

        jbtn_limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit-clear.png"))); // NOI18N
        jbtn_limpiar.setMnemonic('L');
        jbtn_limpiar.setText("<html><a style='text-decoration:underline'>L</a>impiar Búsqueda</html>");
        jbtn_limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_limpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtf_cantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_agregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtn_limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 43, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jbtn_agregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbtn_limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jtf_cantidad, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jPanel2.add(jPanel11);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 23));

        jt_productsSelecteds.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Código", "Cantidad", "Precio unitario", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jt_productsSelecteds);
        if (jt_productsSelecteds.getColumnModel().getColumnCount() > 0) {
            jt_productsSelecteds.getColumnModel().getColumn(1).setMinWidth(200);
            jt_productsSelecteds.getColumnModel().getColumn(1).setPreferredWidth(200);
            jt_productsSelecteds.getColumnModel().getColumn(1).setMaxWidth(200);
            jt_productsSelecteds.getColumnModel().getColumn(2).setMinWidth(60);
            jt_productsSelecteds.getColumnModel().getColumn(2).setPreferredWidth(60);
            jt_productsSelecteds.getColumnModel().getColumn(2).setMaxWidth(60);
            jt_productsSelecteds.getColumnModel().getColumn(3).setMinWidth(90);
            jt_productsSelecteds.getColumnModel().getColumn(3).setPreferredWidth(90);
            jt_productsSelecteds.getColumnModel().getColumn(3).setMaxWidth(90);
            jt_productsSelecteds.getColumnModel().getColumn(4).setMinWidth(150);
            jt_productsSelecteds.getColumnModel().getColumn(4).setPreferredWidth(150);
            jt_productsSelecteds.getColumnModel().getColumn(4).setMaxWidth(150);
        }

        jPanel3.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        jbtn_limpiarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-cancel.png"))); // NOI18N
        jbtn_limpiarVenta.setMnemonic('C');
        jbtn_limpiarVenta.setText("<html><a style='text-decoration:underline'>C</a>ancelar Venta</html>");
        jbtn_limpiarVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_limpiarVentaActionPerformed(evt);
            }
        });
        jPanel3.add(jbtn_limpiarVenta);

        jbtn_quitarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/list-remove.png"))); // NOI18N
        jbtn_quitarProducto.setMnemonic('Q');
        jbtn_quitarProducto.setText("<html><a style='text-decoration:underline'>Q</a>uitar Producto</html>");
        jbtn_quitarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_quitarProductoActionPerformed(evt);
            }
        });
        jPanel3.add(jbtn_quitarProducto);

        jbtn_vender.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/opinion-okay.png"))); // NOI18N
        jbtn_vender.setMnemonic('V');
        jbtn_vender.setText("<html><a style='text-decoration:underline'>V</a>ender</html>");
        jbtn_vender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_venderActionPerformed(evt);
            }
        });
        jPanel3.add(jbtn_vender);

        jbtn_cerrarCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dialog-close.png"))); // NOI18N
        jbtn_cerrarCaja.setText("Cerrar Caja");
        jbtn_cerrarCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtn_cerrarCajaActionPerformed(evt);
            }
        });
        jPanel3.add(jbtn_cerrarCaja);

        jPanel6.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, -5));

        jLabel_alertasCambioPrecio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel_alertasCambioPrecio.setForeground(new java.awt.Color(255, 0, 0));
        jLabel_alertasCambioPrecio.setText("ALERTAS DE CAMBIO DE PRECIO");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel_alertasCambioPrecio, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel_alertasCambioPrecio, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.add(jPanel9);

        jPanel8.setAlignmentY(0.0F);
        jPanel8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel2.setText("Total:");
        jPanel8.add(jLabel2);

        jlabel_valorTotal.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jlabel_valorTotal.setText("$1000");
        jPanel8.add(jlabel_valorTotal);

        jPanel6.add(jPanel8);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 859, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(5, 5, 5))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        fileMenu.setMnemonic('f');
        fileMenu.setText("Archivo");

        verEstadoCaja.setText("Ver estado de caja");
        verEstadoCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verEstadoCajaActionPerformed(evt);
            }
        });
        fileMenu.add(verEstadoCaja);

        AnularVentaMenuItem.setText("Anular venta");
        AnularVentaMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnularVentaMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(AnularVentaMenuItem);

        realizarIngresoMenuItem.setText("Registrar ingreso a caja");
        realizarIngresoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                realizarIngresoMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(realizarIngresoMenuItem);

        realizarEgresoMenuItem.setText("Registrar egreso de caja");
        realizarEgresoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                realizarEgresoMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(realizarEgresoMenuItem);

        alertaCambioPrecioMenuItem.setText("Alertas de cambio de precios");
        alertaCambioPrecioMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alertaCambioPrecioMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(alertaCambioPrecioMenuItem);

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Salir");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Ayuda");

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("Acerca de ...");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        exitConfirm();
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        
        exitConfirm();
    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        //En este método ya se ha cerrado
        //exitConfirm();
    }//GEN-LAST:event_formWindowClosed

    private void jbtn_buscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_buscarProductoActionPerformed
        String codBarrasStr = jtf_codigoBarras.getText().trim();
        String codCortoStr = jtf_codigoCorto.getText().trim();
        long codBarras;
     
        Producto p;
        if ((codCortoStr.isEmpty() && (!codBarrasStr.isEmpty() || jtf_codigoBarras.isFocusOwner())) || 
                (!codCortoStr.isEmpty() && (!codBarrasStr.isEmpty() || jtf_codigoBarras.isFocusOwner()))) {
            try {
                codBarras = Long.parseLong(codBarrasStr);
            }
            catch (NumberFormatException nfe) {
                return;
            }
            
            try {
                p = vc.buscarProducto(codBarras);
            }
            catch (Exception ex) {
                
                return;
            }
        }
        else if ((!codCortoStr.isEmpty() && (codBarrasStr.isEmpty() || jtf_codigoCorto.isFocusOwner())) || 
                (!codCortoStr.isEmpty() && (!codBarrasStr.isEmpty() || jtf_codigoCorto.isFocusOwner()))){
            EntityManagerFactory emf =
                        Utils.getEntityManagerFactory();
            ProductoJpaController pc = new ProductoJpaController(emf);
            try {
                p = pc.findProductoByCodigoCorto(codCortoStr);
            }
            catch (Exception ex ) {
                
                return;
            }
            finally {
                emf.close();
            }
        }
        else {
            //No cumple condiciones para buscar nada
            return;
        }
        productoActual = p;
        if (p != null) {
            cargarDatosProducto(productoActual);
            if ((productoActual.getStock() < 0) && (productoActual.getPrecioEditable())) {
                this.jtf_precio_unitario.requestFocus();
                this.jtf_precio_unitario.selectAll();
            }
            else {
                this.jtf_cantidad.requestFocus();
                this.jtf_cantidad.selectAll();
            }
        }
        else {
            cargarDatosProducto(null);
            jlabel_nombreProducto.setText("Producto no encontrado");
        }
    }//GEN-LAST:event_jbtn_buscarProductoActionPerformed

    private void jbtn_agregarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_agregarProductoActionPerformed
        if (productoActual == null) {
            return;
        }
        
        double valorUnitario, cantidad, valorTotal;
        valorUnitario = Double.parseDouble(jtf_precio_unitario.getText());
        cantidad = Double.parseDouble(jtf_cantidad.getText());
        valorTotal = valorUnitario*cantidad;
        Double stock = productoActual.getStock();
        if (stock != null) {//Los stock null no se cuentan
            if ((stock < cantidad) && stock >= 0) { //Los stock negativos no se cuentan
                int opcion = JOptionPane.showConfirmDialog(this, 
                        "El producto no tiene stock suficiente, ¿Desea agregarlo igualmente a la venta?", 
                        "Confirmación para incluir producto sin stock", 
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (opcion == JOptionPane.NO_OPTION) {
                    return;
                }
            }
        }
        DefaultTableModel model = (DefaultTableModel)jt_productsSelecteds.getModel();
        String nombProd = buildNombreProducto(productoActual);
        model.addRow(new String[] {nombProd, Long.toString(productoActual.getBarcode()),
            jtf_cantidad.getText(), jtf_precio_unitario.getText(), Integer.toString((int)valorTotal)});
        productoActual = null;
        cargarDatosProducto(null);
        
        
        actualizarValorTotal();
        
        this.jtf_codigoBarras.requestFocus();
        
        if (codigoTemporal != null) {
            jtf_codigoBarras.setText(codigoTemporal);
            codigoTemporal = null;
        }
    }//GEN-LAST:event_jbtn_agregarProductoActionPerformed

    private void jbtn_limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_limpiarActionPerformed
        productoActual = null;
        cargarDatosProducto(null);
        jtf_codigoBarras.requestFocus();
        
        cargarMensajesAlertaCambioPrecios();
    }//GEN-LAST:event_jbtn_limpiarActionPerformed

    private void jbtn_venderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_venderActionPerformed
        if (!jlabel_nombreProducto.getText().trim().isEmpty()) {
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "Hay un producto buscado, pero aún no agregado a la venta, ¿Deseea agregarlo?", 
                    "Confirmación para agregar producto", JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                jbtn_agregarProductoActionPerformed(null);
            }
        }
        
        
        if (jt_productsSelecteds.getRowCount() <= 0) {
            JOptionPane.showMessageDialog(this, 
                    "No hay productos agregados para realizar la venta", 
                    "No se han agregado productos", JOptionPane.WARNING_MESSAGE);
            return;
        }

        //Mostrar ventana de pago
        RealizarPago dialogo = new RealizarPago(this, true, (int)totalVenta);
        dialogo.setLocationRelativeTo(this);
        tipoPago = dialogo.showDialog();
        if (tipoPago >= 0) {
            System.out.println("Se ha confirmado la venta");
            
            ventaPagada = true;
            try {
                crearVenta();
                JOptionPane.showMessageDialog(this, 
                    "Se ha realizado una venta por ".concat(jlabel_valorTotal.getText()), 
                    "Venta realizada", JOptionPane.INFORMATION_MESSAGE);
            }
            catch (Exception ex) {
                Logger.getLogger(VentasWindow.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, 
                    "Ha ocurrido un error al registrar la venta en el sistema, intente más tarde", 
                    "Error al registrar venta", JOptionPane.ERROR_MESSAGE);
            }
            limpiarVenta();
            jtf_codigoBarras.requestFocus();
        }
        else {
            System.out.println("No se ha realizado la venta");
            
        }
        
        updateNroVenta();
        
    }//GEN-LAST:event_jbtn_venderActionPerformed

    private void jbtn_quitarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_quitarProductoActionPerformed
        DefaultTableModel model = (DefaultTableModel)jt_productsSelecteds.getModel();
        int selectedRow = jt_productsSelecteds.getSelectedRow();
        if (selectedRow < 0) {
            if (model.getRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, 
                    "No hay productos para quitar de la venta", 
                    "No se ha seleccionado producto", JOptionPane.WARNING_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(this, 
                    "No ha seleccionado un producto para quitar de la venta", 
                    "No se ha seleccionado producto", JOptionPane.WARNING_MESSAGE);
            }
        }
        else {
            int opcion = JOptionPane.showConfirmDialog(this, 
                    "¿Seguro que desea quitar el producto seleccionado de la venta?", 
                    "Confirmación para quitar producto", JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                model.removeRow(selectedRow);
                jtf_codigoBarras.requestFocus();
            }
            actualizarValorTotal();
            
        }
    }//GEN-LAST:event_jbtn_quitarProductoActionPerformed

    private void jbtn_cerrarCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_cerrarCajaActionPerformed
        exitConfirm();
    }//GEN-LAST:event_jbtn_cerrarCajaActionPerformed

    private void jbtn_limpiarVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtn_limpiarVentaActionPerformed
        int opcion = JOptionPane.showConfirmDialog(this, "Se limpiará el listado de productos seleccionados", "¿Realmente desea cancelar la venta?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (opcion == JOptionPane.OK_OPTION) {
            limpiarVenta();
        }
        jtf_codigoBarras.requestFocus();
    }//GEN-LAST:event_jbtn_limpiarVentaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        jtf_codigoBarras.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        AcercaDe dialogo = new AcercaDe(this, true);
        dialogo.setLocationRelativeTo(this);
        dialogo.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void AnularVentaMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnularVentaMenuItemActionPerformed
        Integer numVenta;
        boolean valido = false;
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        VentaJpaController controller = new VentaJpaController(emf);
        Venta venta = null;
        AnulacionVenta anulVentaDialog = new AnulacionVenta(this, true);
        anulVentaDialog.setLocationRelativeTo(this);
        while (!valido) {
            numVenta = anulVentaDialog.showDialog();
            if (numVenta == null) {
                return; //Se presionó cancelar
            }
            
            try {
                venta = controller.findVentaByNum(numVenta);
                if (venta == null) {
                    JOptionPane.showMessageDialog(this, 
                        "No ha ingresado un N° de venta existente", 
                        "Error de validación", JOptionPane.ERROR_MESSAGE);
                    valido = false;
                }
                else {
                    valido = true;
                }
            }
            catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(this, 
                        "No ha ingresado un N° de venta válido", 
                        "Error de validación", JOptionPane.ERROR_MESSAGE);
                valido = false;
            }
        }
        
        VentaAnulada ventaAnulada = new VentaAnulada();
        ventaAnulada.setFecha(new Date());
        ventaAnulada.setMotivo(anulVentaDialog.descripcionAnulacion);
        ventaAnulada.setCajaId(LoginController.getInstance().getCajaActual());
        ventaAnulada.setVenta(venta);
        ventaAnulada.setVentaId(venta.getId());
        
        VentaAnuladaJpaController controllerVentaAnulada = new VentaAnuladaJpaController(emf);
        
        try {
            controllerVentaAnulada.create(ventaAnulada);
            JOptionPane.showMessageDialog(this, 
                        "Venta anulada correctamente, se han agregado los productos de la venta al stock nuevamente", 
                        "Venta anulada", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (Exception ex) {
            Logger.getLogger(VentasWindow.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, 
                        "No ha sido posible anular la venta, intente más tarde", 
                        "Error al anular la venta", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_AnularVentaMenuItemActionPerformed

    private void jtf_codigoBarrasFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_codigoBarrasFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
    }//GEN-LAST:event_jtf_codigoBarrasFocusGained

    private void jtf_codigoCortoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_codigoCortoFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_buscarProducto);
    }//GEN-LAST:event_jtf_codigoCortoFocusGained

    private void jtf_cantidadFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_cantidadFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_agregarProducto);
    }//GEN-LAST:event_jtf_cantidadFocusGained

    private void realizarIngresoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_realizarIngresoMenuItemActionPerformed
        //Se debe mostrar dialogo para realizar un ingreso de dinero a la caja
        RegistrarIngresoCaja dialogo = new RegistrarIngresoCaja(this, true, cajaActual);
        dialogo.setLocationRelativeTo(this);
        dialogo.setVisible(true);
    }//GEN-LAST:event_realizarIngresoMenuItemActionPerformed

    private void realizarEgresoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_realizarEgresoMenuItemActionPerformed
        //Se debe mostrar dialogo para realizar un egreso de dinero de la caja
        RegistrarEgresoCaja dialogo = new RegistrarEgresoCaja(this, true, cajaActual);
        dialogo.setLocationRelativeTo(this);
        dialogo.setVisible(true);
    }//GEN-LAST:event_realizarEgresoMenuItemActionPerformed

    private void verEstadoCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verEstadoCajaActionPerformed
        VerEstadoCaja dialogo = new VerEstadoCaja(this, true);
        dialogo.setLocationRelativeTo(this);
        dialogo.setVisible(true);
    }//GEN-LAST:event_verEstadoCajaActionPerformed

    private void jtf_cantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtf_cantidadKeyReleased
        String loEscrito = jtf_cantidad.getText().trim();
        if (loEscrito.length() >= LARGO_CODIGO_BARRAS) {
            codigoTemporal = loEscrito;
            jtf_cantidad.setText("1");
            jbtn_agregarProductoActionPerformed(null);
        }
    }//GEN-LAST:event_jtf_cantidadKeyReleased

    private void jtf_precio_unitarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtf_precio_unitarioFocusGained
        this.getRootPane().setDefaultButton(this.jbtn_agregarProducto);
    }//GEN-LAST:event_jtf_precio_unitarioFocusGained

    private void alertaCambioPrecioMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alertaCambioPrecioMenuItemActionPerformed
        //Se debe mostrar dialogo que lista los cambios de precio
        ListadoAlertasCambioPrecio dialogo = new ListadoAlertasCambioPrecio(this, true);
        dialogo.setLocationRelativeTo(this);
        dialogo.setVisible(true);
        
        cargarMensajesAlertaCambioPrecios();
    }//GEN-LAST:event_alertaCambioPrecioMenuItemActionPerformed

    private void jtf_codigoBarrasCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jtf_codigoBarrasCaretUpdate
        if (jtf_codigoBarras.getText().trim().length() == LARGO_CODIGO_BARRAS) {
            if (productoActual == null) {
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        jbtn_buscarProductoActionPerformed(null); //Aprieto botón buscar
                    }
                });
            }
        }
    }//GEN-LAST:event_jtf_codigoBarrasCaretUpdate



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AnularVentaMenuItem;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem alertaCambioPrecioMenuItem;
    private javax.swing.JPanel barcode_group;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel hbox11;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_alertasCambioPrecio;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtn_agregarProducto;
    private javax.swing.JButton jbtn_buscarProducto;
    private javax.swing.JButton jbtn_cerrarCaja;
    private javax.swing.JButton jbtn_limpiar;
    private javax.swing.JButton jbtn_limpiarVenta;
    private javax.swing.JButton jbtn_quitarProducto;
    private javax.swing.JButton jbtn_vender;
    private javax.swing.JLabel jlabel_nombreProducto;
    private javax.swing.JLabel jlabel_nombreVendedor;
    private javax.swing.JLabel jlabel_nroVenta;
    private javax.swing.JLabel jlabel_stock;
    private javax.swing.JLabel jlabel_valorTotal;
    private javax.swing.JTable jt_productsSelecteds;
    private javax.swing.JTextField jtf_cantidad;
    private javax.swing.JTextField jtf_codigoBarras;
    private javax.swing.JTextField jtf_codigoCorto;
    private javax.swing.JTextField jtf_precio_unitario;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem realizarEgresoMenuItem;
    private javax.swing.JMenuItem realizarIngresoMenuItem;
    private javax.swing.JPanel singleCode_group;
    private javax.swing.JPanel vbox1_product;
    private javax.swing.JMenuItem verEstadoCaja;
    // End of variables declaration//GEN-END:variables

}
