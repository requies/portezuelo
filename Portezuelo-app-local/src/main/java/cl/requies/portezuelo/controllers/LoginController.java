/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.controllers;

import cl.requies.portezuelo.entities.Caja;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.others.Utils;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author victor
 */
public class LoginController {
    
    private Users usuarioLogueado;
    private Negocio negocio;
    private Caja cajaActual;
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        usuarioLogueado = null;
    }
    
    public static LoginController getInstance() {
        return LoginControllerHolder.getInstance();
    }
    
    private static class LoginControllerHolder {
        private static LoginController INSTANCE = null;
        
        private static LoginController getInstance() {
            if (INSTANCE == null) {
                INSTANCE = new LoginController();
            }
            return INSTANCE;
        }
        
    }
    
    public boolean login(String rutStr, String password) {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em;
        em = emf.createEntityManager();
        Query q = em.createNamedQuery("Users.findByUsuarioAndPassword");
        q.setParameter("rut", rutStr);
        q.setParameter("password", Utils.convertToMD5(password));
        try {
            Users res = (Users)q.getSingleResult();
            usuarioLogueado = res;
            cargarNegocio();
            
            return true;
        }
        catch (NoResultException nre) {
            
        }
        finally {
            em.close();
        }
        usuarioLogueado = null;
        return false;
    }
    
    private void cargarNegocio() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em;
        em = emf.createEntityManager();
        Query q = em.createNamedQuery("Negocio.findAll");
        try {
            List<Negocio> listaNegocios = (List<Negocio>)q.getResultList();
            if (!listaNegocios.isEmpty()) {
                negocio = listaNegocios.get(0);
            }
            else {
                negocio = null;
            }
        }
        catch (Exception e) {
            negocio = null;
        }
        finally {
            em.close();
        }
    }
    
    public void logout() {
        usuarioLogueado = null;
        negocio = null;
        cajaActual = null;
    }
    
    public Users getUsuarioLogueado() {
        return usuarioLogueado;
    }
    
    public Negocio getNegocio() {
        return negocio;
    }
    
    public Caja getCajaActual() {
        return cajaActual;
    }

    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }

    public void setCajaActual(Caja cajaActual) {
        this.cajaActual = cajaActual;
    }
    
}
