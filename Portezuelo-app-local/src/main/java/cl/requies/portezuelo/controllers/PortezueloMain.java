/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.controllers;

import cl.requies.portezuelo.GUI.ConfiguracionInicial;
import cl.requies.portezuelo.GUI.LoginWindow;
import cl.requies.portezuelo.GUI.ventas.VentasWindow;
import cl.requies.portezuelo.entities.Negocio;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.others.Utils;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author victor
 */
public class PortezueloMain {
    public static LoginWindow loginWindow;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                //System.out.println(info.getName());
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
      
        
        while (!Utils.establecerDBConfig()) {
            JOptionPane.showMessageDialog(null, 
                    "No ha sido posible conectarse a la base de datos, debe configurarla", 
                    "Error al conectarse a la base de datos", JOptionPane.ERROR_MESSAGE);
            
            ConfiguracionInicial dialog = new ConfiguracionInicial(null, true);
            dialog.setVisible(true);
            if (!dialog.isConfigurado()) {
                System.exit(0);
            }
        }
        
        //Estos dos no deben hacerse en teoría, porque en el díalogo anterior se setearon
        
        if(isFirstInitUsers()) {
            JOptionPane.showMessageDialog(null, 
                    "No se ha encontrado un usuario administrador, se iniciará el asistente de configuración inicial", 
                    "Error al encontrar usuario administrador", JOptionPane.ERROR_MESSAGE);
            
            ConfiguracionInicial dialog = new ConfiguracionInicial(null, true);
            dialog.setVisible(true);
            if (!dialog.isConfigurado()) {
                System.exit(0);
            }
        }
        
        
        if(isFirstInitNegocio()) {
            JOptionPane.showMessageDialog(null, 
                    "No se ha encontrado un negocio/local registrado en la base de datos, se iniciará el asistente de configuración inicial", 
                    "Error al encontrar negocio/local", JOptionPane.ERROR_MESSAGE);
            
            ConfiguracionInicial dialog = new ConfiguracionInicial(null, true);
            dialog.setVisible(true);
            if (!dialog.isConfigurado()) {
                System.exit(0);
            }
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                loginWindow = new LoginWindow();
                loginWindow.setVisible(true);
            }
        });
    }
    
    @Deprecated
    public static boolean isFirstInitUsers() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        Long c = 1L;
        try {
            em.getTransaction().begin();
            Query q = em.createNamedQuery("Users.haveUsersAdmin");
            c = (Long)q.getSingleResult();
            em.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            em.close();
        }
        return c == 0;
    }
    
    @Deprecated
    public static boolean isFirstInitNegocio() {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        Long c = 1L;
        try {
            em.getTransaction().begin();
            Query q = em.createNamedQuery("Negocio.haveNegocio");
            c = (Long)q.getSingleResult();
            em.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            em.close();
        }
        return c == 0;
    }
    
    @Deprecated
    public static void createUserInitDB() {
        Users u = new Users(1, "19", Utils.convertToMD5("admin"), "Administrador", "");
        u.setApellM("");
        u.setFechaIngreso(new Date());
        u.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
        u.setActivo(true);
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        
        try {
            em.getTransaction().begin();
            System.out.println("Creando usuario por defecto admin");
            em.persist(u);
            em.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            em.close();
        }
    }
    
    @Deprecated
    public static void createNegocioInitDB() {
        Negocio negNvo = new Negocio();
        negNvo.setRut("19");
        negNvo.setRazonSocial("Botillería");
        negNvo.setNombre("Portezuelo");
        negNvo.setDireccion("");
        negNvo.setCiudad("");
        negNvo.setComuna("");
        negNvo.setGiro("");
        negNvo.setFax("");
        negNvo.setFono("");
        negNvo.setSeleccionado(true);
        
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        
        try {
            em.getTransaction().begin();
            System.out.println("Creando negocio por defecto local");
            em.persist(negNvo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            Logger.getLogger(PortezueloMain.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            em.close();
        }
    }
}
