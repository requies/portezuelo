/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.controllers;

import cl.requies.portezuelo.JPAControllers.ProductoJpaController;
import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.others.Utils;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class VentasController {
    EntityManager em;
    
    @Override
    protected void finalize() throws Throwable {
        em = null;
    }
    
    public Producto buscarProducto(long codBarras) {
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        em = emf.createEntityManager();
        ProductoJpaController controller = new ProductoJpaController(emf);
        Producto v = null;
        v = controller.findProducto(codBarras);
        return v;
    }
}
