/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.controllers;

import cl.requies.portezuelo.entities.Producto;
import cl.requies.portezuelo.others.Utils;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author victor
 */
public class BusquedaController {
    
    public List<Producto> findProductos(String txt) {
        List<Producto> resultados = null;
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
        
        try {
            //Intento buscar por código de barras o código corto
            Long codigoBarras = Long.parseLong(txt);
            
            Query q = em.createNamedQuery("Producto.findByBarcodeOrCodigoCorto");
            q.setParameter("cod", codigoBarras.toString());
            resultados = q.getResultList();
            if (resultados.isEmpty()) {
                Query q1 = em.createNamedQuery("Producto.findByText");
                q1.setParameter("txt", "%".concat(txt).concat("%"));
                resultados = q1.getResultList();
            }
            
            return resultados;
        }
        catch (NumberFormatException ex) {
            //Busco por descripción o marca, ya que no se ingresó un código de barras o código corto
            Query q = em.createNamedQuery("Producto.findByText");
            q.setParameter("txt", "%".concat(txt).concat("%"));
            resultados = q.getResultList();
            
            return resultados;
        }
        catch (Exception e) {
            return new ArrayList<Producto>();
        }
        finally {
            if (em != null)
                em.close();
        }
    }
    
}
