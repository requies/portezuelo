/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.controllers;

import cl.requies.portezuelo.JPAControllers.UsersJpaController;
import cl.requies.portezuelo.entities.enums.TipoUsuario;
import cl.requies.portezuelo.entities.Users;
import cl.requies.portezuelo.others.Utils;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author victor
 */
public class UserController {
    
    public void addUser(String rutStr, String nombre, 
            String ap_pat, String ap_mat, String password) throws Exception {
        password = Utils.convertToMD5(password);
        Users u = new Users();
        u.setRut(rutStr);
        u.setNombre(nombre);
        u.setPasswd(password);
        u.setApellP(ap_pat);
        u.setApellM(ap_mat);
        u.setActivo(true);
        u.setTipoUsuario(TipoUsuario.CAJERO);
        
        EntityManagerFactory emf = Utils.getEntityManagerFactory();
        UsersJpaController userJPA = new UsersJpaController(emf);
        userJPA.create(u);
        
    }
}
