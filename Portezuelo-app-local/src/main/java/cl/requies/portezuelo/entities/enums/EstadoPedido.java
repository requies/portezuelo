/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities.enums;

/**
 *
 * @author victor
 */
public class EstadoPedido {
    public static final short ESTADO_CREADO = 0;
    public static final short ESTADO_LEIDO = 1;
    public static final short ESTADO_ENVIADO = 2;
    public static final short ESTADO_RECIBIDO = 3;
    public static final short ESTADO_CANCELADO = 4;
    public static final String[] ESTADOS_PEDIDO = {"Creado", "Leido", "Enviado", "Recibido", "Cancelado"};
}