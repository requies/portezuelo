/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "alerta_cambio_precio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlertaCambioPrecio.marcarComoLeida", query = "UPDATE AlertaCambioPrecio AS a SET a.leido = TRUE WHERE a.id = :id"),
    @NamedQuery(name = "AlertaCambioPrecio.countPendientes", query = "SELECT count(a) FROM AlertaCambioPrecio a WHERE a.leido = FALSE"),
    @NamedQuery(name = "AlertaCambioPrecio.findAll", query = "SELECT a FROM AlertaCambioPrecio a"),
    @NamedQuery(name = "AlertaCambioPrecio.findById", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.id = :id"),
    @NamedQuery(name = "AlertaCambioPrecio.findByLeido", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.leido = :leido"),
    @NamedQuery(name = "AlertaCambioPrecio.findByDescripcion", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "AlertaCambioPrecio.findByPrecioAntiguo", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.precioAntiguo = :precioAntiguo"),
    @NamedQuery(name = "AlertaCambioPrecio.findByPrecioNuevo", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.precioNuevo = :precioNuevo"),
    @NamedQuery(name = "AlertaCambioPrecio.findByFecha", query = "SELECT a FROM AlertaCambioPrecio a WHERE a.fecha = :fecha")})
public class AlertaCambioPrecio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "leido")
    private boolean leido;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio_antiguo")
    private Double precioAntiguo;
    @Column(name = "precio_nuevo")
    private Double precioNuevo;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode")
    @ManyToOne
    private Producto barcode;
    @JoinColumn(name = "negocio_id", referencedColumnName = "id")
    @ManyToOne
    private Negocio negocioId;

    public AlertaCambioPrecio() {
    }

    public AlertaCambioPrecio(Integer id) {
        this.id = id;
    }

    public AlertaCambioPrecio(Integer id, boolean leido) {
        this.id = id;
        this.leido = leido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getLeido() {
        return leido;
    }

    public void setLeido(boolean leido) {
        this.leido = leido;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecioAntiguo() {
        return precioAntiguo;
    }

    public void setPrecioAntiguo(Double precioAntiguo) {
        this.precioAntiguo = precioAntiguo;
    }

    public Double getPrecioNuevo() {
        return precioNuevo;
    }

    public void setPrecioNuevo(Double precioNuevo) {
        this.precioNuevo = precioNuevo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Producto getBarcode() {
        return barcode;
    }

    public void setBarcode(Producto barcode) {
        this.barcode = barcode;
    }

    public Negocio getNegocioId() {
        return negocioId;
    }

    public void setNegocioId(Negocio negocioId) {
        this.negocioId = negocioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlertaCambioPrecio)) {
            return false;
        }
        AlertaCambioPrecio other = (AlertaCambioPrecio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.AlertaCambioPrecio[ id=" + id + " ]";
    }
    
}
