/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author victor
 */
@Embeddable
public class PedidoDetallePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "barcode")
    private long barcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pedido_id")
    private int pedidoId;

    public PedidoDetallePK() {
    }

    public PedidoDetallePK(long barcode, int pedidoId) {
        this.barcode = barcode;
        this.pedidoId = pedidoId;
    }

    public long getBarcode() {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public int getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(int pedidoId) {
        this.pedidoId = pedidoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) barcode;
        hash += (int) pedidoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoDetallePK)) {
            return false;
        }
        PedidoDetallePK other = (PedidoDetallePK) object;
        if (this.barcode != other.barcode) {
            return false;
        }
        if (this.pedidoId != other.pedidoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.ProductoDetallePK[ barcode=" + barcode + ", pedidoId=" + pedidoId + " ]";
    }
    
}
