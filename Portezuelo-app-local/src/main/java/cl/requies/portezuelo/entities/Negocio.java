/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "negocio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Negocio.haveNegocio", query = "SELECT count(n) FROM Negocio n"),
    @NamedQuery(name = "Negocio.findAll", query = "SELECT n FROM Negocio n"),
    @NamedQuery(name = "Negocio.findById", query = "SELECT n FROM Negocio n WHERE n.id = :id"),
    @NamedQuery(name = "Negocio.findByRut", query = "SELECT n FROM Negocio n WHERE n.rut = :rut"),
    @NamedQuery(name = "Negocio.findByRazonSocial", query = "SELECT n FROM Negocio n WHERE n.razonSocial = :razonSocial"),
    @NamedQuery(name = "Negocio.findByNombre", query = "SELECT n FROM Negocio n WHERE n.nombre = :nombre"),
    @NamedQuery(name = "Negocio.findByFono", query = "SELECT n FROM Negocio n WHERE n.fono = :fono"),
    @NamedQuery(name = "Negocio.findByFax", query = "SELECT n FROM Negocio n WHERE n.fax = :fax"),
    @NamedQuery(name = "Negocio.findByDireccion", query = "SELECT n FROM Negocio n WHERE n.direccion = :direccion"),
    @NamedQuery(name = "Negocio.findByComuna", query = "SELECT n FROM Negocio n WHERE n.comuna = :comuna"),
    @NamedQuery(name = "Negocio.findByCiudad", query = "SELECT n FROM Negocio n WHERE n.ciudad = :ciudad"),
    @NamedQuery(name = "Negocio.findByGiro", query = "SELECT n FROM Negocio n WHERE n.giro = :giro")})
public class Negocio implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "negocioId")
    private List<Compra> compraList;
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="negocio_id_sequence",sequenceName="\"negocio_id_seq\"", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "negocio_id_sequence")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(max = 15)
    @Column(name = "rut")
    private String rut;
    @Size(max = 255)
    @Column(name = "razon_social")
    private String razonSocial;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 15)
    @Column(name = "fono")
    private String fono;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 15)
    @Column(name = "fax")
    private String fax;
    @Size(max = 255)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 64)
    @Column(name = "comuna")
    private String comuna;
    @Size(max = 64)
    @Column(name = "ciudad")
    private String ciudad;
    @Size(max = 100)
    @Column(name = "giro")
    private String giro;
    @NotNull
    @Column(name = "seleccionado")
    private boolean seleccionado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "negocioId")
    private List<Inventario> inventarioList;
    @OneToMany(mappedBy = "negocioId")
    private List<Pedido> pedidoList;
    @OneToMany(mappedBy = "destinoId")
    private List<Traspaso> traspasoList;
    @OneToMany(mappedBy = "origenId")
    private List<Traspaso> traspasoList1;
    @OneToMany(mappedBy = "negocioId")
    private List<AlertaCambioPrecio> alertaCambioPrecioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "negocioId")
    private List<Asistencia> asistenciaList;
    
    @Transient
    private String url;

    public Negocio() {
    }

    public Negocio(Integer id) {
        this.id = id;
    }

    public Negocio(Integer id, String rut) {
        this.id = id;
        this.rut = rut;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public boolean getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    @XmlTransient
    public List<Inventario> getInventarioList() {
        return inventarioList;
    }

    public void setInventarioList(List<Inventario> inventarioList) {
        this.inventarioList = inventarioList;
    }

    @XmlTransient
    public List<Pedido> getPedidoList() {
        return pedidoList;
    }

    public void setPedidoList(List<Pedido> pedidoList) {
        this.pedidoList = pedidoList;
    }

    @XmlTransient
    public List<Traspaso> getTraspasoList() {
        return traspasoList;
    }

    public void setTraspasoList(List<Traspaso> traspasoList) {
        this.traspasoList = traspasoList;
    }

    @XmlTransient
    public List<Traspaso> getTraspasoList1() {
        return traspasoList1;
    }

    public void setTraspasoList1(List<Traspaso> traspasoList1) {
        this.traspasoList1 = traspasoList1;
    }

    @XmlTransient
    public List<AlertaCambioPrecio> getAlertaCambioPrecioList() {
        return alertaCambioPrecioList;
    }

    public void setAlertaCambioPrecioList(List<AlertaCambioPrecio> alertaCambioPrecioList) {
        this.alertaCambioPrecioList = alertaCambioPrecioList;
    }

    @XmlTransient
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Negocio)) {
            return false;
        }
        Negocio other = (Negocio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre.concat(" - ").concat(comuna).concat(" - ").concat(direccion);
    }

    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }
    
}
