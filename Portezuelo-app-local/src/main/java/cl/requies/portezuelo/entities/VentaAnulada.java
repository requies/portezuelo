/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "venta_anulada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaAnulada.findAll", query = "SELECT v FROM VentaAnulada v"),
    @NamedQuery(name = "VentaAnulada.findByVentaId", query = "SELECT v FROM VentaAnulada v WHERE v.ventaId = :ventaId"),
    @NamedQuery(name = "VentaAnulada.findByFecha", query = "SELECT v FROM VentaAnulada v WHERE v.fecha = :fecha")})
public class VentaAnulada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "venta_id")
    private Integer ventaId;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "venta_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Venta venta;
    @JoinColumn(name = "caja_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Caja cajaId;
    @Column(name = "motivo")
    private String motivo;

    public VentaAnulada() {
    }

    public VentaAnulada(Integer ventaId) {
        this.ventaId = ventaId;
    }

    public Integer getVentaId() {
        return ventaId;
    }

    public void setVentaId(Integer ventaId) {
        this.ventaId = ventaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Caja getCajaId() {
        return cajaId;
    }

    public void setCajaId(Caja cajaId) {
        this.cajaId = cajaId;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ventaId != null ? ventaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaAnulada)) {
            return false;
        }
        VentaAnulada other = (VentaAnulada) object;
        if ((this.ventaId == null && other.ventaId != null) || (this.ventaId != null && !this.ventaId.equals(other.ventaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.VentaAnulada[ ventaId=" + ventaId + " ]";
    }
    
}
