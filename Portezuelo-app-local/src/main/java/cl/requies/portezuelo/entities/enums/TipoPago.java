/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities.enums;

/**
 *
 * @author victor
 */
public class TipoPago {
    public static final short EFECTIVO = 0;
    public static final short TARJETA = 1;
    public static final short FIADO = 2;
    public static final String[] TIPOS_PAGO = {"Efectivo", "Red Compra", "Fiado"};
}
