/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.haveUsers", query = "SELECT count(u) FROM Users u"),
    @NamedQuery(name = "Users.haveUsersAdmin", query = "SELECT count(u) FROM Users u WHERE u.tipoUsuario=0"),
    @NamedQuery(name = "Users.findByUsuarioAndPassword", query = "SELECT u FROM Users u WHERE u.rut = :rut AND u.passwd = :password AND u.activo = true"),
    @NamedQuery(name = "Users.findAllActivos", query = "SELECT u FROM Users u WHERE u.tipoUsuario = :tipoUsuario AND u.activo = true"),
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
    @NamedQuery(name = "Users.findByRut", query = "SELECT u FROM Users u WHERE u.rut = :rut"),
    @NamedQuery(name = "Users.findByPasswd", query = "SELECT u FROM Users u WHERE u.passwd = :passwd"),
    @NamedQuery(name = "Users.findByNombre", query = "SELECT u FROM Users u WHERE u.nombre = :nombre"),
    @NamedQuery(name = "Users.findByApellP", query = "SELECT u FROM Users u WHERE u.apellP = :apellP"),
    @NamedQuery(name = "Users.findByApellM", query = "SELECT u FROM Users u WHERE u.apellM = :apellM"),
    @NamedQuery(name = "Users.findByFechaIngreso", query = "SELECT u FROM Users u WHERE u.fechaIngreso = :fechaIngreso"),
    @NamedQuery(name = "Users.findByTipoUsuario", query = "SELECT u FROM Users u WHERE u.tipoUsuario = :tipoUsuario")})
public class Users implements Serializable {
    @Column(name = "tipo_usuario")
    private Short tipoUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Compra> compraList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "rut")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "passwd")
    private String passwd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "apell_p")
    private String apellP;
    @Size(max = 64)
    @Column(name = "apell_m")
    private String apellM;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @OneToMany(mappedBy = "userId")
    private List<Inventario> inventarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Caja> cajaList;
    @OneToMany(mappedBy = "userId")
    private List<Pedido> pedidoList;
    @OneToMany(mappedBy = "userId")
    private List<Traspaso> traspasoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Asistencia> asistenciaList;
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Users(Integer id, String rut, String passwd, String nombre, String apellP) {
        this.id = id;
        this.rut = rut;
        this.passwd = passwd;
        this.nombre = nombre;
        this.apellP = apellP;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellP() {
        return apellP;
    }

    public void setApellP(String apellP) {
        this.apellP = apellP;
    }

    public String getApellM() {
        return apellM;
    }

    public void setApellM(String apellM) {
        this.apellM = apellM;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Short getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(Short tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @XmlTransient
    public List<Inventario> getInventarioList() {
        return inventarioList;
    }

    public void setInventarioList(List<Inventario> inventarioList) {
        this.inventarioList = inventarioList;
    }

    @XmlTransient
    public List<Caja> getCajaList() {
        return cajaList;
    }

    public void setCajaList(List<Caja> cajaList) {
        this.cajaList = cajaList;
    }
    
    @XmlTransient
    public List<Pedido> getPedidoList() {
        return pedidoList;
    }

    public void setPedidoList(List<Pedido> pedidoList) {
        this.pedidoList = pedidoList;
    }

    @XmlTransient
    public List<Traspaso> getTraspasoList() {
        return traspasoList;
    }

    public void setTraspasoList(List<Traspaso> traspasoList) {
        this.traspasoList = traspasoList;
    }

    @XmlTransient
    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }
    
    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return rut.concat(" - ").concat(nombre).concat(" ").concat(apellP);
    }
    
}
