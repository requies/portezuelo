/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Venta.lastNroVenta", query = "SELECT MAX(v.nroVenta) FROM Venta v"),
    @NamedQuery(name = "Venta.findAll", query = "SELECT v FROM Venta v"),
    @NamedQuery(name = "Venta.findById", query = "SELECT v FROM Venta v WHERE v.id = :id"),
    @NamedQuery(name = "Venta.findByMonto", query = "SELECT v FROM Venta v WHERE v.monto = :monto"),
    @NamedQuery(name = "Venta.findByFecha", query = "SELECT v FROM Venta v WHERE v.fecha = :fecha"),
    @NamedQuery(name = "Venta.findByMaquina", query = "SELECT v FROM Venta v WHERE v.maquina = :maquina"),
    @NamedQuery(name = "Venta.findByTipoDocumento", query = "SELECT v FROM Venta v WHERE v.tipoDocumento = :tipoDocumento"),
    @NamedQuery(name = "Venta.findByTipoVenta", query = "SELECT v FROM Venta v WHERE v.tipoVenta = :tipoVenta"),
    @NamedQuery(name = "Venta.findByDescuento", query = "SELECT v FROM Venta v WHERE v.descuento = :descuento"),
    @NamedQuery(name = "Venta.findByIdDocumento", query = "SELECT v FROM Venta v WHERE v.idDocumento = :idDocumento"),
    @NamedQuery(name = "Venta.findByCancelada", query = "SELECT v FROM Venta v WHERE v.cancelada = :cancelada"),
    @NamedQuery(name = "Venta.findByNumVenta", query = "SELECT v FROM Venta v WHERE v.nroVenta = :num")})
public class Venta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "monto")
    private Integer monto;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "maquina_id", referencedColumnName = "id")
    @ManyToOne
    private Maquina maquina;
    @Column(name = "tipo_documento")
    private Short tipoDocumento;
    @Column(name = "tipo_venta")
    private Short tipoVenta;
    @Column(name = "descuento")
    private Integer descuento;
    @Column(name = "id_documento")
    private Integer idDocumento;
    @NotNull
    @Column(name = "nro_venta")
    private Integer nroVenta;
    @Column(name = "cancelada")
    private Boolean cancelada;
    @JoinColumn(name = "caja_id", referencedColumnName = "id")
    @ManyToOne
    private Caja cajaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "venta")
    private List<VentaDetalle> ventaDetalleList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "venta")
    private VentaAnulada ventaAnulada;

    public Venta() {
    }

    public Venta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Maquina getMaquina() {
        return maquina;
    }

    public void setMaquina(Maquina maquina) {
        this.maquina = maquina;
    }

    public Short getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Short tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Short getTipoVenta() {
        return tipoVenta;
    }

    public void setTipoVenta(Short tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Boolean getCancelada() {
        return cancelada;
    }

    public void setCancelada(Boolean cancelada) {
        this.cancelada = cancelada;
    }

    @XmlTransient
    public List<VentaDetalle> getVentaDetalleList() {
        return ventaDetalleList;
    }

    public void setVentaDetalleList(List<VentaDetalle> ventaDetalleList) {
        this.ventaDetalleList = ventaDetalleList;
    }

    public VentaAnulada getVentaAnulada() {
        return ventaAnulada;
    }

    public void setVentaAnulada(VentaAnulada ventaAnulada) {
        this.ventaAnulada = ventaAnulada;
    }

    public Integer getNroVenta() {
        return nroVenta;
    }

    public void setNroVenta(Integer nroVenta) {
        this.nroVenta = nroVenta;
    }

    public Caja getCajaId() {
        return cajaId;
    }

    public void setCajaId(Caja cajaId) {
        this.cajaId = cajaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venta)) {
            return false;
        }
        Venta other = (Venta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.Venta[ id=" + id + " ]";
    }
    
}
