/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author victor
 */
@Embeddable
public class PromocionDetallePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "barcode")
    private long barcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "promocion_id")
    private int promocionId;

    public PromocionDetallePK() {
    }

    public PromocionDetallePK(long barcode, int promocionId) {
        this.barcode = barcode;
        this.promocionId = promocionId;
    }

    public long getBarcode() {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public int getPromocionId() {
        return promocionId;
    }

    public void setPromocionId(int promocionId) {
        this.promocionId = promocionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) barcode;
        hash += (int) promocionId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionDetallePK)) {
            return false;
        }
        PromocionDetallePK other = (PromocionDetallePK) object;
        if (this.barcode != other.barcode) {
            return false;
        }
        if (this.promocionId != other.promocionId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.PromocionDetallePK[ barcode=" + barcode + ", promocionId=" + promocionId + " ]";
    }
    
}
