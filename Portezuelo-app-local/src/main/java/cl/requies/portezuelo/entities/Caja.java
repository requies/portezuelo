/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "caja")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caja.findAll", query = "SELECT c FROM Caja c"),
    @NamedQuery(name = "Caja.findById", query = "SELECT c FROM Caja c WHERE c.id = :id"),
    @NamedQuery(name = "Caja.findByFechaInicio", query = "SELECT c FROM Caja c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Caja.findByMontoInicio", query = "SELECT c FROM Caja c WHERE c.montoInicio = :montoInicio"),
    @NamedQuery(name = "Caja.findByFechaTermino", query = "SELECT c FROM Caja c WHERE c.fechaTermino = :fechaTermino"),
    @NamedQuery(name = "Caja.findByMontoTermino", query = "SELECT c FROM Caja c WHERE c.montoTermino = :montoTermino"),
    @NamedQuery(name = "Caja.findByPerdida", query = "SELECT c FROM Caja c WHERE c.perdida = :perdida")})
public class Caja implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "monto_inicio")
    private Integer montoInicio;
    @Column(name = "fecha_termino")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaTermino;
    @Column(name = "monto_termino")
    private Integer montoTermino;
    @Column(name = "perdida")
    private Integer perdida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cajaId")
    private List<Ingreso> ingresoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cajaId")
    private List<Venta> ventaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cajaId")
    private List<VentaAnulada> ventaAnuladaList;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;
    @JoinColumn(name = "maquina_id", referencedColumnName = "id")
    @ManyToOne
    private Maquina maquinaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cajaId")
    private List<Egreso> egresoList;

    public Caja() {
    }

    public Caja(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Integer getMontoInicio() {
        return montoInicio;
    }

    public void setMontoInicio(Integer montoInicio) {
        this.montoInicio = montoInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Integer getMontoTermino() {
        return montoTermino;
    }

    public void setMontoTermino(Integer montoTermino) {
        this.montoTermino = montoTermino;
    }

    public Integer getPerdida() {
        return perdida;
    }

    public void setPerdida(Integer perdida) {
        this.perdida = perdida;
    }

    @XmlTransient
    public List<Ingreso> getIngresoList() {
        return ingresoList;
    }

    public void setIngresoList(List<Ingreso> ingresoList) {
        this.ingresoList = ingresoList;
    }
    
    @XmlTransient
    public List<VentaAnulada> getVentaAnuladaList() {
        return ventaAnuladaList;
    }

    public void setVentaAnuladaList(List<VentaAnulada> ventaAnuladaList) {
        this.ventaAnuladaList = ventaAnuladaList;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Maquina getMaquinaId() {
        return maquinaId;
    }

    public void setMaquinaId(Maquina maquinaId) {
        this.maquinaId = maquinaId;
    }

    @XmlTransient
    public List<Egreso> getEgresoList() {
        return egresoList;
    }

    public void setEgresoList(List<Egreso> egresoList) {
        this.egresoList = egresoList;
    }
    @XmlTransient
    public List<Venta> getVentaList() {
        return ventaList;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caja)) {
            return false;
        }
        Caja other = (Caja) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.Caja[ id=" + id + " ]";
    }
    
}
