/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "merma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Merma.findAll", query = "SELECT m FROM Merma m"),
    @NamedQuery(name = "Merma.findById", query = "SELECT m FROM Merma m WHERE m.id = :id"),
    @NamedQuery(name = "Merma.findByTipo", query = "SELECT m FROM Merma m WHERE m.tipo = :tipo"),
    @NamedQuery(name = "Merma.findByUnidades", query = "SELECT m FROM Merma m WHERE m.unidades = :unidades"),
    @NamedQuery(name = "Merma.findByFecha", query = "SELECT m FROM Merma m WHERE m.fecha = :fecha")})
public class Merma implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tipo")
    private Integer tipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unidades")
    private Double unidades;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "tipo_merma_id", referencedColumnName = "id")
    @ManyToOne
    private TipoMerma tipoMermaId;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode")
    @ManyToOne
    private Producto barcode;

    public Merma() {
    }

    public Merma(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Double getUnidades() {
        return unidades;
    }

    public void setUnidades(Double unidades) {
        this.unidades = unidades;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoMerma getTipoMermaId() {
        return tipoMermaId;
    }

    public void setTipoMermaId(TipoMerma tipoMermaId) {
        this.tipoMermaId = tipoMermaId;
    }

    public Producto getBarcode() {
        return barcode;
    }

    public void setBarcode(Producto barcode) {
        this.barcode = barcode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Merma)) {
            return false;
        }
        Merma other = (Merma) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.Merma[ id=" + id + " ]";
    }
    
}
