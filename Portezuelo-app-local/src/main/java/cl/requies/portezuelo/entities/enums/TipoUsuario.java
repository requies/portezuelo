/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities.enums;

/**
 *
 * @author victor
 */
public class TipoUsuario {
    public static final short ADMINISTRADOR = 0;
    public static final short CAJERO = 1;
    public static final String[] TIPOS_USUARIOS = {"Administrador", "Cajero"};
}