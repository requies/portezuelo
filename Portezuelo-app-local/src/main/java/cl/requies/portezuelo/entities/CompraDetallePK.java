/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author victor
 */
@Embeddable
public class CompraDetallePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "barcode")
    private long barcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "compra_id")
    private int compraId;

    public CompraDetallePK() {
    }

    public CompraDetallePK(long barcode, int compraId) {
        this.barcode = barcode;
        this.compraId = compraId;
    }

    public long getBarcode() {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public int getCompraId() {
        return compraId;
    }

    public void setCompraId(int compraId) {
        this.compraId = compraId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) barcode;
        hash += (int) compraId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompraDetallePK)) {
            return false;
        }
        CompraDetallePK other = (CompraDetallePK) object;
        if (this.barcode != other.barcode) {
            return false;
        }
        if (this.compraId != other.compraId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.CompraDetallePK[ barcode=" + barcode + ", compraId=" + compraId + " ]";
    }
    
}
