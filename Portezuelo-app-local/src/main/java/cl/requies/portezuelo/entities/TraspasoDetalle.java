/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "traspaso_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TraspasoDetalle.findAll", query = "SELECT t FROM TraspasoDetalle t"),
    @NamedQuery(name = "TraspasoDetalle.findById", query = "SELECT t FROM TraspasoDetalle t WHERE t.id = :id"),
    @NamedQuery(name = "TraspasoDetalle.findByCantidad", query = "SELECT t FROM TraspasoDetalle t WHERE t.cantidad = :cantidad"),
    @NamedQuery(name = "TraspasoDetalle.findByPrecio", query = "SELECT t FROM TraspasoDetalle t WHERE t.precio = :precio"),
    @NamedQuery(name = "TraspasoDetalle.findByPrecioVenta", query = "SELECT t FROM TraspasoDetalle t WHERE t.precioVenta = :precioVenta"),
    @NamedQuery(name = "TraspasoDetalle.findByCostoModificado", query = "SELECT t FROM TraspasoDetalle t WHERE t.costoModificado = :costoModificado")})
public class TraspasoDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cantidad")
    private Double cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private double precio;
    @Column(name = "precio_venta")
    private Double precioVenta;
    @Column(name = "costo_modificado")
    private Boolean costoModificado;
    @JoinColumn(name = "traspaso_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Traspaso traspasoId;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode")
    @ManyToOne(optional = false)
    private Producto barcode;

    public TraspasoDetalle() {
    }

    public TraspasoDetalle(Integer id) {
        this.id = id;
    }

    public TraspasoDetalle(Integer id, double precio) {
        this.id = id;
        this.precio = precio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Boolean getCostoModificado() {
        return costoModificado;
    }

    public void setCostoModificado(Boolean costoModificado) {
        this.costoModificado = costoModificado;
    }

    public Traspaso getTraspasoId() {
        return traspasoId;
    }

    public void setTraspasoId(Traspaso traspasoId) {
        this.traspasoId = traspasoId;
    }

    public Producto getBarcode() {
        return barcode;
    }

    public void setBarcode(Producto barcode) {
        this.barcode = barcode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TraspasoDetalle)) {
            return false;
        }
        TraspasoDetalle other = (TraspasoDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.TraspasoDetalle[ id=" + id + " ]";
    }
    
}
