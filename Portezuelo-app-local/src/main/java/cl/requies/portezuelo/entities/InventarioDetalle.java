/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "inventario_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InventarioDetalle.findAll", query = "SELECT i FROM InventarioDetalle i"),
    @NamedQuery(name = "InventarioDetalle.findById", query = "SELECT i FROM InventarioDetalle i WHERE i.id = :id"),
    @NamedQuery(name = "InventarioDetalle.findByCantidad", query = "SELECT i FROM InventarioDetalle i WHERE i.cantidad = :cantidad")})
public class InventarioDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @NotNull
    @Column(name = "cantidad")
    private Double cantidad;
    @NotNull
    @Column(name = "cantidad_esperada")
    private Double cantidadEsperada;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode")
    @ManyToOne
    private Producto barcode;
    @JoinColumn(name = "inventario_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Inventario inventarioId;

    public InventarioDetalle() {
    }

    public InventarioDetalle(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getBarcode() {
        return barcode;
    }

    public void setBarcode(Producto barcode) {
        this.barcode = barcode;
    }

    public Inventario getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Inventario inventarioId) {
        this.inventarioId = inventarioId;
    }
    
    public Double getCantidadEsperada() {
        return cantidadEsperada;
    }

    public void setCantidadEsperada(Double cantidadEsperada) {
        this.cantidadEsperada = cantidadEsperada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventarioDetalle)) {
            return false;
        }
        InventarioDetalle other = (InventarioDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.InventarioDetalle[ id=" + id + " ]";
    }
    
}
