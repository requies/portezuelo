/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "promocion_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromocionDetalle.findAll", query = "SELECT p FROM PromocionDetalle p"),
    @NamedQuery(name = "PromocionDetalle.findByBarcode", query = "SELECT p FROM PromocionDetalle p WHERE p.promocionDetallePK.barcode = :barcode"),
    @NamedQuery(name = "PromocionDetalle.findByPromocionId", query = "SELECT p FROM PromocionDetalle p WHERE p.promocionDetallePK.promocionId = :promocionId"),
    @NamedQuery(name = "PromocionDetalle.findByCantidad", query = "SELECT p FROM PromocionDetalle p WHERE p.cantidad = :cantidad")})
public class PromocionDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromocionDetallePK promocionDetallePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cantidad")
    private Double cantidad;
    @JoinColumn(name = "promocion_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public PromocionDetalle() {
    }

    public PromocionDetalle(PromocionDetallePK promocionDetallePK) {
        this.promocionDetallePK = promocionDetallePK;
    }

    public PromocionDetalle(long barcode, int promocionId) {
        this.promocionDetallePK = new PromocionDetallePK(barcode, promocionId);
    }

    public PromocionDetallePK getPromocionDetallePK() {
        return promocionDetallePK;
    }

    public void setPromocionDetallePK(PromocionDetallePK promocionDetallePK) {
        this.promocionDetallePK = promocionDetallePK;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promocionDetallePK != null ? promocionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromocionDetalle)) {
            return false;
        }
        PromocionDetalle other = (PromocionDetalle) object;
        if ((this.promocionDetallePK == null && other.promocionDetallePK != null) || (this.promocionDetallePK != null && !this.promocionDetallePK.equals(other.promocionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.PromocionDetalle[ promocionDetallePK=" + promocionDetallePK + " ]";
    }
    
}
