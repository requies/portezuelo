/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "pedido_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoDetalle.findAll", query = "SELECT p FROM PedidoDetalle p"),
    @NamedQuery(name = "PedidoDetalle.findByBarcode", query = "SELECT p FROM PedidoDetalle p WHERE p.pedidoDetallePK.barcode = :barcode"),
    @NamedQuery(name = "PedidoDetalle.findByPedidoId", query = "SELECT p FROM PedidoDetalle p WHERE p.pedidoDetallePK.pedidoId = :pedidoId"),
    @NamedQuery(name = "PedidoDetalle.findByCantidad", query = "SELECT p FROM PedidoDetalle p WHERE p.cantidad = :cantidad")})
public class PedidoDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PedidoDetallePK pedidoDetallePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @NotNull
    @Column(name = "cantidad")
    private Double cantidad;
    @Column(name = "cantidad_recibida")
    private Double cantidadRecibida;
    @Column(name = "costo")
    private Integer costo;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "pedido_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedido pedido;

    public PedidoDetalle() {
    }

    public PedidoDetalle(PedidoDetallePK pedidoDetallePK) {
        this.pedidoDetallePK = pedidoDetallePK;
    }

    public PedidoDetalle(long barcode, int pedidoId) {
        this.pedidoDetallePK = new PedidoDetallePK(barcode, pedidoId);
    }

    public PedidoDetallePK getPedidoDetallePK() {
        return pedidoDetallePK;
    }

    public void setPedidoDetallePK(PedidoDetallePK pedidoDetallePK) {
        this.pedidoDetallePK = pedidoDetallePK;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getCantidadRecibida() {
        return cantidadRecibida;
    }

    public void setCantidadRecibida(Double cantidadRecibida) {
        this.cantidadRecibida = cantidadRecibida;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Integer getCosto() {
        return costo;
    }

    public void setCosto(Integer costo) {
        this.costo = costo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pedidoDetallePK != null ? pedidoDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoDetalle)) {
            return false;
        }
        PedidoDetalle other = (PedidoDetalle) object;
        if ((this.pedidoDetallePK == null && other.pedidoDetallePK != null) || (this.pedidoDetallePK != null && !this.pedidoDetallePK.equals(other.pedidoDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.PedidoDetalle[ productoDetallePK=" + pedidoDetallePK + " ]";
    }
    
}
