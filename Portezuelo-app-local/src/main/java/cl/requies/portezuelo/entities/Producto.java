/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.countByText", query = "SELECT count(DISTINCT p) FROM Producto p WHERE (UPPER(p.descripcion) LIKE UPPER(:txt)) OR (UPPER(p.marca) LIKE UPPER(:txt)) OR (UPPER(p.familia.nombre) LIKE UPPER(:txt))"),
    @NamedQuery(name = "Producto.findByText", query = "SELECT DISTINCT p FROM Producto p WHERE (UPPER(p.descripcion) LIKE UPPER(:txt)) OR (UPPER(p.marca) LIKE UPPER(:txt)) OR (UPPER(p.familia.nombre) LIKE UPPER(:txt))"),
    @NamedQuery(name = "Producto.findByBarcodeOrCodigoCorto", query = "SELECT DISTINCT p FROM Producto p WHERE (p.barcode = :cod) OR (p.codigoCorto = :cod)"),
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findByBarcode", query = "SELECT p FROM Producto p WHERE p.barcode = :barcode"),
    @NamedQuery(name = "Producto.findByCodigoCorto", query = "SELECT p FROM Producto p WHERE p.codigoCorto = :codigoCorto"),
    @NamedQuery(name = "Producto.findByMarca", query = "SELECT p FROM Producto p WHERE p.marca = :marca"),
    @NamedQuery(name = "Producto.findByDescripcion", query = "SELECT p FROM Producto p WHERE p.descripcion = :descripcion"),
    @NamedQuery(name = "Producto.findByContenido", query = "SELECT p FROM Producto p WHERE p.contenido = :contenido"),
    @NamedQuery(name = "Producto.findByUnidad", query = "SELECT p FROM Producto p WHERE p.unidad = :unidad"),
    @NamedQuery(name = "Producto.findByStock", query = "SELECT p FROM Producto p WHERE p.stock = :stock"),
    @NamedQuery(name = "Producto.findByPrecio", query = "SELECT p FROM Producto p WHERE p.precio = :precio"),
    @NamedQuery(name = "Producto.findByPrecioNeto", query = "SELECT p FROM Producto p WHERE p.precioNeto = :precioNeto"),
    @NamedQuery(name = "Producto.findByCostoPromedio", query = "SELECT p FROM Producto p WHERE p.costoPromedio = :costoPromedio"),
    @NamedQuery(name = "Producto.findByVendidos", query = "SELECT p FROM Producto p WHERE p.vendidos = :vendidos"),
    @NamedQuery(name = "Producto.findByAplicaIva", query = "SELECT p FROM Producto p WHERE p.aplicaIva = :aplicaIva"),
    @NamedQuery(name = "Producto.findByEsPerecible", query = "SELECT p FROM Producto p WHERE p.esPerecible = :esPerecible"),
    @NamedQuery(name = "Producto.findByDiasStock", query = "SELECT p FROM Producto p WHERE p.diasStock = :diasStock"),
    @NamedQuery(name = "Producto.findByMargenPromedio", query = "SELECT p FROM Producto p WHERE p.margenPromedio = :margenPromedio"),
    @NamedQuery(name = "Producto.findByEsVentaFraccionada", query = "SELECT p FROM Producto p WHERE p.esVentaFraccionada = :esVentaFraccionada"),
    @NamedQuery(name = "Producto.findByStockPro", query = "SELECT p FROM Producto p WHERE p.stockPro = :stockPro"),
    @NamedQuery(name = "Producto.findByTasaCanje", query = "SELECT p FROM Producto p WHERE p.tasaCanje = :tasaCanje"),
    @NamedQuery(name = "Producto.findByPrecioMayor", query = "SELECT p FROM Producto p WHERE p.precioMayor = :precioMayor"),
    @NamedQuery(name = "Producto.findByCantidadMayor", query = "SELECT p FROM Producto p WHERE p.cantidadMayor = :cantidadMayor"),
    @NamedQuery(name = "Producto.findByEsMayorista", query = "SELECT p FROM Producto p WHERE p.esMayorista = :esMayorista"),
    @NamedQuery(name = "Producto.findByEstado", query = "SELECT p FROM Producto p WHERE p.estado = :estado")})
public class Producto implements Serializable {
    @Column(name = "fifo")
    private Double fifo;
    @Column(name = "precio_editable")
    private Boolean precioEditable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<CompraDetalle> compraDetalleList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "barcode")
    @SequenceGenerator(name="producto_barcode_sequence",sequenceName="\"producto_barcode_seq\"", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "producto_barcode_sequence")
    private Long barcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "codigo_corto")
    private String codigoCorto;
    @Size(max = 35)
    @Column(name = "marca")
    private String marca;
    @Size(max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 10)
    @Column(name = "contenido")
    private String contenido;
    @Size(max = 10)
    @Column(name = "unidad")
    private String unidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "stock")
    private Double stock;
    @Column(name = "precio")
    private Double precio;
    @Column(name = "precio_neto")
    private Double precioNeto;
    @Column(name = "costo_promedio")
    private Double costoPromedio;
    @Column(name = "vendidos")
    private Double vendidos;
    @Column(name = "aplica_iva")
    private Boolean aplicaIva;
    @Column(name = "es_perecible")
    private Boolean esPerecible;
    @Column(name = "dias_stock")
    private Double diasStock;
    @Column(name = "margen_promedio")
    private Double margenPromedio;
    @Column(name = "es_venta_fraccionada")
    private Boolean esVentaFraccionada;
    @Column(name = "stock_pro")
    private Double stockPro;
    @Column(name = "tasa_canje")
    private Double tasaCanje;
    @Column(name = "precio_mayor")
    private Double precioMayor;
    @Column(name = "cantidad_mayor")
    private Double cantidadMayor;
    @Column(name = "es_mayorista")
    private Boolean esMayorista;
    @Column(name = "estado")
    private Boolean estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<PedidoDetalle> productoDetalleList;
    @OneToMany(mappedBy = "barcode")
    private List<InventarioDetalle> inventarioDetalleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<PromocionDetalle> promocionDetalleList;
    @JoinColumn(name = "id_tipo_producto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoProducto idTipoProducto;
    @JoinColumn(name = "otros_impuestos", referencedColumnName = "id")
    @ManyToOne
    private Impuesto otrosImpuestos;
    @JoinColumn(name = "familia", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private FamiliaProducto familia;
    @OneToMany(mappedBy = "barcode")
    private List<Merma> mermaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barcode")
    private List<Promocion> promocionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barcode")
    private List<VentaDetalle> ventaDetalleList;
    @OneToMany(mappedBy = "barcode")
    private List<AlertaCambioPrecio> alertaCambioPrecioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barcode")
    private List<TraspasoDetalle> traspasoDetalleList;

    public Producto() {
    }

    public Producto(Long barcode) {
        this.barcode = barcode;
    }

    public Producto(Long barcode, String codigoCorto) {
        this.barcode = barcode;
        this.codigoCorto = codigoCorto;
    }

    public Long getBarcode() {
        return barcode;
    }

    public void setBarcode(Long barcode) {
        this.barcode = barcode;
    }

    public String getCodigoCorto() {
        return codigoCorto;
    }

    public void setCodigoCorto(String codigoCorto) {
        this.codigoCorto = codigoCorto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getPrecioNeto() {
        return precioNeto;
    }

    public void setPrecioNeto(Double precioNeto) {
        this.precioNeto = precioNeto;
    }

    public Double getCostoPromedio() {
        return costoPromedio;
    }

    public void setCostoPromedio(Double costoPromedio) {
        this.costoPromedio = costoPromedio;
    }

    public Double getVendidos() {
        return vendidos;
    }

    public void setVendidos(Double vendidos) {
        this.vendidos = vendidos;
    }

    public Boolean getAplicaIva() {
        return aplicaIva;
    }

    public void setAplicaIva(Boolean aplicaIva) {
        this.aplicaIva = aplicaIva;
    }

    public Boolean getEsPerecible() {
        return esPerecible;
    }

    public void setEsPerecible(Boolean esPerecible) {
        this.esPerecible = esPerecible;
    }

    public Double getDiasStock() {
        return diasStock;
    }

    public void setDiasStock(Double diasStock) {
        this.diasStock = diasStock;
    }

    public Double getMargenPromedio() {
        return margenPromedio;
    }

    public void setMargenPromedio(Double margenPromedio) {
        this.margenPromedio = margenPromedio;
    }

    public Boolean getEsVentaFraccionada() {
        return esVentaFraccionada;
    }

    public void setEsVentaFraccionada(Boolean esVentaFraccionada) {
        this.esVentaFraccionada = esVentaFraccionada;
    }

    public Double getStockPro() {
        return stockPro;
    }

    public void setStockPro(Double stockPro) {
        this.stockPro = stockPro;
    }

    public Double getTasaCanje() {
        return tasaCanje;
    }

    public void setTasaCanje(Double tasaCanje) {
        this.tasaCanje = tasaCanje;
    }

    public Double getPrecioMayor() {
        return precioMayor;
    }

    public void setPrecioMayor(Double precioMayor) {
        this.precioMayor = precioMayor;
    }

    public Double getCantidadMayor() {
        return cantidadMayor;
    }

    public void setCantidadMayor(Double cantidadMayor) {
        this.cantidadMayor = cantidadMayor;
    }

    public Boolean getEsMayorista() {
        return esMayorista;
    }

    public void setEsMayorista(Boolean esMayorista) {
        this.esMayorista = esMayorista;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<PedidoDetalle> getProductoDetalleList() {
        return productoDetalleList;
    }

    public void setProductoDetalleList(List<PedidoDetalle> productoDetalleList) {
        this.productoDetalleList = productoDetalleList;
    }

    @XmlTransient
    public List<InventarioDetalle> getInventarioDetalleList() {
        return inventarioDetalleList;
    }

    public void setInventarioDetalleList(List<InventarioDetalle> inventarioDetalleList) {
        this.inventarioDetalleList = inventarioDetalleList;
    }

    @XmlTransient
    public List<PromocionDetalle> getPromocionDetalleList() {
        return promocionDetalleList;
    }

    public void setPromocionDetalleList(List<PromocionDetalle> promocionDetalleList) {
        this.promocionDetalleList = promocionDetalleList;
    }

    public TipoProducto getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(TipoProducto idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public Impuesto getOtrosImpuestos() {
        return otrosImpuestos;
    }

    public void setOtrosImpuestos(Impuesto otrosImpuestos) {
        this.otrosImpuestos = otrosImpuestos;
    }

    public FamiliaProducto getFamilia() {
        return familia;
    }

    public void setFamilia(FamiliaProducto familia) {
        this.familia = familia;
    }

    @XmlTransient
    public List<Merma> getMermaList() {
        return mermaList;
    }

    public void setMermaList(List<Merma> mermaList) {
        this.mermaList = mermaList;
    }

    @XmlTransient
    public List<Promocion> getPromocionList() {
        return promocionList;
    }

    public void setPromocionList(List<Promocion> promocionList) {
        this.promocionList = promocionList;
    }

    @XmlTransient
    public List<VentaDetalle> getVentaDetalleList() {
        return ventaDetalleList;
    }

    public void setVentaDetalleList(List<VentaDetalle> ventaDetalleList) {
        this.ventaDetalleList = ventaDetalleList;
    }

    @XmlTransient
    public List<AlertaCambioPrecio> getAlertaCambioPrecioList() {
        return alertaCambioPrecioList;
    }

    public void setAlertaCambioPrecioList(List<AlertaCambioPrecio> alertaCambioPrecioList) {
        this.alertaCambioPrecioList = alertaCambioPrecioList;
    }

    @XmlTransient
    public List<TraspasoDetalle> getTraspasoDetalleList() {
        return traspasoDetalleList;
    }

    public void setTraspasoDetalleList(List<TraspasoDetalle> traspasoDetalleList) {
        this.traspasoDetalleList = traspasoDetalleList;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (barcode != null ? barcode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.barcode == null && other.barcode != null) || (this.barcode != null && !this.barcode.equals(other.barcode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getDescripcion().concat(" ").concat(this.getMarca()).
                    concat(" ").concat(this.getContenido()).concat(this.getUnidad());
    }

    public Double getFifo() {
        return fifo;
    }

    public void setFifo(Double fifo) {
        this.fifo = fifo;
    }

    public Boolean getPrecioEditable() {
        return precioEditable;
    }

    public void setPrecioEditable(Boolean precioEditable) {
        this.precioEditable = precioEditable;
    }

    @XmlTransient
    public List<CompraDetalle> getCompraDetalleList() {
        return compraDetalleList;
    }

    public void setCompraDetalleList(List<CompraDetalle> compraDetalleList) {
        this.compraDetalleList = compraDetalleList;
    }
    
}
