/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "traspaso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Traspaso.findAll", query = "SELECT t FROM Traspaso t"),
    @NamedQuery(name = "Traspaso.findById", query = "SELECT t FROM Traspaso t WHERE t.id = :id"),
    @NamedQuery(name = "Traspaso.findByMonto", query = "SELECT t FROM Traspaso t WHERE t.monto = :monto"),
    @NamedQuery(name = "Traspaso.findByFecha", query = "SELECT t FROM Traspaso t WHERE t.fecha = :fecha")})
public class Traspaso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "monto")
    private Double monto;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private Users userId;
    @JoinColumn(name = "destino_id", referencedColumnName = "id")
    @ManyToOne
    private Negocio destinoId;
    @JoinColumn(name = "origen_id", referencedColumnName = "id")
    @ManyToOne
    private Negocio origenId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "traspasoId")
    private List<TraspasoDetalle> traspasoDetalleList;

    public Traspaso() {
    }

    public Traspaso(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Negocio getDestinoId() {
        return destinoId;
    }

    public void setDestinoId(Negocio destinoId) {
        this.destinoId = destinoId;
    }

    public Negocio getOrigenId() {
        return origenId;
    }

    public void setOrigenId(Negocio origenId) {
        this.origenId = origenId;
    }

    @XmlTransient
    public List<TraspasoDetalle> getTraspasoDetalleList() {
        return traspasoDetalleList;
    }

    public void setTraspasoDetalleList(List<TraspasoDetalle> traspasoDetalleList) {
        this.traspasoDetalleList = traspasoDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Traspaso)) {
            return false;
        }
        Traspaso other = (Traspaso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.Traspaso[ id=" + id + " ]";
    }
    
}
