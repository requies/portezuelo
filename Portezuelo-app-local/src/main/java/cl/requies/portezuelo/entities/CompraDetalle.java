/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "compra_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompraDetalle.findAll", query = "SELECT c FROM CompraDetalle c"),
    @NamedQuery(name = "CompraDetalle.findByBarcode", query = "SELECT c FROM CompraDetalle c WHERE c.compraDetallePK.barcode = :barcode"),
    @NamedQuery(name = "CompraDetalle.findByCompraId", query = "SELECT c FROM CompraDetalle c WHERE c.compraDetallePK.compraId = :compraId"),
    @NamedQuery(name = "CompraDetalle.findByCantidad", query = "SELECT c FROM CompraDetalle c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "CompraDetalle.findByCosto", query = "SELECT c FROM CompraDetalle c WHERE c.costo = :costo")})
public class CompraDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CompraDetallePK compraDetallePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private double cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "costo")
    private int costo;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "compra_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Compra compra;

    public CompraDetalle() {
    }

    public CompraDetalle(CompraDetallePK compraDetallePK) {
        this.compraDetallePK = compraDetallePK;
    }

    public CompraDetalle(CompraDetallePK compraDetallePK, double cantidad, int costo) {
        this.compraDetallePK = compraDetallePK;
        this.cantidad = cantidad;
        this.costo = costo;
    }

    public CompraDetalle(long barcode, int compraId) {
        this.compraDetallePK = new CompraDetallePK(barcode, compraId);
    }

    public CompraDetallePK getCompraDetallePK() {
        return compraDetallePK;
    }

    public void setCompraDetallePK(CompraDetallePK compraDetallePK) {
        this.compraDetallePK = compraDetallePK;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compraDetallePK != null ? compraDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompraDetalle)) {
            return false;
        }
        CompraDetalle other = (CompraDetalle) object;
        if ((this.compraDetallePK == null && other.compraDetallePK != null) || (this.compraDetallePK != null && !this.compraDetallePK.equals(other.compraDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.CompraDetalle[ compraDetallePK=" + compraDetallePK + " ]";
    }
    
}
