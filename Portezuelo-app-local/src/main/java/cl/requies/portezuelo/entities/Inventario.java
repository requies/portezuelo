/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventario.countByText", query = "SELECT count(DISTINCT p.inventarioId) FROM InventarioDetalle p WHERE (UPPER(p.barcode.descripcion) LIKE UPPER(:txt)) OR (UPPER(p.barcode.marca) LIKE UPPER(:txt)) OR (UPPER(p.barcode.familia.nombre) LIKE UPPER(:txt))"),
    @NamedQuery(name = "Inventario.findByText", query = "SELECT DISTINCT p.inventarioId FROM InventarioDetalle p WHERE (UPPER(p.barcode.descripcion) LIKE UPPER(:txt)) OR (UPPER(p.barcode.marca) LIKE UPPER(:txt)) OR (UPPER(p.barcode.familia.nombre) LIKE UPPER(:txt))"),
    @NamedQuery(name = "Inventario.findAll", query = "SELECT i FROM Inventario i"),
    @NamedQuery(name = "Inventario.findById", query = "SELECT i FROM Inventario i WHERE i.id = :id"),
    @NamedQuery(name = "Inventario.findByFecha", query = "SELECT i FROM Inventario i WHERE i.fecha = :fecha")})
public class Inventario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private Users userId;
    @JoinColumn(name = "negocio_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Negocio negocioId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventarioId")
    private List<InventarioDetalle> inventarioDetalleList;

    public Inventario() {
    }

    public Inventario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Negocio getNegocioId() {
        return negocioId;
    }

    public void setNegocioId(Negocio negocioId) {
        this.negocioId = negocioId;
    }

    @XmlTransient
    public List<InventarioDetalle> getInventarioDetalleList() {
        return inventarioDetalleList;
    }

    public void setInventarioDetalleList(List<InventarioDetalle> inventarioDetalleList) {
        this.inventarioDetalleList = inventarioDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.Inventario[ id=" + id + " ]";
    }
    
}
