/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.requies.portezuelo.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "venta_detalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaDetalle.findAll", query = "SELECT v FROM VentaDetalle v"),
    @NamedQuery(name = "VentaDetalle.findById", query = "SELECT v FROM VentaDetalle v WHERE v.id = :id"),
    @NamedQuery(name = "VentaDetalle.findByVentaId", query = "SELECT v FROM VentaDetalle v WHERE v.venta.id = :ventaId"),
    @NamedQuery(name = "VentaDetalle.findByCantidad", query = "SELECT v FROM VentaDetalle v WHERE v.cantidad = :cantidad"),
    @NamedQuery(name = "VentaDetalle.findByPrecio", query = "SELECT v FROM VentaDetalle v WHERE v.precio = :precio"),
    @NamedQuery(name = "VentaDetalle.findByPrecioNeto", query = "SELECT v FROM VentaDetalle v WHERE v.precioNeto = :precioNeto"),
    @NamedQuery(name = "VentaDetalle.findByFifo", query = "SELECT v FROM VentaDetalle v WHERE v.fifo = :fifo"),
    @NamedQuery(name = "VentaDetalle.findByGanancia", query = "SELECT v FROM VentaDetalle v WHERE v.ganancia = :ganancia"),
    @NamedQuery(name = "VentaDetalle.findByIva", query = "SELECT v FROM VentaDetalle v WHERE v.iva = :iva"),
    @NamedQuery(name = "VentaDetalle.findByOtros", query = "SELECT v FROM VentaDetalle v WHERE v.otros = :otros"),
    @NamedQuery(name = "VentaDetalle.findByIvaResidual", query = "SELECT v FROM VentaDetalle v WHERE v.ivaResidual = :ivaResidual"),
    @NamedQuery(name = "VentaDetalle.findByOtrosResidual", query = "SELECT v FROM VentaDetalle v WHERE v.otrosResidual = :otrosResidual"),
    @NamedQuery(name = "VentaDetalle.findByProporcionIva", query = "SELECT v FROM VentaDetalle v WHERE v.proporcionIva = :proporcionIva"),
    @NamedQuery(name = "VentaDetalle.findByProporcionOtros", query = "SELECT v FROM VentaDetalle v WHERE v.proporcionOtros = :proporcionOtros"),
    @NamedQuery(name = "VentaDetalle.findByTieneImpuestos", query = "SELECT v FROM VentaDetalle v WHERE v.tieneImpuestos = :tieneImpuestos")})
public class VentaDetalle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cantidad")
    private Double cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private double precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_neto")
    private double precioNeto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fifo")
    private double fifo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ganancia")
    private double ganancia;
    @Column(name = "iva")
    private Double iva;
    @Column(name = "otros")
    private Double otros;
    @Column(name = "iva_residual")
    private Double ivaResidual;
    @Column(name = "otros_residual")
    private Double otrosResidual;
    @Column(name = "proporcion_iva")
    private Double proporcionIva;
    @Column(name = "proporcion_otros")
    private Double proporcionOtros;
    @Column(name = "tiene_impuestos")
    private Boolean tieneImpuestos;
    @JoinColumn(name = "venta_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Venta venta;
    @JoinColumn(name = "barcode", referencedColumnName = "barcode")
    @ManyToOne(optional = false)
    private Producto barcode;

    public VentaDetalle() {
    }

    public VentaDetalle(Integer id) {
        this.id = id;
    }

    public VentaDetalle(Integer id, double precio, double precioNeto, double fifo, double ganancia) {
        this.id = id;
        this.precio = precio;
        this.precioNeto = precioNeto;
        this.fifo = fifo;
        this.ganancia = ganancia;
    }

    public Integer getId() {
        return id;
    }

    public void setVentaDetallePK(Integer id) {
        this.id = id;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getPrecioNeto() {
        return precioNeto;
    }

    public void setPrecioNeto(double precioNeto) {
        this.precioNeto = precioNeto;
    }

    public double getFifo() {
        return fifo;
    }

    public void setFifo(double fifo) {
        this.fifo = fifo;
    }

    public double getGanancia() {
        return ganancia;
    }

    public void setGanancia(double ganancia) {
        this.ganancia = ganancia;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Double getOtros() {
        return otros;
    }

    public void setOtros(Double otros) {
        this.otros = otros;
    }

    public Double getIvaResidual() {
        return ivaResidual;
    }

    public void setIvaResidual(Double ivaResidual) {
        this.ivaResidual = ivaResidual;
    }

    public Double getOtrosResidual() {
        return otrosResidual;
    }

    public void setOtrosResidual(Double otrosResidual) {
        this.otrosResidual = otrosResidual;
    }

    public Double getProporcionIva() {
        return proporcionIva;
    }

    public void setProporcionIva(Double proporcionIva) {
        this.proporcionIva = proporcionIva;
    }

    public Double getProporcionOtros() {
        return proporcionOtros;
    }

    public void setProporcionOtros(Double proporcionOtros) {
        this.proporcionOtros = proporcionOtros;
    }

    public Boolean getTieneImpuestos() {
        return tieneImpuestos;
    }

    public void setTieneImpuestos(Boolean tieneImpuestos) {
        this.tieneImpuestos = tieneImpuestos;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Producto getBarcode() {
        return barcode;
    }

    public void setBarcode(Producto barcode) {
        this.barcode = barcode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDetalle)) {
            return false;
        }
        VentaDetalle other = (VentaDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.requies.portezuelo.entities.VentaDetalle[ id=" + id + " ]";
    }
    
}
