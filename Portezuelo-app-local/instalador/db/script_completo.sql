﻿CREATE ROLE portezuelo LOGIN
  PASSWORD 'admin'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
CREATE DATABASE "portezueloDB"
  WITH OWNER = portezuelo
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish_Spain.1252'
       LC_CTYPE = 'Spanish_Spain.1252'
       CONNECTION LIMIT = -1;
GRANT ALL ON DATABASE "portezueloDB" TO portezuelo;
GRANT ALL ON DATABASE "portezueloDB" TO public;

# db_structure_renovada_1.4.sql
# db_initvalues.4.sql


GRANT ALL ON SEQUENCE alerta_cambio_precio_id_seq TO public;
GRANT ALL ON SEQUENCE asistencia_id_seq TO public;
GRANT ALL ON SEQUENCE caja_id_seq TO public;
GRANT ALL ON SEQUENCE compra_id_seq TO public;
GRANT ALL ON SEQUENCE egreso_id_seq TO public;
GRANT ALL ON SEQUENCE familia_producto_id_seq TO public;
GRANT ALL ON SEQUENCE impuesto_id_seq TO public;
GRANT ALL ON SEQUENCE ingreso_id_seq TO public;
GRANT ALL ON SEQUENCE inventario_detalle_id_seq TO public;
GRANT ALL ON SEQUENCE inventario_id_seq TO public;
GRANT ALL ON SEQUENCE log_id_seq TO public;
GRANT ALL ON SEQUENCE maquina_id_seq TO public;
GRANT ALL ON SEQUENCE merma_id_seq TO public;
GRANT ALL ON SEQUENCE negocio_id_seq TO public;
GRANT ALL ON SEQUENCE nro_venta TO public;
GRANT ALL ON SEQUENCE pedido_id_seq TO public;
GRANT ALL ON SEQUENCE producto_barcode_seq TO public;
GRANT ALL ON SEQUENCE promocion_id_seq TO public;
GRANT ALL ON SEQUENCE tipo_egreso_id_seq TO public;
GRANT ALL ON SEQUENCE tipo_ingreso_id_seq TO public;
GRANT ALL ON SEQUENCE tipo_merma_id_seq TO public;
GRANT ALL ON SEQUENCE tipo_producto_id_seq TO public;
GRANT ALL ON SEQUENCE traspaso_detalle_id_seq TO public;
GRANT ALL ON SEQUENCE traspaso_id_seq TO public;
GRANT ALL ON SEQUENCE unidad_producto_id_seq TO public;
GRANT ALL ON SEQUENCE users_id_seq TO public;
GRANT ALL ON SEQUENCE venta_detalle_id_seq TO public;
GRANT ALL ON SEQUENCE venta_id_seq TO public;
GRANT ALL ON TABLE alerta_cambio_precio TO public;
GRANT ALL ON TABLE asistencia TO public;
GRANT ALL ON TABLE caja TO public;
GRANT ALL ON TABLE compra TO public;
GRANT ALL ON TABLE compra_detalle TO public;
GRANT ALL ON TABLE egreso TO public;
GRANT ALL ON TABLE familia_producto TO public;
GRANT ALL ON TABLE impuesto TO public;
GRANT ALL ON TABLE ingreso TO public;
GRANT ALL ON TABLE inventario TO public;
GRANT ALL ON TABLE inventario_detalle TO public;
GRANT ALL ON TABLE log TO public;
GRANT ALL ON TABLE maquina TO public;
GRANT ALL ON TABLE merma TO public;
GRANT ALL ON TABLE negocio TO public;
GRANT ALL ON TABLE pedido TO public;
GRANT ALL ON TABLE pedido_detalle TO public;
GRANT ALL ON TABLE producto TO public;
GRANT ALL ON TABLE promocion TO public;
GRANT ALL ON TABLE promocion_detalle TO public;
GRANT ALL ON TABLE tipo_egreso TO public;
GRANT ALL ON TABLE tipo_ingreso TO public;
GRANT ALL ON TABLE tipo_merma TO public;
GRANT ALL ON TABLE tipo_producto TO public;
GRANT ALL ON TABLE traspaso TO public;
GRANT ALL ON TABLE traspaso_detalle TO public;
GRANT ALL ON TABLE unidad_producto TO public;
GRANT ALL ON TABLE users TO public;
GRANT ALL ON TABLE venta TO public;
GRANT ALL ON TABLE venta_anulada TO public;
GRANT ALL ON TABLE venta_detalle TO public;

